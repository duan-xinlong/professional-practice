#ifndef PURCHASEORDER_H
#define PURCHASEORDER_H

#include <QWidget>
#include <QSqlQuery>

namespace Ui {
class PurchaseOrder;
}

class PurchaseOrder : public QWidget
{
    Q_OBJECT

public:
    explicit PurchaseOrder(QWidget *parent = nullptr);
    ~PurchaseOrder();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::PurchaseOrder *ui;
    bool refresh(QSqlQuery query);
    void qcombobox_refresh();
};

#endif // PURCHASEORDER_H
