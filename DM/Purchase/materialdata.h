#ifndef MATERIALDATA_H
#define MATERIALDATA_H

#include <QWidget>
#include <QSqlQuery>

namespace Ui {
class materialdata;
}

class materialdata : public QWidget
{
    Q_OBJECT

public:
    explicit materialdata(QWidget *parent = nullptr);
    ~materialdata();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::materialdata *ui;
    void qcombobox_refresh();
    bool refresh(QSqlQuery query);
};

#endif // MATERIALDATA_H
