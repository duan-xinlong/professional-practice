#ifndef SUPPLIERDATA_H
#define SUPPLIERDATA_H

#include <QWidget>
#include <QSqlQuery>

namespace Ui {
class supplierdata;
}

class supplierdata : public QWidget
{
    Q_OBJECT

public:
    explicit supplierdata(QWidget *parent = nullptr);
    ~supplierdata();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::supplierdata *ui;
    bool refresh(QSqlQuery query);
    void qcombobox_refresh();
};

#endif // SUPPLIERDATA_H
