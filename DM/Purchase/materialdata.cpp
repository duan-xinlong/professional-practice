﻿#include "materialdata.h"
#include "ui_materialdata.h"
#include "DataBase/database.h"
#include <QDebug>

materialdata::materialdata(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::materialdata)
{
    ui->setupUi(this);
    qcombobox_refresh();
}

materialdata::~materialdata()
{
    delete ui;
}

void materialdata::qcombobox_refresh(){
    ui->comboBox_12->clear();
    ui->comboBox_12->addItem("");
    QSqlQuery query_name;
    query_name.exec("select ProductName FROM product_data_table where PurchaseRequest=1");
    while(query_name.next()){
        QString name = query_name.value(0).toString();
        ui->comboBox_12->addItem(name);
    }
}

bool materialdata::refresh(QSqlQuery query)
{
    for(int row = ui->tableWidget->rowCount() - 1;row >= 0; row--)
    {
        ui->tableWidget->removeRow(row);
    }
    int rowIndex = ui->tableWidget->rowCount();//当前表格的行数
    while(query.next()){
        ui->tableWidget->insertRow(rowIndex);//在最后一行的后面插入一行
        for(int j=0; j<=2; j++){
            ui->tableWidget->setItem(rowIndex,j,new QTableWidgetItem(query.value(j).toString()));
            ui->tableWidget->item(rowIndex,j)->setTextAlignment(Qt::AlignCenter);
        }
        rowIndex += 1;
    }
    return true;
}

void materialdata::on_pushButton_2_clicked()
{
    QString current_name = ui->comboBox_12->currentText();
    QString current_number = ui->lineEdit_2->text();
    QString check = "Select ProductCode, ProductName, PurchaseRequest from product_data_table Where PurchaseRequest = 1";
    QString where = "";

    if(current_number.isEmpty() && current_name.isEmpty())
    {
        check = "Select ProductCode, ProductName, PurchaseRequest from product_data_table";
    }
    else
    {
        if (!current_name.isEmpty())
        {
            where += QString(" AND ProductName LIKE '%1'").arg(current_name);
        }
        if (!current_number.isEmpty())
        {
            where += QString(" AND ProductCode LIKE '%1'").arg(current_number);
        }
        check += where;
    }
    qDebug() << check;
    QSqlQuery query;
    query.exec(check);
    refresh(query);
}

void materialdata::on_pushButton_clicked()
{
    int rowIndex = ui->tableWidget->rowCount();//当前表格的行数
    qDebug() << rowIndex;
    ui->tableWidget->insertRow(rowIndex);//在最后一行的后面插入一行
}

void materialdata::on_pushButton_4_clicked()
{
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString item_number = ui->tableWidget->item(rowIdx,0)==nullptr? " ": ui->tableWidget->item(rowIdx,0)->text();
        QString item_name = ui->tableWidget->item(rowIdx,1)==nullptr? " ": ui->tableWidget->item(rowIdx,1)->text();
        QString update_row = QString("update product_data_table set PurchaseRequest = 0 where ProductCode = '%1' ").arg(item_number);
        qDebug() << update_row;
        query.exec(update_row);
//        QString item_name = ui->tableWidget->item(rowIdx,1)->text();
//        QString item_mainbusiness = ui->tableWidget->item(rowIdx,2)->text();
//        QString item_relationship = ui->tableWidget->item(rowIdx,3)->text();
//        QString delete_row = QString("delete from supplierdata where number = '%1' and name = '%2' and mainbusiness = '%3' and relationship = '%4'").arg(item_number).arg(item_name).arg(item_mainbusiness).arg(item_relationship);
//        qDebug() << delete_row;
//        query.exec(delete_row);
        ui->tableWidget->removeRow(rowIdx);
    }
    qcombobox_refresh();
}

void materialdata::on_pushButton_3_clicked()
{
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString item_number = ui->tableWidget->item(rowIdx,0)==nullptr? "": ui->tableWidget->item(rowIdx,0)->text();
        QString item_name = ui->tableWidget->item(rowIdx,1)==nullptr? "": ui->tableWidget->item(rowIdx,1)->text();
        QString item_detail = ui->tableWidget->item(rowIdx,2)==nullptr? "": ui->tableWidget->item(rowIdx,2)->text();
        QString update_row = QString("update product_data_table set PurchaseRequest = %1 where ProductCode = '%2' ").arg(item_detail).arg(item_number);
        qDebug() << update_row;
        query.exec(update_row);
    }
    qcombobox_refresh();
}

void materialdata::on_pushButton_5_clicked()
{
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString item_number = ui->tableWidget->item(rowIdx,0)==nullptr? "": ui->tableWidget->item(rowIdx,0)->text();
        QString item_name = ui->tableWidget->item(rowIdx,1)==nullptr? "": ui->tableWidget->item(rowIdx,1)->text();
        QString item_detail = ui->tableWidget->item(rowIdx,2)==nullptr? "": ui->tableWidget->item(rowIdx,2)->text();

        QString insert_row = QString("update product_data_table set PurchaseRequest = 1 where ProductCode = '%1' ").arg(item_number);
                //QString("insert into materialdata(number,name,details) values('%1','%2','%3')").arg(item_number).arg(item_name).arg(item_detail);
        qDebug() << insert_row;
        query.exec(insert_row);
    }
    qcombobox_refresh();
}
