#include "buyerdata.h"
#include "ui_buyerdata.h"
#include "DataBase/database.h"
#include <QDebug>

buyerdata::buyerdata(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::buyerdata)
{
    ui->setupUi(this);        
    mydb = new Database();
    qcombobox_refresh();
}

void buyerdata::qcombobox_refresh(){
    ui->comboBox_12->clear();
    ui->comboBox_12->addItem("");
    QSqlQuery query_name;
    query_name.exec("select name FROM buyerdata where del_flag=0");
    while(query_name.next()){
        QString name = query_name.value(0).toString();
        ui->comboBox_12->addItem(name);
    }
}

bool buyerdata::refresh(QSqlQuery query)
{
    for(int row = ui->tableWidget->rowCount() - 1;row >= 0; row--)
    {
        ui->tableWidget->removeRow(row);
    }
    int rowIndex = ui->tableWidget->rowCount();//当前表格的行数
    while(query.next()){
        ui->tableWidget->insertRow(rowIndex);//在最后一行的后面插入一行
        for(int j=0; j<=2; j++){
            ui->tableWidget->setItem(rowIndex,j,new QTableWidgetItem(query.value(j).toString()));
            ui->tableWidget->item(rowIndex,j)->setTextAlignment(Qt::AlignCenter);
        }
        rowIndex += 1;
    }
    return true;
}

buyerdata::~buyerdata()
{
    delete ui;
}

void buyerdata::on_pushButton_2_clicked()
{
    QString current_name = ui->comboBox_12->currentText();
    QString current_number = ui->lineEdit_2->text();
    QString check = "Select * from buyerdata where del_flag=0";
    QString where = "";

    if(current_number.isEmpty() && current_name.isEmpty()){
        check = "Select * from buyerdata where del_flag=0";
    }else{
        if (!current_name.isEmpty()) {
            where += QString(" AND name LIKE '%1'").arg(current_name);
        }
        if (!current_number.isEmpty()) {
            where += QString(" AND number LIKE '%1'").arg(current_number);
        }
        check += where;
    }
    qDebug() << check;
    QSqlQuery query;
    query.exec(check);
    refresh(query);
}

void buyerdata::on_pushButton_clicked()
{
    int rowIndex = ui->tableWidget->rowCount();//当前表格的行数
    qDebug() << rowIndex;
    ui->tableWidget->insertRow(rowIndex);//在最后一行的后面插入一行
}

void buyerdata::on_pushButton_4_clicked()
{
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString item_number = ui->tableWidget->item(rowIdx,0)==nullptr? " ": ui->tableWidget->item(rowIdx,0)->text();
        QString item_name = ui->tableWidget->item(rowIdx,1)==nullptr? " ": ui->tableWidget->item(rowIdx,1)->text();
        QString update_row = QString("update buyerdata set del_flag = 1 where number = '%1' and name = '%2'").arg(item_number).arg(item_name);
        qDebug() << update_row;
        query.exec(update_row);
//        QString item_name = ui->tableWidget->item(rowIdx,1)->text();
//        QString item_mainbusiness = ui->tableWidget->item(rowIdx,2)->text();
//        QString item_relationship = ui->tableWidget->item(rowIdx,3)->text();
//        QString delete_row = QString("delete from supplierdata where number = '%1' and name = '%2' and mainbusiness = '%3' and relationship = '%4'").arg(item_number).arg(item_name).arg(item_mainbusiness).arg(item_relationship);
//        qDebug() << delete_row;
//        query.exec(delete_row);
        ui->tableWidget->removeRow(rowIdx);
    }
    qcombobox_refresh();
}

void buyerdata::on_pushButton_3_clicked()
{
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString item_number = ui->tableWidget->item(rowIdx,0)==nullptr? "": ui->tableWidget->item(rowIdx,0)->text();
        QString item_name = ui->tableWidget->item(rowIdx,1)==nullptr? "": ui->tableWidget->item(rowIdx,1)->text();
        QString item_detail = ui->tableWidget->item(rowIdx,2)==nullptr? "": ui->tableWidget->item(rowIdx,2)->text();
        QString update_row = QString("update buyerdata set number = '%1', name = '%2', details = '%3' where number = '%1' or name ='%2' ").arg(item_number).arg(item_name).arg(item_detail);
        qDebug() << update_row;
        query.exec(update_row);
    }
    qcombobox_refresh();
}

void buyerdata::on_pushButton_5_clicked()
{
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString item_number = ui->tableWidget->item(rowIdx,0)==nullptr? "": ui->tableWidget->item(rowIdx,0)->text();
        QString item_name = ui->tableWidget->item(rowIdx,1)==nullptr? "": ui->tableWidget->item(rowIdx,1)->text();
        QString item_detail = ui->tableWidget->item(rowIdx,2)==nullptr? "": ui->tableWidget->item(rowIdx,2)->text();
        QString insert_row = QString("insert into buyerdata(number,name,details) values('%1','%2','%3')").arg(item_number).arg(item_name).arg(item_detail);
        qDebug() << insert_row;
        query.exec(insert_row);
    }
    qcombobox_refresh();
}
