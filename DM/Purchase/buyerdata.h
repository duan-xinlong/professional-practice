#ifndef BUYERDATA_H
#define BUYERDATA_H

#include <QWidget>
#include <QSqlQuery>

namespace Ui {
class buyerdata;
}

class buyerdata : public QWidget
{
    Q_OBJECT

public:
    explicit buyerdata(QWidget *parent = nullptr);
    ~buyerdata();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

private:
    Ui::buyerdata *ui;
    bool refresh(QSqlQuery query);
    void qcombobox_refresh();
};

#endif // BUYERDATA_H
