﻿#include "purchaseorder.h"
#include "ui_purchaseorder.h"
#include "DataBase/database.h"
#include <QDebug>
#include <QDateTime>

PurchaseOrder::PurchaseOrder(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PurchaseOrder)
{
    ui->setupUi(this);
    mydb = new Database();
    qcombobox_refresh();
}

PurchaseOrder::~PurchaseOrder()
{
    delete ui;
}

bool PurchaseOrder::refresh(QSqlQuery query)
{
    for(int row = ui->tableWidget->rowCount() - 1;row >= 0; row--)
    {
        ui->tableWidget->removeRow(row);
    }
    int rowIndex = ui->tableWidget->rowCount();//当前表格的行数
    while(query.next()){
        ui->tableWidget->insertRow(rowIndex);//在最后一行的后面插入一行
        for(int j=0; j<=7; j++){
            if(j!=7){
                ui->tableWidget->setItem(rowIndex,j,new QTableWidgetItem(query.value(j+1).toString()));
                ui->tableWidget->item(rowIndex,j)->setTextAlignment(Qt::AlignCenter);
            }else{
                if(query.value(10)==0){
                    ui->tableWidget->setItem(rowIndex,j,new QTableWidgetItem("未收货"));
                }else{
                    ui->tableWidget->setItem(rowIndex,j,new QTableWidgetItem("已收货"));
                    ui->tableWidget->setItem(rowIndex,j+1,new QTableWidgetItem(query.value(11).toString()));
                    ui->tableWidget->item(rowIndex,j+1)->setTextAlignment(Qt::AlignCenter);
                }
                ui->tableWidget->item(rowIndex,j)->setTextAlignment(Qt::AlignCenter);
            }

        }
        rowIndex += 1;
    }
    return true;
}

void PurchaseOrder::qcombobox_refresh(){
    for (QComboBox *combobox : ui->tabWidget->findChildren<QComboBox *>()) {
        if(combobox==ui->datetime) continue;
        combobox->clear();
        combobox->addItem("");
    }
    QSqlQuery query_supplier;
    query_supplier.exec("select name FROM supplierdata where del_flag=0");
    while(query_supplier.next()){
        QString name = query_supplier.value(0).toString();
        ui->comboBox1->addItem(name);
        ui->comboBox21->addItem(name);
        ui->comboBox31->addItem(name);
    }
    QSqlQuery query_material;
    query_material.exec("select ProductName FROM product_data_table where PurchaseRequest=1");
    while(query_material.next()){
        QString name = query_material.value(0).toString();
        ui->comboBox2->addItem(name);
        ui->comboBox22->addItem(name);
        ui->comboBox32->addItem(name);
    }
    QSqlQuery query_buyer;
    query_buyer.exec("select name FROM buyerdata where del_flag=0");
    while(query_buyer.next()){
        QString name = query_buyer.value(0).toString();
        ui->comboBox3->addItem(name);
        ui->comboBox23->addItem(name);
    }
    QSqlQuery query_time;
    query_time.exec("select date FROM purchaseorder where del_flag=0");
    while(query_time.next()){
        QString name = query_time.value(0).toDateTime().toString("yyyy-MM-dd hh:mm:ss");
        ui->datetime_box->addItem(name);
    }
    QSqlQuery query_commend;
    query_commend.exec("select commend FROM purchaseorder where del_flag=0");
    while(query_commend.next()){
        QString name = query_commend.value(0).toString();
        ui->comboBox24->addItem(name);
        ui->comboBox33->addItem(name);
    }
}


void PurchaseOrder::on_pushButton_clicked()
{
    QSqlQuery query;
    QString supplier = ui->comboBox1->currentText()==nullptr? "": ui->comboBox1->currentText();
    QString material = ui->comboBox2->currentText()==nullptr? "": ui->comboBox2->currentText();
    QString number = ui->lineEdit->text()==nullptr? "": ui->lineEdit->text();
    QString buyer = ui->comboBox3->currentText()==nullptr? "": ui->comboBox3->currentText();
    QString datetime = ui->datetime->currentText()==nullptr? "": ui->datetime->currentText();
    QString remark = ui->textEdit->toPlainText()==nullptr? "": ui->textEdit->toPlainText();
    QString insert_row = QString("insert into purchaseorder(date,material,materialnumber,buyer,supplier,remark) values('%1','%2','%3','%4','%5','%6')").arg(datetime).arg(material).arg(number).arg(buyer).arg(supplier).arg(remark);
    qDebug() << insert_row;
    query.exec(insert_row);
    qcombobox_refresh();
    ui->datetime->clear();
    ui->lineEdit->clear();
    ui->textEdit->clear();
}

void PurchaseOrder::on_pushButton_2_clicked()
{
    QString supplier = ui->comboBox21->currentText()==nullptr? "": ui->comboBox21->currentText();
    QString material = ui->comboBox22->currentText()==nullptr? "": ui->comboBox22->currentText();
    QString number = ui->lineEdit_2->text()==nullptr? "": ui->lineEdit_2->text();
    QString buyer = ui->comboBox23->currentText()==nullptr? "": ui->comboBox23->currentText();
    QString datetime = ui->datetime_box->currentText()==nullptr? "": ui->datetime_box->currentText();
    QString commend = ui->comboBox24->currentText()==nullptr? "": ui->comboBox24->currentText();

    QString check = "Select * from purchaseorder where del_flag=0";

    QString where = "";

    if(supplier.isEmpty() && material.isEmpty() && number.isEmpty() && buyer.isEmpty() && datetime.isEmpty() && commend.isEmpty()){
        check = "Select * from purchaseorder where del_flag=0";
    }else{
        if (!supplier.isEmpty()) {
            where += QString(" AND supplier LIKE '%1'").arg(supplier);
        }
        if (!material.isEmpty()) {
            where += QString(" AND material LIKE '%1'").arg(material);
        }
        if (!number.isEmpty()) {
            where += QString(" AND materialnumber LIKE %1").arg(number);
        }
        if (!buyer.isEmpty()) {
            where += QString(" AND buyer LIKE '%1'").arg(buyer);
        }
        if (!datetime.isEmpty()) {
            where += QString(" AND date BETWEEN '%1' AND '%1'").arg(datetime);
        }
        if (!commend.isEmpty()) {
            where += QString(" AND commend LIKE '%1'").arg(commend);
        }
        check += where;
    }
    qDebug() << check;
    QSqlQuery query;
    query.exec(check);
    refresh(query);
}

void PurchaseOrder::on_pushButton_4_clicked()
{
    //int rowIndex = ui->tableWidget->rowCount();
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString datetime = ui->tableWidget->item(rowIdx,0)==nullptr? " ": ui->tableWidget->item(rowIdx,0)->text();
        QString update_row = QString("update purchaseorder set del_flag = 1 where date BETWEEN '%1' AND '%1'").arg(datetime);
        qDebug() << update_row;
        query.exec(update_row);
        ui->tableWidget->removeRow(rowIdx);
    }
    qcombobox_refresh();
}

void PurchaseOrder::on_pushButton_3_clicked()
{
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString datetime = ui->tableWidget->item(rowIdx,0)==nullptr? "": ui->tableWidget->item(rowIdx,0)->text();
        QString material = ui->tableWidget->item(rowIdx,1)==nullptr? "": ui->tableWidget->item(rowIdx,1)->text();
        QString number = ui->tableWidget->item(rowIdx,2)==nullptr? "": ui->tableWidget->item(rowIdx,2)->text();
        QString buyer = ui->tableWidget->item(rowIdx,3)==nullptr? "": ui->tableWidget->item(rowIdx,3)->text();
        QString supplier = ui->tableWidget->item(rowIdx,4)==nullptr? "": ui->tableWidget->item(rowIdx,4)->text();
        QString remark = ui->tableWidget->item(rowIdx,5)==nullptr? "": ui->tableWidget->item(rowIdx,5)->text();
        QString update_row = QString("update purchaseorder set material = '%1', materialnumber = '%2', buyer = '%3', supplier = '%4',remark = '%5' where date between '%6' and '%6'").arg(material).arg(number).arg(buyer).arg(supplier).arg(remark).arg(datetime);
        qDebug() << update_row;
        query.exec(update_row);
    }
    qcombobox_refresh();
}

void PurchaseOrder::on_pushButton_7_clicked()
{
    QString supplier = ui->comboBox31->currentText()==nullptr? "": ui->comboBox31->currentText();
    QString material = ui->comboBox32->currentText()==nullptr? "": ui->comboBox32->currentText();
    QString commend = ui->comboBox33->currentText()==nullptr? "": ui->comboBox33->currentText();

    QString check = "Select * from purchaseorder where del_flag=0";

    QString where = "";

    if(supplier.isEmpty() && material.isEmpty() && commend.isEmpty()){
        check = "Select * from purchaseorder where del_flag=0";
    }else{
        if (!supplier.isEmpty()) {
            where += QString(" AND supplier LIKE '%1'").arg(supplier);
        }
        if (!material.isEmpty()) {
            where += QString(" AND material LIKE '%1'").arg(material);
        }
        if (!commend.isEmpty()) {
            where += QString(" AND commend LIKE '%1'").arg(commend);
        }
        check += where;
    }
    qDebug() << check;
    QSqlQuery query;
    query.exec(check);
    for(int row = ui->tableWidget_2->rowCount() - 1;row >= 0; row--)
    {
        ui->tableWidget_2->removeRow(row);
    }
    int rowIndex = ui->tableWidget_2->rowCount();//当前表格的行数
    while(query.next()){
        ui->tableWidget_2->insertRow(rowIndex);//在最后一行的后面插入一行
        for(int j=0; j<8; j++){
            ui->tableWidget_2->setItem(rowIndex,j,new QTableWidgetItem(query.value(j+1).toString()));
            ui->tableWidget_2->item(rowIndex,j)->setTextAlignment(Qt::AlignCenter);
        }
        rowIndex += 1;
    }
    qcombobox_refresh();
}

void PurchaseOrder::on_pushButton_9_clicked()
{
    int rowIdx = ui->tableWidget_2->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString datetime = ui->tableWidget_2->item(rowIdx,0)==nullptr? "": ui->tableWidget_2->item(rowIdx,0)->text();
        QString material = ui->tableWidget_2->item(rowIdx,1)==nullptr? "": ui->tableWidget_2->item(rowIdx,1)->text();
        QString number = ui->tableWidget_2->item(rowIdx,2)==nullptr? "": ui->tableWidget_2->item(rowIdx,2)->text();
        QString buyer = ui->tableWidget_2->item(rowIdx,3)==nullptr? "": ui->tableWidget_2->item(rowIdx,3)->text();
        QString supplier = ui->tableWidget_2->item(rowIdx,4)==nullptr? "": ui->tableWidget_2->item(rowIdx,4)->text();
        QString remark = ui->tableWidget_2->item(rowIdx,5)==nullptr? "": ui->tableWidget_2->item(rowIdx,5)->text();
        QString commend = ui->tableWidget_2->item(rowIdx,6)==nullptr? "": ui->tableWidget_2->item(rowIdx,6)->text();
        QDateTime evalutime= QDateTime::currentDateTime();//获取系统当前的时间
        QString str = evalutime.toString("yyyy-MM-dd hh:mm:ss");
        QString update_row = QString("update purchaseorder set commend = '%1', evalutime = '%2' where date between '%3' and '%3'").arg(commend).arg(str).arg(datetime);
        qDebug() << update_row;
        query.exec(update_row);
    }
    qcombobox_refresh();
}

void PurchaseOrder::on_pushButton_5_clicked()
{
    int rowIdx = ui->tableWidget->currentRow();
    qDebug() << rowIdx;
    QSqlQuery query;
    if(rowIdx+1){
        QString datetime = ui->tableWidget->item(rowIdx,0)==nullptr? "": ui->tableWidget->item(rowIdx,0)->text();
        QDateTime rectime= QDateTime::currentDateTime();//获取系统当前的时间
        QString str = rectime.toString("yyyy-MM-dd hh:mm:ss");
        QString update_row = QString("update purchaseorder set receive = 1, rectime = '%2' where date between '%1' and '%1'").arg(datetime).arg(str);
        qDebug() << update_row;
        query.exec(update_row);
        ui->tableWidget->item(rowIdx,7)->setText("已收货");
    }
    qcombobox_refresh();
}
