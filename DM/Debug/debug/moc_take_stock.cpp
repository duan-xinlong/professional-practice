/****************************************************************************
** Meta object code from reading C++ file 'take_stock.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../Storage/take_stock.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'take_stock.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TakeStock_t {
    QByteArrayData data[21];
    char stringdata0[368];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TakeStock_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TakeStock_t qt_meta_stringdata_TakeStock = {
    {
QT_MOC_LITERAL(0, 0, 9), // "TakeStock"
QT_MOC_LITERAL(1, 10, 14), // "RightClickSlot"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 3), // "pos"
QT_MOC_LITERAL(4, 30, 16), // "RightClickAddRow"
QT_MOC_LITERAL(5, 47, 8), // "QAction*"
QT_MOC_LITERAL(6, 56, 3), // "act"
QT_MOC_LITERAL(7, 60, 19), // "RightClickDeleteRow"
QT_MOC_LITERAL(8, 80, 26), // "choose_product_btn_clicked"
QT_MOC_LITERAL(9, 107, 24), // "choose_batch_btn_clicked"
QT_MOC_LITERAL(10, 132, 18), // "on_add_btn_clicked"
QT_MOC_LITERAL(11, 151, 21), // "on_delete_btn_clicked"
QT_MOC_LITERAL(12, 173, 21), // "on_submit_btn_clicked"
QT_MOC_LITERAL(13, 195, 28), // "on_select_member_btn_clicked"
QT_MOC_LITERAL(14, 224, 33), // "on_type_combo_currentIndexCha..."
QT_MOC_LITERAL(15, 258, 4), // "arg1"
QT_MOC_LITERAL(16, 263, 36), // "on_storage_combo_currentIndex..."
QT_MOC_LITERAL(17, 300, 29), // "on_start_date_editTextChanged"
QT_MOC_LITERAL(18, 330, 26), // "on_tableWidget_cellChanged"
QT_MOC_LITERAL(19, 357, 3), // "row"
QT_MOC_LITERAL(20, 361, 6) // "column"

    },
    "TakeStock\0RightClickSlot\0\0pos\0"
    "RightClickAddRow\0QAction*\0act\0"
    "RightClickDeleteRow\0choose_product_btn_clicked\0"
    "choose_batch_btn_clicked\0on_add_btn_clicked\0"
    "on_delete_btn_clicked\0on_submit_btn_clicked\0"
    "on_select_member_btn_clicked\0"
    "on_type_combo_currentIndexChanged\0"
    "arg1\0on_storage_combo_currentIndexChanged\0"
    "on_start_date_editTextChanged\0"
    "on_tableWidget_cellChanged\0row\0column"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TakeStock[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x08 /* Private */,
       4,    1,   82,    2, 0x08 /* Private */,
       7,    1,   85,    2, 0x08 /* Private */,
       8,    0,   88,    2, 0x08 /* Private */,
       9,    0,   89,    2, 0x08 /* Private */,
      10,    0,   90,    2, 0x08 /* Private */,
      11,    0,   91,    2, 0x08 /* Private */,
      12,    0,   92,    2, 0x08 /* Private */,
      13,    0,   93,    2, 0x08 /* Private */,
      14,    1,   94,    2, 0x08 /* Private */,
      16,    1,   97,    2, 0x08 /* Private */,
      17,    1,  100,    2, 0x08 /* Private */,
      18,    2,  103,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QPoint,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   19,   20,

       0        // eod
};

void TakeStock::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TakeStock *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->RightClickSlot((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 1: _t->RightClickAddRow((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 2: _t->RightClickDeleteRow((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 3: _t->choose_product_btn_clicked(); break;
        case 4: _t->choose_batch_btn_clicked(); break;
        case 5: _t->on_add_btn_clicked(); break;
        case 6: _t->on_delete_btn_clicked(); break;
        case 7: _t->on_submit_btn_clicked(); break;
        case 8: _t->on_select_member_btn_clicked(); break;
        case 9: _t->on_type_combo_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->on_storage_combo_currentIndexChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->on_start_date_editTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->on_tableWidget_cellChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TakeStock::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_TakeStock.data,
    qt_meta_data_TakeStock,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TakeStock::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TakeStock::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TakeStock.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int TakeStock::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
