/****************************************************************************
** Meta object code from reading C++ file 'picking.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../Planning/picking.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'picking.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Picking_t {
    QByteArrayData data[25];
    char stringdata0[464];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Picking_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Picking_t qt_meta_stringdata_Picking = {
    {
QT_MOC_LITERAL(0, 0, 7), // "Picking"
QT_MOC_LITERAL(1, 8, 28), // "on_warehouse_combo_activated"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 4), // "arg1"
QT_MOC_LITERAL(4, 43, 18), // "on_add_btn_clicked"
QT_MOC_LITERAL(5, 62, 18), // "choose_btn_clicked"
QT_MOC_LITERAL(6, 81, 14), // "RightClickSlot"
QT_MOC_LITERAL(7, 96, 3), // "pos"
QT_MOC_LITERAL(8, 100, 16), // "RightClickAddRow"
QT_MOC_LITERAL(9, 117, 8), // "QAction*"
QT_MOC_LITERAL(10, 126, 3), // "act"
QT_MOC_LITERAL(11, 130, 19), // "RightClickDeleteRow"
QT_MOC_LITERAL(12, 150, 21), // "on_delete_btn_clicked"
QT_MOC_LITERAL(13, 172, 24), // "on_select_member_clicked"
QT_MOC_LITERAL(14, 197, 21), // "on_submit_btn_clicked"
QT_MOC_LITERAL(15, 219, 20), // "on_add_btn_2_clicked"
QT_MOC_LITERAL(16, 240, 20), // "choose_btn_clicked_2"
QT_MOC_LITERAL(17, 261, 16), // "RightClickSlot_2"
QT_MOC_LITERAL(18, 278, 18), // "RightClickAddRow_2"
QT_MOC_LITERAL(19, 297, 21), // "RightClickDeleteRow_2"
QT_MOC_LITERAL(20, 319, 23), // "on_delete_btn_2_clicked"
QT_MOC_LITERAL(21, 343, 26), // "on_select_member_2_clicked"
QT_MOC_LITERAL(22, 370, 23), // "on_submit_btn_2_clicked"
QT_MOC_LITERAL(23, 394, 33), // "on_order_combo_currentTextCha..."
QT_MOC_LITERAL(24, 428, 35) // "on_tableWidget_itemSelectionC..."

    },
    "Picking\0on_warehouse_combo_activated\0"
    "\0arg1\0on_add_btn_clicked\0choose_btn_clicked\0"
    "RightClickSlot\0pos\0RightClickAddRow\0"
    "QAction*\0act\0RightClickDeleteRow\0"
    "on_delete_btn_clicked\0on_select_member_clicked\0"
    "on_submit_btn_clicked\0on_add_btn_2_clicked\0"
    "choose_btn_clicked_2\0RightClickSlot_2\0"
    "RightClickAddRow_2\0RightClickDeleteRow_2\0"
    "on_delete_btn_2_clicked\0"
    "on_select_member_2_clicked\0"
    "on_submit_btn_2_clicked\0"
    "on_order_combo_currentTextChanged\0"
    "on_tableWidget_itemSelectionChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Picking[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x08 /* Private */,
       4,    0,  112,    2, 0x08 /* Private */,
       5,    0,  113,    2, 0x08 /* Private */,
       6,    1,  114,    2, 0x08 /* Private */,
       8,    1,  117,    2, 0x08 /* Private */,
      11,    1,  120,    2, 0x08 /* Private */,
      12,    0,  123,    2, 0x08 /* Private */,
      13,    0,  124,    2, 0x08 /* Private */,
      14,    0,  125,    2, 0x08 /* Private */,
      15,    0,  126,    2, 0x08 /* Private */,
      16,    0,  127,    2, 0x08 /* Private */,
      17,    1,  128,    2, 0x08 /* Private */,
      18,    1,  131,    2, 0x08 /* Private */,
      19,    1,  134,    2, 0x08 /* Private */,
      20,    0,  137,    2, 0x08 /* Private */,
      21,    0,  138,    2, 0x08 /* Private */,
      22,    0,  139,    2, 0x08 /* Private */,
      23,    1,  140,    2, 0x08 /* Private */,
      24,    0,  143,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPoint,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void,

       0        // eod
};

void Picking::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Picking *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_warehouse_combo_activated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->on_add_btn_clicked(); break;
        case 2: _t->choose_btn_clicked(); break;
        case 3: _t->RightClickSlot((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 4: _t->RightClickAddRow((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 5: _t->RightClickDeleteRow((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 6: _t->on_delete_btn_clicked(); break;
        case 7: _t->on_select_member_clicked(); break;
        case 8: _t->on_submit_btn_clicked(); break;
        case 9: _t->on_add_btn_2_clicked(); break;
        case 10: _t->choose_btn_clicked_2(); break;
        case 11: _t->RightClickSlot_2((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 12: _t->RightClickAddRow_2((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 13: _t->RightClickDeleteRow_2((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 14: _t->on_delete_btn_2_clicked(); break;
        case 15: _t->on_select_member_2_clicked(); break;
        case 16: _t->on_submit_btn_2_clicked(); break;
        case 17: _t->on_order_combo_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 18: _t->on_tableWidget_itemSelectionChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Picking::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_Picking.data,
    qt_meta_data_Picking,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Picking::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Picking::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Picking.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int Picking::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
