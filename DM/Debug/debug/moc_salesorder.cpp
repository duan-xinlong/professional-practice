/****************************************************************************
** Meta object code from reading C++ file 'salesorder.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../Sale/salesorder.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'salesorder.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_salesorder_t {
    QByteArrayData data[18];
    char stringdata0[291];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_salesorder_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_salesorder_t qt_meta_stringdata_salesorder = {
    {
QT_MOC_LITERAL(0, 0, 10), // "salesorder"
QT_MOC_LITERAL(1, 11, 14), // "RightClickSlot"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 3), // "pos"
QT_MOC_LITERAL(4, 31, 16), // "RightClickAddRow"
QT_MOC_LITERAL(5, 48, 8), // "QAction*"
QT_MOC_LITERAL(6, 57, 3), // "act"
QT_MOC_LITERAL(7, 61, 19), // "RightClickDeleteRow"
QT_MOC_LITERAL(8, 81, 18), // "choose_btn_clicked"
QT_MOC_LITERAL(9, 100, 18), // "on_add_btn_clicked"
QT_MOC_LITERAL(10, 119, 21), // "on_delete_btn_clicked"
QT_MOC_LITERAL(11, 141, 20), // "on_add_btn_2_clicked"
QT_MOC_LITERAL(12, 162, 23), // "on_delete_btn_2_clicked"
QT_MOC_LITERAL(13, 186, 23), // "on_pushButton_2_clicked"
QT_MOC_LITERAL(14, 210, 29), // "on_comboBox_2_editTextChanged"
QT_MOC_LITERAL(15, 240, 4), // "arg1"
QT_MOC_LITERAL(16, 245, 23), // "on_pushButton_3_clicked"
QT_MOC_LITERAL(17, 269, 21) // "on_pushButton_clicked"

    },
    "salesorder\0RightClickSlot\0\0pos\0"
    "RightClickAddRow\0QAction*\0act\0"
    "RightClickDeleteRow\0choose_btn_clicked\0"
    "on_add_btn_clicked\0on_delete_btn_clicked\0"
    "on_add_btn_2_clicked\0on_delete_btn_2_clicked\0"
    "on_pushButton_2_clicked\0"
    "on_comboBox_2_editTextChanged\0arg1\0"
    "on_pushButton_3_clicked\0on_pushButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_salesorder[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x08 /* Private */,
       4,    1,   77,    2, 0x08 /* Private */,
       7,    1,   80,    2, 0x08 /* Private */,
       8,    0,   83,    2, 0x08 /* Private */,
       9,    0,   84,    2, 0x08 /* Private */,
      10,    0,   85,    2, 0x08 /* Private */,
      11,    0,   86,    2, 0x08 /* Private */,
      12,    0,   87,    2, 0x08 /* Private */,
      13,    0,   88,    2, 0x08 /* Private */,
      14,    1,   89,    2, 0x08 /* Private */,
      16,    0,   92,    2, 0x08 /* Private */,
      17,    0,   93,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::QPoint,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void salesorder::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<salesorder *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->RightClickSlot((*reinterpret_cast< QPoint(*)>(_a[1]))); break;
        case 1: _t->RightClickAddRow((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 2: _t->RightClickDeleteRow((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 3: _t->choose_btn_clicked(); break;
        case 4: _t->on_add_btn_clicked(); break;
        case 5: _t->on_delete_btn_clicked(); break;
        case 6: _t->on_add_btn_2_clicked(); break;
        case 7: _t->on_delete_btn_2_clicked(); break;
        case 8: _t->on_pushButton_2_clicked(); break;
        case 9: _t->on_comboBox_2_editTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->on_pushButton_3_clicked(); break;
        case 11: _t->on_pushButton_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAction* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject salesorder::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_salesorder.data,
    qt_meta_data_salesorder,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *salesorder::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *salesorder::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_salesorder.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int salesorder::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
