/****************************************************************************
** Meta object code from reading C++ file 'orgmgmt.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../System/orgmgmt.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'orgmgmt.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OrgMgmt_t {
    QByteArrayData data[11];
    char stringdata0[167];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OrgMgmt_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OrgMgmt_t qt_meta_stringdata_OrgMgmt = {
    {
QT_MOC_LITERAL(0, 0, 7), // "OrgMgmt"
QT_MOC_LITERAL(1, 8, 18), // "on_btn_add_clicked"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 21), // "on_btn_cancel_clicked"
QT_MOC_LITERAL(4, 50, 19), // "on_btn_save_clicked"
QT_MOC_LITERAL(5, 70, 22), // "on_btn_refresh_clicked"
QT_MOC_LITERAL(6, 93, 25), // "on_treeWidget_itemClicked"
QT_MOC_LITERAL(7, 119, 16), // "QTreeWidgetItem*"
QT_MOC_LITERAL(8, 136, 4), // "item"
QT_MOC_LITERAL(9, 141, 6), // "column"
QT_MOC_LITERAL(10, 148, 18) // "on_btn_del_clicked"

    },
    "OrgMgmt\0on_btn_add_clicked\0\0"
    "on_btn_cancel_clicked\0on_btn_save_clicked\0"
    "on_btn_refresh_clicked\0on_treeWidget_itemClicked\0"
    "QTreeWidgetItem*\0item\0column\0"
    "on_btn_del_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OrgMgmt[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x08 /* Private */,
       3,    0,   45,    2, 0x08 /* Private */,
       4,    0,   46,    2, 0x08 /* Private */,
       5,    0,   47,    2, 0x08 /* Private */,
       6,    2,   48,    2, 0x08 /* Private */,
      10,    0,   53,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int,    8,    9,
    QMetaType::Void,

       0        // eod
};

void OrgMgmt::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<OrgMgmt *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_btn_add_clicked(); break;
        case 1: _t->on_btn_cancel_clicked(); break;
        case 2: _t->on_btn_save_clicked(); break;
        case 3: _t->on_btn_refresh_clicked(); break;
        case 4: _t->on_treeWidget_itemClicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->on_btn_del_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject OrgMgmt::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_OrgMgmt.data,
    qt_meta_data_OrgMgmt,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *OrgMgmt::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OrgMgmt::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OrgMgmt.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int OrgMgmt::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
