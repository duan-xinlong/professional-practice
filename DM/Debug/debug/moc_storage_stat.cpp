/****************************************************************************
** Meta object code from reading C++ file 'storage_stat.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../Storage/storage_stat.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'storage_stat.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_StorageStat_t {
    QByteArrayData data[15];
    char stringdata0[320];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_StorageStat_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_StorageStat_t qt_meta_stringdata_StorageStat = {
    {
QT_MOC_LITERAL(0, 0, 11), // "StorageStat"
QT_MOC_LITERAL(1, 12, 27), // "on_tabWidget_currentChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 5), // "index"
QT_MOC_LITERAL(4, 47, 21), // "on_InEdit_textChanged"
QT_MOC_LITERAL(5, 69, 4), // "arg1"
QT_MOC_LITERAL(6, 74, 22), // "on_OutEdit_textChanged"
QT_MOC_LITERAL(7, 97, 27), // "on_TransferEdit_textChanged"
QT_MOC_LITERAL(8, 125, 23), // "on_TakeEdit_textChanged"
QT_MOC_LITERAL(9, 149, 26), // "on_ProductEdit_textChanged"
QT_MOC_LITERAL(10, 176, 25), // "on_In_refresh_btn_clicked"
QT_MOC_LITERAL(11, 202, 26), // "on_Out_refresh_btn_clicked"
QT_MOC_LITERAL(12, 229, 31), // "on_Transfer_refresh_btn_clicked"
QT_MOC_LITERAL(13, 261, 27), // "on_Take_refresh_btn_clicked"
QT_MOC_LITERAL(14, 289, 30) // "on_Product_refresh_btn_clicked"

    },
    "StorageStat\0on_tabWidget_currentChanged\0"
    "\0index\0on_InEdit_textChanged\0arg1\0"
    "on_OutEdit_textChanged\0"
    "on_TransferEdit_textChanged\0"
    "on_TakeEdit_textChanged\0"
    "on_ProductEdit_textChanged\0"
    "on_In_refresh_btn_clicked\0"
    "on_Out_refresh_btn_clicked\0"
    "on_Transfer_refresh_btn_clicked\0"
    "on_Take_refresh_btn_clicked\0"
    "on_Product_refresh_btn_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_StorageStat[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x08 /* Private */,
       4,    1,   72,    2, 0x08 /* Private */,
       6,    1,   75,    2, 0x08 /* Private */,
       7,    1,   78,    2, 0x08 /* Private */,
       8,    1,   81,    2, 0x08 /* Private */,
       9,    1,   84,    2, 0x08 /* Private */,
      10,    0,   87,    2, 0x08 /* Private */,
      11,    0,   88,    2, 0x08 /* Private */,
      12,    0,   89,    2, 0x08 /* Private */,
      13,    0,   90,    2, 0x08 /* Private */,
      14,    0,   91,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void StorageStat::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<StorageStat *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_tabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_InEdit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->on_OutEdit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->on_TransferEdit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->on_TakeEdit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->on_ProductEdit_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->on_In_refresh_btn_clicked(); break;
        case 7: _t->on_Out_refresh_btn_clicked(); break;
        case 8: _t->on_Transfer_refresh_btn_clicked(); break;
        case 9: _t->on_Take_refresh_btn_clicked(); break;
        case 10: _t->on_Product_refresh_btn_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject StorageStat::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_StorageStat.data,
    qt_meta_data_StorageStat,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *StorageStat::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *StorageStat::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_StorageStat.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int StorageStat::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
