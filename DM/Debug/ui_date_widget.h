/********************************************************************************
** Form generated from reading UI file 'date_widget.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATE_WIDGET_H
#define UI_DATE_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Tool/button_day.h"

QT_BEGIN_NAMESPACE

class Ui_DateWidget
{
public:
    QGridLayout *gridLayout_2;
    QWidget *wdgConfirm;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *btnToday;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *btnCancel;
    QPushButton *btnConfirm;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btnPrev;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    ButtonDay *btnMonth;
    QSpacerItem *horizontalSpacer_4;
    ButtonDay *btnYear;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btnNext;
    QStackedWidget *stackedWidget;
    QWidget *wdgCalendar;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_8;
    QWidget *widget_5;
    QVBoxLayout *verticalLayout_9;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_3;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QWidget *wdgYears;
    QHBoxLayout *horizontalLayout_3;
    QGridLayout *gridLayoutYears;
    QDateEdit *dateEdit;
    QFrame *line_2;

    void setupUi(QWidget *DateWidget)
    {
        if (DateWidget->objectName().isEmpty())
            DateWidget->setObjectName(QString::fromUtf8("DateWidget"));
        DateWidget->resize(300, 378);
        gridLayout_2 = new QGridLayout(DateWidget);
        gridLayout_2->setSpacing(0);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        wdgConfirm = new QWidget(DateWidget);
        wdgConfirm->setObjectName(QString::fromUtf8("wdgConfirm"));
        wdgConfirm->setMinimumSize(QSize(0, 30));
        verticalLayout_6 = new QVBoxLayout(wdgConfirm);
        verticalLayout_6->setSpacing(0);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(6, -1, 6, -1);
        btnToday = new QPushButton(wdgConfirm);
        btnToday->setObjectName(QString::fromUtf8("btnToday"));
        btnToday->setMaximumSize(QSize(50, 16777215));
        btnToday->setProperty("type", QVariant(QString::fromUtf8("today")));

        horizontalLayout_5->addWidget(btnToday);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(10);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        btnCancel = new QPushButton(wdgConfirm);
        btnCancel->setObjectName(QString::fromUtf8("btnCancel"));
        btnCancel->setMaximumSize(QSize(50, 16777215));
        btnCancel->setProperty("type", QVariant(QString::fromUtf8("cancel")));

        horizontalLayout_6->addWidget(btnCancel);

        btnConfirm = new QPushButton(wdgConfirm);
        btnConfirm->setObjectName(QString::fromUtf8("btnConfirm"));
        btnConfirm->setMaximumSize(QSize(50, 16777215));
        btnConfirm->setProperty("type", QVariant(QString::fromUtf8("confirm")));

        horizontalLayout_6->addWidget(btnConfirm);


        horizontalLayout_5->addLayout(horizontalLayout_6);


        verticalLayout_6->addLayout(horizontalLayout_5);


        gridLayout_2->addWidget(wdgConfirm, 4, 0, 1, 2);

        widget_3 = new QWidget(DateWidget);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setMinimumSize(QSize(0, 30));
        verticalLayout_5 = new QVBoxLayout(widget_3);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(10, -1, 10, -1);
        btnPrev = new QPushButton(widget_3);
        btnPrev->setObjectName(QString::fromUtf8("btnPrev"));
        btnPrev->setFlat(false);
        btnPrev->setProperty("type", QVariant(QString::fromUtf8("prev")));

        horizontalLayout_2->addWidget(btnPrev);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btnMonth = new ButtonDay(widget_3);
        btnMonth->setObjectName(QString::fromUtf8("btnMonth"));
        btnMonth->setMinimumSize(QSize(40, 0));
        btnMonth->setMaximumSize(QSize(80, 16777215));
        QFont font;
        font.setPointSize(12);
        btnMonth->setFont(font);
        btnMonth->setFlat(false);
        btnMonth->setProperty("type", QVariant(QString::fromUtf8("month")));

        horizontalLayout->addWidget(btnMonth);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        btnYear = new ButtonDay(widget_3);
        btnYear->setObjectName(QString::fromUtf8("btnYear"));
        btnYear->setMinimumSize(QSize(70, 0));
        btnYear->setMaximumSize(QSize(60, 16777215));
        btnYear->setFont(font);
        btnYear->setLayoutDirection(Qt::LeftToRight);
        btnYear->setInputMethodHints(Qt::ImhNone);
        btnYear->setFlat(false);
        btnYear->setProperty("type", QVariant(QString::fromUtf8("year")));

        horizontalLayout->addWidget(btnYear);

        horizontalLayout->setStretch(1, 1);

        horizontalLayout_2->addLayout(horizontalLayout);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        btnNext = new QPushButton(widget_3);
        btnNext->setObjectName(QString::fromUtf8("btnNext"));
        btnNext->setFlat(false);
        btnNext->setProperty("type", QVariant(QString::fromUtf8("next")));

        horizontalLayout_2->addWidget(btnNext);


        verticalLayout_5->addLayout(horizontalLayout_2);


        gridLayout_2->addWidget(widget_3, 1, 0, 1, 1);

        stackedWidget = new QStackedWidget(DateWidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        wdgCalendar = new QWidget();
        wdgCalendar->setObjectName(QString::fromUtf8("wdgCalendar"));
        verticalLayout_7 = new QVBoxLayout(wdgCalendar);
        verticalLayout_7->setSpacing(0);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(0);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        widget_5 = new QWidget(wdgCalendar);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        widget_5->setMinimumSize(QSize(0, 36));
        verticalLayout_9 = new QVBoxLayout(widget_5);
        verticalLayout_9->setSpacing(0);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(8);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_3 = new QLabel(widget_5);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(30, 30));
        label_3->setMaximumSize(QSize(30, 30));
        label_3->setAlignment(Qt::AlignCenter);
        label_3->setProperty("type", QVariant(QString::fromUtf8("weekend")));

        horizontalLayout_7->addWidget(label_3);

        label_9 = new QLabel(widget_5);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(30, 30));
        label_9->setMaximumSize(QSize(30, 30));
        label_9->setAlignment(Qt::AlignCenter);
        label_9->setProperty("type", QVariant(QString::fromUtf8("workday")));

        horizontalLayout_7->addWidget(label_9);

        label_10 = new QLabel(widget_5);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(30, 30));
        label_10->setMaximumSize(QSize(30, 30));
        label_10->setAlignment(Qt::AlignCenter);
        label_10->setProperty("type", QVariant(QString::fromUtf8("workday")));

        horizontalLayout_7->addWidget(label_10);

        label_11 = new QLabel(widget_5);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(30, 30));
        label_11->setMaximumSize(QSize(30, 30));
        label_11->setAlignment(Qt::AlignCenter);
        label_11->setProperty("type", QVariant(QString::fromUtf8("workday")));

        horizontalLayout_7->addWidget(label_11);

        label_12 = new QLabel(widget_5);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(30, 30));
        label_12->setMaximumSize(QSize(30, 30));
        label_12->setAlignment(Qt::AlignCenter);
        label_12->setProperty("type", QVariant(QString::fromUtf8("workday")));

        horizontalLayout_7->addWidget(label_12);

        label_13 = new QLabel(widget_5);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setMinimumSize(QSize(30, 30));
        label_13->setMaximumSize(QSize(30, 30));
        label_13->setAlignment(Qt::AlignCenter);
        label_13->setProperty("type", QVariant(QString::fromUtf8("workday")));

        horizontalLayout_7->addWidget(label_13);

        label_14 = new QLabel(widget_5);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(30, 30));
        label_14->setMaximumSize(QSize(30, 30));
        label_14->setAlignment(Qt::AlignCenter);
        label_14->setProperty("type", QVariant(QString::fromUtf8("weekend")));

        horizontalLayout_7->addWidget(label_14);


        verticalLayout_9->addLayout(horizontalLayout_7);


        verticalLayout_8->addWidget(widget_5);

        widget = new QWidget(wdgCalendar);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMinimumSize(QSize(0, 240));
        widget->setMaximumSize(QSize(10000, 10000));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(8);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));

        verticalLayout->addLayout(gridLayout);


        verticalLayout_8->addWidget(widget);


        verticalLayout_7->addLayout(verticalLayout_8);

        stackedWidget->addWidget(wdgCalendar);
        wdgYears = new QWidget();
        wdgYears->setObjectName(QString::fromUtf8("wdgYears"));
        horizontalLayout_3 = new QHBoxLayout(wdgYears);
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        gridLayoutYears = new QGridLayout();
        gridLayoutYears->setSpacing(10);
        gridLayoutYears->setObjectName(QString::fromUtf8("gridLayoutYears"));

        horizontalLayout_3->addLayout(gridLayoutYears);

        stackedWidget->addWidget(wdgYears);

        gridLayout_2->addWidget(stackedWidget, 2, 0, 1, 1);

        dateEdit = new QDateEdit(DateWidget);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));
        dateEdit->setMinimumSize(QSize(0, 32));
        dateEdit->setFocusPolicy(Qt::NoFocus);
        dateEdit->setAlignment(Qt::AlignCenter);
        dateEdit->setReadOnly(true);
        dateEdit->setButtonSymbols(QAbstractSpinBox::NoButtons);

        gridLayout_2->addWidget(dateEdit, 0, 0, 1, 1);

        line_2 = new QFrame(DateWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line_2, 3, 0, 1, 2);


        retranslateUi(DateWidget);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(DateWidget);
    } // setupUi

    void retranslateUi(QWidget *DateWidget)
    {
        btnToday->setText(QCoreApplication::translate("DateWidget", "\344\273\212\346\227\245", nullptr));
        btnCancel->setText(QCoreApplication::translate("DateWidget", "\345\217\226\346\266\210", nullptr));
        btnConfirm->setText(QCoreApplication::translate("DateWidget", "\347\241\256\345\256\232", nullptr));
        btnPrev->setText(QString());
        btnMonth->setText(QCoreApplication::translate("DateWidget", "11\346\234\210", nullptr));
        btnYear->setText(QCoreApplication::translate("DateWidget", "2023\345\271\264", nullptr));
        btnNext->setText(QString());
        label_3->setText(QCoreApplication::translate("DateWidget", "\346\227\245", nullptr));
        label_9->setText(QCoreApplication::translate("DateWidget", "\344\270\200", nullptr));
        label_10->setText(QCoreApplication::translate("DateWidget", "\344\272\214", nullptr));
        label_11->setText(QCoreApplication::translate("DateWidget", "\344\270\211", nullptr));
        label_12->setText(QCoreApplication::translate("DateWidget", "\345\233\233", nullptr));
        label_13->setText(QCoreApplication::translate("DateWidget", "\344\272\224", nullptr));
        label_14->setText(QCoreApplication::translate("DateWidget", "\345\205\255", nullptr));
        dateEdit->setDisplayFormat(QCoreApplication::translate("DateWidget", "yyyy/MM/dd", nullptr));
        (void)DateWidget;
    } // retranslateUi

};

namespace Ui {
    class DateWidget: public Ui_DateWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATE_WIDGET_H
