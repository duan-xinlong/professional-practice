/********************************************************************************
** Form generated from reading UI file 'roletoolbox.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ROLETOOLBOX_H
#define UI_ROLETOOLBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_RoleToolBox
{
public:
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btn_func;
    QFrame *line;
    QPushButton *btn_edit;
    QFrame *line_3;
    QPushButton *btn_del;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *RoleToolBox)
    {
        if (RoleToolBox->objectName().isEmpty())
            RoleToolBox->setObjectName(QString::fromUtf8("RoleToolBox"));
        RoleToolBox->resize(363, 57);
        RoleToolBox->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"	background: transparent;\n"
"	border: none;\n"
"	color: rgb(0, 91, 172);\n"
"	font: 10pt \"Microsoft YaHei UI\";\n"
"	text-align : center;\n"
"}\n"
"QPushButton:hover{\n"
"	color:red;\n"
"	text-decoration:underline;\n"
"}"));
        horizontalLayout = new QHBoxLayout(RoleToolBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 6, 0, 6);
        horizontalSpacer = new QSpacerItem(45, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btn_func = new QPushButton(RoleToolBox);
        btn_func->setObjectName(QString::fromUtf8("btn_func"));
        btn_func->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(btn_func);

        line = new QFrame(RoleToolBox);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line);

        btn_edit = new QPushButton(RoleToolBox);
        btn_edit->setObjectName(QString::fromUtf8("btn_edit"));
        btn_edit->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(btn_edit);

        line_3 = new QFrame(RoleToolBox);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line_3);

        btn_del = new QPushButton(RoleToolBox);
        btn_del->setObjectName(QString::fromUtf8("btn_del"));
        btn_del->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(btn_del);

        horizontalSpacer_2 = new QSpacerItem(45, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        retranslateUi(RoleToolBox);

        QMetaObject::connectSlotsByName(RoleToolBox);
    } // setupUi

    void retranslateUi(QWidget *RoleToolBox)
    {
        RoleToolBox->setWindowTitle(QCoreApplication::translate("RoleToolBox", "Form", nullptr));
        btn_func->setText(QCoreApplication::translate("RoleToolBox", "\346\235\203\351\231\220\347\256\241\347\220\206", nullptr));
        btn_edit->setText(QCoreApplication::translate("RoleToolBox", "\347\274\226\350\276\221", nullptr));
        btn_del->setText(QCoreApplication::translate("RoleToolBox", "\345\210\240\351\231\244", nullptr));
    } // retranslateUi

};

namespace Ui {
    class RoleToolBox: public Ui_RoleToolBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ROLETOOLBOX_H
