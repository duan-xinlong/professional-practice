/********************************************************************************
** Form generated from reading UI file 'warehousing.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WAREHOUSING_H
#define UI_WAREHOUSING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>
#include "Tool/date_time_edit.h"

QT_BEGIN_NAMESPACE

class Ui_Warehousing
{
public:
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QPushButton *select_member;
    QLabel *label_10;
    QLabel *label_9;
    QLabel *label_21;
    QPushButton *submit_btn_2;
    QComboBox *type_combo_2;
    QLineEdit *ID_2;
    QCheckBox *confirm_checkBox_3;
    QComboBox *type_combo;
    QTableWidget *tableWidget;
    QCheckBox *confirm_checkBox_6;
    QLabel *label_6;
    QLabel *label_19;
    QLabel *label_24;
    QLabel *label_11;
    QLabel *label_17;
    QLabel *label_18;
    QLabel *label_15;
    QPushButton *add_btn;
    DateTimeEdit *datetime_2;
    QLineEdit *ID_3;
    QLineEdit *ID_5;
    QCheckBox *confirm_checkBox_4;
    QLabel *label_13;
    QPushButton *delete_btn;
    QLineEdit *ID_6;
    QLabel *label_7;
    QCheckBox *confirm_checkBox_5;
    QLineEdit *ID_4;
    QPushButton *select_member_2;
    QLabel *label_8;
    QCheckBox *confirm_checkBox;
    QLabel *label_12;
    QLabel *label_16;
    QCheckBox *confirm_checkBox_2;
    QLabel *label_14;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_20;
    DateTimeEdit *datetime_1;
    QLabel *label_25;
    QPushButton *update_btn;

    void setupUi(QWidget *Warehousing)
    {
        if (Warehousing->objectName().isEmpty())
            Warehousing->setObjectName(QString::fromUtf8("Warehousing"));
        Warehousing->resize(974, 708);
        horizontalLayout = new QHBoxLayout(Warehousing);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        select_member = new QPushButton(Warehousing);
        select_member->setObjectName(QString::fromUtf8("select_member"));
        select_member->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(select_member, 17, 3, 1, 1);

        label_10 = new QLabel(Warehousing);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 1, 0, 1, 3);

        label_9 = new QLabel(Warehousing);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout->addWidget(label_9, 18, 0, 1, 1);

        label_21 = new QLabel(Warehousing);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        gridLayout->addWidget(label_21, 16, 2, 1, 1);

        submit_btn_2 = new QPushButton(Warehousing);
        submit_btn_2->setObjectName(QString::fromUtf8("submit_btn_2"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        submit_btn_2->setFont(font);

        gridLayout->addWidget(submit_btn_2, 21, 4, 1, 1);

        type_combo_2 = new QComboBox(Warehousing);
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->setObjectName(QString::fromUtf8("type_combo_2"));
        type_combo_2->setEnabled(true);
        QFont font1;
        font1.setFamily(QString::fromUtf8("AcadEref"));
        font1.setPointSize(10);
        type_combo_2->setFont(font1);

        gridLayout->addWidget(type_combo_2, 9, 0, 1, 2);

        ID_2 = new QLineEdit(Warehousing);
        ID_2->setObjectName(QString::fromUtf8("ID_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ID_2->sizePolicy().hasHeightForWidth());
        ID_2->setSizePolicy(sizePolicy);
        ID_2->setFont(font1);
        ID_2->setReadOnly(true);

        gridLayout->addWidget(ID_2, 2, 3, 1, 2);

        confirm_checkBox_3 = new QCheckBox(Warehousing);
        confirm_checkBox_3->setObjectName(QString::fromUtf8("confirm_checkBox_3"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("AcadEref"));
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        confirm_checkBox_3->setFont(font2);

        gridLayout->addWidget(confirm_checkBox_3, 6, 3, 1, 1);

        type_combo = new QComboBox(Warehousing);
        type_combo->addItem(QString());
        type_combo->addItem(QString());
        type_combo->addItem(QString());
        type_combo->addItem(QString());
        type_combo->addItem(QString());
        type_combo->addItem(QString());
        type_combo->setObjectName(QString::fromUtf8("type_combo"));
        type_combo->setEnabled(true);
        type_combo->setFont(font1);

        gridLayout->addWidget(type_combo, 2, 0, 1, 2);

        tableWidget = new QTableWidget(Warehousing);
        if (tableWidget->columnCount() < 9)
            tableWidget->setColumnCount(9);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        if (tableWidget->rowCount() < 7)
            tableWidget->setRowCount(7);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(0, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(1, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(2, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(3, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(4, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(5, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(6, __qtablewidgetitem15);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        tableWidget->setCornerButtonEnabled(true);
        tableWidget->horizontalHeader()->setCascadingSectionResizes(false);
        tableWidget->horizontalHeader()->setMinimumSectionSize(110);
        tableWidget->horizontalHeader()->setDefaultSectionSize(110);
        tableWidget->horizontalHeader()->setHighlightSections(true);
        tableWidget->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->horizontalHeader()->setStretchLastSection(false);

        gridLayout->addWidget(tableWidget, 11, 0, 1, 6);

        confirm_checkBox_6 = new QCheckBox(Warehousing);
        confirm_checkBox_6->setObjectName(QString::fromUtf8("confirm_checkBox_6"));
        confirm_checkBox_6->setFont(font2);

        gridLayout->addWidget(confirm_checkBox_6, 20, 0, 1, 1);

        label_6 = new QLabel(Warehousing);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 0, 0, 1, 1);

        label_19 = new QLabel(Warehousing);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout->addWidget(label_19, 13, 0, 1, 1);

        label_24 = new QLabel(Warehousing);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        gridLayout->addWidget(label_24, 19, 2, 1, 1);

        label_11 = new QLabel(Warehousing);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 1, 3, 1, 2);

        label_17 = new QLabel(Warehousing);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout->addWidget(label_17, 8, 0, 1, 2);

        label_18 = new QLabel(Warehousing);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout->addWidget(label_18, 10, 0, 1, 2);

        label_15 = new QLabel(Warehousing);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout->addWidget(label_15, 5, 0, 1, 1);

        add_btn = new QPushButton(Warehousing);
        add_btn->setObjectName(QString::fromUtf8("add_btn"));

        gridLayout->addWidget(add_btn, 12, 0, 1, 1);

        datetime_2 = new DateTimeEdit(Warehousing);
        datetime_2->setObjectName(QString::fromUtf8("datetime_2"));
        datetime_2->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(datetime_2, 20, 2, 1, 1);

        ID_3 = new QLineEdit(Warehousing);
        ID_3->setObjectName(QString::fromUtf8("ID_3"));
        sizePolicy.setHeightForWidth(ID_3->sizePolicy().hasHeightForWidth());
        ID_3->setSizePolicy(sizePolicy);
        ID_3->setFont(font1);
        ID_3->setReadOnly(true);

        gridLayout->addWidget(ID_3, 2, 5, 1, 1);

        ID_5 = new QLineEdit(Warehousing);
        ID_5->setObjectName(QString::fromUtf8("ID_5"));
        sizePolicy.setHeightForWidth(ID_5->sizePolicy().hasHeightForWidth());
        ID_5->setSizePolicy(sizePolicy);
        ID_5->setFont(font1);
        ID_5->setReadOnly(true);

        gridLayout->addWidget(ID_5, 4, 3, 1, 2);

        confirm_checkBox_4 = new QCheckBox(Warehousing);
        confirm_checkBox_4->setObjectName(QString::fromUtf8("confirm_checkBox_4"));
        confirm_checkBox_4->setFont(font2);

        gridLayout->addWidget(confirm_checkBox_4, 6, 4, 1, 1);

        label_13 = new QLabel(Warehousing);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout->addWidget(label_13, 3, 0, 1, 3);

        delete_btn = new QPushButton(Warehousing);
        delete_btn->setObjectName(QString::fromUtf8("delete_btn"));

        gridLayout->addWidget(delete_btn, 12, 2, 1, 1);

        ID_6 = new QLineEdit(Warehousing);
        ID_6->setObjectName(QString::fromUtf8("ID_6"));
        sizePolicy.setHeightForWidth(ID_6->sizePolicy().hasHeightForWidth());
        ID_6->setSizePolicy(sizePolicy);
        ID_6->setFont(font1);
        ID_6->setReadOnly(true);

        gridLayout->addWidget(ID_6, 14, 0, 1, 2);

        label_7 = new QLabel(Warehousing);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 7, 0, 1, 2);

        confirm_checkBox_5 = new QCheckBox(Warehousing);
        confirm_checkBox_5->setObjectName(QString::fromUtf8("confirm_checkBox_5"));
        confirm_checkBox_5->setFont(font2);

        gridLayout->addWidget(confirm_checkBox_5, 17, 0, 1, 1);

        ID_4 = new QLineEdit(Warehousing);
        ID_4->setObjectName(QString::fromUtf8("ID_4"));
        sizePolicy.setHeightForWidth(ID_4->sizePolicy().hasHeightForWidth());
        ID_4->setSizePolicy(sizePolicy);
        ID_4->setFont(font1);
        ID_4->setReadOnly(true);

        gridLayout->addWidget(ID_4, 4, 0, 1, 2);

        select_member_2 = new QPushButton(Warehousing);
        select_member_2->setObjectName(QString::fromUtf8("select_member_2"));
        select_member_2->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(select_member_2, 20, 3, 1, 1);

        label_8 = new QLabel(Warehousing);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 15, 0, 1, 1);

        confirm_checkBox = new QCheckBox(Warehousing);
        confirm_checkBox->setObjectName(QString::fromUtf8("confirm_checkBox"));
        confirm_checkBox->setFont(font2);

        gridLayout->addWidget(confirm_checkBox, 6, 0, 1, 1);

        label_12 = new QLabel(Warehousing);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 1, 5, 1, 1);

        label_16 = new QLabel(Warehousing);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 5, 3, 1, 2);

        confirm_checkBox_2 = new QCheckBox(Warehousing);
        confirm_checkBox_2->setObjectName(QString::fromUtf8("confirm_checkBox_2"));
        confirm_checkBox_2->setFont(font2);

        gridLayout->addWidget(confirm_checkBox_2, 6, 1, 1, 1);

        label_14 = new QLabel(Warehousing);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout->addWidget(label_14, 3, 3, 1, 2);

        label_22 = new QLabel(Warehousing);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        gridLayout->addWidget(label_22, 16, 3, 1, 1);

        label_23 = new QLabel(Warehousing);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        gridLayout->addWidget(label_23, 19, 0, 1, 1);

        label_20 = new QLabel(Warehousing);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        gridLayout->addWidget(label_20, 16, 0, 1, 1);

        datetime_1 = new DateTimeEdit(Warehousing);
        datetime_1->setObjectName(QString::fromUtf8("datetime_1"));
        datetime_1->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(datetime_1, 17, 2, 1, 1);

        label_25 = new QLabel(Warehousing);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        gridLayout->addWidget(label_25, 19, 3, 1, 1);

        update_btn = new QPushButton(Warehousing);
        update_btn->setObjectName(QString::fromUtf8("update_btn"));
        update_btn->setFont(font);

        gridLayout->addWidget(update_btn, 4, 5, 1, 1);


        horizontalLayout->addLayout(gridLayout);


        retranslateUi(Warehousing);

        type_combo_2->setCurrentIndex(0);
        type_combo->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(Warehousing);
    } // setupUi

    void retranslateUi(QWidget *Warehousing)
    {
        Warehousing->setWindowTitle(QCoreApplication::translate("Warehousing", "Form", nullptr));
        select_member->setText(QCoreApplication::translate("Warehousing", "\351\200\211\346\213\251\346\210\220\345\221\230", nullptr));
        select_member->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "SelectMember", nullptr)));
        label_10->setText(QCoreApplication::translate("Warehousing", "\351\200\211\346\213\251\347\224\237\344\272\247\345\267\245\345\215\225", nullptr));
        label_10->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_9->setText(QCoreApplication::translate("Warehousing", "\345\205\245\345\272\223\347\241\256\350\256\244", nullptr));
        label_9->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title1", nullptr)));
        label_21->setText(QCoreApplication::translate("Warehousing", "\350\264\250\346\243\200\346\227\266\351\227\264", nullptr));
        label_21->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        submit_btn_2->setText(QCoreApplication::translate("Warehousing", "\346\217\220\344\272\244", nullptr));
        submit_btn_2->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "upload", nullptr)));
        type_combo_2->setItemText(0, QCoreApplication::translate("Warehousing", "\345\216\237\346\226\231\345\272\223", nullptr));
        type_combo_2->setItemText(1, QCoreApplication::translate("Warehousing", "\346\210\220\345\223\201\345\272\223", nullptr));

        type_combo_2->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "comboBox", nullptr)));
        ID_2->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "ID", nullptr)));
        confirm_checkBox_3->setText(QCoreApplication::translate("Warehousing", "\345\267\262\345\256\214\347\273\223", nullptr));
        confirm_checkBox_3->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "important", nullptr)));
        type_combo->setItemText(0, QCoreApplication::translate("Warehousing", "\347\224\237\344\272\247\346\210\220\345\223\201\345\205\245\345\272\223", nullptr));
        type_combo->setItemText(1, QCoreApplication::translate("Warehousing", "\351\224\200\345\224\256\351\200\200\350\264\247\345\205\245\345\272\223", nullptr));
        type_combo->setItemText(2, QCoreApplication::translate("Warehousing", "\351\224\200\345\224\256\346\215\242\350\264\247\345\205\245\345\272\223", nullptr));
        type_combo->setItemText(3, QCoreApplication::translate("Warehousing", "\350\260\203\346\213\250\345\205\245\345\272\223", nullptr));
        type_combo->setItemText(4, QCoreApplication::translate("Warehousing", "\346\234\237\345\210\235\345\205\245\345\272\223", nullptr));
        type_combo->setItemText(5, QCoreApplication::translate("Warehousing", "\345\205\266\344\273\226", nullptr));

        type_combo->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "comboBox", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("Warehousing", "\345\272\217\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("Warehousing", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("Warehousing", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("Warehousing", "\345\267\245\345\215\225\346\264\276\345\267\245\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("Warehousing", "\345\267\262\345\205\245\345\272\223\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("Warehousing", "\346\234\254\346\254\241\345\205\245\345\272\223\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("Warehousing", "\345\215\225\344\275\215", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("Warehousing", "\344\272\247\345\223\201\346\211\271\346\254\241\345\217\267", nullptr));
        confirm_checkBox_6->setText(QCoreApplication::translate("Warehousing", "\347\241\256\350\256\244", nullptr));
        confirm_checkBox_6->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "important", nullptr)));
        label_6->setText(QCoreApplication::translate("Warehousing", "\347\224\237\344\272\247\345\267\245\345\215\225\344\277\241\346\201\257", nullptr));
        label_6->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title1", nullptr)));
        label_19->setText(QCoreApplication::translate("Warehousing", "\345\205\245\345\272\223\344\272\247\345\223\201\346\200\273\346\225\260", nullptr));
        label_19->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_24->setText(QCoreApplication::translate("Warehousing", "\345\205\245\345\272\223\346\227\266\351\227\264", nullptr));
        label_24->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_11->setText(QCoreApplication::translate("Warehousing", "\347\224\237\344\272\247\345\267\245\345\215\225\345\220\215\347\247\260", nullptr));
        label_11->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_17->setText(QCoreApplication::translate("Warehousing", "\345\205\245\345\272\223\344\273\223\345\272\223", nullptr));
        label_17->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_18->setText(QCoreApplication::translate("Warehousing", "\344\272\247\345\223\201\346\230\216\347\273\206", nullptr));
        label_18->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_15->setText(QCoreApplication::translate("Warehousing", "\345\267\245\345\215\225\347\212\266\346\200\201", nullptr));
        label_15->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        add_btn->setText(QCoreApplication::translate("Warehousing", "\346\267\273\345\212\240", nullptr));
        add_btn->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "addData", nullptr)));
        ID_3->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "ID", nullptr)));
        ID_5->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "ID", nullptr)));
        confirm_checkBox_4->setText(QCoreApplication::translate("Warehousing", "\345\267\262\350\256\241\345\210\222", nullptr));
        confirm_checkBox_4->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "important", nullptr)));
        label_13->setText(QCoreApplication::translate("Warehousing", "\344\272\247\345\223\201\346\211\271\346\254\241\345\217\267", nullptr));
        label_13->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        delete_btn->setText(QCoreApplication::translate("Warehousing", "\345\210\240\351\231\244", nullptr));
        delete_btn->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "deleteData", nullptr)));
        ID_6->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "ID", nullptr)));
        label_7->setText(QCoreApplication::translate("Warehousing", "\345\205\245\345\272\223\346\230\216\347\273\206", nullptr));
        label_7->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title1", nullptr)));
        confirm_checkBox_5->setText(QCoreApplication::translate("Warehousing", "\347\241\256\350\256\244", nullptr));
        confirm_checkBox_5->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "important", nullptr)));
        ID_4->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "ID", nullptr)));
        select_member_2->setText(QCoreApplication::translate("Warehousing", "\351\200\211\346\213\251\346\210\220\345\221\230", nullptr));
        select_member_2->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "SelectMember", nullptr)));
        label_8->setText(QCoreApplication::translate("Warehousing", "\350\264\250\346\243\200\347\241\256\350\256\244", nullptr));
        label_8->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title1", nullptr)));
        confirm_checkBox->setText(QCoreApplication::translate("Warehousing", "\345\267\262\345\256\214\347\273\223", nullptr));
        confirm_checkBox->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "important", nullptr)));
        label_12->setText(QCoreApplication::translate("Warehousing", "\347\224\237\344\272\247\350\256\241\345\210\222\345\220\215\347\247\260", nullptr));
        label_12->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_16->setText(QCoreApplication::translate("Warehousing", "\350\256\241\345\210\222\347\212\266\346\200\201", nullptr));
        label_16->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        confirm_checkBox_2->setText(QCoreApplication::translate("Warehousing", "\345\267\262\346\264\276\345\267\245", nullptr));
        confirm_checkBox_2->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "important", nullptr)));
        label_14->setText(QCoreApplication::translate("Warehousing", "\347\224\237\344\272\247\345\205\245\345\272\223\345\215\225\347\274\226\345\217\267", nullptr));
        label_14->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_22->setText(QCoreApplication::translate("Warehousing", "\350\264\250\346\243\200\345\221\230", nullptr));
        label_22->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_23->setText(QCoreApplication::translate("Warehousing", "\345\220\210\346\240\274\345\223\201\345\205\245\345\272\223\347\241\256\350\256\244", nullptr));
        label_23->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_20->setText(QCoreApplication::translate("Warehousing", "\350\264\250\346\243\200\345\256\214\346\210\220\347\241\256\350\256\244", nullptr));
        label_20->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        label_25->setText(QCoreApplication::translate("Warehousing", "\345\205\245\345\272\223\345\221\230", nullptr));
        label_25->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "title2", nullptr)));
        update_btn->setText(QCoreApplication::translate("Warehousing", "\346\233\264\346\226\260", nullptr));
        update_btn->setProperty("type", QVariant(QCoreApplication::translate("Warehousing", "upload", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class Warehousing: public Ui_Warehousing {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WAREHOUSING_H
