/********************************************************************************
** Form generated from reading UI file 'barchart.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BARCHART_H
#define UI_BARCHART_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BarChart
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *wdgChart;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *chart;

    void setupUi(QWidget *BarChart)
    {
        if (BarChart->objectName().isEmpty())
            BarChart->setObjectName(QString::fromUtf8("BarChart"));
        BarChart->resize(400, 300);
        verticalLayout = new QVBoxLayout(BarChart);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        wdgChart = new QWidget(BarChart);
        wdgChart->setObjectName(QString::fromUtf8("wdgChart"));
        horizontalLayout = new QHBoxLayout(wdgChart);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        chart = new QVBoxLayout();
        chart->setObjectName(QString::fromUtf8("chart"));

        horizontalLayout->addLayout(chart);


        verticalLayout->addWidget(wdgChart);


        retranslateUi(BarChart);

        QMetaObject::connectSlotsByName(BarChart);
    } // setupUi

    void retranslateUi(QWidget *BarChart)
    {
        BarChart->setWindowTitle(QCoreApplication::translate("BarChart", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class BarChart: public Ui_BarChart {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BARCHART_H
