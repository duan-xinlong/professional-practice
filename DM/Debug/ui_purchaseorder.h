/********************************************************************************
** Form generated from reading UI file 'purchaseorder.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PURCHASEORDER_H
#define UI_PURCHASEORDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Tool/date_time_edit.h"

QT_BEGIN_NAMESPACE

class Ui_PurchaseOrder
{
public:
    QGridLayout *gridLayout_3;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout;
    QVBoxLayout *right;
    QLabel *label_2;
    QTextEdit *textEdit;
    QHBoxLayout *shita;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *left;
    QLabel *label;
    QComboBox *comboBox1;
    QLabel *label_11;
    QComboBox *comboBox2;
    QLabel *label_15;
    QLineEdit *lineEdit;
    QLabel *label_16;
    QComboBox *comboBox3;
    QLabel *label_28;
    DateTimeEdit *datetime;
    QWidget *tab_2;
    QGridLayout *gridLayout_2;
    QVBoxLayout *l_layout;
    QVBoxLayout *left_2;
    QLabel *label_3;
    QComboBox *comboBox21;
    QLabel *label_12;
    QComboBox *comboBox22;
    QLabel *label_17;
    QLineEdit *lineEdit_2;
    QLabel *label_18;
    QComboBox *comboBox23;
    QLabel *label_29;
    QComboBox *datetime_box;
    QLabel *label_5;
    QComboBox *comboBox24;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_2;
    QVBoxLayout *r_layout;
    QTableWidget *tableWidget;
    QHBoxLayout *shita_button;
    QPushButton *pushButton_4;
    QPushButton *pushButton_3;
    QPushButton *pushButton_5;
    QWidget *tab_3;
    QGridLayout *gridLayout_4;
    QHBoxLayout *ue_layout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_13;
    QLabel *label_14;
    QLabel *label_19;
    QHBoxLayout *horizontalLayout_3;
    QComboBox *comboBox31;
    QComboBox *comboBox32;
    QComboBox *comboBox33;
    QPushButton *pushButton_7;
    QVBoxLayout *shita_layout;
    QTableWidget *tableWidget_2;
    QHBoxLayout *shita_button_2;
    QPushButton *pushButton_9;

    void setupUi(QWidget *PurchaseOrder)
    {
        if (PurchaseOrder->objectName().isEmpty())
            PurchaseOrder->setObjectName(QString::fromUtf8("PurchaseOrder"));
        PurchaseOrder->resize(800, 570);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PurchaseOrder->sizePolicy().hasHeightForWidth());
        PurchaseOrder->setSizePolicy(sizePolicy);
        gridLayout_3 = new QGridLayout(PurchaseOrder);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        tabWidget = new QTabWidget(PurchaseOrder);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout = new QGridLayout(tab);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        right = new QVBoxLayout();
        right->setObjectName(QString::fromUtf8("right"));
        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setEnabled(true);

        right->addWidget(label_2);

        textEdit = new QTextEdit(tab);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        right->addWidget(textEdit);

        right->setStretch(0, 1);
        right->setStretch(1, 15);

        gridLayout->addLayout(right, 0, 1, 1, 1);

        shita = new QHBoxLayout();
        shita->setObjectName(QString::fromUtf8("shita"));
        horizontalSpacer = new QSpacerItem(178, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        shita->addItem(horizontalSpacer);

        pushButton = new QPushButton(tab);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        shita->addWidget(pushButton);

        horizontalSpacer_2 = new QSpacerItem(438, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        shita->addItem(horizontalSpacer_2);


        gridLayout->addLayout(shita, 1, 0, 1, 2);

        left = new QVBoxLayout();
        left->setSpacing(0);
        left->setObjectName(QString::fromUtf8("left"));
        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));
        label->setTextFormat(Qt::AutoText);

        left->addWidget(label);

        comboBox1 = new QComboBox(tab);
        comboBox1->setObjectName(QString::fromUtf8("comboBox1"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(comboBox1->sizePolicy().hasHeightForWidth());
        comboBox1->setSizePolicy(sizePolicy1);

        left->addWidget(comboBox1);

        label_11 = new QLabel(tab);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        left->addWidget(label_11);

        comboBox2 = new QComboBox(tab);
        comboBox2->setObjectName(QString::fromUtf8("comboBox2"));

        left->addWidget(comboBox2);

        label_15 = new QLabel(tab);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        left->addWidget(label_15);

        lineEdit = new QLineEdit(tab);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        left->addWidget(lineEdit);

        label_16 = new QLabel(tab);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        left->addWidget(label_16);

        comboBox3 = new QComboBox(tab);
        comboBox3->setObjectName(QString::fromUtf8("comboBox3"));

        left->addWidget(comboBox3);

        label_28 = new QLabel(tab);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        left->addWidget(label_28);

        datetime = new DateTimeEdit(tab);
        datetime->setObjectName(QString::fromUtf8("datetime"));
        datetime->setMinimumSize(QSize(0, 0));

        left->addWidget(datetime);


        gridLayout->addLayout(left, 0, 0, 1, 1);

        gridLayout->setColumnStretch(0, 1);
        gridLayout->setColumnStretch(1, 2);
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_2 = new QGridLayout(tab_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        l_layout = new QVBoxLayout();
        l_layout->setObjectName(QString::fromUtf8("l_layout"));
        left_2 = new QVBoxLayout();
        left_2->setSpacing(0);
        left_2->setObjectName(QString::fromUtf8("left_2"));
        label_3 = new QLabel(tab_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setTextFormat(Qt::AutoText);

        left_2->addWidget(label_3);

        comboBox21 = new QComboBox(tab_2);
        comboBox21->setObjectName(QString::fromUtf8("comboBox21"));
        sizePolicy1.setHeightForWidth(comboBox21->sizePolicy().hasHeightForWidth());
        comboBox21->setSizePolicy(sizePolicy1);

        left_2->addWidget(comboBox21);

        label_12 = new QLabel(tab_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        left_2->addWidget(label_12);

        comboBox22 = new QComboBox(tab_2);
        comboBox22->setObjectName(QString::fromUtf8("comboBox22"));

        left_2->addWidget(comboBox22);

        label_17 = new QLabel(tab_2);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        left_2->addWidget(label_17);

        lineEdit_2 = new QLineEdit(tab_2);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        left_2->addWidget(lineEdit_2);

        label_18 = new QLabel(tab_2);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        left_2->addWidget(label_18);

        comboBox23 = new QComboBox(tab_2);
        comboBox23->setObjectName(QString::fromUtf8("comboBox23"));

        left_2->addWidget(comboBox23);

        label_29 = new QLabel(tab_2);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        left_2->addWidget(label_29);

        datetime_box = new QComboBox(tab_2);
        datetime_box->setObjectName(QString::fromUtf8("datetime_box"));
        datetime_box->setMinimumSize(QSize(0, 0));

        left_2->addWidget(datetime_box);

        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        left_2->addWidget(label_5);

        comboBox24 = new QComboBox(tab_2);
        comboBox24->setObjectName(QString::fromUtf8("comboBox24"));

        left_2->addWidget(comboBox24);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        left_2->addItem(verticalSpacer);

        left_2->setStretch(0, 1);
        left_2->setStretch(1, 1);
        left_2->setStretch(2, 1);
        left_2->setStretch(3, 1);
        left_2->setStretch(4, 1);
        left_2->setStretch(5, 1);
        left_2->setStretch(6, 1);
        left_2->setStretch(7, 1);
        left_2->setStretch(8, 1);
        left_2->setStretch(9, 1);
        left_2->setStretch(10, 1);
        left_2->setStretch(12, 1);

        l_layout->addLayout(left_2);

        pushButton_2 = new QPushButton(tab_2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        l_layout->addWidget(pushButton_2);


        gridLayout_2->addLayout(l_layout, 0, 0, 1, 1);

        r_layout = new QVBoxLayout();
        r_layout->setObjectName(QString::fromUtf8("r_layout"));
        tableWidget = new QTableWidget(tab_2);
        if (tableWidget->columnCount() < 9)
            tableWidget->setColumnCount(9);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        r_layout->addWidget(tableWidget);

        shita_button = new QHBoxLayout();
        shita_button->setObjectName(QString::fromUtf8("shita_button"));
        pushButton_4 = new QPushButton(tab_2);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        shita_button->addWidget(pushButton_4);

        pushButton_3 = new QPushButton(tab_2);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        shita_button->addWidget(pushButton_3);

        pushButton_5 = new QPushButton(tab_2);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        shita_button->addWidget(pushButton_5);


        r_layout->addLayout(shita_button);


        gridLayout_2->addLayout(r_layout, 0, 1, 1, 1);

        gridLayout_2->setColumnStretch(0, 1);
        gridLayout_2->setColumnStretch(1, 3);
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        gridLayout_4 = new QGridLayout(tab_3);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        ue_layout = new QHBoxLayout();
        ue_layout->setObjectName(QString::fromUtf8("ue_layout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_13 = new QLabel(tab_3);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout->addWidget(label_13);

        label_14 = new QLabel(tab_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout->addWidget(label_14);

        label_19 = new QLabel(tab_3);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        horizontalLayout->addWidget(label_19);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 1);
        horizontalLayout->setStretch(2, 1);

        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        comboBox31 = new QComboBox(tab_3);
        comboBox31->setObjectName(QString::fromUtf8("comboBox31"));

        horizontalLayout_3->addWidget(comboBox31);

        comboBox32 = new QComboBox(tab_3);
        comboBox32->setObjectName(QString::fromUtf8("comboBox32"));

        horizontalLayout_3->addWidget(comboBox32);

        comboBox33 = new QComboBox(tab_3);
        comboBox33->setObjectName(QString::fromUtf8("comboBox33"));

        horizontalLayout_3->addWidget(comboBox33);

        horizontalLayout_3->setStretch(0, 1);
        horizontalLayout_3->setStretch(1, 1);
        horizontalLayout_3->setStretch(2, 1);

        verticalLayout->addLayout(horizontalLayout_3);


        ue_layout->addLayout(verticalLayout);

        pushButton_7 = new QPushButton(tab_3);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));

        ue_layout->addWidget(pushButton_7);


        gridLayout_4->addLayout(ue_layout, 0, 0, 1, 1);

        shita_layout = new QVBoxLayout();
        shita_layout->setObjectName(QString::fromUtf8("shita_layout"));
        tableWidget_2 = new QTableWidget(tab_3);
        if (tableWidget_2->columnCount() < 8)
            tableWidget_2->setColumnCount(8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(0, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(1, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(2, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(3, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(4, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(5, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(6, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(7, __qtablewidgetitem16);
        tableWidget_2->setObjectName(QString::fromUtf8("tableWidget_2"));
        tableWidget_2->horizontalHeader()->setCascadingSectionResizes(false);

        shita_layout->addWidget(tableWidget_2);

        shita_button_2 = new QHBoxLayout();
        shita_button_2->setObjectName(QString::fromUtf8("shita_button_2"));
        pushButton_9 = new QPushButton(tab_3);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));

        shita_button_2->addWidget(pushButton_9);


        shita_layout->addLayout(shita_button_2);


        gridLayout_4->addLayout(shita_layout, 1, 0, 1, 1);

        tabWidget->addTab(tab_3, QString());

        gridLayout_3->addWidget(tabWidget, 0, 0, 1, 1);


        retranslateUi(PurchaseOrder);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(PurchaseOrder);
    } // setupUi

    void retranslateUi(QWidget *PurchaseOrder)
    {
        PurchaseOrder->setWindowTitle(QCoreApplication::translate("PurchaseOrder", "Form", nullptr));
        PurchaseOrder->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "Child", nullptr)));
        tabWidget->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "child", nullptr)));
        label_2->setText(QCoreApplication::translate("PurchaseOrder", "\345\244\207\346\263\250", nullptr));
        label_2->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        pushButton->setText(QCoreApplication::translate("PurchaseOrder", "\346\217\220\344\272\244", nullptr));
        pushButton->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "upload", nullptr)));
        label->setText(QCoreApplication::translate("PurchaseOrder", "\344\276\233\345\272\224\345\225\206\345\220\215\347\247\260", nullptr));
        label->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        comboBox1->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        label_11->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\235\220\346\226\231", nullptr));
        label_11->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        comboBox2->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        label_15->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\225\260\351\207\217", nullptr));
        label_15->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        lineEdit->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "ID", nullptr)));
        label_16->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\345\221\230", nullptr));
        label_16->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        comboBox3->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        label_28->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\227\266\351\227\264", nullptr));
        label_28->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("PurchaseOrder", "\345\210\233\345\273\272\351\207\207\350\264\255\345\215\225", nullptr));
        label_3->setText(QCoreApplication::translate("PurchaseOrder", "\344\276\233\345\272\224\345\225\206\345\220\215\347\247\260", nullptr));
        label_3->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        comboBox21->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        label_12->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\235\220\346\226\231", nullptr));
        label_12->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        comboBox22->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        label_17->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\225\260\351\207\217", nullptr));
        label_17->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        lineEdit_2->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "ID", nullptr)));
        label_18->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\345\221\230", nullptr));
        label_18->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        comboBox23->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        label_29->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\227\266\351\227\264", nullptr));
        label_29->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        datetime_box->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        label_5->setText(QCoreApplication::translate("PurchaseOrder", "\350\257\204\344\273\267", nullptr));
        label_5->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        comboBox24->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        pushButton_2->setText(QCoreApplication::translate("PurchaseOrder", "\346\237\245\350\257\242", nullptr));
        pushButton_2->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "upload", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\227\266\351\227\264", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\235\220\346\226\231", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\345\221\230", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("PurchaseOrder", "\344\276\233\345\272\224\345\225\206", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("PurchaseOrder", "\350\257\246\347\273\206\344\277\241\346\201\257", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("PurchaseOrder", "\350\257\204\344\273\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("PurchaseOrder", "\346\230\257\345\220\246\346\224\266\350\264\247", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("PurchaseOrder", "\346\224\266\350\264\247\346\227\266\351\227\264", nullptr));
        pushButton_4->setText(QCoreApplication::translate("PurchaseOrder", "\345\210\240\351\231\244\350\241\214", nullptr));
        pushButton_4->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "upload", nullptr)));
        pushButton_3->setText(QCoreApplication::translate("PurchaseOrder", "\346\233\264\346\226\260\350\241\214", nullptr));
        pushButton_3->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "upload", nullptr)));
        pushButton_5->setText(QCoreApplication::translate("PurchaseOrder", "\347\241\256\350\256\244\346\224\266\350\264\247", nullptr));
        pushButton_5->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "upload", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("PurchaseOrder", "\346\237\245\350\257\242\344\270\216\344\277\256\346\224\271\351\207\207\350\264\255\345\215\225", nullptr));
        label_13->setText(QCoreApplication::translate("PurchaseOrder", "\344\276\233\345\272\224\345\225\206\345\220\215\347\247\260", nullptr));
        label_13->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        label_14->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\235\220\346\226\231\345\220\215\347\247\260", nullptr));
        label_14->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        label_19->setText(QCoreApplication::translate("PurchaseOrder", "\346\255\244\346\254\241\351\207\207\350\264\255\350\257\204\344\273\267", nullptr));
        label_19->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "title2", nullptr)));
        comboBox31->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        comboBox32->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        comboBox33->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "comboBox", nullptr)));
        pushButton_7->setText(QCoreApplication::translate("PurchaseOrder", "\346\237\245\350\257\242", nullptr));
        pushButton_7->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "upload", nullptr)));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget_2->horizontalHeaderItem(0);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\227\266\351\227\264", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget_2->horizontalHeaderItem(1);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\235\220\346\226\231\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget_2->horizontalHeaderItem(2);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\346\235\220\346\226\231\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem12 = tableWidget_2->horizontalHeaderItem(3);
        ___qtablewidgetitem12->setText(QCoreApplication::translate("PurchaseOrder", "\344\276\233\345\272\224\345\225\206\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem13 = tableWidget_2->horizontalHeaderItem(4);
        ___qtablewidgetitem13->setText(QCoreApplication::translate("PurchaseOrder", "\351\207\207\350\264\255\345\221\230\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem14 = tableWidget_2->horizontalHeaderItem(5);
        ___qtablewidgetitem14->setText(QCoreApplication::translate("PurchaseOrder", "\350\257\246\347\273\206\344\277\241\346\201\257", nullptr));
        QTableWidgetItem *___qtablewidgetitem15 = tableWidget_2->horizontalHeaderItem(6);
        ___qtablewidgetitem15->setText(QCoreApplication::translate("PurchaseOrder", "\346\255\244\346\254\241\351\207\207\350\264\255\350\257\204\344\273\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem16 = tableWidget_2->horizontalHeaderItem(7);
        ___qtablewidgetitem16->setText(QCoreApplication::translate("PurchaseOrder", "\350\257\204\344\273\267\346\227\266\351\227\264", nullptr));
        pushButton_9->setText(QCoreApplication::translate("PurchaseOrder", "\346\233\264\346\226\260\350\257\204\344\273\267", nullptr));
        pushButton_9->setProperty("type", QVariant(QCoreApplication::translate("PurchaseOrder", "upload", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("PurchaseOrder", "\350\257\204\344\273\267\351\207\207\350\264\255\350\256\242\345\215\225", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PurchaseOrder: public Ui_PurchaseOrder {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PURCHASEORDER_H
