/********************************************************************************
** Form generated from reading UI file 'materialdata.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MATERIALDATA_H
#define UI_MATERIALDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_materialdata
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *ue_button;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLabel *label_12;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit_2;
    QComboBox *comboBox_12;
    QPushButton *pushButton_2;
    QVBoxLayout *shita_layout;
    QTableWidget *tableWidget;
    QHBoxLayout *shita_button;
    QPushButton *pushButton;
    QPushButton *pushButton_4;
    QPushButton *pushButton_3;
    QPushButton *pushButton_5;

    void setupUi(QWidget *materialdata)
    {
        if (materialdata->objectName().isEmpty())
            materialdata->setObjectName(QString::fromUtf8("materialdata"));
        materialdata->resize(800, 570);
        verticalLayout_2 = new QVBoxLayout(materialdata);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        ue_button = new QHBoxLayout();
        ue_button->setObjectName(QString::fromUtf8("ue_button"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(materialdata);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setTextFormat(Qt::AutoText);

        horizontalLayout->addWidget(label_3);

        label_12 = new QLabel(materialdata);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout->addWidget(label_12);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 1);

        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lineEdit_2 = new QLineEdit(materialdata);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        horizontalLayout_2->addWidget(lineEdit_2);

        comboBox_12 = new QComboBox(materialdata);
        comboBox_12->setObjectName(QString::fromUtf8("comboBox_12"));

        horizontalLayout_2->addWidget(comboBox_12);

        horizontalLayout_2->setStretch(0, 1);
        horizontalLayout_2->setStretch(1, 1);

        verticalLayout->addLayout(horizontalLayout_2);


        ue_button->addLayout(verticalLayout);

        pushButton_2 = new QPushButton(materialdata);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        ue_button->addWidget(pushButton_2);


        verticalLayout_2->addLayout(ue_button);

        shita_layout = new QVBoxLayout();
        shita_layout->setObjectName(QString::fromUtf8("shita_layout"));
        tableWidget = new QTableWidget(materialdata);
        if (tableWidget->columnCount() < 3)
            tableWidget->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        shita_layout->addWidget(tableWidget);

        shita_button = new QHBoxLayout();
        shita_button->setObjectName(QString::fromUtf8("shita_button"));
        pushButton = new QPushButton(materialdata);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        shita_button->addWidget(pushButton);

        pushButton_4 = new QPushButton(materialdata);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        shita_button->addWidget(pushButton_4);

        pushButton_3 = new QPushButton(materialdata);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        shita_button->addWidget(pushButton_3);

        pushButton_5 = new QPushButton(materialdata);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        shita_button->addWidget(pushButton_5);


        shita_layout->addLayout(shita_button);


        verticalLayout_2->addLayout(shita_layout);


        retranslateUi(materialdata);

        QMetaObject::connectSlotsByName(materialdata);
    } // setupUi

    void retranslateUi(QWidget *materialdata)
    {
        materialdata->setWindowTitle(QCoreApplication::translate("materialdata", "Form", nullptr));
        label_3->setText(QCoreApplication::translate("materialdata", "\351\207\207\350\264\255\346\235\220\346\226\231\347\274\226\345\217\267", nullptr));
        label_3->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "title2", nullptr)));
        label_12->setText(QCoreApplication::translate("materialdata", "\351\207\207\350\264\255\346\235\220\346\226\231\345\220\215\347\247\260", nullptr));
        label_12->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "title2", nullptr)));
        lineEdit_2->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "ID", nullptr)));
        comboBox_12->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "comboBox", nullptr)));
        pushButton_2->setText(QCoreApplication::translate("materialdata", "\346\237\245\350\257\242", nullptr));
        pushButton_2->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "upload", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("materialdata", "\351\207\207\350\264\255\346\235\220\346\226\231\347\274\226\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("materialdata", "\351\207\207\350\264\255\346\235\220\346\226\231\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("materialdata", "\350\257\246\347\273\206\344\277\241\346\201\257", nullptr));
        pushButton->setText(QCoreApplication::translate("materialdata", "\346\267\273\345\212\240\350\241\214", nullptr));
        pushButton->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "upload", nullptr)));
        pushButton_4->setText(QCoreApplication::translate("materialdata", "\345\210\240\351\231\244\350\241\214", nullptr));
        pushButton_4->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "upload", nullptr)));
        pushButton_3->setText(QCoreApplication::translate("materialdata", "\346\233\264\346\226\260\350\241\214", nullptr));
        pushButton_3->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "upload", nullptr)));
        pushButton_5->setText(QCoreApplication::translate("materialdata", "\346\217\222\345\205\245\350\241\214", nullptr));
        pushButton_5->setProperty("type", QVariant(QCoreApplication::translate("materialdata", "upload", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class materialdata: public Ui_materialdata {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MATERIALDATA_H
