/********************************************************************************
** Form generated from reading UI file 'linechart.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LINECHART_H
#define UI_LINECHART_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LineChart
{
public:
    QVBoxLayout *verticalLayout_2;
    QWidget *wdgChart;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *chart;

    void setupUi(QWidget *LineChart)
    {
        if (LineChart->objectName().isEmpty())
            LineChart->setObjectName(QString::fromUtf8("LineChart"));
        LineChart->resize(400, 300);
        verticalLayout_2 = new QVBoxLayout(LineChart);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        wdgChart = new QWidget(LineChart);
        wdgChart->setObjectName(QString::fromUtf8("wdgChart"));
        verticalLayout = new QVBoxLayout(wdgChart);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        chart = new QVBoxLayout();
        chart->setObjectName(QString::fromUtf8("chart"));

        verticalLayout->addLayout(chart);


        verticalLayout_2->addWidget(wdgChart);


        retranslateUi(LineChart);

        QMetaObject::connectSlotsByName(LineChart);
    } // setupUi

    void retranslateUi(QWidget *LineChart)
    {
        LineChart->setWindowTitle(QCoreApplication::translate("LineChart", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LineChart: public Ui_LineChart {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LINECHART_H
