/********************************************************************************
** Form generated from reading UI file 'supplierdata.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SUPPLIERDATA_H
#define UI_SUPPLIERDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_supplierdata
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *ue_layout;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_3;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit_2;
    QComboBox *comboBox_12;
    QLineEdit *lineEdit_3;
    QComboBox *comboBox_14;
    QPushButton *pushButton_2;
    QVBoxLayout *shita_layout;
    QTableWidget *tableWidget;
    QHBoxLayout *shita_button;
    QPushButton *pushButton;
    QPushButton *pushButton_4;
    QPushButton *pushButton_3;
    QPushButton *pushButton_5;

    void setupUi(QWidget *supplierdata)
    {
        if (supplierdata->objectName().isEmpty())
            supplierdata->setObjectName(QString::fromUtf8("supplierdata"));
        supplierdata->resize(800, 570);
        verticalLayout_2 = new QVBoxLayout(supplierdata);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        ue_layout = new QHBoxLayout();
        ue_layout->setObjectName(QString::fromUtf8("ue_layout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_3 = new QLabel(supplierdata);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setTextFormat(Qt::AutoText);

        horizontalLayout->addWidget(label_3);

        label_12 = new QLabel(supplierdata);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout->addWidget(label_12);

        label_13 = new QLabel(supplierdata);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout->addWidget(label_13);

        label_14 = new QLabel(supplierdata);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout->addWidget(label_14);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 1);
        horizontalLayout->setStretch(2, 1);
        horizontalLayout->setStretch(3, 1);

        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lineEdit_2 = new QLineEdit(supplierdata);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        horizontalLayout_2->addWidget(lineEdit_2);

        comboBox_12 = new QComboBox(supplierdata);
        comboBox_12->setObjectName(QString::fromUtf8("comboBox_12"));

        horizontalLayout_2->addWidget(comboBox_12);

        lineEdit_3 = new QLineEdit(supplierdata);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));

        horizontalLayout_2->addWidget(lineEdit_3);

        comboBox_14 = new QComboBox(supplierdata);
        comboBox_14->setObjectName(QString::fromUtf8("comboBox_14"));

        horizontalLayout_2->addWidget(comboBox_14);

        horizontalLayout_2->setStretch(0, 1);
        horizontalLayout_2->setStretch(1, 1);
        horizontalLayout_2->setStretch(2, 1);
        horizontalLayout_2->setStretch(3, 1);

        verticalLayout->addLayout(horizontalLayout_2);


        ue_layout->addLayout(verticalLayout);

        pushButton_2 = new QPushButton(supplierdata);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        ue_layout->addWidget(pushButton_2);


        verticalLayout_2->addLayout(ue_layout);

        shita_layout = new QVBoxLayout();
        shita_layout->setObjectName(QString::fromUtf8("shita_layout"));
        tableWidget = new QTableWidget(supplierdata);
        if (tableWidget->columnCount() < 5)
            tableWidget->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->horizontalHeader()->setCascadingSectionResizes(false);

        shita_layout->addWidget(tableWidget);

        shita_button = new QHBoxLayout();
        shita_button->setObjectName(QString::fromUtf8("shita_button"));
        pushButton = new QPushButton(supplierdata);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        shita_button->addWidget(pushButton);

        pushButton_4 = new QPushButton(supplierdata);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        shita_button->addWidget(pushButton_4);

        pushButton_3 = new QPushButton(supplierdata);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        shita_button->addWidget(pushButton_3);

        pushButton_5 = new QPushButton(supplierdata);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));

        shita_button->addWidget(pushButton_5);


        shita_layout->addLayout(shita_button);


        verticalLayout_2->addLayout(shita_layout);


        retranslateUi(supplierdata);

        QMetaObject::connectSlotsByName(supplierdata);
    } // setupUi

    void retranslateUi(QWidget *supplierdata)
    {
        supplierdata->setWindowTitle(QCoreApplication::translate("supplierdata", "Form", nullptr));
        label_3->setText(QCoreApplication::translate("supplierdata", "\344\276\233\345\272\224\345\225\206\347\274\226\345\217\267", nullptr));
        label_3->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "title2", nullptr)));
        label_12->setText(QCoreApplication::translate("supplierdata", "\344\276\233\345\272\224\345\225\206\345\220\215\347\247\260", nullptr));
        label_12->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "title2", nullptr)));
        label_13->setText(QCoreApplication::translate("supplierdata", "\344\276\233\345\272\224\345\225\206\344\270\273\350\220\245\344\270\232\345\212\241", nullptr));
        label_13->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "title2", nullptr)));
        label_14->setText(QCoreApplication::translate("supplierdata", "\345\220\210\344\275\234\345\205\263\347\263\273\350\257\204\344\274\260", nullptr));
        label_14->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "title2", nullptr)));
        lineEdit_2->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "ID", nullptr)));
        comboBox_12->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "comboBox", nullptr)));
        lineEdit_3->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "ID", nullptr)));
        comboBox_14->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "comboBox", nullptr)));
        pushButton_2->setText(QCoreApplication::translate("supplierdata", "\346\237\245\350\257\242", nullptr));
        pushButton_2->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "upload", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("supplierdata", "\344\276\233\345\272\224\345\225\206\347\274\226\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("supplierdata", "\344\276\233\345\272\224\345\225\206\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("supplierdata", "\344\276\233\345\272\224\345\225\206\344\270\273\350\220\245\344\270\232\345\212\241", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("supplierdata", "\345\220\210\344\275\234\345\205\263\347\263\273\350\257\204\344\274\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("supplierdata", "\350\257\246\347\273\206\344\277\241\346\201\257", nullptr));
        pushButton->setText(QCoreApplication::translate("supplierdata", "\346\267\273\345\212\240\350\241\214", nullptr));
        pushButton->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "upload", nullptr)));
        pushButton_4->setText(QCoreApplication::translate("supplierdata", "\345\210\240\351\231\244\350\241\214", nullptr));
        pushButton_4->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "upload", nullptr)));
        pushButton_3->setText(QCoreApplication::translate("supplierdata", "\346\233\264\346\226\260\350\241\214", nullptr));
        pushButton_3->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "upload", nullptr)));
        pushButton_5->setText(QCoreApplication::translate("supplierdata", "\346\217\222\345\205\245\350\241\214", nullptr));
        pushButton_5->setProperty("type", QVariant(QCoreApplication::translate("supplierdata", "upload", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class supplierdata: public Ui_supplierdata {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SUPPLIERDATA_H
