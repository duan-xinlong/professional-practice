/********************************************************************************
** Form generated from reading UI file 'salesstatistics.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SALESSTATISTICS_H
#define UI_SALESSTATISTICS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Sale/barchart.h"

QT_BEGIN_NAMESPACE

class Ui_salesstatistics
{
public:
    QVBoxLayout *verticalLayout_22;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_65;
    QWidget *widget_9;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_6;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout_6;
    QSpacerItem *verticalSpacer_9;
    QLabel *label_2;
    QLineEdit *lineEdit_3;
    QSpacerItem *verticalSpacer;
    QWidget *widget_4;
    QVBoxLayout *verticalLayout_7;
    QSpacerItem *verticalSpacer_11;
    QLabel *label_3;
    QLineEdit *lineEdit_4;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_10;
    QLabel *label;
    QLineEdit *lineEdit_2;
    QSpacerItem *verticalSpacer_2;
    QWidget *widget_5;
    QVBoxLayout *verticalLayout_9;
    QSpacerItem *verticalSpacer_13;
    QLabel *label_5;
    QLineEdit *lineEdit_9;
    QSpacerItem *verticalSpacer_8;
    BarChart *wdgVer;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_5;
    QTableView *tableView;
    QWidget *widget1;
    QVBoxLayout *verticalLayout_23;
    QWidget *widget_10;
    QVBoxLayout *verticalLayout_13;
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout_7;
    QWidget *widget_8;
    QVBoxLayout *verticalLayout_14;
    QSpacerItem *verticalSpacer_39;
    QLabel *label_7;
    QLineEdit *lineEdit_5;
    QSpacerItem *verticalSpacer_4;
    QWidget *widget_11;
    QVBoxLayout *verticalLayout_15;
    QSpacerItem *verticalSpacer_40;
    QLabel *label_8;
    QLineEdit *lineEdit_6;
    QSpacerItem *verticalSpacer_5;
    QWidget *widget_12;
    QVBoxLayout *verticalLayout_16;
    QSpacerItem *verticalSpacer_41;
    QLabel *label_9;
    QLineEdit *lineEdit_7;
    QSpacerItem *verticalSpacer_6;
    BarChart *wdgVer_2;
    QHBoxLayout *horizontalLayout_13;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_17;
    QTableWidget *tableWidget_2;
    QWidget *widget_48;
    QVBoxLayout *verticalLayout_18;
    QWidget *widget_13;
    QVBoxLayout *verticalLayout_19;
    QLabel *label_10;
    QComboBox *comboBox_2;
    QSpacerItem *verticalSpacer_42;
    QWidget *widget_14;
    QVBoxLayout *verticalLayout_20;
    QLabel *label_11;
    QComboBox *comboBox_4;
    QSpacerItem *verticalSpacer_43;
    QWidget *widget_15;
    QVBoxLayout *verticalLayout_21;
    QLabel *label_12;
    QComboBox *comboBox_6;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_64;
    QWidget *widget_33;
    QVBoxLayout *verticalLayout_54;
    QHBoxLayout *horizontalLayout_19;
    QHBoxLayout *horizontalLayout_20;
    QWidget *widget_41;
    QVBoxLayout *verticalLayout_55;
    QSpacerItem *verticalSpacer_31;
    QLabel *label_31;
    QLineEdit *lineEdit_17;
    QSpacerItem *verticalSpacer_32;
    QWidget *widget_42;
    QVBoxLayout *verticalLayout_56;
    QSpacerItem *verticalSpacer_33;
    QLabel *label_32;
    QLineEdit *lineEdit_18;
    QSpacerItem *verticalSpacer_34;
    QWidget *widget_43;
    QVBoxLayout *verticalLayout_57;
    QSpacerItem *verticalSpacer_35;
    QLabel *label_33;
    QLineEdit *lineEdit_19;
    QSpacerItem *verticalSpacer_36;
    BarChart *wdgVer_6;
    QHBoxLayout *horizontalLayout_21;
    QVBoxLayout *verticalLayout_58;
    QGroupBox *groupBox_7;
    QVBoxLayout *verticalLayout_59;
    QTableWidget *tableWidget_6;
    QWidget *widget_44;
    QVBoxLayout *verticalLayout_60;
    QWidget *widget_45;
    QVBoxLayout *verticalLayout_61;
    QLabel *label_34;
    QComboBox *comboBox_16;
    QSpacerItem *verticalSpacer_37;
    QWidget *widget_46;
    QVBoxLayout *verticalLayout_62;
    QLabel *label_35;
    QComboBox *comboBox_17;
    QSpacerItem *verticalSpacer_38;
    QWidget *widget_47;
    QVBoxLayout *verticalLayout_63;
    QLabel *label_36;
    QComboBox *comboBox_18;

    void setupUi(QWidget *salesstatistics)
    {
        if (salesstatistics->objectName().isEmpty())
            salesstatistics->setObjectName(QString::fromUtf8("salesstatistics"));
        salesstatistics->resize(875, 591);
        verticalLayout_22 = new QVBoxLayout(salesstatistics);
        verticalLayout_22->setObjectName(QString::fromUtf8("verticalLayout_22"));
        widget = new QWidget(salesstatistics);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMaximumSize(QSize(10000, 10000));
        widget->setStyleSheet(QString::fromUtf8(""));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabWidget = new QTabWidget(widget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_65 = new QVBoxLayout(tab);
        verticalLayout_65->setObjectName(QString::fromUtf8("verticalLayout_65"));
        widget_9 = new QWidget(tab);
        widget_9->setObjectName(QString::fromUtf8("widget_9"));
        verticalLayout_10 = new QVBoxLayout(widget_9);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        widget_3 = new QWidget(widget_9);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        verticalLayout_6 = new QVBoxLayout(widget_3);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_9);

        label_2 = new QLabel(widget_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_6->addWidget(label_2);

        lineEdit_3 = new QLineEdit(widget_3);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setMinimumSize(QSize(0, 40));

        verticalLayout_6->addWidget(lineEdit_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer);

        verticalLayout_6->setStretch(0, 1);
        verticalLayout_6->setStretch(1, 2);
        verticalLayout_6->setStretch(2, 2);
        verticalLayout_6->setStretch(3, 1);

        horizontalLayout_6->addWidget(widget_3);

        widget_4 = new QWidget(widget_9);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        verticalLayout_7 = new QVBoxLayout(widget_4);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_11);

        label_3 = new QLabel(widget_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_7->addWidget(label_3);

        lineEdit_4 = new QLineEdit(widget_4);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setMinimumSize(QSize(0, 40));

        verticalLayout_7->addWidget(lineEdit_4);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_3);

        verticalLayout_7->setStretch(0, 1);
        verticalLayout_7->setStretch(1, 2);
        verticalLayout_7->setStretch(2, 2);
        verticalLayout_7->setStretch(3, 1);

        horizontalLayout_6->addWidget(widget_4);

        horizontalLayout_6->setStretch(0, 1);
        horizontalLayout_6->setStretch(1, 1);

        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        widget_2 = new QWidget(widget_9);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        verticalLayout_4 = new QVBoxLayout(widget_2);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_10);

        label = new QLabel(widget_2);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_4->addWidget(label);

        lineEdit_2 = new QLineEdit(widget_2);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setMinimumSize(QSize(0, 40));

        verticalLayout_4->addWidget(lineEdit_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_2);

        verticalLayout_4->setStretch(0, 1);
        verticalLayout_4->setStretch(1, 2);
        verticalLayout_4->setStretch(2, 1);
        verticalLayout_4->setStretch(3, 1);

        horizontalLayout_2->addWidget(widget_2);

        widget_5 = new QWidget(widget_9);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        verticalLayout_9 = new QVBoxLayout(widget_5);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalSpacer_13 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_13);

        label_5 = new QLabel(widget_5);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_9->addWidget(label_5);

        lineEdit_9 = new QLineEdit(widget_5);
        lineEdit_9->setObjectName(QString::fromUtf8("lineEdit_9"));
        lineEdit_9->setMinimumSize(QSize(0, 40));

        verticalLayout_9->addWidget(lineEdit_9);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_8);

        verticalLayout_9->setStretch(0, 1);
        verticalLayout_9->setStretch(1, 2);
        verticalLayout_9->setStretch(2, 1);
        verticalLayout_9->setStretch(3, 1);

        horizontalLayout_2->addWidget(widget_5);


        verticalLayout_2->addLayout(horizontalLayout_2);


        horizontalLayout_3->addLayout(verticalLayout_2);

        wdgVer = new BarChart(widget_9);
        wdgVer->setObjectName(QString::fromUtf8("wdgVer"));

        horizontalLayout_3->addWidget(wdgVer);

        horizontalLayout_3->setStretch(0, 1);
        horizontalLayout_3->setStretch(1, 3);

        verticalLayout_10->addLayout(horizontalLayout_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(widget_9);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy1);
        verticalLayout_5 = new QVBoxLayout(groupBox_2);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        tableView = new QTableView(groupBox_2);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        verticalLayout_5->addWidget(tableView);


        verticalLayout->addWidget(groupBox_2);


        horizontalLayout_5->addLayout(verticalLayout);

        horizontalLayout_5->setStretch(0, 5);

        verticalLayout_10->addLayout(horizontalLayout_5);


        verticalLayout_65->addWidget(widget_9);

        tabWidget->addTab(tab, QString());
        widget1 = new QWidget();
        widget1->setObjectName(QString::fromUtf8("widget1"));
        verticalLayout_23 = new QVBoxLayout(widget1);
        verticalLayout_23->setObjectName(QString::fromUtf8("verticalLayout_23"));
        widget_10 = new QWidget(widget1);
        widget_10->setObjectName(QString::fromUtf8("widget_10"));
        verticalLayout_13 = new QVBoxLayout(widget_10);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        widget_8 = new QWidget(widget_10);
        widget_8->setObjectName(QString::fromUtf8("widget_8"));
        verticalLayout_14 = new QVBoxLayout(widget_8);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        verticalSpacer_39 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_14->addItem(verticalSpacer_39);

        label_7 = new QLabel(widget_8);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        verticalLayout_14->addWidget(label_7);

        lineEdit_5 = new QLineEdit(widget_8);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
        lineEdit_5->setMinimumSize(QSize(0, 40));

        verticalLayout_14->addWidget(lineEdit_5);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_14->addItem(verticalSpacer_4);

        verticalLayout_14->setStretch(0, 1);
        verticalLayout_14->setStretch(1, 2);
        verticalLayout_14->setStretch(2, 2);
        verticalLayout_14->setStretch(3, 1);

        horizontalLayout_7->addWidget(widget_8);

        widget_11 = new QWidget(widget_10);
        widget_11->setObjectName(QString::fromUtf8("widget_11"));
        verticalLayout_15 = new QVBoxLayout(widget_11);
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        verticalSpacer_40 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_40);

        label_8 = new QLabel(widget_11);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        verticalLayout_15->addWidget(label_8);

        lineEdit_6 = new QLineEdit(widget_11);
        lineEdit_6->setObjectName(QString::fromUtf8("lineEdit_6"));
        lineEdit_6->setMinimumSize(QSize(0, 40));

        verticalLayout_15->addWidget(lineEdit_6);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_5);

        verticalLayout_15->setStretch(0, 1);
        verticalLayout_15->setStretch(1, 2);
        verticalLayout_15->setStretch(2, 2);
        verticalLayout_15->setStretch(3, 1);

        horizontalLayout_7->addWidget(widget_11);

        widget_12 = new QWidget(widget_10);
        widget_12->setObjectName(QString::fromUtf8("widget_12"));
        verticalLayout_16 = new QVBoxLayout(widget_12);
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        verticalSpacer_41 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_16->addItem(verticalSpacer_41);

        label_9 = new QLabel(widget_12);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        verticalLayout_16->addWidget(label_9);

        lineEdit_7 = new QLineEdit(widget_12);
        lineEdit_7->setObjectName(QString::fromUtf8("lineEdit_7"));
        lineEdit_7->setMinimumSize(QSize(0, 40));

        verticalLayout_16->addWidget(lineEdit_7);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_16->addItem(verticalSpacer_6);

        verticalLayout_16->setStretch(0, 1);
        verticalLayout_16->setStretch(1, 2);
        verticalLayout_16->setStretch(2, 1);
        verticalLayout_16->setStretch(3, 1);

        horizontalLayout_7->addWidget(widget_12);


        horizontalLayout_4->addLayout(horizontalLayout_7);

        wdgVer_2 = new BarChart(widget_10);
        wdgVer_2->setObjectName(QString::fromUtf8("wdgVer_2"));

        horizontalLayout_4->addWidget(wdgVer_2);

        horizontalLayout_4->setStretch(0, 1);
        horizontalLayout_4->setStretch(1, 1);

        verticalLayout_13->addLayout(horizontalLayout_4);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox_3 = new QGroupBox(widget_10);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        sizePolicy1.setHeightForWidth(groupBox_3->sizePolicy().hasHeightForWidth());
        groupBox_3->setSizePolicy(sizePolicy1);
        verticalLayout_17 = new QVBoxLayout(groupBox_3);
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        tableWidget_2 = new QTableWidget(groupBox_3);
        if (tableWidget_2->columnCount() < 16)
            tableWidget_2->setColumnCount(16);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(10, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(11, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(12, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(13, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(14, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(15, __qtablewidgetitem15);
        if (tableWidget_2->rowCount() < 1)
            tableWidget_2->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableWidget_2->setVerticalHeaderItem(0, __qtablewidgetitem16);
        tableWidget_2->setObjectName(QString::fromUtf8("tableWidget_2"));

        verticalLayout_17->addWidget(tableWidget_2);


        verticalLayout_3->addWidget(groupBox_3);


        horizontalLayout_13->addLayout(verticalLayout_3);

        widget_48 = new QWidget(widget_10);
        widget_48->setObjectName(QString::fromUtf8("widget_48"));
        verticalLayout_18 = new QVBoxLayout(widget_48);
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        widget_13 = new QWidget(widget_48);
        widget_13->setObjectName(QString::fromUtf8("widget_13"));
        verticalLayout_19 = new QVBoxLayout(widget_13);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        label_10 = new QLabel(widget_13);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        verticalLayout_19->addWidget(label_10);

        comboBox_2 = new QComboBox(widget_13);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(comboBox_2->sizePolicy().hasHeightForWidth());
        comboBox_2->setSizePolicy(sizePolicy2);

        verticalLayout_19->addWidget(comboBox_2);

        verticalLayout_19->setStretch(0, 1);
        verticalLayout_19->setStretch(1, 2);

        verticalLayout_18->addWidget(widget_13);

        verticalSpacer_42 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_18->addItem(verticalSpacer_42);

        widget_14 = new QWidget(widget_48);
        widget_14->setObjectName(QString::fromUtf8("widget_14"));
        verticalLayout_20 = new QVBoxLayout(widget_14);
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        label_11 = new QLabel(widget_14);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        verticalLayout_20->addWidget(label_11);

        comboBox_4 = new QComboBox(widget_14);
        comboBox_4->setObjectName(QString::fromUtf8("comboBox_4"));
        sizePolicy2.setHeightForWidth(comboBox_4->sizePolicy().hasHeightForWidth());
        comboBox_4->setSizePolicy(sizePolicy2);

        verticalLayout_20->addWidget(comboBox_4);

        verticalLayout_20->setStretch(0, 1);
        verticalLayout_20->setStretch(1, 2);

        verticalLayout_18->addWidget(widget_14);

        verticalSpacer_43 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_18->addItem(verticalSpacer_43);

        widget_15 = new QWidget(widget_48);
        widget_15->setObjectName(QString::fromUtf8("widget_15"));
        verticalLayout_21 = new QVBoxLayout(widget_15);
        verticalLayout_21->setObjectName(QString::fromUtf8("verticalLayout_21"));
        label_12 = new QLabel(widget_15);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        verticalLayout_21->addWidget(label_12);

        comboBox_6 = new QComboBox(widget_15);
        comboBox_6->setObjectName(QString::fromUtf8("comboBox_6"));
        sizePolicy2.setHeightForWidth(comboBox_6->sizePolicy().hasHeightForWidth());
        comboBox_6->setSizePolicy(sizePolicy2);

        verticalLayout_21->addWidget(comboBox_6);

        verticalLayout_21->setStretch(0, 1);
        verticalLayout_21->setStretch(1, 2);

        verticalLayout_18->addWidget(widget_15);


        horizontalLayout_13->addWidget(widget_48);

        horizontalLayout_13->setStretch(0, 5);
        horizontalLayout_13->setStretch(1, 1);

        verticalLayout_13->addLayout(horizontalLayout_13);

        verticalLayout_13->setStretch(0, 1);
        verticalLayout_13->setStretch(1, 2);

        verticalLayout_23->addWidget(widget_10);

        tabWidget->addTab(widget1, QString());
        tabWidget->setTabText(tabWidget->indexOf(widget1), QString::fromUtf8("\351\224\200\345\224\256\346\257\233\345\210\251\345\210\206\346\236\220"));
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_64 = new QVBoxLayout(tab_2);
        verticalLayout_64->setObjectName(QString::fromUtf8("verticalLayout_64"));
        widget_33 = new QWidget(tab_2);
        widget_33->setObjectName(QString::fromUtf8("widget_33"));
        verticalLayout_54 = new QVBoxLayout(widget_33);
        verticalLayout_54->setObjectName(QString::fromUtf8("verticalLayout_54"));
        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setObjectName(QString::fromUtf8("horizontalLayout_20"));
        widget_41 = new QWidget(widget_33);
        widget_41->setObjectName(QString::fromUtf8("widget_41"));
        verticalLayout_55 = new QVBoxLayout(widget_41);
        verticalLayout_55->setObjectName(QString::fromUtf8("verticalLayout_55"));
        verticalSpacer_31 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_55->addItem(verticalSpacer_31);

        label_31 = new QLabel(widget_41);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        verticalLayout_55->addWidget(label_31);

        lineEdit_17 = new QLineEdit(widget_41);
        lineEdit_17->setObjectName(QString::fromUtf8("lineEdit_17"));
        lineEdit_17->setMinimumSize(QSize(0, 40));

        verticalLayout_55->addWidget(lineEdit_17);

        verticalSpacer_32 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_55->addItem(verticalSpacer_32);

        verticalLayout_55->setStretch(0, 1);
        verticalLayout_55->setStretch(1, 2);
        verticalLayout_55->setStretch(2, 2);
        verticalLayout_55->setStretch(3, 1);

        horizontalLayout_20->addWidget(widget_41);

        widget_42 = new QWidget(widget_33);
        widget_42->setObjectName(QString::fromUtf8("widget_42"));
        verticalLayout_56 = new QVBoxLayout(widget_42);
        verticalLayout_56->setObjectName(QString::fromUtf8("verticalLayout_56"));
        verticalSpacer_33 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_56->addItem(verticalSpacer_33);

        label_32 = new QLabel(widget_42);
        label_32->setObjectName(QString::fromUtf8("label_32"));

        verticalLayout_56->addWidget(label_32);

        lineEdit_18 = new QLineEdit(widget_42);
        lineEdit_18->setObjectName(QString::fromUtf8("lineEdit_18"));
        lineEdit_18->setMinimumSize(QSize(0, 40));

        verticalLayout_56->addWidget(lineEdit_18);

        verticalSpacer_34 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_56->addItem(verticalSpacer_34);

        verticalLayout_56->setStretch(0, 1);
        verticalLayout_56->setStretch(1, 2);
        verticalLayout_56->setStretch(2, 2);
        verticalLayout_56->setStretch(3, 1);

        horizontalLayout_20->addWidget(widget_42);

        widget_43 = new QWidget(widget_33);
        widget_43->setObjectName(QString::fromUtf8("widget_43"));
        verticalLayout_57 = new QVBoxLayout(widget_43);
        verticalLayout_57->setObjectName(QString::fromUtf8("verticalLayout_57"));
        verticalSpacer_35 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_57->addItem(verticalSpacer_35);

        label_33 = new QLabel(widget_43);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        verticalLayout_57->addWidget(label_33);

        lineEdit_19 = new QLineEdit(widget_43);
        lineEdit_19->setObjectName(QString::fromUtf8("lineEdit_19"));
        lineEdit_19->setMinimumSize(QSize(0, 40));

        verticalLayout_57->addWidget(lineEdit_19);

        verticalSpacer_36 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_57->addItem(verticalSpacer_36);

        verticalLayout_57->setStretch(0, 1);
        verticalLayout_57->setStretch(1, 2);
        verticalLayout_57->setStretch(2, 1);
        verticalLayout_57->setStretch(3, 1);

        horizontalLayout_20->addWidget(widget_43);


        horizontalLayout_19->addLayout(horizontalLayout_20);

        wdgVer_6 = new BarChart(widget_33);
        wdgVer_6->setObjectName(QString::fromUtf8("wdgVer_6"));

        horizontalLayout_19->addWidget(wdgVer_6);

        horizontalLayout_19->setStretch(0, 1);
        horizontalLayout_19->setStretch(1, 1);

        verticalLayout_54->addLayout(horizontalLayout_19);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QString::fromUtf8("horizontalLayout_21"));
        verticalLayout_58 = new QVBoxLayout();
        verticalLayout_58->setObjectName(QString::fromUtf8("verticalLayout_58"));
        groupBox_7 = new QGroupBox(widget_33);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        sizePolicy1.setHeightForWidth(groupBox_7->sizePolicy().hasHeightForWidth());
        groupBox_7->setSizePolicy(sizePolicy1);
        verticalLayout_59 = new QVBoxLayout(groupBox_7);
        verticalLayout_59->setObjectName(QString::fromUtf8("verticalLayout_59"));
        tableWidget_6 = new QTableWidget(groupBox_7);
        if (tableWidget_6->columnCount() < 16)
            tableWidget_6->setColumnCount(16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(0, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(1, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(2, __qtablewidgetitem19);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(3, __qtablewidgetitem20);
        QTableWidgetItem *__qtablewidgetitem21 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(4, __qtablewidgetitem21);
        QTableWidgetItem *__qtablewidgetitem22 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(5, __qtablewidgetitem22);
        QTableWidgetItem *__qtablewidgetitem23 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(6, __qtablewidgetitem23);
        QTableWidgetItem *__qtablewidgetitem24 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(7, __qtablewidgetitem24);
        QTableWidgetItem *__qtablewidgetitem25 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(8, __qtablewidgetitem25);
        QTableWidgetItem *__qtablewidgetitem26 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(9, __qtablewidgetitem26);
        QTableWidgetItem *__qtablewidgetitem27 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(10, __qtablewidgetitem27);
        QTableWidgetItem *__qtablewidgetitem28 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(11, __qtablewidgetitem28);
        QTableWidgetItem *__qtablewidgetitem29 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(12, __qtablewidgetitem29);
        QTableWidgetItem *__qtablewidgetitem30 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(13, __qtablewidgetitem30);
        QTableWidgetItem *__qtablewidgetitem31 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(14, __qtablewidgetitem31);
        QTableWidgetItem *__qtablewidgetitem32 = new QTableWidgetItem();
        tableWidget_6->setHorizontalHeaderItem(15, __qtablewidgetitem32);
        if (tableWidget_6->rowCount() < 1)
            tableWidget_6->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem33 = new QTableWidgetItem();
        tableWidget_6->setVerticalHeaderItem(0, __qtablewidgetitem33);
        tableWidget_6->setObjectName(QString::fromUtf8("tableWidget_6"));

        verticalLayout_59->addWidget(tableWidget_6);


        verticalLayout_58->addWidget(groupBox_7);


        horizontalLayout_21->addLayout(verticalLayout_58);

        widget_44 = new QWidget(widget_33);
        widget_44->setObjectName(QString::fromUtf8("widget_44"));
        verticalLayout_60 = new QVBoxLayout(widget_44);
        verticalLayout_60->setObjectName(QString::fromUtf8("verticalLayout_60"));
        widget_45 = new QWidget(widget_44);
        widget_45->setObjectName(QString::fromUtf8("widget_45"));
        verticalLayout_61 = new QVBoxLayout(widget_45);
        verticalLayout_61->setObjectName(QString::fromUtf8("verticalLayout_61"));
        label_34 = new QLabel(widget_45);
        label_34->setObjectName(QString::fromUtf8("label_34"));

        verticalLayout_61->addWidget(label_34);

        comboBox_16 = new QComboBox(widget_45);
        comboBox_16->setObjectName(QString::fromUtf8("comboBox_16"));
        sizePolicy2.setHeightForWidth(comboBox_16->sizePolicy().hasHeightForWidth());
        comboBox_16->setSizePolicy(sizePolicy2);

        verticalLayout_61->addWidget(comboBox_16);

        verticalLayout_61->setStretch(0, 1);
        verticalLayout_61->setStretch(1, 2);

        verticalLayout_60->addWidget(widget_45);

        verticalSpacer_37 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_60->addItem(verticalSpacer_37);

        widget_46 = new QWidget(widget_44);
        widget_46->setObjectName(QString::fromUtf8("widget_46"));
        verticalLayout_62 = new QVBoxLayout(widget_46);
        verticalLayout_62->setObjectName(QString::fromUtf8("verticalLayout_62"));
        label_35 = new QLabel(widget_46);
        label_35->setObjectName(QString::fromUtf8("label_35"));

        verticalLayout_62->addWidget(label_35);

        comboBox_17 = new QComboBox(widget_46);
        comboBox_17->setObjectName(QString::fromUtf8("comboBox_17"));
        sizePolicy2.setHeightForWidth(comboBox_17->sizePolicy().hasHeightForWidth());
        comboBox_17->setSizePolicy(sizePolicy2);

        verticalLayout_62->addWidget(comboBox_17);

        verticalLayout_62->setStretch(0, 1);
        verticalLayout_62->setStretch(1, 2);

        verticalLayout_60->addWidget(widget_46);

        verticalSpacer_38 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_60->addItem(verticalSpacer_38);

        widget_47 = new QWidget(widget_44);
        widget_47->setObjectName(QString::fromUtf8("widget_47"));
        verticalLayout_63 = new QVBoxLayout(widget_47);
        verticalLayout_63->setObjectName(QString::fromUtf8("verticalLayout_63"));
        label_36 = new QLabel(widget_47);
        label_36->setObjectName(QString::fromUtf8("label_36"));

        verticalLayout_63->addWidget(label_36);

        comboBox_18 = new QComboBox(widget_47);
        comboBox_18->setObjectName(QString::fromUtf8("comboBox_18"));
        sizePolicy2.setHeightForWidth(comboBox_18->sizePolicy().hasHeightForWidth());
        comboBox_18->setSizePolicy(sizePolicy2);

        verticalLayout_63->addWidget(comboBox_18);

        verticalLayout_63->setStretch(0, 1);
        verticalLayout_63->setStretch(1, 2);

        verticalLayout_60->addWidget(widget_47);


        horizontalLayout_21->addWidget(widget_44);

        horizontalLayout_21->setStretch(0, 5);
        horizontalLayout_21->setStretch(1, 1);

        verticalLayout_54->addLayout(horizontalLayout_21);

        verticalLayout_54->setStretch(0, 1);
        verticalLayout_54->setStretch(1, 2);

        verticalLayout_64->addWidget(widget_33);

        tabWidget->addTab(tab_2, QString());

        horizontalLayout->addWidget(tabWidget);


        verticalLayout_22->addWidget(widget);


        retranslateUi(salesstatistics);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(salesstatistics);
    } // setupUi

    void retranslateUi(QWidget *salesstatistics)
    {
        salesstatistics->setWindowTitle(QCoreApplication::translate("salesstatistics", "Form", nullptr));
        salesstatistics->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "Child", nullptr)));
        widget->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "child", nullptr)));
        label_2->setText(QCoreApplication::translate("salesstatistics", "\347\255\276\345\215\225\345\256\242\346\210\267\346\225\260", nullptr));
        label_2->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        label_3->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\347\255\276\347\275\262\346\225\260\351\207\217", nullptr));
        label_3->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        label->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\346\200\273\351\207\221\351\242\235/\345\205\203", nullptr));
        label->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        label_5->setText(QCoreApplication::translate("salesstatistics", "\344\270\213\346\234\210\351\224\200\345\224\256\351\242\204\346\265\213\351\207\221\351\242\235/\345\205\203", nullptr));
        label_5->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        groupBox_2->setTitle(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\346\230\216\347\273\206", nullptr));
        groupBox_2->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "Group1", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\347\273\237\350\256\241", nullptr));
        label_7->setText(QCoreApplication::translate("salesstatistics", "\347\255\276\345\215\225\345\256\242\346\210\267\346\225\260", nullptr));
        label_7->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        label_8->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\347\255\276\347\275\262\346\225\260\351\207\217", nullptr));
        label_8->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        label_9->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\346\200\273\351\207\221\351\242\235", nullptr));
        label_9->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        groupBox_3->setTitle(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\346\230\216\347\273\206", nullptr));
        groupBox_3->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "Group1", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget_2->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget_2->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\347\261\273\345\210\253", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget_2->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget_2->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\350\201\224\347\263\273\344\272\272\345\247\223\345\220\215", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget_2->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\350\201\224\347\263\273\344\272\272\346\211\213\346\234\272", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget_2->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\345\221\230", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget_2->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("salesstatistics", "\350\256\242\345\215\225\347\255\276\350\256\242\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget_2->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\347\274\226\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget_2->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("salesstatistics", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget_2->horizontalHeaderItem(9);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("salesstatistics", "\344\272\247\345\223\201\345\261\236\346\200\247", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget_2->horizontalHeaderItem(10);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("salesstatistics", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget_2->horizontalHeaderItem(11);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem12 = tableWidget_2->horizontalHeaderItem(12);
        ___qtablewidgetitem12->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\345\215\225\344\273\267/\345\205\203", nullptr));
        QTableWidgetItem *___qtablewidgetitem13 = tableWidget_2->horizontalHeaderItem(13);
        ___qtablewidgetitem13->setText(QCoreApplication::translate("salesstatistics", "\345\224\256\344\273\267\345\220\210\350\256\241/\345\205\203", nullptr));
        QTableWidgetItem *___qtablewidgetitem14 = tableWidget_2->horizontalHeaderItem(14);
        ___qtablewidgetitem14->setText(QCoreApplication::translate("salesstatistics", "\345\207\272\345\272\223\344\273\223\345\272\223", nullptr));
        QTableWidgetItem *___qtablewidgetitem15 = tableWidget_2->horizontalHeaderItem(15);
        ___qtablewidgetitem15->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\350\257\246\347\273\206\345\234\260\345\235\200", nullptr));
        QTableWidgetItem *___qtablewidgetitem16 = tableWidget_2->verticalHeaderItem(0);
        ___qtablewidgetitem16->setText(QCoreApplication::translate("salesstatistics", "1", nullptr));
        label_10->setText(QCoreApplication::translate("salesstatistics", "\350\256\242\345\215\225\347\255\276\350\256\242\346\227\245\346\234\237", nullptr));
        label_10->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title2", nullptr)));
        label_11->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\345\220\215\347\247\260", nullptr));
        label_11->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title2", nullptr)));
        label_12->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\345\221\230", nullptr));
        label_12->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title2", nullptr)));
        label_31->setText(QCoreApplication::translate("salesstatistics", "\347\255\276\345\215\225\345\256\242\346\210\267\346\225\260", nullptr));
        label_31->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        label_32->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\347\255\276\347\275\262\346\225\260\351\207\217", nullptr));
        label_32->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        label_33->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\346\200\273\351\207\221\351\242\235", nullptr));
        label_33->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title1", nullptr)));
        groupBox_7->setTitle(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\346\230\216\347\273\206", nullptr));
        groupBox_7->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "Group1", nullptr)));
        QTableWidgetItem *___qtablewidgetitem17 = tableWidget_6->horizontalHeaderItem(0);
        ___qtablewidgetitem17->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem18 = tableWidget_6->horizontalHeaderItem(1);
        ___qtablewidgetitem18->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\347\261\273\345\210\253", nullptr));
        QTableWidgetItem *___qtablewidgetitem19 = tableWidget_6->horizontalHeaderItem(2);
        ___qtablewidgetitem19->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem20 = tableWidget_6->horizontalHeaderItem(3);
        ___qtablewidgetitem20->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\350\201\224\347\263\273\344\272\272\345\247\223\345\220\215", nullptr));
        QTableWidgetItem *___qtablewidgetitem21 = tableWidget_6->horizontalHeaderItem(4);
        ___qtablewidgetitem21->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\350\201\224\347\263\273\344\272\272\346\211\213\346\234\272", nullptr));
        QTableWidgetItem *___qtablewidgetitem22 = tableWidget_6->horizontalHeaderItem(5);
        ___qtablewidgetitem22->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\345\221\230", nullptr));
        QTableWidgetItem *___qtablewidgetitem23 = tableWidget_6->horizontalHeaderItem(6);
        ___qtablewidgetitem23->setText(QCoreApplication::translate("salesstatistics", "\350\256\242\345\215\225\347\255\276\350\256\242\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem24 = tableWidget_6->horizontalHeaderItem(7);
        ___qtablewidgetitem24->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\350\256\242\345\215\225\347\274\226\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem25 = tableWidget_6->horizontalHeaderItem(8);
        ___qtablewidgetitem25->setText(QCoreApplication::translate("salesstatistics", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem26 = tableWidget_6->horizontalHeaderItem(9);
        ___qtablewidgetitem26->setText(QCoreApplication::translate("salesstatistics", "\344\272\247\345\223\201\345\261\236\346\200\247", nullptr));
        QTableWidgetItem *___qtablewidgetitem27 = tableWidget_6->horizontalHeaderItem(10);
        ___qtablewidgetitem27->setText(QCoreApplication::translate("salesstatistics", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem28 = tableWidget_6->horizontalHeaderItem(11);
        ___qtablewidgetitem28->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem29 = tableWidget_6->horizontalHeaderItem(12);
        ___qtablewidgetitem29->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\345\215\225\344\273\267/\345\205\203", nullptr));
        QTableWidgetItem *___qtablewidgetitem30 = tableWidget_6->horizontalHeaderItem(13);
        ___qtablewidgetitem30->setText(QCoreApplication::translate("salesstatistics", "\345\224\256\344\273\267\345\220\210\350\256\241/\345\205\203", nullptr));
        QTableWidgetItem *___qtablewidgetitem31 = tableWidget_6->horizontalHeaderItem(14);
        ___qtablewidgetitem31->setText(QCoreApplication::translate("salesstatistics", "\345\207\272\345\272\223\344\273\223\345\272\223", nullptr));
        QTableWidgetItem *___qtablewidgetitem32 = tableWidget_6->horizontalHeaderItem(15);
        ___qtablewidgetitem32->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\350\257\246\347\273\206\345\234\260\345\235\200", nullptr));
        QTableWidgetItem *___qtablewidgetitem33 = tableWidget_6->verticalHeaderItem(0);
        ___qtablewidgetitem33->setText(QCoreApplication::translate("salesstatistics", "1", nullptr));
        label_34->setText(QCoreApplication::translate("salesstatistics", "\350\256\242\345\215\225\347\255\276\350\256\242\346\227\245\346\234\237", nullptr));
        label_34->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title2", nullptr)));
        label_35->setText(QCoreApplication::translate("salesstatistics", "\345\256\242\346\210\267\345\220\215\347\247\260", nullptr));
        label_35->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title2", nullptr)));
        label_36->setText(QCoreApplication::translate("salesstatistics", "\351\224\200\345\224\256\345\221\230", nullptr));
        label_36->setProperty("type", QVariant(QCoreApplication::translate("salesstatistics", "title2", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("salesstatistics", "\344\272\247\345\223\201\351\224\200\351\207\217\347\273\237\350\256\241", nullptr));
    } // retranslateUi

};

namespace Ui {
    class salesstatistics: public Ui_salesstatistics {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SALESSTATISTICS_H
