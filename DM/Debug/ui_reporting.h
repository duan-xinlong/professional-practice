/********************************************************************************
** Form generated from reading UI file 'reporting.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REPORTING_H
#define UI_REPORTING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>
#include "Tool/date_time_edit.h"

QT_BEGIN_NAMESPACE

class Ui_Reporting
{
public:
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QLineEdit *ID_4;
    QTableWidget *tableWidget;
    QComboBox *type_combo_2;
    QLineEdit *ID;
    QLabel *label_10;
    QLabel *label_14;
    QLineEdit *ID_3;
    QPushButton *delete_btn;
    QPushButton *add_btn;
    QPushButton *submit_btn;
    QLineEdit *ID_2;
    QLabel *label_12;
    QLabel *label_16;
    QLabel *label_18;
    QLabel *label_13;
    QLabel *label_15;
    QLabel *label_11;
    QLabel *label_19;
    QLabel *label_17;
    QCheckBox *confirm_checkBox;
    DateTimeEdit *datetime;

    void setupUi(QWidget *Reporting)
    {
        if (Reporting->objectName().isEmpty())
            Reporting->setObjectName(QString::fromUtf8("Reporting"));
        Reporting->resize(581, 473);
        horizontalLayout = new QHBoxLayout(Reporting);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        ID_4 = new QLineEdit(Reporting);
        ID_4->setObjectName(QString::fromUtf8("ID_4"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ID_4->sizePolicy().hasHeightForWidth());
        ID_4->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("AcadEref"));
        font.setPointSize(10);
        ID_4->setFont(font);
        ID_4->setReadOnly(true);

        gridLayout->addWidget(ID_4, 4, 2, 1, 2);

        tableWidget = new QTableWidget(Reporting);
        if (tableWidget->columnCount() < 9)
            tableWidget->setColumnCount(9);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        tableWidget->setCornerButtonEnabled(true);
        tableWidget->horizontalHeader()->setCascadingSectionResizes(false);
        tableWidget->horizontalHeader()->setMinimumSectionSize(110);
        tableWidget->horizontalHeader()->setDefaultSectionSize(110);
        tableWidget->horizontalHeader()->setHighlightSections(true);
        tableWidget->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->horizontalHeader()->setStretchLastSection(false);

        gridLayout->addWidget(tableWidget, 7, 0, 1, 4);

        type_combo_2 = new QComboBox(Reporting);
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->setObjectName(QString::fromUtf8("type_combo_2"));
        type_combo_2->setEnabled(true);
        type_combo_2->setFont(font);

        gridLayout->addWidget(type_combo_2, 2, 0, 1, 1);

        ID = new QLineEdit(Reporting);
        ID->setObjectName(QString::fromUtf8("ID"));
        sizePolicy.setHeightForWidth(ID->sizePolicy().hasHeightForWidth());
        ID->setSizePolicy(sizePolicy);
        ID->setFont(font);
        ID->setReadOnly(true);

        gridLayout->addWidget(ID, 2, 1, 1, 1);

        label_10 = new QLabel(Reporting);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 0, 0, 1, 1);

        label_14 = new QLabel(Reporting);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout->addWidget(label_14, 1, 1, 1, 1);

        ID_3 = new QLineEdit(Reporting);
        ID_3->setObjectName(QString::fromUtf8("ID_3"));
        sizePolicy.setHeightForWidth(ID_3->sizePolicy().hasHeightForWidth());
        ID_3->setSizePolicy(sizePolicy);
        ID_3->setFont(font);
        ID_3->setReadOnly(true);

        gridLayout->addWidget(ID_3, 4, 1, 1, 1);

        delete_btn = new QPushButton(Reporting);
        delete_btn->setObjectName(QString::fromUtf8("delete_btn"));

        gridLayout->addWidget(delete_btn, 8, 1, 1, 1);

        add_btn = new QPushButton(Reporting);
        add_btn->setObjectName(QString::fromUtf8("add_btn"));

        gridLayout->addWidget(add_btn, 8, 0, 1, 1);

        submit_btn = new QPushButton(Reporting);
        submit_btn->setObjectName(QString::fromUtf8("submit_btn"));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        submit_btn->setFont(font1);

        gridLayout->addWidget(submit_btn, 10, 2, 1, 1);

        ID_2 = new QLineEdit(Reporting);
        ID_2->setObjectName(QString::fromUtf8("ID_2"));
        sizePolicy.setHeightForWidth(ID_2->sizePolicy().hasHeightForWidth());
        ID_2->setSizePolicy(sizePolicy);
        ID_2->setFont(font);
        ID_2->setReadOnly(true);

        gridLayout->addWidget(ID_2, 2, 2, 1, 2);

        label_12 = new QLabel(Reporting);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 8, 3, 1, 1);

        label_16 = new QLabel(Reporting);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 3, 0, 1, 1);

        label_18 = new QLabel(Reporting);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout->addWidget(label_18, 3, 2, 1, 1);

        label_13 = new QLabel(Reporting);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout->addWidget(label_13, 1, 0, 1, 1);

        label_15 = new QLabel(Reporting);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout->addWidget(label_15, 1, 2, 1, 2);

        label_11 = new QLabel(Reporting);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 5, 0, 1, 1);

        label_19 = new QLabel(Reporting);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout->addWidget(label_19, 6, 0, 1, 1);

        label_17 = new QLabel(Reporting);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout->addWidget(label_17, 3, 1, 1, 1);

        confirm_checkBox = new QCheckBox(Reporting);
        confirm_checkBox->setObjectName(QString::fromUtf8("confirm_checkBox"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("AcadEref"));
        font2.setPointSize(10);
        font2.setBold(true);
        font2.setWeight(75);
        confirm_checkBox->setFont(font2);

        gridLayout->addWidget(confirm_checkBox, 9, 3, 1, 1);

        datetime = new DateTimeEdit(Reporting);
        datetime->setObjectName(QString::fromUtf8("datetime"));
        datetime->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(datetime, 4, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout);


        retranslateUi(Reporting);

        type_combo_2->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(Reporting);
    } // setupUi

    void retranslateUi(QWidget *Reporting)
    {
        Reporting->setWindowTitle(QCoreApplication::translate("Reporting", "Form", nullptr));
        ID_4->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "ID", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem->setText(QCoreApplication::translate("Reporting", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("Reporting", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("Reporting", "\347\224\237\344\272\247\346\264\276\345\267\245\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("Reporting", "\345\267\262\346\264\276\345\267\245\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("Reporting", "\346\234\254\346\254\241\346\264\276\345\267\245\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("Reporting", "\350\264\250\346\243\200\345\221\230", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("Reporting", "\345\267\245\344\275\234\344\270\255\345\277\203id", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("Reporting", "\350\256\241\345\210\222\346\200\273\346\227\266\351\227\264", nullptr));
        type_combo_2->setItemText(0, QCoreApplication::translate("Reporting", "\347\224\237\344\272\247\346\210\220\345\223\201\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(1, QCoreApplication::translate("Reporting", "\351\224\200\345\224\256\351\200\200\350\264\247\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(2, QCoreApplication::translate("Reporting", "\351\224\200\345\224\256\346\215\242\350\264\247\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(3, QCoreApplication::translate("Reporting", "\350\260\203\346\213\250\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(4, QCoreApplication::translate("Reporting", "\346\234\237\345\210\235\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(5, QCoreApplication::translate("Reporting", "\345\205\266\344\273\226", nullptr));

        type_combo_2->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "comboBox", nullptr)));
        ID->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "ID", nullptr)));
        label_10->setText(QCoreApplication::translate("Reporting", "\347\224\237\344\272\247\345\267\245\345\215\225\344\277\241\346\201\257", nullptr));
        label_10->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title1", nullptr)));
        label_14->setText(QCoreApplication::translate("Reporting", "\347\224\237\344\272\247\345\267\245\345\215\225\345\220\215\347\247\260", nullptr));
        label_14->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title2", nullptr)));
        ID_3->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "ID", nullptr)));
        delete_btn->setText(QCoreApplication::translate("Reporting", "\345\210\240\351\231\244", nullptr));
        delete_btn->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "deleteData", nullptr)));
        add_btn->setText(QCoreApplication::translate("Reporting", "\346\267\273\345\212\240", nullptr));
        add_btn->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "addData", nullptr)));
        submit_btn->setText(QCoreApplication::translate("Reporting", "\346\217\220\344\272\244", nullptr));
        submit_btn->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "upload", nullptr)));
        ID_2->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "ID", nullptr)));
        label_12->setText(QCoreApplication::translate("Reporting", "\344\272\247\345\223\201\346\212\245\345\267\245\347\241\256\350\256\244", nullptr));
        label_12->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title2", nullptr)));
        label_16->setText(QCoreApplication::translate("Reporting", "\346\264\276\345\267\245\346\227\266\351\227\264", nullptr));
        label_16->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title2", nullptr)));
        label_18->setText(QCoreApplication::translate("Reporting", "\347\224\237\344\272\247\346\212\245\345\267\245\345\215\225\347\274\226\345\217\267", nullptr));
        label_18->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title2", nullptr)));
        label_13->setText(QCoreApplication::translate("Reporting", "\351\200\211\346\213\251\347\224\237\344\272\247\345\267\245\345\215\225", nullptr));
        label_13->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title2", nullptr)));
        label_15->setText(QCoreApplication::translate("Reporting", "\347\224\237\344\272\247\350\256\241\345\210\222\345\220\215\347\247\260", nullptr));
        label_15->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title2", nullptr)));
        label_11->setText(QCoreApplication::translate("Reporting", "\347\224\237\344\272\247\346\264\276\345\267\245\346\230\216\347\273\206", nullptr));
        label_11->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title1", nullptr)));
        label_19->setText(QCoreApplication::translate("Reporting", "\344\272\247\345\223\201\346\230\216\347\273\206", nullptr));
        label_19->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title2", nullptr)));
        label_17->setText(QCoreApplication::translate("Reporting", "\344\272\247\346\210\220\345\223\201\346\211\271\346\254\241\345\217\267", nullptr));
        label_17->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "title2", nullptr)));
        confirm_checkBox->setText(QCoreApplication::translate("Reporting", "\347\241\256\350\256\244", nullptr));
        confirm_checkBox->setProperty("type", QVariant(QCoreApplication::translate("Reporting", "important", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class Reporting: public Ui_Reporting {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REPORTING_H
