/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *lb_company;
    QSpacerItem *horizontalSpacer;
    QLabel *lb_user;
    QPushButton *btn_exit;
    QTreeWidget *treeWidget;
    QTabWidget *tabWidget;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1031, 734);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setStyleSheet(QString::fromUtf8(""));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        widget = new QWidget(centralwidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMinimumSize(QSize(0, 60));
        widget->setStyleSheet(QString::fromUtf8("QWidget{\n"
"	background-color: #29416c;\n"
"	font: 20px 'Microsoft YaHei';\n"
"	color: rgb(255, 255, 255);\n"
"	/*border-top-left-radius:15px;\n"
"	border-top-right-radius:15px; */\n"
"}\n"
"#widget{\n"
"	border-bottom: 5px solid #1d3861;\n"
"}"));
        horizontalLayout_2 = new QHBoxLayout(widget);
        horizontalLayout_2->setSpacing(15);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, -1, 9);
        lb_company = new QLabel(widget);
        lb_company->setObjectName(QString::fromUtf8("lb_company"));
        lb_company->setStyleSheet(QString::fromUtf8("border:none;"));

        horizontalLayout_2->addWidget(lb_company);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        lb_user = new QLabel(widget);
        lb_user->setObjectName(QString::fromUtf8("lb_user"));
        QFont font;
        font.setFamily(QString::fromUtf8("Microsoft YaHei"));
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        lb_user->setFont(font);
        lb_user->setStyleSheet(QString::fromUtf8("font: 14px 'Microsoft YaHei';"));

        horizontalLayout_2->addWidget(lb_user);

        btn_exit = new QPushButton(widget);
        btn_exit->setObjectName(QString::fromUtf8("btn_exit"));
        btn_exit->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btn_exit->sizePolicy().hasHeightForWidth());
        btn_exit->setSizePolicy(sizePolicy1);
        btn_exit->setMaximumSize(QSize(32, 32));
        btn_exit->setFocusPolicy(Qt::NoFocus);
        btn_exit->setStyleSheet(QString::fromUtf8("QPushButton{background-image: url(:/Resources/Pictures/exit.png);\n"
"	border: 0;\n"
"	}"));
        btn_exit->setCheckable(false);
        btn_exit->setChecked(false);
        btn_exit->setAutoExclusive(false);
        btn_exit->setAutoDefault(false);
        btn_exit->setFlat(true);

        horizontalLayout_2->addWidget(btn_exit);


        gridLayout->addWidget(widget, 0, 0, 1, 2);

        treeWidget = new QTreeWidget(centralwidget);
        treeWidget->headerItem()->setText(0, QString());
        new QTreeWidgetItem(treeWidget);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(__qtreewidgetitem);
        new QTreeWidgetItem(__qtreewidgetitem);
        new QTreeWidgetItem(__qtreewidgetitem);
        new QTreeWidgetItem(__qtreewidgetitem);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem1);
        new QTreeWidgetItem(__qtreewidgetitem1);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        new QTreeWidgetItem(__qtreewidgetitem2);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(__qtreewidgetitem3);
        new QTreeWidgetItem(__qtreewidgetitem3);
        new QTreeWidgetItem(__qtreewidgetitem3);
        new QTreeWidgetItem(__qtreewidgetitem3);
        new QTreeWidgetItem(__qtreewidgetitem3);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(treeWidget);
        new QTreeWidgetItem(__qtreewidgetitem4);
        new QTreeWidgetItem(__qtreewidgetitem4);
        new QTreeWidgetItem(__qtreewidgetitem4);
        new QTreeWidgetItem(__qtreewidgetitem4);
        new QTreeWidgetItem(__qtreewidgetitem4);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(treeWidget->sizePolicy().hasHeightForWidth());
        treeWidget->setSizePolicy(sizePolicy2);
        treeWidget->setMinimumSize(QSize(250, 0));
        treeWidget->setMaximumSize(QSize(250, 16777215));
        treeWidget->setAutoFillBackground(false);
        treeWidget->setStyleSheet(QString::fromUtf8("QTreeWidget{\n"
"	background-color: #29416c;\n"
"	font: 20px 'Microsoft YaHei';\n"
"	color: rgb(255, 255, 255);\n"
"	outline:0px;\n"
"	border-bottom-left-radius:15px;\n"
"}\n"
"QTreeView {\n"
"    border: none;\n"
"    show-decoration-selected: 1;\n"
"}\n"
"QTreeView::item {\n"
"    outline: none;\n"
"    min-height: 48px;\n"
"    padding-left: 6px;\n"
"}\n"
"QTreeView::item:hover {\n"
"	outline: none;\n"
"    background: #1d3861;\n"
"	/*rgb(69, 187, 217);*/\n"
"}\n"
"QTreeView::item:selected {\n"
"	/*outline: none;\n"
"    color: #4F8BFB;*/\n"
"	border: none;\n"
"	outline: none;\n"
"	color:#4f9de8;\n"
"    background: #1d3861;\n"
"    border-right: 4px solid #4f9de8;\n"
"}\n"
"QTreeView::branch::selected {\n"
"	color:#4f9de8;\n"
"    background: #1d3861;\n"
"}\n"
"QTreeView::branch:hover {\n"
"    background: #1d3861;\n"
"}\n"
"\n"
"QTreeView::branch:closed:has-children{\n"
"	image: url(:/Resources/Pictures/icons8-plus-80.png);\n"
"}\n"
"QTreeView::branch:open:has-children{\n"
"    image: url(:/Resources/Pict"
                        "ures/icons8-minus-80.png);\n"
"};\n"
"\n"
"\n"
"\n"
""));
        treeWidget->setFrameShadow(QFrame::Sunken);
        treeWidget->setLineWidth(0);
        treeWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        treeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        treeWidget->setAutoScroll(false);
        treeWidget->setProperty("showDropIndicator", QVariant(false));
        treeWidget->setRootIsDecorated(true);
        treeWidget->setUniformRowHeights(false);
        treeWidget->setItemsExpandable(true);
        treeWidget->setSortingEnabled(false);
        treeWidget->setAnimated(true);
        treeWidget->setAllColumnsShowFocus(false);
        treeWidget->setWordWrap(false);
        treeWidget->setExpandsOnDoubleClick(true);
        treeWidget->header()->setVisible(false);
        treeWidget->header()->setCascadingSectionResizes(true);
        treeWidget->header()->setMinimumSectionSize(5);
        treeWidget->header()->setDefaultSectionSize(100);
        treeWidget->header()->setHighlightSections(true);
        treeWidget->header()->setProperty("showSortIndicator", QVariant(false));

        gridLayout->addWidget(treeWidget, 1, 0, 1, 1);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setStyleSheet(QString::fromUtf8("QTabBar::tab{\n"
"	font: 75 12pt \"Arial\";		#\350\256\276\347\275\256\345\255\227\344\275\223\n"
"	width:84px;					#\350\256\276\347\275\256\345\256\275\345\272\246\n"
"	height:30; 					#\350\256\276\347\275\256\351\253\230\345\272\246\n"
"	margin-top:5px; 			#\350\256\276\347\275\256\350\276\271\350\267\235\n"
"	margin-right:1px;\n"
"	margin-left:1px;\n"
"	margin-bottom:0px;\n"
"}\n"
"\n"
"QTabBar::tab:first:!selected {\n"
"	color:#000000;\n"
"	border-image: url(:/common/images/common/\345\267\246_normal.png);\n"
"}\n"
"\n"
"QTabBar::tab:first:selected {\n"
"	color:#FFFFFF;\n"
"	border-image: url(:/common/images/common/\345\267\246_pressed.png);\n"
"}\n"
"\n"
"QTabBar::tab:last:!selected {\n"
"	color:#000000;\n"
"	border-image: url(:/common/images/common/\345\217\263_normal.png);\n"
"}\n"
"\n"
"QTabBar::tab:last:selected {\n"
"	color:#FFFFFF;\n"
"	border-image: url(:/common/images/common/\345\217\263_pressed.png);\n"
"}\n"
"\n"
"QTabBar::tab:!selected {\n"
"	color:#000000;\n"
"	border-image: url(:/common/ima"
                        "ges/common/\344\270\255_normal.png);\n"
"}\n"
"\n"
"QTabBar::tab:selected {\n"
"	color:#FFFFFF;\n"
"	border-image: url(:/common/images/common/\344\270\255_pressed.png);\n"
"}"));

        gridLayout->addWidget(tabWidget, 1, 1, 1, 1);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        btn_exit->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        lb_company->setText(QCoreApplication::translate("MainWindow", "DManufacturing | ERP\347\263\273\347\273\237", nullptr));
        lb_user->setText(QCoreApplication::translate("MainWindow", "\346\254\242\350\277\216\344\275\240\357\274\214\346\265\213\350\257\225\347\224\250\346\210\267", nullptr));
        btn_exit->setText(QString());

        const bool __sortingEnabled = treeWidget->isSortingEnabled();
        treeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->topLevelItem(0);
        ___qtreewidgetitem->setText(0, QCoreApplication::translate("MainWindow", "\351\246\226\351\241\265", nullptr));
        QTreeWidgetItem *___qtreewidgetitem1 = treeWidget->topLevelItem(1);
        ___qtreewidgetitem1->setText(0, QCoreApplication::translate("MainWindow", "\347\224\237\344\272\247\350\256\241\345\210\222", nullptr));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(0, QCoreApplication::translate("MainWindow", "\347\224\237\344\272\247\345\267\245\345\215\225", nullptr));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem3->setText(0, QCoreApplication::translate("MainWindow", "\351\242\206\346\226\231\351\200\200\346\226\231", nullptr));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem4->setText(0, QCoreApplication::translate("MainWindow", "\347\224\237\344\272\247\345\205\245\345\272\223", nullptr));
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem1->child(3);
        ___qtreewidgetitem5->setText(0, QCoreApplication::translate("MainWindow", "\347\224\237\344\272\247\346\264\276\345\267\245", nullptr));
        QTreeWidgetItem *___qtreewidgetitem6 = treeWidget->topLevelItem(2);
        ___qtreewidgetitem6->setText(0, QCoreApplication::translate("MainWindow", "\351\224\200\345\224\256\347\256\241\347\220\206", nullptr));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem6->child(0);
        ___qtreewidgetitem7->setText(0, QCoreApplication::translate("MainWindow", "\351\224\200\345\224\256\350\256\242\345\215\225", nullptr));
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem6->child(1);
        ___qtreewidgetitem8->setText(0, QCoreApplication::translate("MainWindow", "\351\200\200\350\264\247\350\256\242\345\215\225", nullptr));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem6->child(2);
        ___qtreewidgetitem9->setText(0, QCoreApplication::translate("MainWindow", "\346\215\242\350\264\247\350\256\242\345\215\225", nullptr));
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem6->child(3);
        ___qtreewidgetitem10->setText(0, QCoreApplication::translate("MainWindow", "\350\256\242\345\215\225\347\273\237\350\256\241", nullptr));
        QTreeWidgetItem *___qtreewidgetitem11 = treeWidget->topLevelItem(3);
        ___qtreewidgetitem11->setText(0, QCoreApplication::translate("MainWindow", "\351\207\207\350\264\255\347\256\241\347\220\206", nullptr));
        QTreeWidgetItem *___qtreewidgetitem12 = ___qtreewidgetitem11->child(0);
        ___qtreewidgetitem12->setText(0, QCoreApplication::translate("MainWindow", "\351\207\207\350\264\255\350\256\242\345\215\225", nullptr));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem11->child(1);
        ___qtreewidgetitem13->setText(0, QCoreApplication::translate("MainWindow", "\351\207\207\350\264\255\346\235\220\346\226\231", nullptr));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem11->child(2);
        ___qtreewidgetitem14->setText(0, QCoreApplication::translate("MainWindow", "\351\207\207\350\264\255\345\221\230", nullptr));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem11->child(3);
        ___qtreewidgetitem15->setText(0, QCoreApplication::translate("MainWindow", "\344\276\233\345\272\224\345\225\206", nullptr));
        QTreeWidgetItem *___qtreewidgetitem16 = treeWidget->topLevelItem(4);
        ___qtreewidgetitem16->setText(0, QCoreApplication::translate("MainWindow", "\345\272\223\345\255\230\347\256\241\347\220\206", nullptr));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem16->child(0);
        ___qtreewidgetitem17->setText(0, QCoreApplication::translate("MainWindow", "\345\205\245\345\272\223\345\215\225", nullptr));
        QTreeWidgetItem *___qtreewidgetitem18 = ___qtreewidgetitem16->child(1);
        ___qtreewidgetitem18->setText(0, QCoreApplication::translate("MainWindow", "\345\207\272\345\272\223\345\215\225", nullptr));
        QTreeWidgetItem *___qtreewidgetitem19 = ___qtreewidgetitem16->child(2);
        ___qtreewidgetitem19->setText(0, QCoreApplication::translate("MainWindow", "\347\247\273\345\272\223\350\256\260\345\275\225", nullptr));
        QTreeWidgetItem *___qtreewidgetitem20 = ___qtreewidgetitem16->child(3);
        ___qtreewidgetitem20->setText(0, QCoreApplication::translate("MainWindow", "\345\272\223\345\255\230\347\233\230\347\202\271", nullptr));
        QTreeWidgetItem *___qtreewidgetitem21 = ___qtreewidgetitem16->child(4);
        ___qtreewidgetitem21->setText(0, QCoreApplication::translate("MainWindow", "\344\272\247\345\223\201\347\273\237\350\256\241", nullptr));
        QTreeWidgetItem *___qtreewidgetitem22 = treeWidget->topLevelItem(5);
        ___qtreewidgetitem22->setText(0, QCoreApplication::translate("MainWindow", "\347\263\273\347\273\237\347\256\241\347\220\206", nullptr));
        QTreeWidgetItem *___qtreewidgetitem23 = ___qtreewidgetitem22->child(0);
        ___qtreewidgetitem23->setText(0, QCoreApplication::translate("MainWindow", "\347\263\273\347\273\237\346\227\245\345\277\227", nullptr));
        QTreeWidgetItem *___qtreewidgetitem24 = ___qtreewidgetitem22->child(1);
        ___qtreewidgetitem24->setText(0, QCoreApplication::translate("MainWindow", "\350\247\222\350\211\262\347\256\241\347\220\206", nullptr));
        QTreeWidgetItem *___qtreewidgetitem25 = ___qtreewidgetitem22->child(2);
        ___qtreewidgetitem25->setText(0, QCoreApplication::translate("MainWindow", "\347\224\250\346\210\267\347\256\241\347\220\206", nullptr));
        QTreeWidgetItem *___qtreewidgetitem26 = ___qtreewidgetitem22->child(3);
        ___qtreewidgetitem26->setText(0, QCoreApplication::translate("MainWindow", "\346\234\272\346\236\204\347\256\241\347\220\206", nullptr));
        QTreeWidgetItem *___qtreewidgetitem27 = ___qtreewidgetitem22->child(4);
        ___qtreewidgetitem27->setText(0, QCoreApplication::translate("MainWindow", "\347\263\273\347\273\237\351\205\215\347\275\256", nullptr));
        treeWidget->setSortingEnabled(__sortingEnabled);

    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
