/********************************************************************************
** Form generated from reading UI file 'take_stock.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TAKE_STOCK_H
#define UI_TAKE_STOCK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Tool/date_edit.h"

QT_BEGIN_NAMESPACE

class Ui_TakeStock
{
public:
    QVBoxLayout *verticalLayout_2;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_6;
    QComboBox *type_combo;
    DateEdit *start_date;
    DateEdit *end_date;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_5;
    QLabel *label_6;
    QWidget *widget_5;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *select_member_btn;
    QLineEdit *ID;
    QWidget *widget_4;
    QLabel *label_7;
    QLabel *label_8;
    QHBoxLayout *horizontalLayout_8;
    QComboBox *storage_combo;
    QWidget *widget_2;
    QWidget *widget_3;
    QLabel *label_9;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *add_btn;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *delete_btn;
    QSpacerItem *horizontalSpacer_3;
    QTableWidget *tableWidget;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout_7;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *submit_btn;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *TakeStock)
    {
        if (TakeStock->objectName().isEmpty())
            TakeStock->setObjectName(QString::fromUtf8("TakeStock"));
        TakeStock->resize(860, 685);
        verticalLayout_2 = new QVBoxLayout(TakeStock);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(9, 9, 9, 9);
        widget = new QWidget(TakeStock);
        widget->setObjectName(QString::fromUtf8("widget"));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(30, 20, 30, 30);
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(100);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, 100, -1);
        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_2->addWidget(label_3);

        label_4 = new QLabel(widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_2->addWidget(label_4);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(100);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(-1, -1, 100, -1);
        type_combo = new QComboBox(widget);
        type_combo->addItem(QString());
        type_combo->addItem(QString());
        type_combo->addItem(QString());
        type_combo->setObjectName(QString::fromUtf8("type_combo"));

        horizontalLayout_6->addWidget(type_combo);

        start_date = new DateEdit(widget);
        start_date->setObjectName(QString::fromUtf8("start_date"));

        horizontalLayout_6->addWidget(start_date);

        end_date = new DateEdit(widget);
        end_date->setObjectName(QString::fromUtf8("end_date"));

        horizontalLayout_6->addWidget(end_date);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(100);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, -1, 100, -1);
        label_5 = new QLabel(widget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_3->addWidget(label_5);

        label_6 = new QLabel(widget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_3->addWidget(label_6);

        widget_5 = new QWidget(widget);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));

        horizontalLayout_3->addWidget(widget_5);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(100);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, -1, 100, -1);
        select_member_btn = new QPushButton(widget);
        select_member_btn->setObjectName(QString::fromUtf8("select_member_btn"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(select_member_btn->sizePolicy().hasHeightForWidth());
        select_member_btn->setSizePolicy(sizePolicy);
        select_member_btn->setMinimumSize(QSize(0, 0));

        horizontalLayout_4->addWidget(select_member_btn);

        ID = new QLineEdit(widget);
        ID->setObjectName(QString::fromUtf8("ID"));
        sizePolicy.setHeightForWidth(ID->sizePolicy().hasHeightForWidth());
        ID->setSizePolicy(sizePolicy);
        ID->setReadOnly(true);

        horizontalLayout_4->addWidget(ID);

        widget_4 = new QWidget(widget);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));

        horizontalLayout_4->addWidget(widget_4);

        horizontalLayout_4->setStretch(0, 1);
        horizontalLayout_4->setStretch(1, 1);
        horizontalLayout_4->setStretch(2, 1);

        verticalLayout->addLayout(horizontalLayout_4);

        label_7 = new QLabel(widget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        verticalLayout->addWidget(label_7);

        label_8 = new QLabel(widget);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        verticalLayout->addWidget(label_8);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(100);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(-1, -1, 100, -1);
        storage_combo = new QComboBox(widget);
        storage_combo->setObjectName(QString::fromUtf8("storage_combo"));
        sizePolicy.setHeightForWidth(storage_combo->sizePolicy().hasHeightForWidth());
        storage_combo->setSizePolicy(sizePolicy);

        horizontalLayout_8->addWidget(storage_combo);

        widget_2 = new QWidget(widget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));

        horizontalLayout_8->addWidget(widget_2);

        widget_3 = new QWidget(widget);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));

        horizontalLayout_8->addWidget(widget_3);


        verticalLayout->addLayout(horizontalLayout_8);

        label_9 = new QLabel(widget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        verticalLayout->addWidget(label_9);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        add_btn = new QPushButton(widget);
        add_btn->setObjectName(QString::fromUtf8("add_btn"));

        horizontalLayout_5->addWidget(add_btn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);

        delete_btn = new QPushButton(widget);
        delete_btn->setObjectName(QString::fromUtf8("delete_btn"));

        horizontalLayout_5->addWidget(delete_btn);


        gridLayout->addLayout(horizontalLayout_5, 1, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(468, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 1, 1, 1, 1);

        tableWidget = new QTableWidget(widget);
        if (tableWidget->columnCount() < 12)
            tableWidget->setColumnCount(12);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(10, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(11, __qtablewidgetitem11);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        gridLayout->addWidget(tableWidget, 0, 0, 1, 2);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, -1, 0, -1);
        horizontalSpacer_4 = new QSpacerItem(600, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_4);

        submit_btn = new QPushButton(widget);
        submit_btn->setObjectName(QString::fromUtf8("submit_btn"));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        submit_btn->setFont(font);

        horizontalLayout_7->addWidget(submit_btn);

        horizontalSpacer = new QSpacerItem(170, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout_7);

        verticalLayout->setStretch(0, 1);
        verticalLayout->setStretch(1, 1);
        verticalLayout->setStretch(2, 1);
        verticalLayout->setStretch(3, 1);
        verticalLayout->setStretch(4, 1);
        verticalLayout->setStretch(5, 1);
        verticalLayout->setStretch(6, 1);
        verticalLayout->setStretch(7, 1);
        verticalLayout->setStretch(8, 1);
        verticalLayout->setStretch(9, 10);
        verticalLayout->setStretch(11, 1);

        verticalLayout_2->addWidget(widget);


        retranslateUi(TakeStock);

        type_combo->setCurrentIndex(-1);
        storage_combo->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(TakeStock);
    } // setupUi

    void retranslateUi(QWidget *TakeStock)
    {
        TakeStock->setWindowTitle(QCoreApplication::translate("TakeStock", "Form", nullptr));
        widget->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "child", nullptr)));
        label->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\345\237\272\346\234\254\344\277\241\346\201\257", nullptr));
        label->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title1", nullptr)));
        label_2->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\347\261\273\345\236\213", nullptr));
        label_2->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title2", nullptr)));
        label_3->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\345\274\200\345\247\213\346\227\245\346\234\237", nullptr));
        label_3->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title2", nullptr)));
        label_4->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\347\273\223\346\235\237\346\227\245\346\234\237", nullptr));
        label_4->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title2", nullptr)));
        type_combo->setItemText(0, QCoreApplication::translate("TakeStock", "\346\234\237\345\210\235\347\233\230\347\202\271", nullptr));
        type_combo->setItemText(1, QCoreApplication::translate("TakeStock", "\345\255\230\350\264\247\347\233\230\347\202\271", nullptr));
        type_combo->setItemText(2, QCoreApplication::translate("TakeStock", "\346\234\237\346\234\253\347\233\230\347\202\271", nullptr));

        type_combo->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "comboBox", nullptr)));
        label_5->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\345\221\230", nullptr));
        label_5->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title2", nullptr)));
        label_6->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\345\215\225\347\274\226\345\217\267", nullptr));
        label_6->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title2", nullptr)));
        select_member_btn->setText(QCoreApplication::translate("TakeStock", "\351\200\211\346\213\251\346\210\220\345\221\230", nullptr));
        select_member_btn->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "SelectMember", nullptr)));
        ID->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "ID", nullptr)));
        label_7->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\344\272\247\345\223\201\344\277\241\346\201\257", nullptr));
        label_7->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title1", nullptr)));
        label_8->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\344\273\223\345\272\223", nullptr));
        label_8->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title2", nullptr)));
        storage_combo->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "comboBox", nullptr)));
        label_9->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\344\272\247\345\223\201\346\230\216\347\273\206", nullptr));
        label_9->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "title2", nullptr)));
        add_btn->setText(QCoreApplication::translate("TakeStock", "\346\267\273\345\212\240", nullptr));
        add_btn->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "addData", nullptr)));
#if QT_CONFIG(tooltip)
        delete_btn->setToolTip(QCoreApplication::translate("TakeStock", "<html><head/><body><p>\345\256\277\350\210\215</p></body></html>", nullptr));
#endif // QT_CONFIG(tooltip)
        delete_btn->setText(QCoreApplication::translate("TakeStock", "\345\210\240\351\231\244", nullptr));
        delete_btn->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "deleteData", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("TakeStock", "\345\272\217\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("TakeStock", "\351\200\211\346\213\251\344\272\247\345\223\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("TakeStock", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("TakeStock", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("TakeStock", "\344\272\247\345\223\201\347\261\273\345\236\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("TakeStock", "\351\200\211\346\213\251\346\211\271\346\254\241", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("TakeStock", "\344\272\247\345\223\201\346\211\271\346\254\241", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("TakeStock", "\350\264\246\351\235\242\345\275\223\345\211\215\346\225\260\346\215\256", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\202\271\344\273\223\344\275\215", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->horizontalHeaderItem(9);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("TakeStock", "\346\234\254\346\254\241\347\233\230\347\202\271\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget->horizontalHeaderItem(10);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("TakeStock", "\347\233\230\344\272\217\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget->horizontalHeaderItem(11);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("TakeStock", "\347\233\230\347\233\210\346\225\260\351\207\217", nullptr));
        submit_btn->setText(QCoreApplication::translate("TakeStock", "\346\217\220\344\272\244", nullptr));
        submit_btn->setProperty("type", QVariant(QCoreApplication::translate("TakeStock", "upload", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class TakeStock: public Ui_TakeStock {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TAKE_STOCK_H
