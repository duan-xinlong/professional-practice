/********************************************************************************
** Form generated from reading UI file 'mps.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MPS_H
#define UI_MPS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include "Tool/date_time_edit.h"

QT_BEGIN_NAMESPACE

class Ui_MPS
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QComboBox *comboBox_2;
    QLabel *label_9;
    QLabel *label_4;
    QLabel *label_6;
    QComboBox *comboBox_5;
    QLineEdit *lineEdit;
    QLabel *label_3;
    QComboBox *comboBox;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_2;
    QLabel *label_14;
    QLabel *label_2;
    DateTimeEdit *datetime_2;
    QLabel *label_5;
    QLabel *label_8;
    QLineEdit *lineEdit_5;
    QLabel *label_13;
    DateTimeEdit *datetime;
    QLabel *label_7;
    QLabel *label_10;
    QComboBox *comboBox_6;
    QLabel *label;
    QSplitter *splitter;
    QCheckBox *checkBox;
    QPushButton *pushButton;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MPS)
    {
        if (MPS->objectName().isEmpty())
            MPS->setObjectName(QString::fromUtf8("MPS"));
        MPS->resize(1182, 755);
        QFont font;
        font.setPointSize(12);
        MPS->setFont(font);
        centralwidget = new QWidget(MPS);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        comboBox_2 = new QComboBox(centralwidget);
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        gridLayout->addWidget(comboBox_2, 13, 2, 1, 1);

        label_9 = new QLabel(centralwidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        QFont font1;
        font1.setPointSize(14);
        label_9->setFont(font1);

        gridLayout->addWidget(label_9, 12, 0, 1, 1);

        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font2;
        font2.setPointSize(13);
        label_4->setFont(font2);

        gridLayout->addWidget(label_4, 2, 3, 1, 1);

        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font1);

        gridLayout->addWidget(label_6, 6, 0, 1, 1);

        comboBox_5 = new QComboBox(centralwidget);
        comboBox_5->setObjectName(QString::fromUtf8("comboBox_5"));
        comboBox_5->setFrame(false);

        gridLayout->addWidget(comboBox_5, 17, 0, 1, 1);

        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setReadOnly(true);

        gridLayout->addWidget(lineEdit, 3, 2, 1, 1);

        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font3;
        font3.setPointSize(15);
        label_3->setFont(font3);

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        comboBox = new QComboBox(centralwidget);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));

        gridLayout->addWidget(comboBox, 13, 0, 1, 1);

        lineEdit_4 = new QLineEdit(centralwidget);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));

        gridLayout->addWidget(lineEdit_4, 7, 0, 1, 1);

        lineEdit_2 = new QLineEdit(centralwidget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setStyleSheet(QString::fromUtf8("background-color: #F1F1F1;\n"
"	border: 1px solid #CDC9C9;\n"
"	border-radius:4px;	\n"
"	height: 30px;\n"
"	font: 10pt;\n"
"	color: rgba(0, 0, 0, 0.65);\n"
"	padding-left: 10px;"));

        gridLayout->addWidget(lineEdit_2, 3, 3, 1, 1);

        label_14 = new QLabel(centralwidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font1);

        gridLayout->addWidget(label_14, 15, 2, 1, 1);

        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        gridLayout->addWidget(label_2, 2, 2, 1, 1);

        datetime_2 = new DateTimeEdit(centralwidget);
        datetime_2->setObjectName(QString::fromUtf8("datetime_2"));
        datetime_2->setMinimumSize(QSize(230, 0));

        gridLayout->addWidget(datetime_2, 7, 3, 1, 1);

        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font3);

        gridLayout->addWidget(label_5, 5, 0, 1, 3);

        label_8 = new QLabel(centralwidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font1);

        gridLayout->addWidget(label_8, 6, 3, 1, 1);

        lineEdit_5 = new QLineEdit(centralwidget);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
        lineEdit_5->setStyleSheet(QString::fromUtf8("background-color: #F1F1F1;\n"
"	border: 1px solid #CDC9C9;\n"
"	border-radius:4px;	\n"
"	height: 30px;\n"
"	font: 10pt;\n"
"	color: rgba(0, 0, 0, 0.65);\n"
"	padding-left: 10px;"));
        lineEdit_5->setReadOnly(true);

        gridLayout->addWidget(lineEdit_5, 17, 2, 1, 1);

        label_13 = new QLabel(centralwidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setFont(font1);

        gridLayout->addWidget(label_13, 15, 0, 1, 1);

        datetime = new DateTimeEdit(centralwidget);
        datetime->setObjectName(QString::fromUtf8("datetime"));
        datetime->setMinimumSize(QSize(230, 0));

        gridLayout->addWidget(datetime, 7, 2, 1, 1);

        label_7 = new QLabel(centralwidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font1);

        gridLayout->addWidget(label_7, 6, 2, 1, 1);

        label_10 = new QLabel(centralwidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setFont(font1);

        gridLayout->addWidget(label_10, 12, 2, 1, 1);

        comboBox_6 = new QComboBox(centralwidget);
        comboBox_6->addItem(QString());
        comboBox_6->addItem(QString());
        comboBox_6->setObjectName(QString::fromUtf8("comboBox_6"));

        gridLayout->addWidget(comboBox_6, 3, 0, 1, 1);

        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font1);

        gridLayout->addWidget(label, 2, 0, 1, 2);

        splitter = new QSplitter(centralwidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        checkBox = new QCheckBox(splitter);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setStyleSheet(QString::fromUtf8("color:red"));
        splitter->addWidget(checkBox);
        pushButton = new QPushButton(splitter);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        splitter->addWidget(pushButton);

        gridLayout->addWidget(splitter, 18, 0, 1, 6);


        horizontalLayout->addLayout(gridLayout);

        MPS->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(MPS);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MPS->setStatusBar(statusbar);

        retranslateUi(MPS);

        QMetaObject::connectSlotsByName(MPS);
    } // setupUi

    void retranslateUi(QMainWindow *MPS)
    {
        MPS->setWindowTitle(QCoreApplication::translate("MPS", "MainWindow", nullptr));
        comboBox_2->setItemText(0, QCoreApplication::translate("MPS", "\345\276\205\350\256\241\345\210\222", nullptr));
        comboBox_2->setItemText(1, QCoreApplication::translate("MPS", "\345\276\205\345\256\214\346\210\220", nullptr));
        comboBox_2->setItemText(2, QCoreApplication::translate("MPS", "\345\267\262\345\256\214\346\210\220", nullptr));

        comboBox_2->setProperty("type", QVariant(QCoreApplication::translate("MPS", "comboBox", nullptr)));
        label_9->setText(QCoreApplication::translate("MPS", "\347\224\237\344\272\247\350\256\241\345\210\222\347\247\215\347\261\273", nullptr));
        label_9->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        label_4->setText(QCoreApplication::translate("MPS", "\351\224\200\345\224\256\350\256\242\345\215\225\347\274\226\345\217\267", nullptr));
        label_4->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        label_6->setText(QCoreApplication::translate("MPS", "\347\224\237\344\272\247\350\256\241\345\210\222\345\220\215\347\247\260", nullptr));
        label_6->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        comboBox_5->setProperty("type", QVariant(QCoreApplication::translate("MPS", "comboBox", nullptr)));
        lineEdit->setProperty("type", QVariant(QCoreApplication::translate("MPS", "ID", nullptr)));
        label_3->setText(QCoreApplication::translate("MPS", "\351\224\200\345\224\256\350\256\242\345\215\225\344\277\241\346\201\257", nullptr));
        label_3->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title1", nullptr)));
        comboBox->setItemText(0, QCoreApplication::translate("MPS", "\345\272\223\345\255\230\350\256\241\345\210\222", nullptr));
        comboBox->setItemText(1, QCoreApplication::translate("MPS", "\351\224\200\345\224\256\350\256\242\345\215\225\350\256\241\345\210\222", nullptr));

        comboBox->setProperty("type", QVariant(QCoreApplication::translate("MPS", "comboBox", nullptr)));
        lineEdit_4->setProperty("type", QVariant(QCoreApplication::translate("MPS", "ID", nullptr)));
        lineEdit_2->setProperty("type", QVariant(QCoreApplication::translate("MPS", "ID", nullptr)));
        label_14->setText(QCoreApplication::translate("MPS", "\347\224\237\344\272\247\350\256\241\345\210\222\347\274\226\345\217\267", nullptr));
        label_14->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        label_2->setText(QCoreApplication::translate("MPS", "\351\224\200\345\224\256\345\221\230", nullptr));
        label_2->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        label_5->setText(QCoreApplication::translate("MPS", "\347\224\237\344\272\247\350\256\241\345\210\222\344\277\241\346\201\257", nullptr));
        label_5->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title1", nullptr)));
        label_8->setText(QCoreApplication::translate("MPS", "\350\256\241\345\210\222\347\273\223\346\235\237\346\227\245\346\234\237", nullptr));
        label_8->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        lineEdit_5->setProperty("type", QVariant(QCoreApplication::translate("MPS", "ID", nullptr)));
        label_13->setText(QCoreApplication::translate("MPS", "\347\224\237\344\272\247\350\256\241\345\210\222\350\264\237\350\264\243\344\272\272", nullptr));
        label_13->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        label_7->setText(QCoreApplication::translate("MPS", "\350\256\241\345\210\222\345\274\200\345\247\213\346\227\245\346\234\237", nullptr));
        label_7->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        label_10->setText(QCoreApplication::translate("MPS", "\347\224\237\344\272\247\350\256\241\345\210\222\347\212\266\346\200\201", nullptr));
        label_10->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        comboBox_6->setItemText(0, QCoreApplication::translate("MPS", "1", nullptr));
        comboBox_6->setItemText(1, QCoreApplication::translate("MPS", "2", nullptr));

        comboBox_6->setProperty("type", QVariant(QCoreApplication::translate("MPS", "comboBox", nullptr)));
        label->setText(QCoreApplication::translate("MPS", "\351\200\211\346\213\251\346\225\260\346\215\256", nullptr));
        label->setProperty("type", QVariant(QCoreApplication::translate("MPS", "title2", nullptr)));
        checkBox->setText(QCoreApplication::translate("MPS", "\347\224\237\344\272\247\350\256\241\345\210\222\347\241\256\350\256\244", nullptr));
        pushButton->setText(QCoreApplication::translate("MPS", "\347\224\237\344\272\247\350\256\241\345\210\222\347\241\256\350\256\244", nullptr));
        pushButton->setProperty("type", QVariant(QCoreApplication::translate("MPS", "upload", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class MPS: public Ui_MPS {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MPS_H
