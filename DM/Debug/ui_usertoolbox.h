/********************************************************************************
** Form generated from reading UI file 'usertoolbox.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERTOOLBOX_H
#define UI_USERTOOLBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserToolBox
{
public:
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btn_edit;
    QFrame *line;
    QPushButton *btn_del;
    QFrame *line_3;
    QPushButton *btn_func;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *UserToolBox)
    {
        if (UserToolBox->objectName().isEmpty())
            UserToolBox->setObjectName(QString::fromUtf8("UserToolBox"));
        UserToolBox->resize(255, 61);
        UserToolBox->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"	background: transparent;\n"
"	border: none;\n"
"	color: rgb(0, 91, 172);\n"
"	font: 10pt \"Microsoft YaHei UI\";\n"
"	text-align : center;\n"
"}\n"
"QPushButton:hover{\n"
"	color:red;\n"
"	text-decoration:underline;\n"
"}"));
        horizontalLayout = new QHBoxLayout(UserToolBox);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 6, 0, 6);
        horizontalSpacer = new QSpacerItem(43, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btn_edit = new QPushButton(UserToolBox);
        btn_edit->setObjectName(QString::fromUtf8("btn_edit"));
        btn_edit->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(btn_edit);

        line = new QFrame(UserToolBox);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line);

        btn_del = new QPushButton(UserToolBox);
        btn_del->setObjectName(QString::fromUtf8("btn_del"));
        btn_del->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(btn_del);

        line_3 = new QFrame(UserToolBox);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line_3);

        btn_func = new QPushButton(UserToolBox);
        btn_func->setObjectName(QString::fromUtf8("btn_func"));
        btn_func->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(btn_func);

        horizontalSpacer_2 = new QSpacerItem(42, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        retranslateUi(UserToolBox);

        QMetaObject::connectSlotsByName(UserToolBox);
    } // setupUi

    void retranslateUi(QWidget *UserToolBox)
    {
        UserToolBox->setWindowTitle(QCoreApplication::translate("UserToolBox", "Form", nullptr));
        btn_edit->setText(QCoreApplication::translate("UserToolBox", "\347\274\226\350\276\221", nullptr));
        btn_del->setText(QCoreApplication::translate("UserToolBox", "\345\210\240\351\231\244", nullptr));
        btn_func->setText(QCoreApplication::translate("UserToolBox", "\351\207\215\347\275\256\345\257\206\347\240\201", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UserToolBox: public Ui_UserToolBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERTOOLBOX_H
