/********************************************************************************
** Form generated from reading UI file 'saleschange.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SALESCHANGE_H
#define UI_SALESCHANGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Tool/date_edit.h"

QT_BEGIN_NAMESPACE

class Ui_saleschange
{
public:
    QVBoxLayout *verticalLayout_3;
    QWidget *widget;
    QVBoxLayout *verticalLayout_19;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QPushButton *pushButton_2;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_5;
    QLineEdit *lineEdit;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_10;
    QLineEdit *lineEdit_10;
    QSpacerItem *horizontalSpacer_6;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_6;
    QLineEdit *lineEdit_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_20;
    QLineEdit *lineEdit_8;
    QSpacerItem *horizontalSpacer_7;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_19;
    QLineEdit *lineEdit_7;
    QSpacerItem *horizontalSpacer_8;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_29;
    QLineEdit *lineEdit_11;
    QSpacerItem *horizontalSpacer_9;
    QVBoxLayout *verticalLayout_15;
    QLabel *label_22;
    QLineEdit *lineEdit_12;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_16;
    QLabel *label_28;
    DateEdit *comboBox_2;
    QSpacerItem *horizontalSpacer_10;
    QVBoxLayout *verticalLayout_14;
    QLabel *label_21;
    QLineEdit *lineEdit_9;
    QSpacerItem *horizontalSpacer_11;
    QVBoxLayout *verticalLayout_17;
    QLabel *label_23;
    QLineEdit *lineEdit_13;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_7;
    QLabel *label;
    QComboBox *comboBox_5;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_8;
    QLineEdit *lineEdit_5;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_9;
    QLineEdit *lineEdit_6;
    QTableWidget *tableWidget;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *add_btn;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *delete_btn;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_3;

    void setupUi(QWidget *saleschange)
    {
        if (saleschange->objectName().isEmpty())
            saleschange->setObjectName(QString::fromUtf8("saleschange"));
        saleschange->resize(813, 503);
        verticalLayout_3 = new QVBoxLayout(saleschange);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        widget = new QWidget(saleschange);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMaximumSize(QSize(10000, 10000));
        widget->setStyleSheet(QString::fromUtf8(""));
        verticalLayout_19 = new QVBoxLayout(widget);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        groupBox = new QGroupBox(widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        pushButton_2 = new QPushButton(groupBox);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_2->addWidget(pushButton_2);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_8->addWidget(label_5);

        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        verticalLayout_8->addWidget(lineEdit);


        horizontalLayout->addLayout(verticalLayout_8);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        verticalLayout_10->addWidget(label_10);

        lineEdit_10 = new QLineEdit(groupBox);
        lineEdit_10->setObjectName(QString::fromUtf8("lineEdit_10"));

        verticalLayout_10->addWidget(lineEdit_10);


        horizontalLayout->addLayout(verticalLayout_10);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_6);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_9->addWidget(label_6);

        lineEdit_2 = new QLineEdit(groupBox);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        verticalLayout_9->addWidget(lineEdit_2);


        horizontalLayout->addLayout(verticalLayout_9);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(2, 1);
        horizontalLayout->setStretch(4, 1);
        horizontalLayout->setStretch(6, 1);

        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        label_20 = new QLabel(groupBox);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        verticalLayout_11->addWidget(label_20);

        lineEdit_8 = new QLineEdit(groupBox);
        lineEdit_8->setObjectName(QString::fromUtf8("lineEdit_8"));

        verticalLayout_11->addWidget(lineEdit_8);


        horizontalLayout_2->addLayout(verticalLayout_11);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        label_19 = new QLabel(groupBox);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        verticalLayout_12->addWidget(label_19);

        lineEdit_7 = new QLineEdit(groupBox);
        lineEdit_7->setObjectName(QString::fromUtf8("lineEdit_7"));

        verticalLayout_12->addWidget(lineEdit_7);


        horizontalLayout_2->addLayout(verticalLayout_12);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);

        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        label_29 = new QLabel(groupBox);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        verticalLayout_13->addWidget(label_29);

        lineEdit_11 = new QLineEdit(groupBox);
        lineEdit_11->setObjectName(QString::fromUtf8("lineEdit_11"));

        verticalLayout_13->addWidget(lineEdit_11);


        horizontalLayout_2->addLayout(verticalLayout_13);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_9);

        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        label_22 = new QLabel(groupBox);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        verticalLayout_15->addWidget(label_22);

        lineEdit_12 = new QLineEdit(groupBox);
        lineEdit_12->setObjectName(QString::fromUtf8("lineEdit_12"));

        verticalLayout_15->addWidget(lineEdit_12);


        horizontalLayout_2->addLayout(verticalLayout_15);

        horizontalLayout_2->setStretch(0, 1);
        horizontalLayout_2->setStretch(2, 1);
        horizontalLayout_2->setStretch(4, 1);
        horizontalLayout_2->setStretch(6, 1);

        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        label_28 = new QLabel(groupBox);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        verticalLayout_16->addWidget(label_28);

        comboBox_2 = new DateEdit(groupBox);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        verticalLayout_16->addWidget(comboBox_2);


        horizontalLayout_7->addLayout(verticalLayout_16);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_10);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        label_21 = new QLabel(groupBox);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        verticalLayout_14->addWidget(label_21);

        lineEdit_9 = new QLineEdit(groupBox);
        lineEdit_9->setObjectName(QString::fromUtf8("lineEdit_9"));

        verticalLayout_14->addWidget(lineEdit_9);


        horizontalLayout_7->addLayout(verticalLayout_14);

        horizontalSpacer_11 = new QSpacerItem(90, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_11);

        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        label_23 = new QLabel(groupBox);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        verticalLayout_17->addWidget(label_23);

        lineEdit_13 = new QLineEdit(groupBox);
        lineEdit_13->setObjectName(QString::fromUtf8("lineEdit_13"));

        verticalLayout_17->addWidget(lineEdit_13);


        horizontalLayout_7->addLayout(verticalLayout_17);

        horizontalLayout_7->setStretch(0, 2);
        horizontalLayout_7->setStretch(2, 3);
        horizontalLayout_7->setStretch(4, 3);

        verticalLayout->addLayout(horizontalLayout_7);


        verticalLayout_19->addWidget(groupBox);

        groupBox_2 = new QGroupBox(widget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy1);
        verticalLayout_6 = new QVBoxLayout(groupBox_2);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_7->addWidget(label);

        comboBox_5 = new QComboBox(groupBox_2);
        comboBox_5->setObjectName(QString::fromUtf8("comboBox_5"));

        verticalLayout_7->addWidget(comboBox_5);


        horizontalLayout_3->addLayout(verticalLayout_7);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        verticalLayout_5->addWidget(label_8);

        lineEdit_5 = new QLineEdit(groupBox_2);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));

        verticalLayout_5->addWidget(lineEdit_5);


        horizontalLayout_3->addLayout(verticalLayout_5);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        verticalLayout_4->addWidget(label_9);

        lineEdit_6 = new QLineEdit(groupBox_2);
        lineEdit_6->setObjectName(QString::fromUtf8("lineEdit_6"));

        verticalLayout_4->addWidget(lineEdit_6);


        horizontalLayout_3->addLayout(verticalLayout_4);

        horizontalLayout_3->setStretch(0, 1);
        horizontalLayout_3->setStretch(1, 1);
        horizontalLayout_3->setStretch(2, 1);

        verticalLayout_6->addLayout(horizontalLayout_3);

        tableWidget = new QTableWidget(groupBox_2);
        if (tableWidget->columnCount() < 8)
            tableWidget->setColumnCount(8);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        verticalLayout_6->addWidget(tableWidget);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        add_btn = new QPushButton(groupBox_2);
        add_btn->setObjectName(QString::fromUtf8("add_btn"));

        horizontalLayout_6->addWidget(add_btn);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        delete_btn = new QPushButton(groupBox_2);
        delete_btn->setObjectName(QString::fromUtf8("delete_btn"));

        horizontalLayout_6->addWidget(delete_btn);


        verticalLayout_6->addLayout(horizontalLayout_6);


        verticalLayout_19->addWidget(groupBox_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_4);

        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_5->addWidget(pushButton);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);


        verticalLayout_19->addLayout(horizontalLayout_5);


        verticalLayout_3->addWidget(widget);


        retranslateUi(saleschange);

        QMetaObject::connectSlotsByName(saleschange);
    } // setupUi

    void retranslateUi(QWidget *saleschange)
    {
        saleschange->setWindowTitle(QCoreApplication::translate("saleschange", "Form", nullptr));
        saleschange->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "Child", nullptr)));
        widget->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "child", nullptr)));
        groupBox->setTitle(QCoreApplication::translate("saleschange", "\346\215\242\350\264\247\344\277\241\346\201\257", nullptr));
        groupBox->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "Group1", nullptr)));
        label_2->setText(QCoreApplication::translate("saleschange", "\351\200\211\346\213\251\351\224\200\345\224\256\350\256\242\345\215\225", nullptr));
        label_2->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        pushButton_2->setText(QString());
        pushButton_2->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "databaseBtn2", nullptr)));
        label_5->setText(QCoreApplication::translate("saleschange", "\345\256\242\346\210\267\345\220\215\347\247\260", nullptr));
        label_5->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_10->setText(QCoreApplication::translate("saleschange", "\345\256\242\346\210\267\347\261\273\345\210\253", nullptr));
        label_10->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_10->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_6->setText(QCoreApplication::translate("saleschange", "\345\256\242\346\210\267\347\274\226\347\240\201", nullptr));
        label_6->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_2->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_20->setText(QCoreApplication::translate("saleschange", "\345\256\242\346\210\267\350\201\224\347\263\273\344\272\272\345\247\223\345\220\215", nullptr));
        label_20->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_8->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_19->setText(QCoreApplication::translate("saleschange", "\345\256\242\346\210\267\350\201\224\347\263\273\344\272\272\346\211\213\346\234\272", nullptr));
        label_19->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_7->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_29->setText(QCoreApplication::translate("saleschange", "\351\224\200\345\224\256\345\221\230", nullptr));
        label_29->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_11->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_22->setText(QCoreApplication::translate("saleschange", "\350\256\242\345\215\225\347\255\276\350\256\242\346\227\245\346\234\237", nullptr));
        label_22->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_12->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_28->setText(QCoreApplication::translate("saleschange", "\346\215\242\350\264\247\347\224\263\350\257\267\346\227\245\346\234\237", nullptr));
        label_28->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        comboBox_2->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "datetime", nullptr)));
        label_21->setText(QCoreApplication::translate("saleschange", "\351\224\200\345\224\256\350\256\242\345\215\225\347\274\226\345\217\267", nullptr));
        label_21->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_9->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_23->setText(QCoreApplication::translate("saleschange", "\346\215\242\350\264\247\345\215\225\347\274\226\345\217\267", nullptr));
        label_23->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_13->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        groupBox_2->setTitle(QCoreApplication::translate("saleschange", "\346\215\242\350\264\247\344\272\247\345\223\201\346\230\216\347\273\206", nullptr));
        groupBox_2->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "Group1", nullptr)));
        label->setText(QCoreApplication::translate("saleschange", "\346\215\242\350\264\247\345\216\237\345\233\240", nullptr));
        label->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        comboBox_5->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "comboBox", nullptr)));
        label_8->setText(QCoreApplication::translate("saleschange", "\346\215\242\350\264\247\344\272\247\345\223\201\346\200\273\346\225\260", nullptr));
        label_8->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_5->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        label_9->setText(QCoreApplication::translate("saleschange", "\346\215\242\350\264\247\344\272\247\345\223\201\345\224\256\344\273\267\346\200\273\351\242\235", nullptr));
        label_9->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "title2", nullptr)));
        lineEdit_6->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "ID", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("saleschange", "\351\200\211\346\213\251\351\224\200\345\224\256\350\256\242\345\215\225", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("saleschange", "\346\215\242\350\264\247\345\215\225\350\257\246\347\273\206\347\274\226\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("saleschange", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("saleschange", "\344\272\247\345\223\201\345\261\236\346\200\247", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("saleschange", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("saleschange", "\351\224\200\345\224\256\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("saleschange", "\351\224\200\345\224\256\345\215\225\344\273\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("saleschange", "\345\224\256\344\273\267\345\220\210\350\256\241", nullptr));
        add_btn->setText(QCoreApplication::translate("saleschange", "\346\267\273\345\212\240", nullptr));
        add_btn->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "addData", nullptr)));
        delete_btn->setText(QCoreApplication::translate("saleschange", "\345\210\240\351\231\244", nullptr));
        delete_btn->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "deleteData", nullptr)));
        pushButton->setText(QCoreApplication::translate("saleschange", "\346\217\220\344\272\244", nullptr));
        pushButton->setProperty("type", QVariant(QCoreApplication::translate("saleschange", "upload", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class saleschange: public Ui_saleschange {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SALESCHANGE_H
