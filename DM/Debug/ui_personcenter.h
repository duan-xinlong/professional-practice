/********************************************************************************
** Form generated from reading UI file 'personcenter.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PERSONCENTER_H
#define UI_PERSONCENTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PersonCenter
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *PersonCenter)
    {
        if (PersonCenter->objectName().isEmpty())
            PersonCenter->setObjectName(QString::fromUtf8("PersonCenter"));
        PersonCenter->resize(850, 685);
        verticalLayout = new QVBoxLayout(PersonCenter);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(-1, 200, -1, -1);
        label = new QLabel(PersonCenter);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("background: transparent;\n"
"color: rgb(41, 155, 255);\n"
"font: center;\n"
"font: 60pt \"Segoe Print\";\n"
""));

        verticalLayout->addWidget(label);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(PersonCenter);

        QMetaObject::connectSlotsByName(PersonCenter);
    } // setupUi

    void retranslateUi(QWidget *PersonCenter)
    {
        PersonCenter->setWindowTitle(QCoreApplication::translate("PersonCenter", "Form", nullptr));
        label->setText(QCoreApplication::translate("PersonCenter", "DManufacturing", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PersonCenter: public Ui_PersonCenter {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PERSONCENTER_H
