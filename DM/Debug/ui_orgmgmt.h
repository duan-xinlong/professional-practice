/********************************************************************************
** Form generated from reading UI file 'orgmgmt.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ORGMGMT_H
#define UI_ORGMGMT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OrgMgmt
{
public:
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *btn_add;
    QPushButton *btn_del;
    QPushButton *btn_refresh;
    QTreeWidget *treeWidget;
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer;
    QLabel *label_4;
    QLabel *label_2;
    QLineEdit *le_pos;
    QTextEdit *ed_remark;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btn_save;
    QPushButton *btn_cancel;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_5;
    QLineEdit *le_code;
    QComboBox *cmb_parent;
    QLabel *label_3;
    QLineEdit *le_name;
    QLabel *label_6;
    QFrame *line;
    QLabel *lb_choose;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *OrgMgmt)
    {
        if (OrgMgmt->objectName().isEmpty())
            OrgMgmt->setObjectName(QString::fromUtf8("OrgMgmt"));
        OrgMgmt->resize(860, 685);
        OrgMgmt->setStyleSheet(QString::fromUtf8("*{\n"
"	font: 12pt \"Microsoft YaHei UI\";\n"
"}\n"
"QLineEdit,QDateTimeEdit,QTextEdit{\n"
"	border-radius: 8px;\n"
"	border: 1px solid rgb(0, 0, 0);\n"
"}\n"
"QPushButton{\n"
"	border-radius: 8px;\n"
"	border: 1px solid rgb(0, 0, 0);\n"
"}\n"
"\n"
"QComboBox {\n"
"\342\200\213    border: 2px solid #f3f3f3;/*\350\256\276\347\275\256\347\272\277\345\256\275*/\n"
"\342\200\213	/*background-color: rgb(237, 242, 255);\350\203\214\346\231\257\351\242\234\350\211\262*/\n"
"\342\200\213    border-radius: 8px;/*\345\234\206\350\247\222*/\n"
"\342\200\213    padding: 1px 2px 1px 2px;  /*\351\222\210\345\257\271\344\272\216\347\273\204\345\220\210\346\241\206\344\270\255\347\232\204\346\226\207\346\234\254\345\206\205\345\256\271*/\n"
"\342\200\213	text-align:bottom;\n"
"\342\200\213    min-width: 9em;   /*# \347\273\204\345\220\210\346\241\206\347\232\204\346\234\200\345\260\217\345\256\275\345\272\246*/\n"
"\342\200\213    /*min-height: 5em;*/\n"
"\342\200\213	border-style:solid;/*\350\276\271\346\241\206\344\270\272\345"
                        "\256\236\347\272\277\345\236\213*/\n"
"\342\200\213	border-width:2px;/*\350\276\271\346\241\206\345\256\275\345\272\246*/\n"
"\342\200\213	border-color:black;/*\350\276\271\346\241\206\351\242\234\350\211\262*/\n"
"\342\200\213	padding-left: 10px;/*\345\267\246\344\276\247\350\276\271\350\267\235*/\n"
"}"));
        gridLayout_2 = new QGridLayout(OrgMgmt);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btn_add = new QPushButton(OrgMgmt);
        btn_add->setObjectName(QString::fromUtf8("btn_add"));
        btn_add->setMinimumSize(QSize(90, 40));
        btn_add->setStyleSheet(QString::fromUtf8("\n"
"border-radius: 8px;\n"
"color:white;\n"
"font-weight:bold;\n"
"background:rgb(24, 144, 255);"));

        horizontalLayout->addWidget(btn_add);

        btn_del = new QPushButton(OrgMgmt);
        btn_del->setObjectName(QString::fromUtf8("btn_del"));
        btn_del->setMinimumSize(QSize(90, 40));
        btn_del->setStyleSheet(QString::fromUtf8("border: 2px solid rgb(0, 0, 0);\n"
"border-radius: 8px;\n"
"font-weight:bold;\n"
""));

        horizontalLayout->addWidget(btn_del);

        btn_refresh = new QPushButton(OrgMgmt);
        btn_refresh->setObjectName(QString::fromUtf8("btn_refresh"));
        btn_refresh->setMinimumSize(QSize(90, 40));
        btn_refresh->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(btn_refresh);


        gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 1);

        treeWidget = new QTreeWidget(OrgMgmt);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setStyleSheet(QString::fromUtf8(""));
        treeWidget->header()->setVisible(false);

        gridLayout_2->addWidget(treeWidget, 2, 0, 1, 2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setVerticalSpacing(10);
        gridLayout->setContentsMargins(50, -1, 50, -1);
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 5, 0, 1, 1);

        label_4 = new QLabel(OrgMgmt);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 2, 0, 1, 1);

        label_2 = new QLabel(OrgMgmt);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        le_pos = new QLineEdit(OrgMgmt);
        le_pos->setObjectName(QString::fromUtf8("le_pos"));

        gridLayout->addWidget(le_pos, 3, 1, 1, 1);

        ed_remark = new QTextEdit(OrgMgmt);
        ed_remark->setObjectName(QString::fromUtf8("ed_remark"));

        gridLayout->addWidget(ed_remark, 4, 1, 2, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(10);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        btn_save = new QPushButton(OrgMgmt);
        btn_save->setObjectName(QString::fromUtf8("btn_save"));
        btn_save->setMinimumSize(QSize(90, 40));
        btn_save->setStyleSheet(QString::fromUtf8("\n"
"border-radius: 8px;\n"
"color:white;\n"
"font-weight:bold;\n"
"background:rgb(24, 144, 255);"));

        horizontalLayout_3->addWidget(btn_save);

        btn_cancel = new QPushButton(OrgMgmt);
        btn_cancel->setObjectName(QString::fromUtf8("btn_cancel"));
        btn_cancel->setMinimumSize(QSize(90, 40));
        btn_cancel->setStyleSheet(QString::fromUtf8("border: 2px solid rgb(0, 0, 0);\n"
"border-radius: 8px;\n"
"font-weight:bold;\n"
""));

        horizontalLayout_3->addWidget(btn_cancel);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);


        gridLayout->addLayout(horizontalLayout_3, 6, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 7, 1, 1, 1);

        label_5 = new QLabel(OrgMgmt);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1);

        le_code = new QLineEdit(OrgMgmt);
        le_code->setObjectName(QString::fromUtf8("le_code"));

        gridLayout->addWidget(le_code, 1, 1, 1, 1);

        cmb_parent = new QComboBox(OrgMgmt);
        cmb_parent->setObjectName(QString::fromUtf8("cmb_parent"));
        cmb_parent->setModelColumn(0);

        gridLayout->addWidget(cmb_parent, 2, 1, 1, 1);

        label_3 = new QLabel(OrgMgmt);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        le_name = new QLineEdit(OrgMgmt);
        le_name->setObjectName(QString::fromUtf8("le_name"));

        gridLayout->addWidget(le_name, 0, 1, 1, 1);

        label_6 = new QLabel(OrgMgmt);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 4, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 2, 3, 1, 1);

        line = new QFrame(OrgMgmt);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_2->addWidget(line, 1, 2, 2, 1);

        lb_choose = new QLabel(OrgMgmt);
        lb_choose->setObjectName(QString::fromUtf8("lb_choose"));

        gridLayout_2->addWidget(lb_choose, 1, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 1, 1, 1, 1);


        retranslateUi(OrgMgmt);

        QMetaObject::connectSlotsByName(OrgMgmt);
    } // setupUi

    void retranslateUi(QWidget *OrgMgmt)
    {
        OrgMgmt->setWindowTitle(QCoreApplication::translate("OrgMgmt", "Form", nullptr));
        btn_add->setText(QCoreApplication::translate("OrgMgmt", "\346\267\273\345\212\240\346\234\272\346\236\204", nullptr));
        btn_del->setText(QCoreApplication::translate("OrgMgmt", "\346\211\271\351\207\217\345\210\240\351\231\244", nullptr));
        btn_refresh->setText(QCoreApplication::translate("OrgMgmt", "\345\210\267\346\226\260", nullptr));
        label_4->setText(QCoreApplication::translate("OrgMgmt", "\344\270\212\347\272\247\346\234\272\346\236\204", nullptr));
        label_2->setText(QCoreApplication::translate("OrgMgmt", "\345\220\215\347\247\260*", nullptr));
        btn_save->setText(QCoreApplication::translate("OrgMgmt", "\346\267\273\345\212\240", nullptr));
        btn_cancel->setText(QCoreApplication::translate("OrgMgmt", "\345\217\226\346\266\210", nullptr));
        label_5->setText(QCoreApplication::translate("OrgMgmt", "\346\216\222\345\272\217", nullptr));
        cmb_parent->setCurrentText(QString());
        label_3->setText(QCoreApplication::translate("OrgMgmt", "\347\274\226\345\217\267*", nullptr));
        label_6->setText(QCoreApplication::translate("OrgMgmt", "\345\244\207\346\263\250", nullptr));
        lb_choose->setText(QCoreApplication::translate("OrgMgmt", "\345\275\223\345\211\215\351\200\211\346\213\251:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OrgMgmt: public Ui_OrgMgmt {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ORGMGMT_H
