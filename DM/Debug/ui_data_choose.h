/********************************************************************************
** Form generated from reading UI file 'data_choose.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATA_CHOOSE_H
#define UI_DATA_CHOOSE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DataChoose
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *quit_btn;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit;
    QTableView *tableView;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *btnCancel;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *btnConfirm;

    void setupUi(QWidget *DataChoose)
    {
        if (DataChoose->objectName().isEmpty())
            DataChoose->setObjectName(QString::fromUtf8("DataChoose"));
        DataChoose->resize(500, 500);
        verticalLayout = new QVBoxLayout(DataChoose);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(20, 20, 20, 20);
        widget = new QWidget(DataChoose);
        widget->setObjectName(QString::fromUtf8("widget"));
        verticalLayout_2 = new QVBoxLayout(widget);
        verticalLayout_2->setSpacing(15);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        quit_btn = new QPushButton(widget);
        quit_btn->setObjectName(QString::fromUtf8("quit_btn"));

        horizontalLayout->addWidget(quit_btn);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lineEdit = new QLineEdit(widget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);


        verticalLayout_2->addLayout(horizontalLayout_2);

        tableView = new QTableView(widget);
        tableView->setObjectName(QString::fromUtf8("tableView"));

        verticalLayout_2->addWidget(tableView);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(158, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(10);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        btnCancel = new QPushButton(widget);
        btnCancel->setObjectName(QString::fromUtf8("btnCancel"));
        btnCancel->setMaximumSize(QSize(50, 16777215));
        btnCancel->setProperty("type", QVariant(QString::fromUtf8("cancel")));

        horizontalLayout_6->addWidget(btnCancel);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        btnConfirm = new QPushButton(widget);
        btnConfirm->setObjectName(QString::fromUtf8("btnConfirm"));
        btnConfirm->setMaximumSize(QSize(50, 16777215));
        btnConfirm->setProperty("type", QVariant(QString::fromUtf8("confirm")));

        horizontalLayout_6->addWidget(btnConfirm);


        horizontalLayout_3->addLayout(horizontalLayout_6);


        verticalLayout_2->addLayout(horizontalLayout_3);


        verticalLayout->addWidget(widget);


        retranslateUi(DataChoose);

        QMetaObject::connectSlotsByName(DataChoose);
    } // setupUi

    void retranslateUi(QWidget *DataChoose)
    {
        DataChoose->setWindowTitle(QCoreApplication::translate("DataChoose", "Form", nullptr));
        label->setText(QCoreApplication::translate("DataChoose", "\351\200\211\346\213\251\346\225\260\346\215\256", nullptr));
        label->setProperty("type", QVariant(QCoreApplication::translate("DataChoose", "title2", nullptr)));
        quit_btn->setText(QString());
        quit_btn->setProperty("type", QVariant(QCoreApplication::translate("DataChoose", "quitBtn", nullptr)));
        lineEdit->setProperty("type", QVariant(QCoreApplication::translate("DataChoose", "search", nullptr)));
        btnCancel->setText(QCoreApplication::translate("DataChoose", "\345\217\226\346\266\210", nullptr));
        btnConfirm->setText(QCoreApplication::translate("DataChoose", "\347\241\256\345\256\232", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DataChoose: public Ui_DataChoose {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATA_CHOOSE_H
