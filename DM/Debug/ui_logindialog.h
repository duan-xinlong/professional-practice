/********************************************************************************
** Form generated from reading UI file 'logindialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINDIALOG_H
#define UI_LOGINDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginDialog
{
public:
    QStackedWidget *stackedWidget;
    QWidget *page;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_2;
    QGridLayout *gridLayout;
    QLineEdit *lineEdit_password;
    QLineEdit *lineEdit_username;
    QLabel *label_4;
    QLabel *label_5;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_2;
    QPushButton *btn_signin;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btn_to_signup;
    QLabel *label_image;
    QWidget *page_2;
    QLabel *label_image_2;
    QWidget *centralwidget_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_3;
    QLabel *label_6;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEdit_password_3;
    QLineEdit *lineEdit_username_2;
    QLineEdit *lineEdit_password_2;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QSpacerItem *verticalSpacer;
    QPushButton *btn_signup;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *btn_to_signin;

    void setupUi(QWidget *LoginDialog)
    {
        if (LoginDialog->objectName().isEmpty())
            LoginDialog->setObjectName(QString::fromUtf8("LoginDialog"));
        LoginDialog->resize(830, 520);
        LoginDialog->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"\n"
""));
        stackedWidget = new QStackedWidget(LoginDialog);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 830, 520));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        centralwidget = new QWidget(page);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        centralwidget->setGeometry(QRect(0, 0, 380, 520));
        centralwidget->setStyleSheet(QString::fromUtf8("background:rgba(255,255,255,0.95);"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        label->setStyleSheet(QString::fromUtf8("background: transparent;\n"
"color: rgb(41, 155, 255);\n"
"font: 20pt \"Segoe Print\";"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setStyleSheet(QString::fromUtf8("background: transparent;\n"
"font: 700 20pt \"Microsoft YaHei UI\";\n"
"color: rgb(0, 91, 172)"));

        verticalLayout->addWidget(label_2);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lineEdit_password = new QLineEdit(centralwidget);
        lineEdit_password->setObjectName(QString::fromUtf8("lineEdit_password"));
        lineEdit_password->setMinimumSize(QSize(0, 45));
        lineEdit_password->setStyleSheet(QString::fromUtf8("background-color: rgb(247, 247, 247);\n"
"border:1px groove gray;border-radius:\n"
"7px;padding:2px 4px;\n"
"font: 10pt \"Candara\";"));
        lineEdit_password->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEdit_password, 1, 1, 1, 1);

        lineEdit_username = new QLineEdit(centralwidget);
        lineEdit_username->setObjectName(QString::fromUtf8("lineEdit_username"));
        lineEdit_username->setMinimumSize(QSize(0, 45));
        lineEdit_username->setStyleSheet(QString::fromUtf8("background-color: rgb(247, 247, 247);\n"
"border:1px groove gray;border-radius:\n"
"7px;padding:2px 4px;\n"
"font: 10pt \"Candara\";"));

        gridLayout->addWidget(lineEdit_username, 0, 1, 1, 1);

        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(40, 40));
        label_4->setMaximumSize(QSize(40, 40));
        label_4->setStyleSheet(QString::fromUtf8("border-image: url(:/Resources/Pictures/username.png);\n"
"border: 0;\n"
"background-color: rgb(215, 215, 215)\n"
"\n"
""));

        gridLayout->addWidget(label_4, 1, 0, 1, 1);

        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(40, 40));
        label_5->setMaximumSize(QSize(40, 40));
        label_5->setStyleSheet(QString::fromUtf8("border-image: url(:/Resources/Pictures/password.png);\n"
"border: 0;\n"
"background-color: rgb(215, 215, 215)\n"
"\n"
""));

        gridLayout->addWidget(label_5, 0, 0, 1, 1);


        verticalLayout->addLayout(gridLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_3);

        verticalSpacer_2 = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        btn_signin = new QPushButton(centralwidget);
        btn_signin->setObjectName(QString::fromUtf8("btn_signin"));
        btn_signin->setMinimumSize(QSize(50, 40));
        btn_signin->setCursor(QCursor(Qt::PointingHandCursor));
        btn_signin->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 91, 172);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"border:0px groove gray;border-radius:\n"
"7px;padding:2px 4px;\n"
"font: 14pt \"Candara\";"));

        verticalLayout->addWidget(btn_signin);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(330, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        btn_to_signup = new QPushButton(centralwidget);
        btn_to_signup->setObjectName(QString::fromUtf8("btn_to_signup"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btn_to_signup->sizePolicy().hasHeightForWidth());
        btn_to_signup->setSizePolicy(sizePolicy1);
        btn_to_signup->setMinimumSize(QSize(80, 40));
        btn_to_signup->setCursor(QCursor(Qt::PointingHandCursor));
        btn_to_signup->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"	background: transparent;\n"
"	border: none;\n"
"	color: rgb(0, 91, 172);\n"
"	font: 10pt \"Candara\";\n"
"}\n"
"QPushButton:hover {\n"
"	text-decoration:underline;\n"
"};\n"
"\n"
""));
        btn_to_signup->setFlat(false);

        horizontalLayout->addWidget(btn_to_signup);


        verticalLayout->addLayout(horizontalLayout);

        label_image = new QLabel(page);
        label_image->setObjectName(QString::fromUtf8("label_image"));
        label_image->setGeometry(QRect(0, 0, 830, 520));
        label_image->setStyleSheet(QString::fromUtf8("border-radius:7px;padding:0px 0px;\n"
"border-image: url(:/Resources/Pictures/wp4469118.jpg);\n"
""));
        stackedWidget->addWidget(page);
        label_image->raise();
        centralwidget->raise();
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        label_image_2 = new QLabel(page_2);
        label_image_2->setObjectName(QString::fromUtf8("label_image_2"));
        label_image_2->setGeometry(QRect(0, 0, 830, 520));
        label_image_2->setStyleSheet(QString::fromUtf8("border-radius:7px;padding:0px 0px;\n"
"border-image: url(:/Resources/Pictures/wp4469118.jpg);\n"
""));
        centralwidget_2 = new QWidget(page_2);
        centralwidget_2->setObjectName(QString::fromUtf8("centralwidget_2"));
        centralwidget_2->setGeometry(QRect(0, 0, 380, 520));
        centralwidget_2->setStyleSheet(QString::fromUtf8("background:rgba(255,255,255,0.95);"));
        verticalLayout_2 = new QVBoxLayout(centralwidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_3 = new QLabel(centralwidget_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMaximumSize(QSize(16777215, 16777215));
        label_3->setStyleSheet(QString::fromUtf8("background: transparent;\n"
"color: rgb(41, 155, 255);\n"
"font: 20pt \"Segoe Print\";"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_3);

        label_6 = new QLabel(centralwidget_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setStyleSheet(QString::fromUtf8("background: transparent;\n"
"font: 700 20pt \"Microsoft YaHei UI\";\n"
"color: rgb(0, 91, 172)"));

        verticalLayout_2->addWidget(label_6);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lineEdit_password_3 = new QLineEdit(centralwidget_2);
        lineEdit_password_3->setObjectName(QString::fromUtf8("lineEdit_password_3"));
        lineEdit_password_3->setMinimumSize(QSize(0, 45));
        lineEdit_password_3->setStyleSheet(QString::fromUtf8("background-color: rgb(247, 247, 247);\n"
"border:1px groove gray;border-radius:\n"
"7px;padding:2px 4px;\n"
"font: 10pt \"Candara\";"));
        lineEdit_password_3->setEchoMode(QLineEdit::Password);

        gridLayout_2->addWidget(lineEdit_password_3, 2, 1, 1, 1);

        lineEdit_username_2 = new QLineEdit(centralwidget_2);
        lineEdit_username_2->setObjectName(QString::fromUtf8("lineEdit_username_2"));
        lineEdit_username_2->setMinimumSize(QSize(0, 45));
        lineEdit_username_2->setStyleSheet(QString::fromUtf8("background-color: rgb(247, 247, 247);\n"
"border:1px groove gray;border-radius:\n"
"7px;padding:2px 4px;\n"
"font: 10pt \"Candara\";"));

        gridLayout_2->addWidget(lineEdit_username_2, 0, 1, 1, 1);

        lineEdit_password_2 = new QLineEdit(centralwidget_2);
        lineEdit_password_2->setObjectName(QString::fromUtf8("lineEdit_password_2"));
        lineEdit_password_2->setMinimumSize(QSize(0, 45));
        lineEdit_password_2->setStyleSheet(QString::fromUtf8("background-color: rgb(247, 247, 247);\n"
"border:1px groove gray;border-radius:\n"
"7px;padding:2px 4px;\n"
"font: 10pt \"Candara\";"));
        lineEdit_password_2->setEchoMode(QLineEdit::Password);

        gridLayout_2->addWidget(lineEdit_password_2, 1, 1, 1, 1);

        label_7 = new QLabel(centralwidget_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(40, 40));
        label_7->setMaximumSize(QSize(40, 40));
        label_7->setStyleSheet(QString::fromUtf8("border-image: url(:/Resources/Pictures/username.png);\n"
"border: 0;\n"
"background-color: rgb(215, 215, 215)\n"
"\n"
""));

        gridLayout_2->addWidget(label_7, 0, 0, 1, 1);

        label_8 = new QLabel(centralwidget_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMaximumSize(QSize(40, 40));
        label_8->setStyleSheet(QString::fromUtf8("border-image: url(:/Resources/Pictures/password.png);\n"
"border: 0;\n"
"background-color: rgb(215, 215, 215)\n"
"\n"
""));

        gridLayout_2->addWidget(label_8, 1, 0, 1, 1);

        label_9 = new QLabel(centralwidget_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMaximumSize(QSize(40, 40));
        label_9->setStyleSheet(QString::fromUtf8("border-image: url(:/Resources/Pictures/password.png);\n"
"border: 0;\n"
"background-color: rgb(215, 215, 215)\n"
"\n"
""));

        gridLayout_2->addWidget(label_9, 2, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer);

        btn_signup = new QPushButton(centralwidget_2);
        btn_signup->setObjectName(QString::fromUtf8("btn_signup"));
        btn_signup->setMinimumSize(QSize(50, 40));
        btn_signup->setCursor(QCursor(Qt::PointingHandCursor));
        btn_signup->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 91, 172);\n"
"color: rgb(255, 255, 255);\n"
"\n"
"border:0px groove gray;border-radius:\n"
"7px;padding:2px 4px;\n"
"font: 14pt \"Candara\";"));

        verticalLayout_2->addWidget(btn_signup);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btn_to_signin = new QPushButton(centralwidget_2);
        btn_to_signin->setObjectName(QString::fromUtf8("btn_to_signin"));
        btn_to_signin->setMinimumSize(QSize(80, 40));
        btn_to_signin->setCursor(QCursor(Qt::PointingHandCursor));
        btn_to_signin->setStyleSheet(QString::fromUtf8("QPushButton {\n"
"	background: transparent;\n"
"	border: none;\n"
"	color: rgb(0, 91, 172);\n"
"	font: 10pt \"Candara\";\n"
"}\n"
"QPushButton:hover {\n"
"	text-decoration:underline;\n"
"};\n"
"\n"
""));
        btn_to_signin->setFlat(false);

        horizontalLayout_2->addWidget(btn_to_signin);


        verticalLayout_2->addLayout(horizontalLayout_2);

        stackedWidget->addWidget(page_2);
        QWidget::setTabOrder(lineEdit_username, lineEdit_password);
        QWidget::setTabOrder(lineEdit_password, btn_signin);
        QWidget::setTabOrder(btn_signin, btn_to_signup);
        QWidget::setTabOrder(btn_to_signup, lineEdit_username_2);
        QWidget::setTabOrder(lineEdit_username_2, lineEdit_password_2);
        QWidget::setTabOrder(lineEdit_password_2, lineEdit_password_3);
        QWidget::setTabOrder(lineEdit_password_3, btn_signup);
        QWidget::setTabOrder(btn_signup, btn_to_signin);

        retranslateUi(LoginDialog);

        stackedWidget->setCurrentIndex(1);
        btn_to_signup->setDefault(false);
        btn_to_signin->setDefault(false);


        QMetaObject::connectSlotsByName(LoginDialog);
    } // setupUi

    void retranslateUi(QWidget *LoginDialog)
    {
        LoginDialog->setWindowTitle(QCoreApplication::translate("LoginDialog", "ERP\347\263\273\347\273\237\347\231\273\345\275\225", nullptr));
        label->setText(QCoreApplication::translate("LoginDialog", "DManufacturing", nullptr));
        label_2->setText(QCoreApplication::translate("LoginDialog", "ERP\347\263\273\347\273\237\347\231\273\345\275\225", nullptr));
        lineEdit_password->setText(QCoreApplication::translate("LoginDialog", "123", nullptr));
        lineEdit_password->setPlaceholderText(QCoreApplication::translate("LoginDialog", "\345\257\206\347\240\201", nullptr));
        lineEdit_username->setText(QCoreApplication::translate("LoginDialog", "123", nullptr));
        lineEdit_username->setPlaceholderText(QCoreApplication::translate("LoginDialog", "\347\224\250\346\210\267\345\220\215", nullptr));
        label_4->setText(QString());
        label_5->setText(QString());
        btn_signin->setText(QCoreApplication::translate("LoginDialog", "\347\231\273 \345\275\225", nullptr));
        btn_to_signup->setText(QCoreApplication::translate("LoginDialog", "\346\263\250\345\206\214\350\264\246\345\217\267", nullptr));
        label_image->setText(QString());
        label_image_2->setText(QString());
        label_3->setText(QCoreApplication::translate("LoginDialog", "DManufacturing", nullptr));
        label_6->setText(QCoreApplication::translate("LoginDialog", "\346\263\250\345\206\214\350\264\246\345\217\267", nullptr));
        lineEdit_password_3->setPlaceholderText(QCoreApplication::translate("LoginDialog", "\347\241\256\350\256\244\345\257\206\347\240\201\342\200\230", nullptr));
        lineEdit_username_2->setPlaceholderText(QCoreApplication::translate("LoginDialog", "\347\224\250\346\210\267\345\220\215", nullptr));
        lineEdit_password_2->setPlaceholderText(QCoreApplication::translate("LoginDialog", "\345\257\206\347\240\201", nullptr));
        label_7->setText(QString());
        label_8->setText(QString());
        label_9->setText(QString());
        btn_signup->setText(QCoreApplication::translate("LoginDialog", "\346\263\250\345\206\214", nullptr));
        btn_to_signin->setText(QCoreApplication::translate("LoginDialog", "\350\277\224\345\233\236\347\231\273\345\275\225", nullptr));
    } // retranslateUi

};

namespace Ui {
    class LoginDialog: public Ui_LoginDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINDIALOG_H
