/********************************************************************************
** Form generated from reading UI file 'storage_stat.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STORAGE_STAT_H
#define UI_STORAGE_STAT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StorageStat
{
public:
    QHBoxLayout *horizontalLayout_3;
    QTabWidget *tabWidget;
    QWidget *InStorage;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *In_refresh_btn;
    QLineEdit *InEdit;
    QTableView *In_tableView;
    QWidget *OutStorage;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *Out_refresh_btn;
    QLineEdit *OutEdit;
    QTableView *Out_tableView;
    QWidget *TransferStorage;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *Transfer_refresh_btn;
    QLineEdit *TransferEdit;
    QTableView *Transfer_tableView;
    QWidget *TakeStorage;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout;
    QPushButton *Take_refresh_btn;
    QLineEdit *TakeEdit;
    QTableView *Take_tableView;
    QWidget *ProductData;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *Product_refresh_btn;
    QLineEdit *ProductEdit;
    QTableView *Product_tableView;

    void setupUi(QWidget *StorageStat)
    {
        if (StorageStat->objectName().isEmpty())
            StorageStat->setObjectName(QString::fromUtf8("StorageStat"));
        StorageStat->resize(860, 685);
        horizontalLayout_3 = new QHBoxLayout(StorageStat);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        tabWidget = new QTabWidget(StorageStat);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        InStorage = new QWidget();
        InStorage->setObjectName(QString::fromUtf8("InStorage"));
        verticalLayout = new QVBoxLayout(InStorage);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(100);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, 6, -1);
        In_refresh_btn = new QPushButton(InStorage);
        In_refresh_btn->setObjectName(QString::fromUtf8("In_refresh_btn"));

        horizontalLayout_2->addWidget(In_refresh_btn);

        InEdit = new QLineEdit(InStorage);
        InEdit->setObjectName(QString::fromUtf8("InEdit"));

        horizontalLayout_2->addWidget(InEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        In_tableView = new QTableView(InStorage);
        In_tableView->setObjectName(QString::fromUtf8("In_tableView"));

        verticalLayout->addWidget(In_tableView);

        tabWidget->addTab(InStorage, QString());
        OutStorage = new QWidget();
        OutStorage->setObjectName(QString::fromUtf8("OutStorage"));
        verticalLayout_2 = new QVBoxLayout(OutStorage);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(100);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, -1, 6, -1);
        Out_refresh_btn = new QPushButton(OutStorage);
        Out_refresh_btn->setObjectName(QString::fromUtf8("Out_refresh_btn"));

        horizontalLayout_4->addWidget(Out_refresh_btn);

        OutEdit = new QLineEdit(OutStorage);
        OutEdit->setObjectName(QString::fromUtf8("OutEdit"));

        horizontalLayout_4->addWidget(OutEdit);


        verticalLayout_2->addLayout(horizontalLayout_4);

        Out_tableView = new QTableView(OutStorage);
        Out_tableView->setObjectName(QString::fromUtf8("Out_tableView"));

        verticalLayout_2->addWidget(Out_tableView);

        tabWidget->addTab(OutStorage, QString());
        tabWidget->setTabText(tabWidget->indexOf(OutStorage), QString::fromUtf8("\345\207\272\345\272\223\347\273\237\350\256\241"));
        TransferStorage = new QWidget();
        TransferStorage->setObjectName(QString::fromUtf8("TransferStorage"));
        verticalLayout_3 = new QVBoxLayout(TransferStorage);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(100);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(-1, -1, 6, -1);
        Transfer_refresh_btn = new QPushButton(TransferStorage);
        Transfer_refresh_btn->setObjectName(QString::fromUtf8("Transfer_refresh_btn"));

        horizontalLayout_5->addWidget(Transfer_refresh_btn);

        TransferEdit = new QLineEdit(TransferStorage);
        TransferEdit->setObjectName(QString::fromUtf8("TransferEdit"));

        horizontalLayout_5->addWidget(TransferEdit);


        verticalLayout_3->addLayout(horizontalLayout_5);

        Transfer_tableView = new QTableView(TransferStorage);
        Transfer_tableView->setObjectName(QString::fromUtf8("Transfer_tableView"));

        verticalLayout_3->addWidget(Transfer_tableView);

        tabWidget->addTab(TransferStorage, QString());
        TakeStorage = new QWidget();
        TakeStorage->setObjectName(QString::fromUtf8("TakeStorage"));
        verticalLayout_4 = new QVBoxLayout(TakeStorage);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(100);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Take_refresh_btn = new QPushButton(TakeStorage);
        Take_refresh_btn->setObjectName(QString::fromUtf8("Take_refresh_btn"));

        horizontalLayout->addWidget(Take_refresh_btn);

        TakeEdit = new QLineEdit(TakeStorage);
        TakeEdit->setObjectName(QString::fromUtf8("TakeEdit"));

        horizontalLayout->addWidget(TakeEdit);


        verticalLayout_4->addLayout(horizontalLayout);

        Take_tableView = new QTableView(TakeStorage);
        Take_tableView->setObjectName(QString::fromUtf8("Take_tableView"));

        verticalLayout_4->addWidget(Take_tableView);

        tabWidget->addTab(TakeStorage, QString());
        ProductData = new QWidget();
        ProductData->setObjectName(QString::fromUtf8("ProductData"));
        verticalLayout_5 = new QVBoxLayout(ProductData);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(100);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        Product_refresh_btn = new QPushButton(ProductData);
        Product_refresh_btn->setObjectName(QString::fromUtf8("Product_refresh_btn"));

        horizontalLayout_6->addWidget(Product_refresh_btn);

        ProductEdit = new QLineEdit(ProductData);
        ProductEdit->setObjectName(QString::fromUtf8("ProductEdit"));

        horizontalLayout_6->addWidget(ProductEdit);


        verticalLayout_5->addLayout(horizontalLayout_6);

        Product_tableView = new QTableView(ProductData);
        Product_tableView->setObjectName(QString::fromUtf8("Product_tableView"));

        verticalLayout_5->addWidget(Product_tableView);

        tabWidget->addTab(ProductData, QString());

        horizontalLayout_3->addWidget(tabWidget);


        retranslateUi(StorageStat);

        tabWidget->setCurrentIndex(4);


        QMetaObject::connectSlotsByName(StorageStat);
    } // setupUi

    void retranslateUi(QWidget *StorageStat)
    {
        StorageStat->setWindowTitle(QCoreApplication::translate("StorageStat", "Form", nullptr));
        In_refresh_btn->setText(QCoreApplication::translate("StorageStat", "\345\210\267\346\226\260", nullptr));
        InEdit->setProperty("type", QVariant(QCoreApplication::translate("StorageStat", "search", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(InStorage), QCoreApplication::translate("StorageStat", "\345\205\245\345\272\223\347\273\237\350\256\241", nullptr));
        Out_refresh_btn->setText(QCoreApplication::translate("StorageStat", "\345\210\267\346\226\260", nullptr));
        OutEdit->setProperty("type", QVariant(QCoreApplication::translate("StorageStat", "search", nullptr)));
        Transfer_refresh_btn->setText(QCoreApplication::translate("StorageStat", "\345\210\267\346\226\260", nullptr));
        TransferEdit->setProperty("type", QVariant(QCoreApplication::translate("StorageStat", "search", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(TransferStorage), QCoreApplication::translate("StorageStat", "\347\247\273\345\272\223\347\273\237\350\256\241", nullptr));
        Take_refresh_btn->setText(QCoreApplication::translate("StorageStat", "\345\210\267\346\226\260", nullptr));
        TakeEdit->setProperty("type", QVariant(QCoreApplication::translate("StorageStat", "search", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(TakeStorage), QCoreApplication::translate("StorageStat", "\347\233\230\347\202\271\347\273\237\350\256\241", nullptr));
        Product_refresh_btn->setText(QCoreApplication::translate("StorageStat", "\345\210\267\346\226\260", nullptr));
        ProductEdit->setProperty("type", QVariant(QCoreApplication::translate("StorageStat", "search", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(ProductData), QCoreApplication::translate("StorageStat", "\347\211\251\346\226\231\344\277\241\346\201\257", nullptr));
    } // retranslateUi

};

namespace Ui {
    class StorageStat: public Ui_StorageStat {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STORAGE_STAT_H
