/********************************************************************************
** Form generated from reading UI file 'salesorder.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SALESORDER_H
#define UI_SALESORDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "Tool/date_edit.h"

QT_BEGIN_NAMESPACE

class Ui_salesorder
{
public:
    QVBoxLayout *verticalLayout_3;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_14;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QPushButton *pushButton_2;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_5;
    QLineEdit *lineEdit;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_6;
    QLineEdit *lineEdit_2;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_7;
    QLineEdit *lineEdit_4;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_20;
    QLineEdit *lineEdit_8;
    QVBoxLayout *verticalLayout_10;
    QLabel *label_19;
    QLineEdit *lineEdit_7;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_28;
    DateEdit *comboBox_2;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_29;
    QPushButton *pushButton_3;
    QVBoxLayout *verticalLayout_13;
    QLabel *label_21;
    QLineEdit *lineEdit_9;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_5;
    QTableWidget *tableWidget;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *add_btn;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *delete_btn;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_4;
    QLabel *label;
    QLineEdit *lineEdit_3;
    QTableWidget *tableWidget_3;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *add_btn_2;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *delete_btn_2;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *salesorder)
    {
        if (salesorder->objectName().isEmpty())
            salesorder->setObjectName(QString::fromUtf8("salesorder"));
        salesorder->resize(875, 591);
        verticalLayout_3 = new QVBoxLayout(salesorder);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        widget = new QWidget(salesorder);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMaximumSize(QSize(10000, 10000));
        widget->setStyleSheet(QString::fromUtf8(""));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox = new QGroupBox(widget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_14 = new QVBoxLayout(groupBox);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        pushButton_2 = new QPushButton(groupBox);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_2->addWidget(pushButton_2);


        verticalLayout_14->addLayout(verticalLayout_2);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_6->addWidget(label_5);

        lineEdit = new QLineEdit(groupBox);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        verticalLayout_6->addWidget(lineEdit);


        verticalLayout_14->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_7->addWidget(label_6);

        lineEdit_2 = new QLineEdit(groupBox);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        verticalLayout_7->addWidget(lineEdit_2);


        verticalLayout_14->addLayout(verticalLayout_7);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        verticalLayout_8->addWidget(label_7);

        lineEdit_4 = new QLineEdit(groupBox);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));

        verticalLayout_8->addWidget(lineEdit_4);


        verticalLayout_14->addLayout(verticalLayout_8);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        label_20 = new QLabel(groupBox);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        verticalLayout_9->addWidget(label_20);

        lineEdit_8 = new QLineEdit(groupBox);
        lineEdit_8->setObjectName(QString::fromUtf8("lineEdit_8"));

        verticalLayout_9->addWidget(lineEdit_8);


        verticalLayout_14->addLayout(verticalLayout_9);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        label_19 = new QLabel(groupBox);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        verticalLayout_10->addWidget(label_19);

        lineEdit_7 = new QLineEdit(groupBox);
        lineEdit_7->setObjectName(QString::fromUtf8("lineEdit_7"));

        verticalLayout_10->addWidget(lineEdit_7);


        verticalLayout_14->addLayout(verticalLayout_10);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        label_28 = new QLabel(groupBox);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        verticalLayout_11->addWidget(label_28);

        comboBox_2 = new DateEdit(groupBox);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));

        verticalLayout_11->addWidget(comboBox_2);


        verticalLayout_14->addLayout(verticalLayout_11);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        label_29 = new QLabel(groupBox);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        verticalLayout_12->addWidget(label_29);

        pushButton_3 = new QPushButton(groupBox);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout_12->addWidget(pushButton_3);


        verticalLayout_14->addLayout(verticalLayout_12);

        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        label_21 = new QLabel(groupBox);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        verticalLayout_13->addWidget(label_21);

        lineEdit_9 = new QLineEdit(groupBox);
        lineEdit_9->setObjectName(QString::fromUtf8("lineEdit_9"));

        verticalLayout_13->addWidget(lineEdit_9);


        verticalLayout_14->addLayout(verticalLayout_13);


        horizontalLayout->addWidget(groupBox);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(widget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy1);
        verticalLayout_5 = new QVBoxLayout(groupBox_2);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        tableWidget = new QTableWidget(groupBox_2);
        if (tableWidget->columnCount() < 9)
            tableWidget->setColumnCount(9);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        verticalLayout_5->addWidget(tableWidget);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        add_btn = new QPushButton(groupBox_2);
        add_btn->setObjectName(QString::fromUtf8("add_btn"));

        horizontalLayout_6->addWidget(add_btn);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        delete_btn = new QPushButton(groupBox_2);
        delete_btn->setObjectName(QString::fromUtf8("delete_btn"));

        horizontalLayout_6->addWidget(delete_btn);


        verticalLayout_5->addLayout(horizontalLayout_6);


        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(widget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout_4 = new QVBoxLayout(groupBox_3);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_4->addWidget(label);

        lineEdit_3 = new QLineEdit(groupBox_3);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));

        verticalLayout_4->addWidget(lineEdit_3);

        tableWidget_3 = new QTableWidget(groupBox_3);
        if (tableWidget_3->columnCount() < 3)
            tableWidget_3->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(0, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(1, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(2, __qtablewidgetitem11);
        tableWidget_3->setObjectName(QString::fromUtf8("tableWidget_3"));

        verticalLayout_4->addWidget(tableWidget_3);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        add_btn_2 = new QPushButton(groupBox_3);
        add_btn_2->setObjectName(QString::fromUtf8("add_btn_2"));

        horizontalLayout_7->addWidget(add_btn_2);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_7->addItem(horizontalSpacer_5);

        delete_btn_2 = new QPushButton(groupBox_3);
        delete_btn_2->setObjectName(QString::fromUtf8("delete_btn_2"));

        horizontalLayout_7->addWidget(delete_btn_2);


        verticalLayout_4->addLayout(horizontalLayout_7);


        verticalLayout->addWidget(groupBox_3);


        horizontalLayout->addLayout(verticalLayout);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 3);

        verticalLayout_3->addWidget(widget);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        pushButton = new QPushButton(salesorder);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_5->addWidget(pushButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout_5);


        retranslateUi(salesorder);

        QMetaObject::connectSlotsByName(salesorder);
    } // setupUi

    void retranslateUi(QWidget *salesorder)
    {
        salesorder->setWindowTitle(QCoreApplication::translate("salesorder", "Form", nullptr));
        salesorder->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "Child", nullptr)));
        widget->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "child", nullptr)));
        groupBox->setTitle(QCoreApplication::translate("salesorder", "\351\224\200\345\224\256\350\256\242\345\215\225\344\277\241\346\201\257", nullptr));
        groupBox->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "Group1", nullptr)));
        label_2->setText(QCoreApplication::translate("salesorder", "\351\200\211\346\213\251\345\256\242\346\210\267", nullptr));
        label_2->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        pushButton_2->setText(QString());
        pushButton_2->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "databaseBtn2", nullptr)));
        label_5->setText(QCoreApplication::translate("salesorder", "\345\256\242\346\210\267\345\220\215\347\247\260", nullptr));
        label_5->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        lineEdit->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "ID", nullptr)));
        label_6->setText(QCoreApplication::translate("salesorder", "\345\256\242\346\210\267\347\261\273\345\210\253", nullptr));
        label_6->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        lineEdit_2->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "ID", nullptr)));
        label_7->setText(QCoreApplication::translate("salesorder", "\345\256\242\346\210\267\347\274\226\347\240\201", nullptr));
        label_7->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        lineEdit_4->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "ID", nullptr)));
        label_20->setText(QCoreApplication::translate("salesorder", "\345\256\242\346\210\267\350\201\224\347\263\273\344\272\272\345\247\223\345\220\215", nullptr));
        label_20->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        lineEdit_8->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "ID", nullptr)));
        label_19->setText(QCoreApplication::translate("salesorder", "\345\256\242\346\210\267\350\201\224\347\263\273\344\272\272\346\211\213\346\234\272", nullptr));
        label_19->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        lineEdit_7->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "ID", nullptr)));
        label_28->setText(QCoreApplication::translate("salesorder", "\350\256\242\345\215\225\347\255\276\350\256\242\346\227\245\346\234\237", nullptr));
        label_28->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        comboBox_2->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "datetime", nullptr)));
        label_29->setText(QCoreApplication::translate("salesorder", "\351\224\200\345\224\256\345\221\230", nullptr));
        label_29->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        pushButton_3->setText(QString());
        pushButton_3->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "databaseBtn2", nullptr)));
        label_21->setText(QCoreApplication::translate("salesorder", "\351\224\200\345\224\256\350\256\242\345\215\225\347\274\226\345\217\267", nullptr));
        label_21->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        lineEdit_9->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "ID", nullptr)));
        groupBox_2->setTitle(QCoreApplication::translate("salesorder", "\351\224\200\345\224\256\344\272\247\345\223\201\346\230\216\347\273\206", nullptr));
        groupBox_2->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "Group1", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("salesorder", "\351\200\211\346\213\251\344\272\247\345\223\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("salesorder", "\351\224\200\345\224\256\350\256\242\345\215\225\350\257\246\347\273\206\347\274\226\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("salesorder", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("salesorder", "\344\272\247\345\223\201\345\261\236\346\200\247", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("salesorder", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("salesorder", "\345\275\223\345\211\215\345\217\257\347\224\250\345\272\223\345\255\230\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("salesorder", "\351\224\200\345\224\256\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("salesorder", "\351\224\200\345\224\256\345\215\225\344\273\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("salesorder", "\345\224\256\344\273\267\345\220\210\350\256\241", nullptr));
        add_btn->setText(QCoreApplication::translate("salesorder", "\346\267\273\345\212\240", nullptr));
        add_btn->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "addData", nullptr)));
        delete_btn->setText(QCoreApplication::translate("salesorder", "\345\210\240\351\231\244", nullptr));
        delete_btn->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "deleteData", nullptr)));
        groupBox_3->setTitle(QCoreApplication::translate("salesorder", "\351\200\201\350\264\247\350\256\241\345\210\222", nullptr));
        groupBox_3->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "Group1", nullptr)));
        label->setText(QCoreApplication::translate("salesorder", "\351\200\201\350\264\247\350\257\246\347\273\206\345\234\260\345\235\200", nullptr));
        label->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "title2", nullptr)));
        lineEdit_3->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "ID2", nullptr)));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget_3->horizontalHeaderItem(0);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("salesorder", "\351\200\201\350\264\247\346\211\271\346\254\241", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget_3->horizontalHeaderItem(1);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("salesorder", "\350\256\241\345\210\222\351\200\201\350\264\247\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget_3->horizontalHeaderItem(2);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("salesorder", "\350\256\241\345\210\222\351\200\201\350\264\247\345\206\205\345\256\271\345\244\207\346\263\250", nullptr));
        add_btn_2->setText(QCoreApplication::translate("salesorder", "\346\267\273\345\212\240", nullptr));
        add_btn_2->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "addData", nullptr)));
        delete_btn_2->setText(QCoreApplication::translate("salesorder", "\345\210\240\351\231\244", nullptr));
        delete_btn_2->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "deleteData", nullptr)));
        pushButton->setText(QCoreApplication::translate("salesorder", "\346\217\220\344\272\244", nullptr));
        pushButton->setProperty("type", QVariant(QCoreApplication::translate("salesorder", "upload", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class salesorder: public Ui_salesorder {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SALESORDER_H
