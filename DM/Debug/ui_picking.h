/********************************************************************************
** Form generated from reading UI file 'picking.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PICKING_H
#define UI_PICKING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>
#include "Tool/date_time_edit.h"

QT_BEGIN_NAMESPACE

class Ui_Picking
{
public:
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_2;
    QGridLayout *gridLayout;
    DateTimeEdit *datetime;
    QPushButton *add_btn;
    QCheckBox *confirm_checkBox;
    QSpacerItem *horizontalSpacer;
    QLabel *label_12;
    QTableWidget *tableWidget;
    QPushButton *delete_btn;
    QLabel *label_10;
    QLabel *label_7;
    QLabel *label_11;
    QPushButton *submit_btn;
    QPushButton *select_member;
    QLabel *label_6;
    QComboBox *warehouse_combo;
    QLabel *label_16;
    QComboBox *order_combo;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_3;
    QGridLayout *gridLayout_2;
    QPushButton *add_btn_2;
    QLabel *label_20;
    QLabel *label_14;
    QLabel *label_8;
    QComboBox *type_combo_3;
    DateTimeEdit *datetime_4;
    QLabel *label_19;
    QLabel *label_13;
    QPushButton *delete_btn_2;
    QLabel *label_9;
    QComboBox *type_combo_2;
    QCheckBox *confirm_checkBox_2;
    QComboBox *type_combo_4;
    QLabel *label_15;
    QTableWidget *tableWidget_2;
    QPushButton *select_member_2;
    QPushButton *submit_btn_2;

    void setupUi(QWidget *Picking)
    {
        if (Picking->objectName().isEmpty())
            Picking->setObjectName(QString::fromUtf8("Picking"));
        Picking->resize(776, 652);
        horizontalLayout = new QHBoxLayout(Picking);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabWidget = new QTabWidget(Picking);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        horizontalLayout_2 = new QHBoxLayout(tab);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        datetime = new DateTimeEdit(tab);
        datetime->setObjectName(QString::fromUtf8("datetime"));
        datetime->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(datetime, 7, 0, 1, 1);

        add_btn = new QPushButton(tab);
        add_btn->setObjectName(QString::fromUtf8("add_btn"));

        gridLayout->addWidget(add_btn, 4, 0, 1, 1);

        confirm_checkBox = new QCheckBox(tab);
        confirm_checkBox->setObjectName(QString::fromUtf8("confirm_checkBox"));
        QFont font;
        font.setFamily(QString::fromUtf8("AcadEref"));
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        confirm_checkBox->setFont(font);

        gridLayout->addWidget(confirm_checkBox, 7, 5, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 4, 5, 1, 1);

        label_12 = new QLabel(tab);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 5, 5, 1, 1);

        tableWidget = new QTableWidget(tab);
        if (tableWidget->columnCount() < 11)
            tableWidget->setColumnCount(11);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(10, __qtablewidgetitem10);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        tableWidget->setCornerButtonEnabled(true);
        tableWidget->horizontalHeader()->setCascadingSectionResizes(false);
        tableWidget->horizontalHeader()->setMinimumSectionSize(110);
        tableWidget->horizontalHeader()->setDefaultSectionSize(110);
        tableWidget->horizontalHeader()->setHighlightSections(true);
        tableWidget->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->horizontalHeader()->setStretchLastSection(false);

        gridLayout->addWidget(tableWidget, 3, 0, 1, 6);

        delete_btn = new QPushButton(tab);
        delete_btn->setObjectName(QString::fromUtf8("delete_btn"));

        gridLayout->addWidget(delete_btn, 4, 1, 1, 1);

        label_10 = new QLabel(tab);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 5, 0, 1, 1);

        label_7 = new QLabel(tab);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 2, 0, 1, 1);

        label_11 = new QLabel(tab);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 5, 2, 1, 1);

        submit_btn = new QPushButton(tab);
        submit_btn->setObjectName(QString::fromUtf8("submit_btn"));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        submit_btn->setFont(font1);

        gridLayout->addWidget(submit_btn, 8, 4, 1, 1);

        select_member = new QPushButton(tab);
        select_member->setObjectName(QString::fromUtf8("select_member"));
        select_member->setMinimumSize(QSize(0, 0));

        gridLayout->addWidget(select_member, 7, 2, 1, 1);

        label_6 = new QLabel(tab);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 0, 1, 1, 1);

        warehouse_combo = new QComboBox(tab);
        warehouse_combo->addItem(QString());
        warehouse_combo->setObjectName(QString::fromUtf8("warehouse_combo"));
        warehouse_combo->setEnabled(true);
        QFont font2;
        font2.setFamily(QString::fromUtf8("AcadEref"));
        font2.setPointSize(10);
        warehouse_combo->setFont(font2);

        gridLayout->addWidget(warehouse_combo, 1, 1, 1, 1);

        label_16 = new QLabel(tab);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 0, 0, 1, 1);

        order_combo = new QComboBox(tab);
        order_combo->addItem(QString());
        order_combo->addItem(QString());
        order_combo->addItem(QString());
        order_combo->addItem(QString());
        order_combo->addItem(QString());
        order_combo->addItem(QString());
        order_combo->setObjectName(QString::fromUtf8("order_combo"));
        order_combo->setEnabled(true);
        order_combo->setFont(font2);

        gridLayout->addWidget(order_combo, 1, 0, 1, 1);


        horizontalLayout_2->addLayout(gridLayout);

        tabWidget->addTab(tab, QString());
        widget = new QWidget();
        widget->setObjectName(QString::fromUtf8("widget"));
        horizontalLayout_3 = new QHBoxLayout(widget);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        add_btn_2 = new QPushButton(widget);
        add_btn_2->setObjectName(QString::fromUtf8("add_btn_2"));

        gridLayout_2->addWidget(add_btn_2, 6, 0, 1, 1);

        label_20 = new QLabel(widget);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        gridLayout_2->addWidget(label_20, 7, 4, 1, 1);

        label_14 = new QLabel(widget);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_2->addWidget(label_14, 3, 2, 1, 1);

        label_8 = new QLabel(widget);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_2->addWidget(label_8, 2, 0, 1, 1);

        type_combo_3 = new QComboBox(widget);
        type_combo_3->addItem(QString());
        type_combo_3->addItem(QString());
        type_combo_3->setObjectName(QString::fromUtf8("type_combo_3"));
        type_combo_3->setEnabled(true);
        type_combo_3->setFont(font2);

        gridLayout_2->addWidget(type_combo_3, 4, 0, 1, 1);

        datetime_4 = new DateTimeEdit(widget);
        datetime_4->setObjectName(QString::fromUtf8("datetime_4"));
        datetime_4->setMinimumSize(QSize(0, 0));

        gridLayout_2->addWidget(datetime_4, 8, 0, 1, 1);

        label_19 = new QLabel(widget);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout_2->addWidget(label_19, 7, 2, 1, 1);

        label_13 = new QLabel(widget);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_2->addWidget(label_13, 3, 0, 1, 1);

        delete_btn_2 = new QPushButton(widget);
        delete_btn_2->setObjectName(QString::fromUtf8("delete_btn_2"));

        gridLayout_2->addWidget(delete_btn_2, 6, 1, 1, 1);

        label_9 = new QLabel(widget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_2->addWidget(label_9, 0, 0, 1, 1);

        type_combo_2 = new QComboBox(widget);
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->addItem(QString());
        type_combo_2->setObjectName(QString::fromUtf8("type_combo_2"));
        type_combo_2->setEnabled(true);
        type_combo_2->setFont(font2);

        gridLayout_2->addWidget(type_combo_2, 1, 0, 1, 1);

        confirm_checkBox_2 = new QCheckBox(widget);
        confirm_checkBox_2->setObjectName(QString::fromUtf8("confirm_checkBox_2"));
        confirm_checkBox_2->setFont(font);

        gridLayout_2->addWidget(confirm_checkBox_2, 8, 4, 1, 1);

        type_combo_4 = new QComboBox(widget);
        type_combo_4->addItem(QString());
        type_combo_4->addItem(QString());
        type_combo_4->addItem(QString());
        type_combo_4->setObjectName(QString::fromUtf8("type_combo_4"));
        type_combo_4->setEnabled(true);
        type_combo_4->setFont(font2);

        gridLayout_2->addWidget(type_combo_4, 4, 2, 1, 1);

        label_15 = new QLabel(widget);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_2->addWidget(label_15, 7, 0, 1, 1);

        tableWidget_2 = new QTableWidget(widget);
        if (tableWidget_2->columnCount() < 7)
            tableWidget_2->setColumnCount(7);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(0, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(1, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(2, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(3, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(4, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(5, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(6, __qtablewidgetitem17);
        tableWidget_2->setObjectName(QString::fromUtf8("tableWidget_2"));
        tableWidget_2->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        tableWidget_2->setCornerButtonEnabled(true);
        tableWidget_2->horizontalHeader()->setCascadingSectionResizes(false);
        tableWidget_2->horizontalHeader()->setMinimumSectionSize(110);
        tableWidget_2->horizontalHeader()->setDefaultSectionSize(110);
        tableWidget_2->horizontalHeader()->setHighlightSections(true);
        tableWidget_2->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget_2->horizontalHeader()->setStretchLastSection(false);

        gridLayout_2->addWidget(tableWidget_2, 5, 0, 1, 5);

        select_member_2 = new QPushButton(widget);
        select_member_2->setObjectName(QString::fromUtf8("select_member_2"));
        select_member_2->setMinimumSize(QSize(0, 0));

        gridLayout_2->addWidget(select_member_2, 8, 2, 1, 1);

        submit_btn_2 = new QPushButton(widget);
        submit_btn_2->setObjectName(QString::fromUtf8("submit_btn_2"));
        submit_btn_2->setFont(font1);

        gridLayout_2->addWidget(submit_btn_2, 9, 3, 1, 1);


        horizontalLayout_3->addLayout(gridLayout_2);

        tabWidget->addTab(widget, QString());
        tabWidget->setTabText(tabWidget->indexOf(widget), QString::fromUtf8("\347\224\237\344\272\247\351\200\200\346\226\231"));

        horizontalLayout->addWidget(tabWidget);


        retranslateUi(Picking);

        tabWidget->setCurrentIndex(0);
        warehouse_combo->setCurrentIndex(0);
        order_combo->setCurrentIndex(-1);
        type_combo_3->setCurrentIndex(0);
        type_combo_2->setCurrentIndex(-1);
        type_combo_4->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Picking);
    } // setupUi

    void retranslateUi(QWidget *Picking)
    {
        Picking->setWindowTitle(QCoreApplication::translate("Picking", "Form", nullptr));
        add_btn->setText(QCoreApplication::translate("Picking", "\346\267\273\345\212\240", nullptr));
        add_btn->setProperty("type", QVariant(QCoreApplication::translate("Picking", "addData", nullptr)));
        confirm_checkBox->setText(QCoreApplication::translate("Picking", "\347\241\256\350\256\244", nullptr));
        confirm_checkBox->setProperty("type", QVariant(QCoreApplication::translate("Picking", "important", nullptr)));
        label_12->setText(QCoreApplication::translate("Picking", "\344\272\247\345\223\201\345\207\272\345\272\223\347\241\256\350\256\244", nullptr));
        label_12->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title2", nullptr)));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("Picking", "\345\272\217\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("Picking", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("Picking", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("Picking", "\351\234\200\346\261\202\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("Picking", "\345\267\262\351\242\206\346\226\231\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("Picking", "\346\211\271\346\254\241", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("Picking", "\345\275\223\345\211\215\345\217\257\347\224\250\345\272\223\345\255\230\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("Picking", "\346\234\254\346\254\241\345\207\272\345\272\223\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(9);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("Picking", "\345\215\225\344\275\215", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->horizontalHeaderItem(10);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("Picking", "\351\242\206\346\226\231\345\215\225\345\217\267", nullptr));
        delete_btn->setText(QCoreApplication::translate("Picking", "\345\210\240\351\231\244", nullptr));
        delete_btn->setProperty("type", QVariant(QCoreApplication::translate("Picking", "deleteData", nullptr)));
        label_10->setText(QCoreApplication::translate("Picking", "\351\242\206\346\226\231\345\207\272\345\272\223\346\227\266\351\227\264", nullptr));
        label_10->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title2", nullptr)));
        label_7->setText(QCoreApplication::translate("Picking", "\345\207\272\345\272\223\344\272\247\345\223\201\346\230\216\347\273\206", nullptr));
        label_7->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title1", nullptr)));
        label_11->setText(QCoreApplication::translate("Picking", "\345\207\272\345\272\223\345\221\230", nullptr));
        label_11->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title2", nullptr)));
        submit_btn->setText(QCoreApplication::translate("Picking", "\346\217\220\344\272\244", nullptr));
        submit_btn->setProperty("type", QVariant(QCoreApplication::translate("Picking", "upload", nullptr)));
        select_member->setText(QCoreApplication::translate("Picking", "\351\200\211\346\213\251\346\210\220\345\221\230", nullptr));
        select_member->setProperty("type", QVariant(QCoreApplication::translate("Picking", "SelectMember", nullptr)));
        label_6->setText(QCoreApplication::translate("Picking", "\345\207\272\345\272\223\344\273\223\345\272\223", nullptr));
        label_6->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title1", nullptr)));
        warehouse_combo->setItemText(0, QCoreApplication::translate("Picking", "\345\216\237\346\226\231\345\272\223", nullptr));

        warehouse_combo->setProperty("type", QVariant(QCoreApplication::translate("Picking", "comboBox", nullptr)));
        label_16->setText(QCoreApplication::translate("Picking", "\351\200\211\346\213\251\346\212\245\345\267\245\345\215\225", nullptr));
        label_16->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title1", nullptr)));
        order_combo->setItemText(0, QCoreApplication::translate("Picking", "\347\224\237\344\272\247\346\210\220\345\223\201\345\205\245\345\272\223", nullptr));
        order_combo->setItemText(1, QCoreApplication::translate("Picking", "\351\224\200\345\224\256\351\200\200\350\264\247\345\205\245\345\272\223", nullptr));
        order_combo->setItemText(2, QCoreApplication::translate("Picking", "\351\224\200\345\224\256\346\215\242\350\264\247\345\205\245\345\272\223", nullptr));
        order_combo->setItemText(3, QCoreApplication::translate("Picking", "\350\260\203\346\213\250\345\205\245\345\272\223", nullptr));
        order_combo->setItemText(4, QCoreApplication::translate("Picking", "\346\234\237\345\210\235\345\205\245\345\272\223", nullptr));
        order_combo->setItemText(5, QCoreApplication::translate("Picking", "\345\205\266\344\273\226", nullptr));

        order_combo->setProperty("type", QVariant(QCoreApplication::translate("Picking", "comboBox", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("Picking", "\347\224\237\344\272\247\351\242\206\346\226\231", nullptr));
        add_btn_2->setText(QCoreApplication::translate("Picking", "\346\267\273\345\212\240", nullptr));
        add_btn_2->setProperty("type", QVariant(QCoreApplication::translate("Picking", "addData", nullptr)));
        label_20->setText(QCoreApplication::translate("Picking", "\344\272\247\345\223\201\345\205\245\345\272\223\347\241\256\350\256\244", nullptr));
        label_20->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title2", nullptr)));
        label_14->setText(QCoreApplication::translate("Picking", "\351\200\200\346\226\231\345\216\237\345\233\240", nullptr));
        label_14->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title2", nullptr)));
        label_8->setText(QCoreApplication::translate("Picking", "\345\205\245\345\272\223\344\272\247\345\223\201\346\230\216\347\273\206", nullptr));
        label_8->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title1", nullptr)));
        type_combo_3->setItemText(0, QCoreApplication::translate("Picking", "\345\216\237\346\226\231\345\272\223", nullptr));
        type_combo_3->setItemText(1, QCoreApplication::translate("Picking", "\346\210\220\345\223\201\345\272\223", nullptr));

        type_combo_3->setProperty("type", QVariant(QCoreApplication::translate("Picking", "comboBox", nullptr)));
        label_19->setText(QCoreApplication::translate("Picking", "\345\205\245\345\272\223\345\221\230", nullptr));
        label_19->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title2", nullptr)));
        label_13->setText(QCoreApplication::translate("Picking", "\345\205\245\345\272\223\344\273\223\345\272\223", nullptr));
        label_13->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title2", nullptr)));
        delete_btn_2->setText(QCoreApplication::translate("Picking", "\345\210\240\351\231\244", nullptr));
        delete_btn_2->setProperty("type", QVariant(QCoreApplication::translate("Picking", "deleteData", nullptr)));
        label_9->setText(QCoreApplication::translate("Picking", "\351\200\211\346\213\251\347\224\237\344\272\247\351\242\206\346\226\231\345\215\225", nullptr));
        label_9->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title1", nullptr)));
        type_combo_2->setItemText(0, QCoreApplication::translate("Picking", "\347\224\237\344\272\247\346\210\220\345\223\201\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(1, QCoreApplication::translate("Picking", "\351\224\200\345\224\256\351\200\200\350\264\247\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(2, QCoreApplication::translate("Picking", "\351\224\200\345\224\256\346\215\242\350\264\247\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(3, QCoreApplication::translate("Picking", "\350\260\203\346\213\250\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(4, QCoreApplication::translate("Picking", "\346\234\237\345\210\235\345\205\245\345\272\223", nullptr));
        type_combo_2->setItemText(5, QCoreApplication::translate("Picking", "\345\205\266\344\273\226", nullptr));

        type_combo_2->setProperty("type", QVariant(QCoreApplication::translate("Picking", "comboBox", nullptr)));
        confirm_checkBox_2->setText(QCoreApplication::translate("Picking", "\347\241\256\350\256\244", nullptr));
        confirm_checkBox_2->setProperty("type", QVariant(QCoreApplication::translate("Picking", "important", nullptr)));
        type_combo_4->setItemText(0, QCoreApplication::translate("Picking", "\350\264\250\351\207\217\351\227\256\351\242\230", nullptr));
        type_combo_4->setItemText(1, QCoreApplication::translate("Picking", "\345\236\213\345\217\267\351\224\231\350\257\257", nullptr));
        type_combo_4->setItemText(2, QCoreApplication::translate("Picking", "\345\257\214\344\275\231\345\216\237\346\226\231", nullptr));

        type_combo_4->setProperty("type", QVariant(QCoreApplication::translate("Picking", "comboBox", nullptr)));
        label_15->setText(QCoreApplication::translate("Picking", "\351\200\200\346\226\231\345\205\245\345\272\223\346\227\266\351\227\264", nullptr));
        label_15->setProperty("type", QVariant(QCoreApplication::translate("Picking", "title2", nullptr)));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget_2->horizontalHeaderItem(0);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("Picking", "\345\272\217\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget_2->horizontalHeaderItem(2);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("Picking", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem12 = tableWidget_2->horizontalHeaderItem(3);
        ___qtablewidgetitem12->setText(QCoreApplication::translate("Picking", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem13 = tableWidget_2->horizontalHeaderItem(4);
        ___qtablewidgetitem13->setText(QCoreApplication::translate("Picking", "\344\272\247\345\223\201\346\211\271\346\254\241\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem14 = tableWidget_2->horizontalHeaderItem(5);
        ___qtablewidgetitem14->setText(QCoreApplication::translate("Picking", "\346\234\254\346\254\241\345\205\245\345\272\223\346\225\260\351\207\217", nullptr));
        QTableWidgetItem *___qtablewidgetitem15 = tableWidget_2->horizontalHeaderItem(6);
        ___qtablewidgetitem15->setText(QCoreApplication::translate("Picking", "\345\215\225\344\275\215", nullptr));
        select_member_2->setText(QCoreApplication::translate("Picking", "\351\200\211\346\213\251\346\210\220\345\221\230", nullptr));
        select_member_2->setProperty("type", QVariant(QCoreApplication::translate("Picking", "SelectMember", nullptr)));
        submit_btn_2->setText(QCoreApplication::translate("Picking", "\346\217\220\344\272\244", nullptr));
        submit_btn_2->setProperty("type", QVariant(QCoreApplication::translate("Picking", "upload", nullptr)));
    } // retranslateUi

};

namespace Ui {
    class Picking: public Ui_Picking {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PICKING_H
