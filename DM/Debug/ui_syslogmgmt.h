/********************************************************************************
** Form generated from reading UI file 'syslogmgmt.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYSLOGMGMT_H
#define UI_SYSLOGMGMT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SysLogMgmt
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_5;
    QLineEdit *le_module;
    QLabel *label;
    QLineEdit *le_content;
    QLabel *label_3;
    QLineEdit *le_person;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_2;
    QDateTimeEdit *dte_start;
    QLabel *label_2;
    QDateTimeEdit *dte_end;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btn_search;
    QPushButton *btn_reset;
    QSpacerItem *horizontalSpacer_3;
    QTableWidget *tableWidget;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *btn_page_head;
    QPushButton *btn_page_minus;
    QLineEdit *le_nowpage;
    QLabel *lb_totolpage;
    QPushButton *btn_page_plus;
    QPushButton *btn_page_end;

    void setupUi(QWidget *SysLogMgmt)
    {
        if (SysLogMgmt->objectName().isEmpty())
            SysLogMgmt->setObjectName(QString::fromUtf8("SysLogMgmt"));
        SysLogMgmt->resize(860, 685);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SysLogMgmt->sizePolicy().hasHeightForWidth());
        SysLogMgmt->setSizePolicy(sizePolicy);
        SysLogMgmt->setStyleSheet(QString::fromUtf8("*{\n"
"	font: 12pt \"Microsoft YaHei UI\";\n"
"}\n"
"QLineEdit,QDateTimeEdit,QTextEdit{\n"
"	border-radius: 8px;\n"
"	border: 1px solid rgb(0, 0, 0);\n"
"}\n"
"QPushButton{\n"
"	border-radius: 8px;\n"
"	border: 1px solid rgb(0, 0, 0);\n"
"}\n"
"\n"
"QComboBox {\n"
"\342\200\213    border: 2px solid #f3f3f3;/*\350\256\276\347\275\256\347\272\277\345\256\275*/\n"
"\342\200\213	/*background-color: rgb(237, 242, 255);\350\203\214\346\231\257\351\242\234\350\211\262*/\n"
"\342\200\213    border-radius: 8px;/*\345\234\206\350\247\222*/\n"
"\342\200\213    padding: 1px 2px 1px 2px;  /*\351\222\210\345\257\271\344\272\216\347\273\204\345\220\210\346\241\206\344\270\255\347\232\204\346\226\207\346\234\254\345\206\205\345\256\271*/\n"
"\342\200\213	text-align:bottom;\n"
"\342\200\213    min-width: 9em;   /*# \347\273\204\345\220\210\346\241\206\347\232\204\346\234\200\345\260\217\345\256\275\345\272\246*/\n"
"\342\200\213    /*min-height: 5em;*/\n"
"\342\200\213	border-style:solid;/*\350\276\271\346\241\206\344\270\272\345"
                        "\256\236\347\272\277\345\236\213*/\n"
"\342\200\213	border-width:2px;/*\350\276\271\346\241\206\345\256\275\345\272\246*/\n"
"\342\200\213	border-color:black;/*\350\276\271\346\241\206\351\242\234\350\211\262*/\n"
"\342\200\213	padding-left: 10px;/*\345\267\246\344\276\247\350\276\271\350\267\235*/\n"
"}"));
        verticalLayout = new QVBoxLayout(SysLogMgmt);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_5 = new QLabel(SysLogMgmt);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setWordWrap(false);

        horizontalLayout->addWidget(label_5);

        le_module = new QLineEdit(SysLogMgmt);
        le_module->setObjectName(QString::fromUtf8("le_module"));
        le_module->setMinimumSize(QSize(0, 30));
        le_module->setMaximumSize(QSize(200, 30));

        horizontalLayout->addWidget(le_module);

        label = new QLabel(SysLogMgmt);
        label->setObjectName(QString::fromUtf8("label"));
        label->setWordWrap(false);

        horizontalLayout->addWidget(label);

        le_content = new QLineEdit(SysLogMgmt);
        le_content->setObjectName(QString::fromUtf8("le_content"));
        le_content->setMinimumSize(QSize(0, 30));
        le_content->setMaximumSize(QSize(200, 30));
        le_content->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(le_content);

        label_3 = new QLabel(SysLogMgmt);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setWordWrap(false);

        horizontalLayout->addWidget(label_3);

        le_person = new QLineEdit(SysLogMgmt);
        le_person->setObjectName(QString::fromUtf8("le_person"));
        le_person->setMinimumSize(QSize(0, 30));
        le_person->setMaximumSize(QSize(200, 30));
        QFont font;
        font.setFamily(QString::fromUtf8("Microsoft YaHei UI"));
        font.setPointSize(12);
        font.setBold(false);
        font.setItalic(false);
        le_person->setFont(font);

        horizontalLayout->addWidget(le_person);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(SysLogMgmt);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setWordWrap(false);

        horizontalLayout_4->addWidget(label_4);

        widget = new QWidget(SysLogMgmt);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMaximumSize(QSize(16777215, 35));
        widget->setStyleSheet(QString::fromUtf8("#widget{\n"
"	border-radius: 8px;\n"
"	border: 1px solid rgb(0, 0, 0);\n"
"}"));
        horizontalLayout_2 = new QHBoxLayout(widget);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(2, 2, 2, 2);
        dte_start = new QDateTimeEdit(widget);
        dte_start->setObjectName(QString::fromUtf8("dte_start"));
        dte_start->setMinimumSize(QSize(200, 30));
        dte_start->setMaximumSize(QSize(200, 30));
        dte_start->setStyleSheet(QString::fromUtf8("border-radius: 8px;\n"
"border: 0px solid rgb(0, 0, 0);\n"
"font: 10pt \"Microsoft YaHei UI\";"));
        dte_start->setAlignment(Qt::AlignCenter);
        dte_start->setButtonSymbols(QAbstractSpinBox::NoButtons);
        dte_start->setDateTime(QDateTime(QDate(2000, 1, 1), QTime(0, 0, 0)));
        dte_start->setTime(QTime(0, 0, 0));
        dte_start->setCalendarPopup(false);

        horizontalLayout_2->addWidget(dte_start);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        dte_end = new QDateTimeEdit(widget);
        dte_end->setObjectName(QString::fromUtf8("dte_end"));
        dte_end->setMinimumSize(QSize(200, 30));
        dte_end->setMaximumSize(QSize(200, 30));
        dte_end->setStyleSheet(QString::fromUtf8("border-radius: 8px;\n"
"border: 0px solid rgb(0, 0, 0);\n"
"font: 10pt \"Microsoft YaHei UI\";"));
        dte_end->setAlignment(Qt::AlignCenter);
        dte_end->setButtonSymbols(QAbstractSpinBox::NoButtons);
        dte_end->setCalendarPopup(false);

        horizontalLayout_2->addWidget(dte_end);


        horizontalLayout_4->addWidget(widget);

        horizontalSpacer_2 = new QSpacerItem(200, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        btn_search = new QPushButton(SysLogMgmt);
        btn_search->setObjectName(QString::fromUtf8("btn_search"));
        btn_search->setMinimumSize(QSize(90, 40));
        btn_search->setStyleSheet(QString::fromUtf8("\n"
"border-radius: 8px;\n"
"color:white;\n"
"font-weight:bold;\n"
"background:rgb(24, 144, 255);"));

        horizontalLayout_4->addWidget(btn_search);

        btn_reset = new QPushButton(SysLogMgmt);
        btn_reset->setObjectName(QString::fromUtf8("btn_reset"));
        btn_reset->setMinimumSize(QSize(90, 40));
        btn_reset->setStyleSheet(QString::fromUtf8("border: 2px solid rgb(0, 0, 0);\n"
"border-radius: 8px;\n"
"font-weight:bold;\n"
""));

        horizontalLayout_4->addWidget(btn_reset);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout->addLayout(horizontalLayout_4);

        tableWidget = new QTableWidget(SysLogMgmt);
        if (tableWidget->columnCount() < 8)
            tableWidget->setColumnCount(8);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setStyleSheet(QString::fromUtf8("/*tabelwidget*/\n"
"QTableView{\n"
"	color:#DCDCDC;\n"
"	background:#444444;\n"
"	border:1px solid #242424;\n"
"	alternate-background-color:#525252;/*\344\272\244\351\224\231\351\242\234\350\211\262*/\n"
"	gridline-color:#242424;\n"
"}\n"
"QTableView::indicator {\n"
"        width: 20px;\n"
"        height: 20px;\n"
"}\n"
"\n"
"QTableView::item{\n"
"    text-align:center;\n"
"}\n"
"\n"
"/*\351\200\211\344\270\255item*/\n"
"QTableView::item:selected{\n"
"	color:#DCDCDC;\n"
"	background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #484848,stop:1 #383838);\n"
"}\n"
"\n"
"/*\n"
"\346\202\254\346\265\256item*/\n"
"QTableView::item:hover{\n"
"	background:#5B5B5B;\n"
"}\n"
"/*\350\241\250\345\244\264*/\n"
"QHeaderView::section{\n"
"	text-align:center;\n"
"	background:#5E5E5E;\n"
"	padding:3px;\n"
"	margin:0px;\n"
"	color:#DCDCDC;\n"
"	border:1px solid #242424;\n"
"	border-left-width:0;\n"
"}\n"
"\n"
"\n"
"\n"
"/*\350\241\250\345\217\263\344\276\247\347\232\204\346\273\221\346\235\241*/\n"
"QScrollBar:vertica"
                        "l{\n"
"	background:#484848;\n"
"	padding:0px;\n"
"	border-radius:6px;\n"
"	max-width:12px;\n"
"}\n"
"\n"
"/*\346\273\221\345\235\227*/\n"
"QScrollBar::handle:vertical{\n"
"	background:#CCCCCC;\n"
"}\n"
"/*\n"
"\346\273\221\345\235\227\346\202\254\346\265\256\357\274\214\346\214\211\344\270\213*/\n"
"QScrollBar::handle:hover:vertical,QScrollBar::handle:pressed:vertical{\n"
"	background:#A7A7A7;\n"
"}\n"
"/*\n"
"\346\273\221\345\235\227\345\267\262\347\273\217\345\210\222\350\277\207\347\232\204\345\214\272\345\237\237*/\n"
"QScrollBar::sub-page:vertical{\n"
"	background:444444;\n"
"}\n"
"\n"
"/*\n"
"\346\273\221\345\235\227\350\277\230\346\262\241\346\234\211\345\210\222\350\277\207\347\232\204\345\214\272\345\237\237*/\n"
"QScrollBar::add-page:vertical{\n"
"	background:5B5B5B;\n"
"}\n"
"\n"
"/*\351\241\265\351\235\242\344\270\213\347\247\273\347\232\204\346\214\211\351\222\256*/\n"
"QScrollBar::add-line:vertical{\n"
"	background:none;\n"
"}\n"
"/*\351\241\265\351\235\242\344\270\212\347\247\273\347\232\204\346"
                        "\214\211\351\222\256*/\n"
"QScrollBar::sub-line:vertical{\n"
"	background:none;\n"
"}"));
        tableWidget->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->verticalHeader()->setVisible(false);
        tableWidget->verticalHeader()->setHighlightSections(true);

        verticalLayout->addWidget(tableWidget);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        btn_page_head = new QPushButton(SysLogMgmt);
        btn_page_head->setObjectName(QString::fromUtf8("btn_page_head"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(btn_page_head->sizePolicy().hasHeightForWidth());
        btn_page_head->setSizePolicy(sizePolicy1);
        btn_page_head->setMinimumSize(QSize(40, 30));
        btn_page_head->setMaximumSize(QSize(40, 30));

        horizontalLayout_3->addWidget(btn_page_head);

        btn_page_minus = new QPushButton(SysLogMgmt);
        btn_page_minus->setObjectName(QString::fromUtf8("btn_page_minus"));
        btn_page_minus->setMinimumSize(QSize(40, 30));
        btn_page_minus->setMaximumSize(QSize(40, 30));

        horizontalLayout_3->addWidget(btn_page_minus);

        le_nowpage = new QLineEdit(SysLogMgmt);
        le_nowpage->setObjectName(QString::fromUtf8("le_nowpage"));
        le_nowpage->setMinimumSize(QSize(50, 30));
        le_nowpage->setMaximumSize(QSize(100, 30));
        le_nowpage->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(le_nowpage);

        lb_totolpage = new QLabel(SysLogMgmt);
        lb_totolpage->setObjectName(QString::fromUtf8("lb_totolpage"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Microsoft YaHei UI"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setItalic(false);
        lb_totolpage->setFont(font1);
        lb_totolpage->setStyleSheet(QString::fromUtf8("font: 700 12pt \"Microsoft YaHei UI\";"));
        lb_totolpage->setWordWrap(false);

        horizontalLayout_3->addWidget(lb_totolpage);

        btn_page_plus = new QPushButton(SysLogMgmt);
        btn_page_plus->setObjectName(QString::fromUtf8("btn_page_plus"));
        btn_page_plus->setMinimumSize(QSize(40, 30));
        btn_page_plus->setMaximumSize(QSize(40, 30));

        horizontalLayout_3->addWidget(btn_page_plus);

        btn_page_end = new QPushButton(SysLogMgmt);
        btn_page_end->setObjectName(QString::fromUtf8("btn_page_end"));
        btn_page_end->setMinimumSize(QSize(40, 30));
        btn_page_end->setMaximumSize(QSize(40, 30));

        horizontalLayout_3->addWidget(btn_page_end);


        verticalLayout->addLayout(horizontalLayout_3);


        retranslateUi(SysLogMgmt);

        QMetaObject::connectSlotsByName(SysLogMgmt);
    } // setupUi

    void retranslateUi(QWidget *SysLogMgmt)
    {
        SysLogMgmt->setWindowTitle(QCoreApplication::translate("SysLogMgmt", "Form", nullptr));
        label_5->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\346\250\241\345\235\227", nullptr));
        label->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\345\206\205\345\256\271", nullptr));
        label_3->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\344\272\272\345\221\230", nullptr));
        label_4->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\346\227\266\351\227\264", nullptr));
        dte_start->setDisplayFormat(QCoreApplication::translate("SysLogMgmt", "yyyy-MM-dd hh:mm:ss", nullptr));
        label_2->setText(QCoreApplication::translate("SysLogMgmt", "~", nullptr));
        dte_end->setDisplayFormat(QCoreApplication::translate("SysLogMgmt", "yyyy-MM-dd hh:mm:ss", nullptr));
        btn_search->setText(QCoreApplication::translate("SysLogMgmt", "\346\237\245  \350\257\242", nullptr));
        btn_reset->setText(QCoreApplication::translate("SysLogMgmt", "\351\207\215  \347\275\256", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("SysLogMgmt", "#", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\346\250\241\345\235\227", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\350\257\246\346\203\205", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\345\221\230\350\264\246\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\345\221\230\345\247\223\345\220\215", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("SysLogMgmt", "\346\223\215\344\275\234\347\212\266\346\200\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("SysLogMgmt", "IP\345\234\260\345\235\200", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("SysLogMgmt", "\346\227\266\351\227\264", nullptr));
        btn_page_head->setText(QCoreApplication::translate("SysLogMgmt", "<<", nullptr));
        btn_page_minus->setText(QCoreApplication::translate("SysLogMgmt", "<", nullptr));
        le_nowpage->setText(QCoreApplication::translate("SysLogMgmt", "1", nullptr));
        lb_totolpage->setText(QCoreApplication::translate("SysLogMgmt", "/10000", nullptr));
        btn_page_plus->setText(QCoreApplication::translate("SysLogMgmt", ">", nullptr));
        btn_page_end->setText(QCoreApplication::translate("SysLogMgmt", ">>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SysLogMgmt: public Ui_SysLogMgmt {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYSLOGMGMT_H
