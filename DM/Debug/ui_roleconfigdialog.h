/********************************************************************************
** Form generated from reading UI file 'roleconfigdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ROLECONFIGDIALOG_H
#define UI_ROLECONFIGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_RoleConfigDialog
{
public:
    QGridLayout *gridLayout;
    QLineEdit *le_pos;
    QSpacerItem *verticalSpacer;
    QLabel *label;
    QHBoxLayout *horizontalLayout;
    QPushButton *btn_save;
    QPushButton *btn_cancel;
    QLineEdit *le_name;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label_4;
    QComboBox *cmb_type;
    QLabel *label_3;
    QLineEdit *le_remark;
    QLabel *label_2;
    QLabel *label_5;
    QComboBox *cmb_status;

    void setupUi(QDialog *RoleConfigDialog)
    {
        if (RoleConfigDialog->objectName().isEmpty())
            RoleConfigDialog->setObjectName(QString::fromUtf8("RoleConfigDialog"));
        RoleConfigDialog->resize(400, 300);
        gridLayout = new QGridLayout(RoleConfigDialog);
        gridLayout->setSpacing(9);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(15, 20, 15, 20);
        le_pos = new QLineEdit(RoleConfigDialog);
        le_pos->setObjectName(QString::fromUtf8("le_pos"));

        gridLayout->addWidget(le_pos, 3, 1, 1, 2);

        verticalSpacer = new QSpacerItem(20, 60, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 6, 1, 1, 1);

        label = new QLabel(RoleConfigDialog);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btn_save = new QPushButton(RoleConfigDialog);
        btn_save->setObjectName(QString::fromUtf8("btn_save"));

        horizontalLayout->addWidget(btn_save);

        btn_cancel = new QPushButton(RoleConfigDialog);
        btn_cancel->setObjectName(QString::fromUtf8("btn_cancel"));

        horizontalLayout->addWidget(btn_cancel);


        gridLayout->addLayout(horizontalLayout, 7, 2, 1, 1);

        le_name = new QLineEdit(RoleConfigDialog);
        le_name->setObjectName(QString::fromUtf8("le_name"));

        gridLayout->addWidget(le_name, 0, 1, 1, 2);

        horizontalSpacer_2 = new QSpacerItem(127, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 7, 1, 1, 1);

        label_4 = new QLabel(RoleConfigDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        cmb_type = new QComboBox(RoleConfigDialog);
        cmb_type->addItem(QString());
        cmb_type->addItem(QString());
        cmb_type->addItem(QString());
        cmb_type->setObjectName(QString::fromUtf8("cmb_type"));

        gridLayout->addWidget(cmb_type, 1, 1, 1, 2);

        label_3 = new QLabel(RoleConfigDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        le_remark = new QLineEdit(RoleConfigDialog);
        le_remark->setObjectName(QString::fromUtf8("le_remark"));

        gridLayout->addWidget(le_remark, 2, 1, 1, 2);

        label_2 = new QLabel(RoleConfigDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        label_5 = new QLabel(RoleConfigDialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 1);

        cmb_status = new QComboBox(RoleConfigDialog);
        cmb_status->addItem(QString());
        cmb_status->addItem(QString());
        cmb_status->setObjectName(QString::fromUtf8("cmb_status"));

        gridLayout->addWidget(cmb_status, 4, 1, 1, 2);


        retranslateUi(RoleConfigDialog);

        QMetaObject::connectSlotsByName(RoleConfigDialog);
    } // setupUi

    void retranslateUi(QDialog *RoleConfigDialog)
    {
        RoleConfigDialog->setWindowTitle(QCoreApplication::translate("RoleConfigDialog", "Dialog", nullptr));
        le_pos->setText(QString());
        label->setText(QCoreApplication::translate("RoleConfigDialog", "\350\247\222\350\211\262\345\220\215\347\247\260*", nullptr));
        btn_save->setText(QCoreApplication::translate("RoleConfigDialog", "\347\241\256\345\256\232", nullptr));
        btn_cancel->setText(QCoreApplication::translate("RoleConfigDialog", "\345\217\226\346\266\210", nullptr));
        label_4->setText(QCoreApplication::translate("RoleConfigDialog", "\346\216\222\345\272\217", nullptr));
        cmb_type->setItemText(0, QCoreApplication::translate("RoleConfigDialog", "\344\270\252\344\272\272", nullptr));
        cmb_type->setItemText(1, QCoreApplication::translate("RoleConfigDialog", "\351\203\250\351\227\250", nullptr));
        cmb_type->setItemText(2, QCoreApplication::translate("RoleConfigDialog", "\345\205\250\351\203\250", nullptr));

        label_3->setText(QCoreApplication::translate("RoleConfigDialog", "\345\244\207\346\263\250", nullptr));
        label_2->setText(QCoreApplication::translate("RoleConfigDialog", "\346\225\260\346\215\256\347\261\273\345\236\213*", nullptr));
        label_5->setText(QCoreApplication::translate("RoleConfigDialog", "\347\212\266\346\200\201", nullptr));
        cmb_status->setItemText(0, QCoreApplication::translate("RoleConfigDialog", "\347\246\201\347\224\250", nullptr));
        cmb_status->setItemText(1, QCoreApplication::translate("RoleConfigDialog", "\345\220\257\347\224\250", nullptr));

    } // retranslateUi

};

namespace Ui {
    class RoleConfigDialog: public Ui_RoleConfigDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ROLECONFIGDIALOG_H
