/********************************************************************************
** Form generated from reading UI file 'usermgmt.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERMGMT_H
#define UI_USERMGMT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UserMgmt
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *le_login_name;
    QLabel *label_2;
    QLineEdit *le_username;
    QPushButton *btn_search;
    QPushButton *btn_reset;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btn_add;
    QPushButton *btn_batch_del;
    QSpacerItem *horizontalSpacer_2;
    QTableWidget *tableWidget;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btn_page_head;
    QPushButton *btn_page_minus;
    QLineEdit *lineEdit_nowPage;
    QLabel *label_pages;
    QPushButton *btn_page_plus;
    QPushButton *btn_page_end;

    void setupUi(QWidget *UserMgmt)
    {
        if (UserMgmt->objectName().isEmpty())
            UserMgmt->setObjectName(QString::fromUtf8("UserMgmt"));
        UserMgmt->resize(865, 685);
        UserMgmt->setStyleSheet(QString::fromUtf8("*{\n"
"	font: 12pt \"Microsoft YaHei UI\";\n"
"}\n"
"QLineEdit,QDateTimeEdit,QTextEdit{\n"
"	border-radius: 8px;\n"
"	border: 1px solid rgb(0, 0, 0);\n"
"}\n"
"QPushButton{\n"
"	border-radius: 8px;\n"
"	border: 1px solid rgb(0, 0, 0);\n"
"}\n"
"\n"
"QComboBox {\n"
"\342\200\213    border: 2px solid #f3f3f3;/*\350\256\276\347\275\256\347\272\277\345\256\275*/\n"
"\342\200\213	/*background-color: rgb(237, 242, 255);\350\203\214\346\231\257\351\242\234\350\211\262*/\n"
"\342\200\213    border-radius: 8px;/*\345\234\206\350\247\222*/\n"
"\342\200\213    padding: 1px 2px 1px 2px;  /*\351\222\210\345\257\271\344\272\216\347\273\204\345\220\210\346\241\206\344\270\255\347\232\204\346\226\207\346\234\254\345\206\205\345\256\271*/\n"
"\342\200\213	text-align:bottom;\n"
"\342\200\213    min-width: 9em;   /*# \347\273\204\345\220\210\346\241\206\347\232\204\346\234\200\345\260\217\345\256\275\345\272\246*/\n"
"\342\200\213    /*min-height: 5em;*/\n"
"\342\200\213	border-style:solid;/*\350\276\271\346\241\206\344\270\272\345"
                        "\256\236\347\272\277\345\236\213*/\n"
"\342\200\213	border-width:2px;/*\350\276\271\346\241\206\345\256\275\345\272\246*/\n"
"\342\200\213	border-color:black;/*\350\276\271\346\241\206\351\242\234\350\211\262*/\n"
"\342\200\213	padding-left: 10px;/*\345\267\246\344\276\247\350\276\271\350\267\235*/\n"
"}"));
        verticalLayout = new QVBoxLayout(UserMgmt);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(UserMgmt);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        le_login_name = new QLineEdit(UserMgmt);
        le_login_name->setObjectName(QString::fromUtf8("le_login_name"));
        le_login_name->setMinimumSize(QSize(0, 30));

        horizontalLayout->addWidget(le_login_name);

        label_2 = new QLabel(UserMgmt);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout->addWidget(label_2);

        le_username = new QLineEdit(UserMgmt);
        le_username->setObjectName(QString::fromUtf8("le_username"));
        le_username->setMinimumSize(QSize(0, 30));

        horizontalLayout->addWidget(le_username);

        btn_search = new QPushButton(UserMgmt);
        btn_search->setObjectName(QString::fromUtf8("btn_search"));
        btn_search->setMinimumSize(QSize(90, 40));
        btn_search->setStyleSheet(QString::fromUtf8("\n"
"border-radius: 8px;\n"
"color:white;\n"
"font-weight:bold;\n"
"background:rgb(24, 144, 255);"));

        horizontalLayout->addWidget(btn_search);

        btn_reset = new QPushButton(UserMgmt);
        btn_reset->setObjectName(QString::fromUtf8("btn_reset"));
        btn_reset->setMinimumSize(QSize(90, 40));
        btn_reset->setStyleSheet(QString::fromUtf8("border: 2px solid rgb(0, 0, 0);\n"
"border-radius: 8px;\n"
"font-weight:bold;\n"
""));

        horizontalLayout->addWidget(btn_reset);

        horizontalSpacer = new QSpacerItem(268, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        btn_add = new QPushButton(UserMgmt);
        btn_add->setObjectName(QString::fromUtf8("btn_add"));
        btn_add->setMinimumSize(QSize(90, 40));
        btn_add->setStyleSheet(QString::fromUtf8("\n"
"border-radius: 8px;\n"
"color:white;\n"
"font-weight:bold;\n"
"background:rgb(24, 144, 255);"));

        horizontalLayout_2->addWidget(btn_add);

        btn_batch_del = new QPushButton(UserMgmt);
        btn_batch_del->setObjectName(QString::fromUtf8("btn_batch_del"));
        btn_batch_del->setMinimumSize(QSize(90, 40));
        btn_batch_del->setStyleSheet(QString::fromUtf8("border: 2px solid rgb(0, 0, 0);\n"
"border-radius: 8px;\n"
"font-weight:bold;\n"
""));

        horizontalLayout_2->addWidget(btn_batch_del);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout_2);

        tableWidget = new QTableWidget(UserMgmt);
        if (tableWidget->columnCount() < 11)
            tableWidget->setColumnCount(11);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(8, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(9, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(10, __qtablewidgetitem10);
        if (tableWidget->rowCount() < 1)
            tableWidget->setRowCount(1);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(0, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget->setItem(0, 3, __qtablewidgetitem12);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setStyleSheet(QString::fromUtf8("/*tabelwidget*/\n"
"QTableView{\n"
"	color:#DCDCDC;\n"
"	background:#444444;\n"
"	border:1px solid #242424;\n"
"	alternate-background-color:#525252;/*\344\272\244\351\224\231\351\242\234\350\211\262*/\n"
"	gridline-color:#242424;\n"
"}\n"
"QTableView::indicator {\n"
"        width: 20px;\n"
"        height: 20px;\n"
"}\n"
"\n"
"QTableView::item{\n"
"    text-align:center;\n"
"}\n"
"\n"
"/*\351\200\211\344\270\255item*/\n"
"QTableView::item:selected{\n"
"	color:#DCDCDC;\n"
"	background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #484848,stop:1 #383838);\n"
"}\n"
"\n"
"/*\n"
"\346\202\254\346\265\256item*/\n"
"QTableView::item:hover{\n"
"	background:#5B5B5B;\n"
"}\n"
"/*\350\241\250\345\244\264*/\n"
"QHeaderView::section{\n"
"	text-align:center;\n"
"	background:#5E5E5E;\n"
"	padding:3px;\n"
"	margin:0px;\n"
"	color:#DCDCDC;\n"
"	border:1px solid #242424;\n"
"	border-left-width:0;\n"
"}\n"
"\n"
"\n"
"\n"
"/*\350\241\250\345\217\263\344\276\247\347\232\204\346\273\221\346\235\241*/\n"
"QScrollBar:vertica"
                        "l{\n"
"	background:#484848;\n"
"	padding:0px;\n"
"	border-radius:6px;\n"
"	max-width:12px;\n"
"}\n"
"\n"
"/*\346\273\221\345\235\227*/\n"
"QScrollBar::handle:vertical{\n"
"	background:#CCCCCC;\n"
"}\n"
"/*\n"
"\346\273\221\345\235\227\346\202\254\346\265\256\357\274\214\346\214\211\344\270\213*/\n"
"QScrollBar::handle:hover:vertical,QScrollBar::handle:pressed:vertical{\n"
"	background:#A7A7A7;\n"
"}\n"
"/*\n"
"\346\273\221\345\235\227\345\267\262\347\273\217\345\210\222\350\277\207\347\232\204\345\214\272\345\237\237*/\n"
"QScrollBar::sub-page:vertical{\n"
"	background:444444;\n"
"}\n"
"\n"
"/*\n"
"\346\273\221\345\235\227\350\277\230\346\262\241\346\234\211\345\210\222\350\277\207\347\232\204\345\214\272\345\237\237*/\n"
"QScrollBar::add-page:vertical{\n"
"	background:5B5B5B;\n"
"}\n"
"\n"
"/*\351\241\265\351\235\242\344\270\213\347\247\273\347\232\204\346\214\211\351\222\256*/\n"
"QScrollBar::add-line:vertical{\n"
"	background:none;\n"
"}\n"
"/*\351\241\265\351\235\242\344\270\212\347\247\273\347\232\204\346"
                        "\214\211\351\222\256*/\n"
"QScrollBar::sub-line:vertical{\n"
"	background:none;\n"
"}"));
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setDragDropOverwriteMode(false);
        tableWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
        tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
        tableWidget->setSortingEnabled(true);
        tableWidget->setColumnCount(11);
        tableWidget->horizontalHeader()->setMinimumSectionSize(1);
        tableWidget->horizontalHeader()->setHighlightSections(true);
        tableWidget->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->verticalHeader()->setVisible(false);

        verticalLayout->addWidget(tableWidget);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        btn_page_head = new QPushButton(UserMgmt);
        btn_page_head->setObjectName(QString::fromUtf8("btn_page_head"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(btn_page_head->sizePolicy().hasHeightForWidth());
        btn_page_head->setSizePolicy(sizePolicy);
        btn_page_head->setMinimumSize(QSize(40, 30));
        btn_page_head->setMaximumSize(QSize(40, 30));

        horizontalLayout_3->addWidget(btn_page_head);

        btn_page_minus = new QPushButton(UserMgmt);
        btn_page_minus->setObjectName(QString::fromUtf8("btn_page_minus"));
        btn_page_minus->setMinimumSize(QSize(40, 30));
        btn_page_minus->setMaximumSize(QSize(40, 30));

        horizontalLayout_3->addWidget(btn_page_minus);

        lineEdit_nowPage = new QLineEdit(UserMgmt);
        lineEdit_nowPage->setObjectName(QString::fromUtf8("lineEdit_nowPage"));
        lineEdit_nowPage->setMinimumSize(QSize(50, 30));
        lineEdit_nowPage->setMaximumSize(QSize(100, 30));
        lineEdit_nowPage->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(lineEdit_nowPage);

        label_pages = new QLabel(UserMgmt);
        label_pages->setObjectName(QString::fromUtf8("label_pages"));
        QFont font;
        font.setFamily(QString::fromUtf8("Microsoft YaHei UI"));
        font.setPointSize(12);
        font.setBold(true);
        font.setItalic(false);
        label_pages->setFont(font);
        label_pages->setStyleSheet(QString::fromUtf8("font: 700 12pt \"Microsoft YaHei UI\";"));
        label_pages->setWordWrap(false);

        horizontalLayout_3->addWidget(label_pages);

        btn_page_plus = new QPushButton(UserMgmt);
        btn_page_plus->setObjectName(QString::fromUtf8("btn_page_plus"));
        btn_page_plus->setMinimumSize(QSize(40, 30));
        btn_page_plus->setMaximumSize(QSize(40, 30));

        horizontalLayout_3->addWidget(btn_page_plus);

        btn_page_end = new QPushButton(UserMgmt);
        btn_page_end->setObjectName(QString::fromUtf8("btn_page_end"));
        btn_page_end->setMinimumSize(QSize(40, 30));
        btn_page_end->setMaximumSize(QSize(40, 30));

        horizontalLayout_3->addWidget(btn_page_end);


        verticalLayout->addLayout(horizontalLayout_3);


        retranslateUi(UserMgmt);

        QMetaObject::connectSlotsByName(UserMgmt);
    } // setupUi

    void retranslateUi(QWidget *UserMgmt)
    {
        UserMgmt->setWindowTitle(QCoreApplication::translate("UserMgmt", "Form", nullptr));
        label->setText(QCoreApplication::translate("UserMgmt", "\347\231\273\345\275\225\345\220\215", nullptr));
        le_login_name->setText(QString());
        le_login_name->setPlaceholderText(QCoreApplication::translate("UserMgmt", "\350\257\267\350\276\223\345\205\245\345\205\263\351\224\256\345\255\227", nullptr));
        label_2->setText(QCoreApplication::translate("UserMgmt", "\347\224\250\346\210\267\345\220\215", nullptr));
        le_username->setText(QString());
        le_username->setPlaceholderText(QCoreApplication::translate("UserMgmt", "\350\257\267\350\276\223\345\205\245\345\205\263\351\224\256\345\255\227", nullptr));
        btn_search->setText(QCoreApplication::translate("UserMgmt", "\346\237\245  \350\257\242", nullptr));
        btn_reset->setText(QCoreApplication::translate("UserMgmt", "\351\207\215  \347\275\256", nullptr));
        btn_add->setText(QCoreApplication::translate("UserMgmt", "+  \346\226\260 \345\242\236", nullptr));
        btn_batch_del->setText(QCoreApplication::translate("UserMgmt", "\346\211\271\351\207\217\345\210\240\351\231\244", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("UserMgmt", "id", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("UserMgmt", "#", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("UserMgmt", "\346\223\215\344\275\234", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("UserMgmt", "\347\231\273\345\275\225\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("UserMgmt", "\347\224\250\346\210\267\345\247\223\345\220\215", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("UserMgmt", "\346\234\272\346\236\204", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("UserMgmt", "\350\201\214\344\275\215", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("UserMgmt", "\347\224\265\350\257\235\345\217\267\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(8);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("UserMgmt", "\347\224\265\345\255\220\351\202\256\347\256\261", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->horizontalHeaderItem(9);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("UserMgmt", "\347\212\266\346\200\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget->horizontalHeaderItem(10);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("UserMgmt", "\345\210\233\345\273\272\346\227\266\351\227\264", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget->verticalHeaderItem(0);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("UserMgmt", "1", nullptr));

        const bool __sortingEnabled = tableWidget->isSortingEnabled();
        tableWidget->setSortingEnabled(false);
        QTableWidgetItem *___qtablewidgetitem12 = tableWidget->item(0, 3);
        ___qtablewidgetitem12->setText(QCoreApplication::translate("UserMgmt", "\346\265\213\350\257\225\346\225\260\346\215\256", nullptr));
        tableWidget->setSortingEnabled(__sortingEnabled);

        btn_page_head->setText(QCoreApplication::translate("UserMgmt", "<<", nullptr));
        btn_page_minus->setText(QCoreApplication::translate("UserMgmt", "<", nullptr));
        lineEdit_nowPage->setText(QCoreApplication::translate("UserMgmt", "1", nullptr));
        label_pages->setText(QCoreApplication::translate("UserMgmt", "/10000", nullptr));
        btn_page_plus->setText(QCoreApplication::translate("UserMgmt", ">", nullptr));
        btn_page_end->setText(QCoreApplication::translate("UserMgmt", ">>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UserMgmt: public Ui_UserMgmt {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERMGMT_H
