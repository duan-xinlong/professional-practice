/********************************************************************************
** Form generated from reading UI file 'workorders.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORKORDERS_H
#define UI_WORKORDERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_workOrders
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_7;
    QTableWidget *tableWidget_4;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_62;
    QSpacerItem *horizontalSpacer_61;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_2;
    QTableWidget *tableWidget;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *add_btn;
    QSpacerItem *horizontalSpacer;
    QPushButton *delete_btn;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_11;
    QHBoxLayout *horizontalLayout_10;
    QCheckBox *confirm_checkBox;
    QSpacerItem *horizontalSpacer_14;
    QSpacerItem *horizontalSpacer_15;
    QComboBox *comboBox_3;
    QSpacerItem *horizontalSpacer_16;
    QHBoxLayout *horizontalLayout_11;
    QSpacerItem *horizontalSpacer_19;
    QPushButton *submitButton;
    QSpacerItem *horizontalSpacer_20;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *workOrders)
    {
        if (workOrders->objectName().isEmpty())
            workOrders->setObjectName(QString::fromUtf8("workOrders"));
        workOrders->resize(800, 600);
        centralwidget = new QWidget(workOrders);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setLayoutDirection(Qt::LeftToRight);
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        verticalLayout_7 = new QVBoxLayout(tab_3);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        tableWidget_4 = new QTableWidget(tab_3);
        if (tableWidget_4->columnCount() < 5)
            tableWidget_4->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget_4->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget_4->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget_4->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget_4->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget_4->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        tableWidget_4->setObjectName(QString::fromUtf8("tableWidget_4"));

        verticalLayout_7->addWidget(tableWidget_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_62 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_62);

        horizontalSpacer_61 = new QSpacerItem(548, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_61);


        verticalLayout_7->addLayout(horizontalLayout);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        verticalLayout_2 = new QVBoxLayout(tab_4);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        tableWidget = new QTableWidget(tab_4);
        if (tableWidget->columnCount() < 7)
            tableWidget->setColumnCount(7);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem11);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        verticalLayout_2->addWidget(tableWidget);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        add_btn = new QPushButton(tab_4);
        add_btn->setObjectName(QString::fromUtf8("add_btn"));

        horizontalLayout_2->addWidget(add_btn);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        delete_btn = new QPushButton(tab_4);
        delete_btn->setObjectName(QString::fromUtf8("delete_btn"));

        horizontalLayout_2->addWidget(delete_btn);

        horizontalSpacer_2 = new QSpacerItem(548, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label = new QLabel(tab_4);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setPointSize(11);
        label->setFont(font);

        horizontalLayout_8->addWidget(label);

        horizontalSpacer_4 = new QSpacerItem(138, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_4);

        horizontalSpacer_8 = new QSpacerItem(138, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_8);

        label_3 = new QLabel(tab_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        horizontalLayout_8->addWidget(label_3);

        horizontalSpacer_11 = new QSpacerItem(78, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_11);


        verticalLayout_2->addLayout(horizontalLayout_8);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        confirm_checkBox = new QCheckBox(tab_4);
        confirm_checkBox->setObjectName(QString::fromUtf8("confirm_checkBox"));

        horizontalLayout_10->addWidget(confirm_checkBox);

        horizontalSpacer_14 = new QSpacerItem(178, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_14);

        horizontalSpacer_15 = new QSpacerItem(148, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_15);

        comboBox_3 = new QComboBox(tab_4);
        comboBox_3->setObjectName(QString::fromUtf8("comboBox_3"));

        horizontalLayout_10->addWidget(comboBox_3);

        horizontalSpacer_16 = new QSpacerItem(168, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_16);


        verticalLayout_2->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalSpacer_19 = new QSpacerItem(398, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_19);

        submitButton = new QPushButton(tab_4);
        submitButton->setObjectName(QString::fromUtf8("submitButton"));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        submitButton->setFont(font1);

        horizontalLayout_11->addWidget(submitButton);

        horizontalSpacer_20 = new QSpacerItem(188, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_20);


        verticalLayout_2->addLayout(horizontalLayout_11);

        tabWidget->addTab(tab_4, QString());

        verticalLayout->addWidget(tabWidget);

        workOrders->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(workOrders);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        workOrders->setStatusBar(statusbar);

        retranslateUi(workOrders);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(workOrders);
    } // setupUi

    void retranslateUi(QMainWindow *workOrders)
    {
        workOrders->setWindowTitle(QCoreApplication::translate("workOrders", "MainWindow", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget_4->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("workOrders", "\347\224\237\344\272\247\344\273\273\345\212\241\346\261\240", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget_4->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("workOrders", "\344\272\247\345\223\201\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget_4->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("workOrders", "\344\272\247\345\223\201\347\274\226\347\240\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget_4->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("workOrders", "\344\272\247\345\223\201\345\261\236\346\200\247", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget_4->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("workOrders", "\344\272\247\345\223\201\350\256\241\345\210\222\347\224\237\344\272\247\346\225\260\351\207\217", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("workOrders", "\347\224\237\344\272\247\344\272\247\345\223\201\346\230\216\347\273\206", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("workOrders", "\347\224\237\344\272\247\344\273\273\345\212\241\346\261\240", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("workOrders", "\347\224\237\344\272\247\345\267\245\345\215\225\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("workOrders", "\347\224\237\344\272\247\345\267\245\345\215\225\347\274\226\345\217\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("workOrders", "\347\224\237\344\272\247\347\217\255\347\273\204\347\273\204\351\225\277", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("workOrders", "\347\224\237\344\272\247\345\267\245\345\215\225\347\212\266\346\200\201", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("workOrders", "\345\267\245\345\215\225\345\274\200\345\247\213\346\227\266\351\227\264", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("workOrders", "\345\267\245\345\215\225\347\273\223\346\235\237\346\227\266\351\227\264", nullptr));
        add_btn->setText(QCoreApplication::translate("workOrders", "\346\267\273\345\212\240", nullptr));
        add_btn->setProperty("type", QVariant(QCoreApplication::translate("workOrders", "addData", nullptr)));
        delete_btn->setText(QCoreApplication::translate("workOrders", "\345\210\240\351\231\244", nullptr));
        delete_btn->setProperty("type", QVariant(QCoreApplication::translate("workOrders", "deleteData", nullptr)));
        label->setText(QCoreApplication::translate("workOrders", " \347\241\256\350\256\244\345\267\245\345\215\225", nullptr));
        label->setProperty("type", QVariant(QCoreApplication::translate("workOrders", "title2", nullptr)));
        label_3->setText(QCoreApplication::translate("workOrders", " \350\264\250\346\243\200\345\221\230", nullptr));
        label_3->setProperty("type", QVariant(QCoreApplication::translate("workOrders", "title2", nullptr)));
        confirm_checkBox->setText(QCoreApplication::translate("workOrders", "  \347\241\256\350\256\244", nullptr));
        comboBox_3->setProperty("type", QVariant(QCoreApplication::translate("workOrders", "comboBox", nullptr)));
        submitButton->setText(QCoreApplication::translate("workOrders", "\346\217\220\344\272\244", nullptr));
        submitButton->setProperty("type", QVariant(QCoreApplication::translate("workOrders", "upload", nullptr)));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("workOrders", "\347\224\237\344\272\247\345\267\245\345\215\225\346\230\216\347\273\206", nullptr));
    } // retranslateUi

};

namespace Ui {
    class workOrders: public Ui_workOrders {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORKORDERS_H
