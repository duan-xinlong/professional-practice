/********************************************************************************
** Form generated from reading UI file 'sysconfig.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYSCONFIG_H
#define UI_SYSCONFIG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SysConfig
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *le_loginname;
    QLabel *label_5;
    QLineEdit *le_loginname_6;
    QLabel *label_2;
    QLineEdit *le_loginname_2;
    QLabel *label_6;
    QLineEdit *le_loginname_8;
    QLabel *label_3;
    QLineEdit *le_loginname_3;
    QLabel *label_7;
    QLineEdit *le_loginname_9;
    QLabel *label_4;
    QLineEdit *le_loginname_4;
    QLabel *label_8;
    QLineEdit *le_loginname_10;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btn_search;
    QPushButton *btn_reset;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *SysConfig)
    {
        if (SysConfig->objectName().isEmpty())
            SysConfig->setObjectName(QString::fromUtf8("SysConfig"));
        SysConfig->resize(787, 560);
        SysConfig->setStyleSheet(QString::fromUtf8(""));
        verticalLayout = new QVBoxLayout(SysConfig);
        verticalLayout->setSpacing(15);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(-1, 30, -1, -1);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(20);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(15, -1, 15, -1);
        label = new QLabel(SysConfig);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        le_loginname = new QLineEdit(SysConfig);
        le_loginname->setObjectName(QString::fromUtf8("le_loginname"));

        gridLayout->addWidget(le_loginname, 0, 1, 1, 1);

        label_5 = new QLabel(SysConfig);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_5, 0, 2, 1, 1);

        le_loginname_6 = new QLineEdit(SysConfig);
        le_loginname_6->setObjectName(QString::fromUtf8("le_loginname_6"));

        gridLayout->addWidget(le_loginname_6, 0, 3, 1, 1);

        label_2 = new QLabel(SysConfig);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        le_loginname_2 = new QLineEdit(SysConfig);
        le_loginname_2->setObjectName(QString::fromUtf8("le_loginname_2"));

        gridLayout->addWidget(le_loginname_2, 1, 1, 1, 1);

        label_6 = new QLabel(SysConfig);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_6, 1, 2, 1, 1);

        le_loginname_8 = new QLineEdit(SysConfig);
        le_loginname_8->setObjectName(QString::fromUtf8("le_loginname_8"));

        gridLayout->addWidget(le_loginname_8, 1, 3, 1, 1);

        label_3 = new QLabel(SysConfig);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        le_loginname_3 = new QLineEdit(SysConfig);
        le_loginname_3->setObjectName(QString::fromUtf8("le_loginname_3"));

        gridLayout->addWidget(le_loginname_3, 2, 1, 1, 1);

        label_7 = new QLabel(SysConfig);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_7, 2, 2, 1, 1);

        le_loginname_9 = new QLineEdit(SysConfig);
        le_loginname_9->setObjectName(QString::fromUtf8("le_loginname_9"));

        gridLayout->addWidget(le_loginname_9, 2, 3, 1, 1);

        label_4 = new QLabel(SysConfig);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        le_loginname_4 = new QLineEdit(SysConfig);
        le_loginname_4->setObjectName(QString::fromUtf8("le_loginname_4"));

        gridLayout->addWidget(le_loginname_4, 3, 1, 1, 1);

        label_8 = new QLabel(SysConfig);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(label_8, 3, 2, 1, 1);

        le_loginname_10 = new QLineEdit(SysConfig);
        le_loginname_10->setObjectName(QString::fromUtf8("le_loginname_10"));

        gridLayout->addWidget(le_loginname_10, 3, 3, 1, 1);


        verticalLayout->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btn_search = new QPushButton(SysConfig);
        btn_search->setObjectName(QString::fromUtf8("btn_search"));
        btn_search->setMinimumSize(QSize(90, 40));
        btn_search->setStyleSheet(QString::fromUtf8("\n"
"border-radius: 8px;\n"
"color:white;\n"
"font-weight:bold;\n"
"background:rgb(24, 144, 255);"));

        horizontalLayout->addWidget(btn_search);

        btn_reset = new QPushButton(SysConfig);
        btn_reset->setObjectName(QString::fromUtf8("btn_reset"));
        btn_reset->setMinimumSize(QSize(90, 40));
        btn_reset->setStyleSheet(QString::fromUtf8("border: 2px solid rgb(0, 0, 0);\n"
"border-radius: 8px;\n"
"font-weight:bold;\n"
""));

        horizontalLayout->addWidget(btn_reset);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);

        verticalSpacer = new QSpacerItem(20, 423, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(SysConfig);

        QMetaObject::connectSlotsByName(SysConfig);
    } // setupUi

    void retranslateUi(QWidget *SysConfig)
    {
        SysConfig->setWindowTitle(QCoreApplication::translate("SysConfig", "Form", nullptr));
        label->setText(QCoreApplication::translate("SysConfig", "\345\205\254\345\217\270\345\220\215\347\247\260", nullptr));
        label_5->setText(QCoreApplication::translate("SysConfig", "\350\201\224\347\263\273\344\272\272", nullptr));
        label_2->setText(QCoreApplication::translate("SysConfig", "\345\205\254\345\217\270\345\234\260\345\235\200", nullptr));
        label_6->setText(QCoreApplication::translate("SysConfig", "\345\205\254\345\217\270\347\224\265\350\257\235", nullptr));
        label_3->setText(QCoreApplication::translate("SysConfig", "\345\205\254\345\217\270\344\274\240\347\234\237", nullptr));
        label_7->setText(QCoreApplication::translate("SysConfig", "\345\205\254\345\217\270\351\202\256\347\274\226", nullptr));
        label_4->setText(QCoreApplication::translate("SysConfig", "\351\224\200\345\224\256\345\215\217\350\256\256", nullptr));
        label_8->setText(QCoreApplication::translate("SysConfig", "\347\231\273\345\275\225\345\220\215\347\247\260*", nullptr));
        btn_search->setText(QCoreApplication::translate("SysConfig", "\344\277\235\345\255\230", nullptr));
        btn_reset->setText(QCoreApplication::translate("SysConfig", "\351\207\215  \347\275\256", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SysConfig: public Ui_SysConfig {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYSCONFIG_H
