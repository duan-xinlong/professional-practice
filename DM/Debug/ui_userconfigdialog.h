/********************************************************************************
** Form generated from reading UI file 'userconfigdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERCONFIGDIALOG_H
#define UI_USERCONFIGDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_UserConfigDialog
{
public:
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QPushButton *btn_cancel;
    QPushButton *btn_save;
    QSpacerItem *horizontalSpacer;
    QLabel *label_2;
    QSpacerItem *verticalSpacer;
    QComboBox *cmb_org;
    QLabel *label_10;
    QLineEdit *le_username;
    QLabel *label_9;
    QLineEdit *le_sort;
    QLabel *label_6;
    QLineEdit *le_pos;
    QLineEdit *le_email;
    QLineEdit *le_phone;
    QPlainTextEdit *pte_remark;
    QLabel *label;
    QLineEdit *le_loginname;
    QLabel *label_8;
    QLabel *label_3;
    QComboBox *cmb_role;
    QLabel *label_7;
    QLabel *label_4;
    QComboBox *cmb_status;
    QLabel *label_11;

    void setupUi(QDialog *UserConfigDialog)
    {
        if (UserConfigDialog->objectName().isEmpty())
            UserConfigDialog->setObjectName(QString::fromUtf8("UserConfigDialog"));
        UserConfigDialog->resize(400, 384);
        UserConfigDialog->setStyleSheet(QString::fromUtf8(""));
        gridLayout_2 = new QGridLayout(UserConfigDialog);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        btn_cancel = new QPushButton(UserConfigDialog);
        btn_cancel->setObjectName(QString::fromUtf8("btn_cancel"));

        gridLayout->addWidget(btn_cancel, 0, 2, 1, 1);

        btn_save = new QPushButton(UserConfigDialog);
        btn_save->setObjectName(QString::fromUtf8("btn_save"));

        gridLayout->addWidget(btn_save, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 12, 1, 1, 1);

        label_2 = new QLabel(UserConfigDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer, 11, 0, 1, 1);

        cmb_org = new QComboBox(UserConfigDialog);
        cmb_org->setObjectName(QString::fromUtf8("cmb_org"));

        gridLayout_2->addWidget(cmb_org, 3, 1, 1, 1);

        label_10 = new QLabel(UserConfigDialog);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_10, 10, 0, 1, 1);

        le_username = new QLineEdit(UserConfigDialog);
        le_username->setObjectName(QString::fromUtf8("le_username"));

        gridLayout_2->addWidget(le_username, 1, 1, 1, 1);

        label_9 = new QLabel(UserConfigDialog);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_9, 7, 0, 2, 1);

        le_sort = new QLineEdit(UserConfigDialog);
        le_sort->setObjectName(QString::fromUtf8("le_sort"));

        gridLayout_2->addWidget(le_sort, 8, 1, 1, 1);

        label_6 = new QLabel(UserConfigDialog);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_6, 2, 0, 1, 1);

        le_pos = new QLineEdit(UserConfigDialog);
        le_pos->setObjectName(QString::fromUtf8("le_pos"));

        gridLayout_2->addWidget(le_pos, 4, 1, 1, 1);

        le_email = new QLineEdit(UserConfigDialog);
        le_email->setObjectName(QString::fromUtf8("le_email"));

        gridLayout_2->addWidget(le_email, 6, 1, 2, 1);

        le_phone = new QLineEdit(UserConfigDialog);
        le_phone->setObjectName(QString::fromUtf8("le_phone"));

        gridLayout_2->addWidget(le_phone, 5, 1, 1, 1);

        pte_remark = new QPlainTextEdit(UserConfigDialog);
        pte_remark->setObjectName(QString::fromUtf8("pte_remark"));

        gridLayout_2->addWidget(pte_remark, 10, 1, 2, 1);

        label = new QLabel(UserConfigDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        le_loginname = new QLineEdit(UserConfigDialog);
        le_loginname->setObjectName(QString::fromUtf8("le_loginname"));

        gridLayout_2->addWidget(le_loginname, 0, 1, 1, 1);

        label_8 = new QLabel(UserConfigDialog);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_8, 6, 0, 1, 1);

        label_3 = new QLabel(UserConfigDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_3, 4, 0, 1, 1);

        cmb_role = new QComboBox(UserConfigDialog);
        cmb_role->setObjectName(QString::fromUtf8("cmb_role"));

        gridLayout_2->addWidget(cmb_role, 2, 1, 1, 1);

        label_7 = new QLabel(UserConfigDialog);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_7, 3, 0, 1, 1);

        label_4 = new QLabel(UserConfigDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_4, 5, 0, 1, 1);

        cmb_status = new QComboBox(UserConfigDialog);
        cmb_status->addItem(QString());
        cmb_status->addItem(QString());
        cmb_status->setObjectName(QString::fromUtf8("cmb_status"));

        gridLayout_2->addWidget(cmb_status, 9, 1, 1, 1);

        label_11 = new QLabel(UserConfigDialog);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_11, 9, 0, 1, 1);


        retranslateUi(UserConfigDialog);

        QMetaObject::connectSlotsByName(UserConfigDialog);
    } // setupUi

    void retranslateUi(QDialog *UserConfigDialog)
    {
        UserConfigDialog->setWindowTitle(QCoreApplication::translate("UserConfigDialog", "Dialog", nullptr));
        btn_cancel->setText(QCoreApplication::translate("UserConfigDialog", "\345\217\226\346\266\210", nullptr));
        btn_save->setText(QCoreApplication::translate("UserConfigDialog", "\347\241\256\345\256\232", nullptr));
        label_2->setText(QCoreApplication::translate("UserConfigDialog", "\347\224\250\346\210\267\345\247\223\345\220\215*", nullptr));
        label_10->setText(QCoreApplication::translate("UserConfigDialog", "\345\244\207\346\263\250", nullptr));
        label_9->setText(QCoreApplication::translate("UserConfigDialog", "\346\216\222\345\272\217", nullptr));
        le_sort->setText(QString());
        label_6->setText(QCoreApplication::translate("UserConfigDialog", "\350\247\222\350\211\262", nullptr));
        le_email->setText(QString());
        le_phone->setText(QString());
        label->setText(QCoreApplication::translate("UserConfigDialog", "\347\231\273\345\275\225\345\220\215\347\247\260*", nullptr));
        label_8->setText(QCoreApplication::translate("UserConfigDialog", "\347\224\265\345\255\220\351\202\256\347\256\261", nullptr));
        label_3->setText(QCoreApplication::translate("UserConfigDialog", "\350\201\214\344\275\215", nullptr));
        label_7->setText(QCoreApplication::translate("UserConfigDialog", "\346\234\272\346\236\204", nullptr));
        label_4->setText(QCoreApplication::translate("UserConfigDialog", "\347\224\265\350\257\235\345\217\267\347\240\201", nullptr));
        cmb_status->setItemText(0, QCoreApplication::translate("UserConfigDialog", "\347\246\201\347\224\250", nullptr));
        cmb_status->setItemText(1, QCoreApplication::translate("UserConfigDialog", "\345\220\257\347\224\250", nullptr));

        label_11->setText(QCoreApplication::translate("UserConfigDialog", "\347\212\266\346\200\201", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UserConfigDialog: public Ui_UserConfigDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERCONFIGDIALOG_H
