﻿#include "Login/logindialog.h"
#include "DataBase/database.h"
#include "mainwindow.h"

#include <QApplication>
#include <QFile>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //读取按钮样式表
    QFile qss(":/Resources/qss.qss");
    QString style;
    qss.open(QFile::ReadOnly);
    if (qss.isOpen())
    {
        style = QLatin1String(qss.readAll());
        qss.close();
    }
    a.setStyleSheet(style);

    //启动登录界面
    LoginDialog w;
    w.show();

    //连接数据库
    mydb = new Database();

    return a.exec();
}
