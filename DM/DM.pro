QT       += core gui sql charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    DataBase/database.cpp \
    Login/logindialog.cpp \
    Login/personcenter.cpp \
    Planning/MPS.cpp \
    Planning/picking.cpp \
    Planning/reporting.cpp \
    Planning/warehousing.cpp \
    Planning/workorders.cpp \
    Purchase/buyerdata.cpp \
    Purchase/materialdata.cpp \
    Purchase/purchaseorder.cpp \
    Purchase/supplierdata.cpp \
    Sale/barchart.cpp \
    Sale/clientchoose.cpp \
    Sale/linechart.cpp \
    Sale/picture.cpp \
    Sale/saleschange.cpp \
    Sale/salesorder.cpp \
    Sale/salesreturn.cpp \
    Sale/salesstatistics.cpp \
    Storage/put_in_storage.cpp \
    Storage/put_out_storage.cpp \
    Storage/storage_stat.cpp \
    Storage/take_stock.cpp \
    Storage/transfer_storage.cpp \
    System/orgmgmt.cpp \
    System/roleaccessdialog.cpp \
    System/roleconfigdialog.cpp \
    System/rolemgmt.cpp \
    System/roletoolbox.cpp \
    System/sysconfig.cpp \
    System/syslogmgmt.cpp \
    System/userconfigdialog.cpp \
    System/usermgmt.cpp \
    System/usertoolbox.cpp \
    Tool/button_day.cpp \
    Tool/data_choose.cpp \
    Tool/database_comboBox.cpp \
    Tool/date_edit.cpp \
    Tool/date_time_edit.cpp \
    Tool/date_time_widget.cpp \
    Tool/date_widget.cpp \
    Tool/time_scrollbar.cpp \
    Tool/toolfunction.cpp \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    DataBase/database.h \
    Login/logindialog.h \
    Login/personcenter.h \
    Planning/MPS.h \
    Planning/picking.h \
    Planning/reporting.h \
    Planning/warehousing.h \
    Planning/workorders.h \
    Purchase/buyerdata.h \
    Purchase/materialdata.h \
    Purchase/purchaseorder.h \
    Purchase/supplierdata.h \
    Sale/barchart.h \
    Sale/clientchoose.h \
    Sale/linechart.h \
    Sale/picture.h \
    Sale/saleschange.h \
    Sale/salesorder.h \
    Sale/salesreturn.h \
    Sale/salesstatistics.h \
    Storage/put_in_storage.h \
    Storage/put_out_storage.h \
    Storage/storage_stat.h \
    Storage/take_stock.h \
    Storage/transfer_storage.h \
    System/orgmgmt.h \
    System/roleaccessdialog.h \
    System/roleconfigdialog.h \
    System/rolemgmt.h \
    System/roletoolbox.h \
    System/sysconfig.h \
    System/syslogmgmt.h \
    System/userconfigdialog.h \
    System/usermgmt.h \
    System/usertoolbox.h \
    Tool/button_day.h \
    Tool/data_choose.h \
    Tool/database_comboBox.h \
    Tool/date_edit.h \
    Tool/date_time_defines.h \
    Tool/date_time_edit.h \
    Tool/date_time_widget.h \
    Tool/date_widget.h \
    Tool/time_scrollbar.h \
    Tool/toolfunction.h \
    mainwindow.h

FORMS += \
    Login/logindialog.ui \
    Login/personcenter.ui \
    Planning/mps.ui \
    Planning/picking.ui \
    Planning/reporting.ui \
    Planning/warehousing.ui \
    Planning/workorders.ui \
    Purchase/buyerdata.ui \
    Purchase/materialdata.ui \
    Purchase/purchaseorder.ui \
    Purchase/supplierdata.ui \
    Sale/barchart.ui \
    Sale/clientchoose.ui \
    Sale/linechart.ui \
    Sale/saleschange.ui \
    Sale/salesorder.ui \
    Sale/salesreturn.ui \
    Sale/salesstatistics.ui \
    Storage/put_in_storage.ui \
    Storage/put_out_storage.ui \
    Storage/storage_stat.ui \
    Storage/take_stock.ui \
    Storage/transfer_storage.ui \
    System/orgmgmt.ui \
    System/roleaccessdialog.ui \
    System/roleconfigdialog.ui \
    System/rolemgmt.ui \
    System/roletoolbox.ui \
    System/sysconfig.ui \
    System/syslogmgmt.ui \
    System/userconfigdialog.ui \
    System/usermgmt.ui \
    System/usertoolbox.ui \
    Tool/data_choose.ui \
    Tool/date_time_widget.ui \
    Tool/date_widget.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    main.qrc
