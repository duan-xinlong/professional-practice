﻿#ifndef WORKORDERS_H
#define WORKORDERS_H

#include <QMainWindow>
#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include "Tool/data_choose.h"
namespace Ui {
class workOrders;
}

class workOrders : public QMainWindow
{
    Q_OBJECT

public:
    explicit workOrders(QWidget *parent = nullptr);
    ~workOrders();
    int iPosRow;
    std::vector<int> vecItemIndex;//用于保存选中行的行号

    DataChoose *dc;
    DataChoose *dm;
    int row;    //  当前行
    int l = 0;
    int k=0;
    QMenu *RightClickMenu;  //右键点击菜单
    QAction *m_addAction;     //插入行
    QAction *m_deleteAction;  //删除行

    QLabel *tip_type_combo;
    QLabel *tip_storage_combo;
    QLabel *tip_tableWidget;
    QLabel *tip_confirm_checkbox;
    QLabel *tip_datatimeEdit;
    QLabel *tip_select_member;
private slots:

    void on_add_btn_clicked();



    void RowInit(int index); //行的每个单元格初始化
 //   void RowInit_1(int index);
    void RowInit_2(int index);
    void RowDelete(int index);  //删除行
//    void RowDelete_1(int index);
    void RowDelete_2(int index);
    void choose_btn_clicked();
  //  void choose_btn_2_clicked();
    void choose_btn_3_clicked();
   // void on_add_btn_2_clicked();

  //  void on_delete_btn_2_clicked();

    void on_delete_btn_clicked();

    void on_submitButton_clicked();

private:
    Ui::workOrders *ui;
};

#endif // WORKORDERS_H
