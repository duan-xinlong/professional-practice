﻿#include "picking.h"
#include "ui_picking.h"

Picking::Picking(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Picking)
{
    ui->setupUi(this);
    ui->tableWidget->verticalHeader()->setVisible(false);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Fixed);//第二列“产品选择”宽度设置
    ui->tableWidget->setColumnWidth(1,75);

    ui->tableWidget_2->verticalHeader()->setVisible(false);
    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget_2->setFocusPolicy(Qt::NoFocus);
    ui->tableWidget_2->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Fixed);//第二列“产品选择”宽度设置
    ui->tableWidget_2->setColumnWidth(1,75);


    tip_confirm_checkbox=new QLabel(this);

    RightClickMenu=new QMenu(ui->tableWidget);
    m_addAction=new QAction(QString::fromUtf8("插入"),this);
    m_deleteAction=new QAction(QString::fromUtf8("删除"),this);
    RightClickMenu->addAction(m_addAction);
    RightClickMenu->addAction(m_deleteAction);

    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(RightClickSlot(QPoint)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickAddRow(QAction*)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickDeleteRow(QAction*)));

    //查询1
    QSqlQuery sql_query;
    sql_query.exec("select * from picking order by 序号 asc");
    if(!sql_query.exec())
     {
       qDebug()<<sql_query.lastError();
     }
    else
     {   //行标志
       while(sql_query.next())
         {
           QString  no= sql_query.value(0).toString();
           QString  name= sql_query.value(1).toString();
           QString  code= sql_query.value(2).toString();
           QString neednum = sql_query.value(3).toString();
           QString  gotnum= sql_query.value(4).toString();
           QString batchno=sql_query.value(5).toString();
           QString avainum=sql_query.value(6).toString();
           QString outnum=sql_query.value(7).toString();
           QString unit=sql_query.value(8).toString();
           QString trackno=sql_query.value(10).toString();
           qDebug()<<name;

           ui->tableWidget->setRowCount(k+1);

           ui->tableWidget->setItem(k,0,new QTableWidgetItem(no));
           ui->tableWidget->item(k, 0)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,1,new QTableWidgetItem(""));
           ui->tableWidget->item(k, 1)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,2,new QTableWidgetItem(name));
           ui->tableWidget->item(k, 2)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,3,new QTableWidgetItem(code));
           ui->tableWidget->item(k, 3)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,4,new QTableWidgetItem(neednum));
           ui->tableWidget->item(k, 4)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,5,new QTableWidgetItem(gotnum));
           ui->tableWidget->item(k, 5)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,6,new QTableWidgetItem(batchno));
           ui->tableWidget->item(k, 6)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,7,new QTableWidgetItem(avainum));
           ui->tableWidget->item(k, 7)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,8,new QTableWidgetItem(outnum));
           ui->tableWidget->item(k, 8)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,9,new QTableWidgetItem(unit));
           ui->tableWidget->item(k, 9)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,10,new QTableWidgetItem(trackno));
           ui->tableWidget->item(k, 10)->setTextAlignment(Qt::AlignCenter);//居中显示
           k+=1;
           }
     }

    //查询2
//    QSqlQuery sql_query;
    sql_query.exec("select * from returning order by 序号 asc");
    if(!sql_query.exec())
     {
       qDebug()<<sql_query.lastError();
     }
    else
     {   //行标志
       while(sql_query.next())
         {
           QString  no= sql_query.value(0).toString();
           QString  name= sql_query.value(1).toString();
           QString  code= sql_query.value(2).toString();
           QString batchno = sql_query.value(3).toString();
           QString  tostorenum= sql_query.value(4).toString();
           QString unit=sql_query.value(5).toString();
           qDebug()<<name;

           ui->tableWidget_2->setRowCount(j+1);

           ui->tableWidget_2->setItem(j,0,new QTableWidgetItem(no));
           ui->tableWidget_2->item(j, 0)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_2->setItem(j,1,new QTableWidgetItem(""));
           ui->tableWidget_2->item(j, 1)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_2->setItem(j,2,new QTableWidgetItem(name));
           ui->tableWidget_2->item(j, 2)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_2->setItem(j,3,new QTableWidgetItem(code));
           ui->tableWidget_2->item(j, 3)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_2->setItem(j,4,new QTableWidgetItem(batchno));
           ui->tableWidget_2->item(j, 4)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_2->setItem(j,5,new QTableWidgetItem(tostorenum));
           ui->tableWidget_2->item(j, 5)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_2->setItem(j,6,new QTableWidgetItem(unit));
           ui->tableWidget_2->item(j, 6)->setTextAlignment(Qt::AlignCenter);//居中显示
           j+=1;
           }
     }

    //准备combobox---ok
    ui->order_combo->clear();
    sql_query.prepare("select distinct 生产报工单编号 from reporting_planning ");
    sql_query.exec();
    QSqlRecord rec = sql_query.record();
//    for(int i = 0; i < rec.count(); i++)
//        qDebug() << rec.fieldName(i);
    while(sql_query.next())
    {
        int orderplace = rec.indexOf("生产报工单编号");
        QString  order= sql_query.value(orderplace).toString();
        ui->order_combo->addItem(order);
        qDebug()<<order;
    }
}

Picking::~Picking()
{
    delete ui;
}

void Picking::RowInit(int index)
{
    ui->tableWidget->insertRow(index); //在最后一行的后面插入一行

    //在插入的行中输入产品选择按钮从数据库获得产品数据
    QPushButton *choose_btn = new QPushButton();
    choose_btn->setObjectName("choose_btn" + QString::number(index));
    choose_btn->setProperty("type", "databaseBtn"); //数据库按钮样式

    QWidget *widget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setMargin(0);//一定要有
    layout->addWidget(choose_btn);
    layout->setAlignment(choose_btn, Qt::AlignCenter);//控件在布局中居中显示
    widget->setLayout(layout);

    ui->tableWidget->setCellWidget(index, 1, widget);//放在表格第2列
    connect(choose_btn, &QPushButton::clicked, this, &Picking::choose_btn_clicked);

    for(int i=0;i<ui->tableWidget->columnCount();i++)
    {
        if(i==1)    //为按钮列
            continue;
        QTableWidgetItem *item = new QTableWidgetItem();
        ui->tableWidget->setItem(index,i,item);
        ui->tableWidget->item(index,i)->setTextAlignment(Qt::AlignCenter);
    }
}

void Picking::RowDelete(int index)
{
    if(index!=-1)
    {
        for(int i=0;i<ui->tableWidget->columnCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(index,i);
            //确保不为空
            if(widget)
            {
                ui->tableWidget->removeCellWidget(index,i);
                delete widget;  //释放
            }
        }
        ui->tableWidget->removeRow(index);
    }
}

void Picking::RightClickSlot(QPoint pos)
{

    QModelIndex index = ui->tableWidget->indexAt(pos);    //找到tableview当前位置信息
    iPosRow = index.row();    //获取到了当前右键所选的行数
    if(index.isValid())        //如果行数有效，则显示菜单
    {
        RightClickMenu->exec(QCursor::pos());
    }

}

void Picking::RightClickAddRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("插入"))   //选中了插入这个菜单
    {
        RowInit(iPosRow);
    }
}

void Picking::RightClickDeleteRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("删除"))   //选中了删除这个菜单
    {
        RowDelete(iPosRow);
    }
}

void Picking::choose_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    //按钮状态选择
    if(btn->property("type")=="check")
    {
        btn->setProperty("type","checked");
        style()->polish(btn);
        vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn->property("type")=="checked")
    {
        btn->setProperty("type","check");
        style()->polish(btn);
        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
    else if(btn->property("type")=="databaseBtn")
    {
        //创建数据选择子界面
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择产品");
        dc->myModel->setQuery(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table"));
        dc->myModel->setHeaderData(0,Qt::Horizontal,"产品编码");
        dc->myModel->setHeaderData(1,Qt::Horizontal,"产品名称");
        dc->myModel->setHeaderData(2,Qt::Horizontal,"产品类型");
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            for(int i=0;i<list.size();i++)
            {
                QTableWidgetItem *item = new QTableWidgetItem(list[i].data().toString());
                switch (i) {
                case 0:
                    ui->tableWidget->item(row,3)->setText(list[i].data().toString());
                    qDebug()<<ui->tableWidget->item(row,3)->text();
                    break;
                case 1:
                    ui->tableWidget->item(row,2)->setText(list[i].data().toString());
                    break;
//                case 2:
//                    ui->tableWidget->item(row,4)->setText(list[i].data().toString());
//                    break;
                default:
                    break;
                }
            }
            ui->tableWidget->setItem(row,0,new QTableWidgetItem(QString::number(row+1,10)));
            ui->tableWidget->item(row, 0)->setTextAlignment(Qt::AlignCenter);//居中显示
            qDebug()<<"row:"<<row;
            //填写信息：需求数量、已领料数量、当前可用库存数量、本次出库数量
            QSqlQuery query;
            int num = 0;//成品需求数量
            int num1 = 0,num2 = 0,num3 = 0,num4 = 0,already = 0;
            int stock = 0,temp = 0;
            query.exec(QString("select 本次报工数量 from reporting_planning where 生产报工单编号 = '%1'").arg(ui->order_combo->currentText()));
            while(query.next())
            {
                num = query.value(0).toInt();
                qDebug()<<"需求数量：   "<<num;
            }
            num1 = 2*num;
            num2 = 2*num;
            num3 = num;
            num4 = 2*num;
            qDebug()<<ui->tableWidget->item(row,3)->text()<<"row = "<<row;
            if(ui->tableWidget->item(row,3)->text()=="Y0001")
            {
                ui->tableWidget->item(row,4)->setText(QString::number(num1));
            }
            if(ui->tableWidget->item(row,3)->text()=="Y0002")
            {
                ui->tableWidget->item(row,4)->setText(QString::number(num2));
            }
            if(ui->tableWidget->item(row,3)->text()=="X03")
            {
                ui->tableWidget->item(row,4)->setText(QString::number(num3));
            }
            if(ui->tableWidget->item(row,3)->text()=="X04")
            {
                ui->tableWidget->item(row,4)->setText(QString::number(num4));
            }
            query.exec(QString("select 本次出库数量 from picking where 生产报工单编号 = '%1' and 产品编码 = '%2'").arg(ui->order_combo->currentText()).arg(ui->tableWidget->item(row,3)->text()));
//            qDebug()<<QString("select 本次出库数量 from picking where 生产报工单编号 = '%1' and 产品编码 = '%2'").arg(ui->order_combo->currentText()).arg(ui->tableWidget->item(row,3)->text());
            while(query.next())
            {
                already += query.value(0).toInt();
                qDebug()<<"已经数量：  "<<already;
            }
            ui->tableWidget->item(row,5)->setText(QString::number(already));
            query.exec(QString("select StockQty from product_data_table where ProductCode = '%1'").arg(ui->tableWidget->item(row,3)->text()));
            query.next();
            stock = query.value(0).toInt();
            for(int i=0;i<ui->tableWidget->rowCount();i++)
            {
                if(ui->tableWidget->item(i,3)->text()==list[0].data().toString()){
                stock -= ui->tableWidget->item(i,8)->text().toInt();}
            }
            ui->tableWidget->item(row,7)->setText(QString::number(stock));
        });


    }
}

void Picking::on_warehouse_combo_activated(const QString &arg1)
{
    QMessageBox::information(this,"选择入库负责员","待实现");
}

void Picking::on_add_btn_clicked()
{
    if(ui->add_btn->text()=="添加")
    {
        RowInit(ui->tableWidget->rowCount());
    }
    else if(ui->add_btn->text()=="取消操作")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);
    }
}

void Picking::on_delete_btn_clicked()
{
    if(ui->delete_btn->text()=="删除")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","check");
                style()->polish(btn);
            }
        }
        ui->delete_btn->setText(QString::fromUtf8("删除选中"));
        ui->delete_btn->setProperty("type","deleteConfirm");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("取消操作"));
        ui->add_btn->setProperty("type","deleteCancel");
        style()->polish(ui->add_btn);
    }
    else if(ui->delete_btn->text()=="删除选中")
    {
        sort(vecItemIndex.rbegin(), vecItemIndex.rend());
        for (unsigned int i = 0 ; i < vecItemIndex.size(); i++)
        {
            RowDelete(vecItemIndex[i]);
        }
        vecItemIndex.clear();  //清空容器
        vecItemIndex.shrink_to_fit();   //将容器的容量缩减至和实际存储元素的个数相等，释放内存

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);

        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

    }
}

void Picking::on_select_member_clicked()
{
//    QMessageBox::information(this,"选择入库负责员","待实现");
    //创建数据选择子界面
    dc=new DataChoose();
    dc->setTitle("选择出库员");
    dc->myModel->setQuery(QString("SELECT * FROM stock_out_assistant"));
    dc->myModel->setHeaderData(0,Qt::Horizontal,"出库员");
    dc->show();
    connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
            this, [this](const QModelIndexList& list)
    {
       ui->select_member->setText(list[0].data().toString());
    });
}

void Picking::on_submit_btn_clicked()
{
//    IDGenerate();
    if(ui->confirm_checkBox->isChecked())
    {
        int ret = QMessageBox::question(this, "上传入库单","确认上传", QMessageBox::Yes, QMessageBox::No);
        if(ret == QMessageBox::Yes)
        {
            int rowIdx = ui->tableWidget->rowCount();
            qDebug() << rowIdx-k;
            QSqlQuery query;
            while(rowIdx-k){
                QString item_no = ui->tableWidget->item(k,0)==nullptr? "": ui->tableWidget->item(k,0)->text();
                QString item_name = ui->tableWidget->item(k,2)==nullptr? "": ui->tableWidget->item(k,2)->text();
                QString item_code = ui->tableWidget->item(k,3)==nullptr? "": ui->tableWidget->item(k,3)->text();
                QString item_neednum = ui->tableWidget->item(k,4)==nullptr? "": ui->tableWidget->item(k,4)->text();
                QString item_gotnum = ui->tableWidget->item(k,5)==nullptr? "": ui->tableWidget->item(k,5)->text();
                QString item_batchno = ui->tableWidget->item(k,6)==nullptr? "": ui->tableWidget->item(k,6)->text();
                QString item_avainum = ui->tableWidget->item(k,7)==nullptr? "": ui->tableWidget->item(k,7)->text();
                QString item_outnum = ui->tableWidget->item(k,8)==nullptr? "": ui->tableWidget->item(k,8)->text();
                QString item_unit = ui->tableWidget->item(k,9)==nullptr? "": ui->tableWidget->item(k,9)->text();
                QString item_trackno = ui->tableWidget->item(k,10)==nullptr? "": ui->tableWidget->item(k,10)->text();
                QString insert_row = QString("insert into picking(序号,产品名称,产品编码,需求数量,已领料数量,批次,当前可用库存数量,本次出库数量,单位,生产报工单编号,生产领料单号) values(%1,'%2','%3',%4,%5,'%6',%7,%8,'%9','%10','%11')").arg(item_no).arg(item_name).arg(item_code).arg(item_neednum)
                        .arg(item_gotnum).arg(item_batchno).arg(item_avainum).arg(item_outnum).arg(item_unit).arg(ui->order_combo->currentText()).arg(item_trackno);
                qDebug() << insert_row;
                query.exec(insert_row);
                k++;
            }

            QMessageBox::information(this,"上传入库单","上传成功");
            //不再显示tip
            if(tip_confirm_checkbox)
                 tip_confirm_checkbox->hide();
        }

    }
    else
    {
        tip_confirm_checkbox->setText(QString::fromUtf8("*必填"));
        tip_confirm_checkbox->setStyleSheet("color:red;");
        QPoint posA = ui->confirm_checkBox->mapToGlobal(QPoint(0,0));
        QPoint posB = this->mapFromGlobal(QPoint(posA.x(),posA.y()+ui->confirm_checkBox->height()));
        tip_confirm_checkbox->move(posB);
        tip_confirm_checkbox->show();
    }
}

QString Picking::IDGenerate()
{
    QString type=ui->warehouse_combo->currentText(),
            time=ui->datetime->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),   //去除字符‘-’
            limit;
    if(type=="原料仓")
        limit=QString::fromUtf8("Y01");
    else if(type=="成品仓")
        limit=QString::fromUtf8("C01");

    limit += time;
    QSqlQuery query= mydb->SortData("storage_record_table","StorageRecordID",false,"StorageRecordID",limit); //降序排列

    if(query.next())
    {
        QString str=query.value(0).toString();
        QString num=NumberIncrease(str.mid(11)); //截取后四位,自增
        return limit+num;
    }
    else
        return limit+"0001";
}


void Picking::RowInit_2(int index)
{
    ui->tableWidget_2->insertRow(index); //在最后一行的后面插入一行

    //在插入的行中输入产品选择按钮从数据库获得产品数据
    QPushButton *choose_btn_2 = new QPushButton();
    choose_btn_2->setObjectName("choose_btn" + QString::number(index));
    choose_btn_2->setProperty("type", "databaseBtn"); //数据库按钮样式

    QWidget *widget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setMargin(0);//一定要有
    layout->addWidget(choose_btn_2);
    layout->setAlignment(choose_btn_2, Qt::AlignCenter);//控件在布局中居中显示
    widget->setLayout(layout);

    ui->tableWidget_2->setCellWidget(index, 1, widget);//放在表格第2列
    connect(choose_btn_2, &QPushButton::clicked, this, &Picking::choose_btn_clicked_2);

    for(int i=0;i<ui->tableWidget_2->columnCount();i++)
    {
        if(i==1)    //为按钮列
            continue;
        QTableWidgetItem *item = new QTableWidgetItem();
        ui->tableWidget_2->setItem(index,i,item);
        ui->tableWidget_2->item(index,i)->setTextAlignment(Qt::AlignCenter);
    }
}

void Picking::RowDelete_2(int index)
{
    if(index!=-1)
    {
        for(int i=0;i<ui->tableWidget_2->columnCount();i++)
        {
            QWidget *widget=ui->tableWidget_2->cellWidget(index,i);
            //确保不为空
            if(widget)
            {
                ui->tableWidget_2->removeCellWidget(index,i);
                delete widget;  //释放
            }
        }
        ui->tableWidget_2->removeRow(index);
    }
}

void Picking::RightClickSlot_2(QPoint pos)
{

    QModelIndex index = ui->tableWidget_2->indexAt(pos);    //找到tableview当前位置信息
    iPosRow = index.row();    //获取到了当前右键所选的行数
    if(index.isValid())        //如果行数有效，则显示菜单
    {
        RightClickMenu->exec(QCursor::pos());
    }

}

void Picking::RightClickAddRow_2(QAction *act)
{
    if(act->text() == QString::fromUtf8("插入"))   //选中了插入这个菜单
    {
        RowInit(iPosRow);
    }
}

void Picking::RightClickDeleteRow_2(QAction *act)
{
    if(act->text() == QString::fromUtf8("删除"))   //选中了删除这个菜单
    {
        RowDelete(iPosRow);
    }
}

void Picking::choose_btn_clicked_2()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget_2->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    //按钮状态选择
    if(btn->property("type")=="check")
    {
        btn->setProperty("type","checked");
        style()->polish(btn);
        vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn->property("type")=="checked")
    {
        btn->setProperty("type","check");
        style()->polish(btn);
        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
    else if(btn->property("type")=="databaseBtn")
    {
        dc=new DataChoose();
        dc->setTitle("选择产品");
        dc->myModel->setQuery(QString("SELECT * FROM parts"));
        dc->myModel->setHeaderData(0,Qt::Horizontal,"产品编码");
        dc->myModel->setHeaderData(1,Qt::Horizontal,"产品名称");
        dc->myModel->setHeaderData(2,Qt::Horizontal,"产品类型");
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            for(int i=0;i<list.size();i++)
            {
                switch (i) {
                case 0:
                    ui->tableWidget_2->item(row,3)->setText(list[i].data().toString());
                    break;
                case 1:
                    ui->tableWidget_2->item(row,2)->setText(list[i].data().toString());
                    break;
//                case 2:
//                    ui->tableWidget_2->item(row,4)->setText(list[i].data().toString());
//                    break;
                default:
                    break;
                }
            }
            ui->tableWidget_2->setItem(row,0,new QTableWidgetItem(QString::number(row+1,10)));
            ui->tableWidget_2->item(row, 0)->setTextAlignment(Qt::AlignCenter);//居中显示
        });


    }
}

void Picking::on_add_btn_2_clicked()
{
    if(ui->add_btn_2->text()=="添加")
    {
        RowInit_2(ui->tableWidget_2->rowCount());
    }
    else if(ui->add_btn_2->text()=="取消操作")
    {
        for(int i=0;i<ui->tableWidget_2->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget_2->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

        ui->delete_btn_2->setText(QString::fromUtf8("删除"));
        ui->delete_btn_2->setProperty("type","deleteData");
        style()->polish(ui->delete_btn_2);

        ui->add_btn_2->setText(QString::fromUtf8("添加"));
        ui->add_btn_2->setProperty("type","addData");
        style()->polish(ui->add_btn_2);
    }
}

void Picking::on_delete_btn_2_clicked()
{
    if(ui->delete_btn_2->text()=="删除")
    {
        for(int i=0;i<ui->tableWidget_2->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget_2->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","check");
                style()->polish(btn);
            }
        }
        ui->delete_btn_2->setText(QString::fromUtf8("删除选中"));
        ui->delete_btn_2->setProperty("type","deleteConfirm");
        style()->polish(ui->delete_btn_2);

        ui->add_btn_2->setText(QString::fromUtf8("取消操作"));
        ui->add_btn_2->setProperty("type","deleteCancel");
        style()->polish(ui->add_btn_2);
    }
    else if(ui->delete_btn_2->text()=="删除选中")
    {
        sort(vecItemIndex.rbegin(), vecItemIndex.rend());
        for (unsigned int i = 0 ; i < vecItemIndex.size(); i++)
        {
            RowDelete_2(vecItemIndex[i]);
        }
        vecItemIndex.clear();  //清空容器
        vecItemIndex.shrink_to_fit();   //将容器的容量缩减至和实际存储元素的个数相等，释放内存

        ui->delete_btn_2->setText(QString::fromUtf8("删除"));
        ui->delete_btn_2->setProperty("type","deleteData");
        style()->polish(ui->delete_btn_2);

        ui->add_btn_2->setText(QString::fromUtf8("添加"));
        ui->add_btn_2->setProperty("type","addData");
        style()->polish(ui->add_btn_2);

        for(int i=0;i<ui->tableWidget_2->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget_2->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

    }
}

void Picking::on_select_member_2_clicked()
{
    dc=new DataChoose();
    dc->myModel->setQuery("SELECT * FROM warehouseman");
    dc->myModel->setHeaderData(0,Qt::Horizontal,"入库员");
    dc->show();
    connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
            this, [this](const QModelIndexList& list)
    {
       ui->select_member_2->setText(list[0].data().toString());
    });
}

void Picking::on_submit_btn_2_clicked()
{
//    IDGenerate_2();
    if(ui->confirm_checkBox_2->isChecked())
    {
        int ret = QMessageBox::question(this, "上传入库单","确认上传", QMessageBox::Yes, QMessageBox::No);
        if(ret == QMessageBox::Yes)
        {
            int rowIdx = ui->tableWidget_2->rowCount();
            qDebug() << rowIdx-j;
            QSqlQuery query;
            while(rowIdx-j){
                QString item_no = ui->tableWidget_2->item(j,0)==nullptr? "": ui->tableWidget_2->item(j,0)->text();
                QString item_name = ui->tableWidget_2->item(j,2)==nullptr? "": ui->tableWidget_2->item(j,2)->text();
                QString item_code = ui->tableWidget_2->item(j,3)==nullptr? "": ui->tableWidget_2->item(j,3)->text();
                QString item_batchno = ui->tableWidget_2->item(j,4)==nullptr? "": ui->tableWidget_2->item(j,4)->text();
                QString item_tostorenum = ui->tableWidget_2->item(j,5)==nullptr? "": ui->tableWidget_2->item(j,5)->text();
                QString item_unit = ui->tableWidget_2->item(j,6)==nullptr? "": ui->tableWidget_2->item(j,6)->text();
                QString insert_row = QString("insert into returning(序号,产品名称,产品编码,产品批次号,本次入库数量,单位) values(%1,'%2','%3',%4,%5,'%6')").arg(item_no).arg(item_name).arg(item_code).arg(item_batchno).arg(item_tostorenum).arg(item_unit);
                qDebug() << insert_row;
                query.exec(insert_row);
                j++;
            }

            QMessageBox::information(this,"上传入库单","上传成功");
            //不再显示tip
            if(tip_confirm_checkbox)
                 tip_confirm_checkbox->hide();
        }

    }
    else
    {
        tip_confirm_checkbox->setText(QString::fromUtf8("*必填"));
        tip_confirm_checkbox->setStyleSheet("color:red;");
        QPoint posA = ui->confirm_checkBox->mapToGlobal(QPoint(0,0));
        QPoint posB = this->mapFromGlobal(QPoint(posA.x(),posA.y()+ui->confirm_checkBox->height()));
        tip_confirm_checkbox->move(posB);
        tip_confirm_checkbox->show();
    }
}


void Picking::on_order_combo_currentTextChanged(const QString &arg1)
{
    //查询1
//    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(0);
    QSqlQuery sql_query;
    sql_query.exec(QString("select * from picking where 生产报工单编号 = '%1' order by 序号 asc").arg(ui->order_combo->currentText()));
    if(!sql_query.exec())
     {
       qDebug()<<sql_query.lastError();
     }
    else
     {   //行标志
        k = 0;
       while(sql_query.next())
         {
           QString  no= sql_query.value(0).toString();
           QString  name= sql_query.value(1).toString();
           QString  code= sql_query.value(2).toString();
           QString neednum = sql_query.value(3).toString();
           QString  gotnum= sql_query.value(4).toString();
           QString batchno=sql_query.value(5).toString();
           QString avainum=sql_query.value(6).toString();
           QString outnum=sql_query.value(7).toString();
           QString unit=sql_query.value(8).toString();
           QString trackno=sql_query.value(10).toString();
           qDebug()<<name;

           ui->tableWidget->setRowCount(k+1);

           ui->tableWidget->setItem(k,0,new QTableWidgetItem(no));
           ui->tableWidget->item(k, 0)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,1,new QTableWidgetItem(""));
           ui->tableWidget->item(k, 1)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,2,new QTableWidgetItem(name));
           ui->tableWidget->item(k, 2)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,3,new QTableWidgetItem(code));
           ui->tableWidget->item(k, 3)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,4,new QTableWidgetItem(neednum));
           ui->tableWidget->item(k, 4)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,5,new QTableWidgetItem(gotnum));
           ui->tableWidget->item(k, 5)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,6,new QTableWidgetItem(batchno));
           ui->tableWidget->item(k, 6)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,7,new QTableWidgetItem(avainum));
           ui->tableWidget->item(k, 7)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,8,new QTableWidgetItem(outnum));
           ui->tableWidget->item(k, 8)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,9,new QTableWidgetItem(unit));
           ui->tableWidget->item(k, 9)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(k,10,new QTableWidgetItem(trackno));
           ui->tableWidget->item(k, 10)->setTextAlignment(Qt::AlignCenter);//居中显示
           k+=1;
           }
     }
}

void Picking::on_tableWidget_itemSelectionChanged()
{
    int row = ui->tableWidget->currentRow();
    QString time1=ui->datetime->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),
            limit;
    limit = QString::fromUtf8("LLDH");
    limit += ui->tableWidget->item(row,3)->text();
    limit += time1;
    if(row>=k)
    {
        ui->tableWidget->item(row,10)->setText(limit);
    }
//    QSqlQuery sql_query;
//    int rowIdx = ui->tableWidget->currentRow();
//    int temp = 0;
//    int i = 0;
//    sql_query.exec(QString("select 产成品批次号 from reporting_planning where 生产工单 = '%1'").arg(ui->type_combo_2->currentText()));
//    while(sql_query.next())
//    {
//        QString  batchno= sql_query.value(0).toString();
//        if(i == rowIdx)
//        {
//            ui->ID_3->setText(batchno);
//        }
//        i++;
//    }

//    sql_query.exec(QString("select 生产报工单编号 from reporting_planning where 生产工单 = '%1'").arg(ui->type_combo_2->currentText()));
//    while(sql_query.next())
//    {
//        QString flag = sql_query.value(0).toString();
//        if(rowIdx == temp)
//        {
//            ui->ID_4->setText(flag);
//            break;
//        }
//        else
//        {
//            QString order=ui->type_combo_2->currentText(),
//                    time1=ui->datetime->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),   //去除字符‘-’
//                    batchnum=ui->ID_3->text(),
//                    workid=ui->tableWidget->item(rowIdx,7)->text(),
//                    limit;
//            limit=QString::fromUtf8("BGDH");
//        //    limit += ui->tableWidget->item(currentRow,2)->text();
//            limit += time1;
//            limit += batchnum;
//            limit += workid;
//            ui->ID_4->setText(limit);
//            qDebug()<<limit;
//        }
//        temp++;
//    }
}
