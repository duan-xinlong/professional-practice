﻿#include "reporting.h"
#include "ui_reporting.h"

Reporting::Reporting(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Reporting)
{
    ui->setupUi(this);
    ui->tableWidget->verticalHeader()->setVisible(false);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Fixed);//第二列“产品选择”宽度设置
    ui->tableWidget->setColumnWidth(1,75);

    tip_confirm_checkbox=new QLabel(this);

    RightClickMenu=new QMenu(ui->tableWidget);
    m_addAction=new QAction(QString::fromUtf8("插入"),this);
    m_deleteAction=new QAction(QString::fromUtf8("删除"),this);
    RightClickMenu->addAction(m_addAction);
    RightClickMenu->addAction(m_deleteAction);

    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(RightClickSlot(QPoint)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickAddRow(QAction*)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickDeleteRow(QAction*)));

    //查询
    QSqlQuery sql_query;
    sql_query.exec("select * from reporting_planning ");
    if(!sql_query.exec())
     {
       qDebug()<<sql_query.lastError();
     }
    else
     {   //行标志
       while(sql_query.next())
         {
           QString  name= sql_query.value(0).toString();
           QString  code= sql_query.value(1).toString();
           QString  dispatchnum= sql_query.value(2).toString();
           QString reportednum = sql_query.value(3).toString();
           QString  toreportnum= sql_query.value(4).toString();
           QString checker=sql_query.value(5).toString();
           QString workid=sql_query.value(10).toString();
           qDebug()<<name;

           ui->tableWidget->setRowCount(l+1);

           ui->tableWidget->setItem(l,1,new QTableWidgetItem(name));
           ui->tableWidget->item(l, 1)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,2,new QTableWidgetItem(code));
           ui->tableWidget->item(l, 2)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,3,new QTableWidgetItem(dispatchnum));
           ui->tableWidget->item(l, 3)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,4,new QTableWidgetItem(reportednum));
           ui->tableWidget->item(l, 4)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,5,new QTableWidgetItem(toreportnum));
           ui->tableWidget->item(l, 5)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,6,new QTableWidgetItem(checker));
           ui->tableWidget->item(l, 6)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,7,new QTableWidgetItem(workid));
           ui->tableWidget->item(l, 7)->setTextAlignment(Qt::AlignCenter);//居中显示
           l+=1;
           }
     }

    //准备combobox---ok
    ui->type_combo_2->clear();
    sql_query.prepare("select distinct 生产工单 from producing_orders ");
    sql_query.exec();
    QSqlRecord rec = sql_query.record();
//    for(int i = 0; i < rec.count(); i++)
//        qDebug() << rec.fieldName(i);
    while(sql_query.next())
    {
        int orderplace = rec.indexOf("生产工单");
        QString  order= sql_query.value(orderplace).toString();
        ui->type_combo_2->addItem(order);
        qDebug()<<order;
    }
    ui->ID->setText(ui->type_combo_2->currentText());
}

Reporting::~Reporting()
{
    delete ui;
}

QString Reporting::IDGenerate()
{
//    QString type=ui->storage_combo->currentText(),
//            time=ui->datetime->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),   //去除字符‘-’
//            limit;
//    if(type=="原料仓")
//        limit=QString::fromUtf8("Y01");
//    else if(type=="成品仓")
//        limit=QString::fromUtf8("C01");

//    limit += time;
//    QSqlQuery query= mydb->SortData("storage_record_table","StorageRecordID",false,"StorageRecordID",limit); //降序排列

//    if(query.next())
//    {
//        QString str=query.value(0).toString();
//        QString num=NumberIncrease(str.mid(11)); //截取后四位,自增
//        return limit+num;
//    }
//    else
//        return limit+"0001";
}

void Reporting::RowInit(int index)
{
    ui->tableWidget->insertRow(index); //在最后一行的后面插入一行

    //在插入的行中输入产品选择按钮从数据库获得产品数据
    QPushButton *choose_btn = new QPushButton();
    choose_btn->setObjectName("choose_btn" + QString::number(index));
    choose_btn->setProperty("type", "databaseBtn"); //数据库按钮样式

    QWidget *widget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setMargin(0);//一定要有
    layout->addWidget(choose_btn);
    layout->setAlignment(choose_btn, Qt::AlignCenter);//控件在布局中居中显示
    widget->setLayout(layout);

    ui->tableWidget->setCellWidget(index, 0, widget);//放在表格第2列
    connect(choose_btn, &QPushButton::clicked, this, &Reporting::choose_btn_clicked);

    for(int i=0;i<ui->tableWidget->columnCount();i++)
    {
        if(i==0)    //为按钮列
            continue;
        QTableWidgetItem *item = new QTableWidgetItem();
        ui->tableWidget->setItem(index,i,item);
        ui->tableWidget->item(index,i)->setTextAlignment(Qt::AlignCenter);
    }
}

void Reporting::RowDelete(int index)
{
    if(index!=-1)
    {
        for(int i=0;i<ui->tableWidget->columnCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(index,i);
            //确保不为空
            if(widget)
            {
                ui->tableWidget->removeCellWidget(index,i);
                delete widget;  //释放
            }
        }
        ui->tableWidget->removeRow(index);
    }
}

void Reporting::RightClickSlot(QPoint pos)
{

    QModelIndex index = ui->tableWidget->indexAt(pos);    //找到tableview当前位置信息
    iPosRow = index.row();    //获取到了当前右键所选的行数
    if(index.isValid())        //如果行数有效，则显示菜单
    {
        RightClickMenu->exec(QCursor::pos());
    }

}

void Reporting::RightClickAddRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("插入"))   //选中了插入这个菜单
    {
        RowInit(iPosRow);
    }
}

void Reporting::RightClickDeleteRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("删除"))   //选中了删除这个菜单
    {
        RowDelete(iPosRow);
    }
}

void Reporting::choose_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    //按钮状态选择
    if(btn->property("type")=="check")
    {
        btn->setProperty("type","checked");
        style()->polish(btn);
        vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn->property("type")=="checked")
    {
        btn->setProperty("type","check");
        style()->polish(btn);
        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
    else if(btn->property("type")=="databaseBtn")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择产品");
        dc->myModel->setQuery(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table"));
        dc->myModel->setHeaderData(0,Qt::Horizontal,"产品编码");
        dc->myModel->setHeaderData(1,Qt::Horizontal,"产品名称");
        dc->myModel->setHeaderData(2,Qt::Horizontal,"产品类型");
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            for(int i=0;i<list.size();i++)
            {
                QTableWidgetItem *item = new QTableWidgetItem(list[i].data().toString());
                switch (i) {
                case 0:
                    ui->tableWidget->item(row,2)->setText(list[i].data().toString());
                    break;
                case 1:
                    ui->tableWidget->item(row,1)->setText(list[i].data().toString());
                    break;
//                case 2:
//                    ui->tableWidget->item(row,4)->setText(list[i].data().toString());
//                    break;
                default:
                    break;
                }
            }
        });

        //填写派工数量、已报工数量、待报工数量
        int already = 0;//已报工数量4
        int toreport = 0;//待报工数量5
        int totalwork = 0;//派工数量3
        QSqlQuery query;
        query.exec(QString("select 本次报工数量 from reporting_planning where 生产工单 = '%1'").arg(ui->type_combo_2->currentText()));
        while(query.next())
        {
            already += query.value(0).toInt();
        }
        query.exec(QString("select 数量 from producing_orders where 生产工单 = '%1'").arg(ui->type_combo_2->currentText()));
        while(query.next())
        {
            totalwork = query.value(0).toInt();
            qDebug()<<"totalwork:   "<<totalwork;
        }
        ui->tableWidget->item(row,3)->setText(QString::number(totalwork));
        ui->tableWidget->item(row,4)->setText(QString::number(already));
        ui->tableWidget->item(row,5)->setText(QString::number(totalwork-already));
    }
}

void Reporting::on_add_btn_clicked()
{
    if(ui->add_btn->text()=="添加")
    {
        RowInit(ui->tableWidget->rowCount());
    }
    else if(ui->add_btn->text()=="取消操作")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,0);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);
    }
}

void Reporting::on_delete_btn_clicked()
{
    if(ui->delete_btn->text()=="删除")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,0);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","check");
                style()->polish(btn);
            }
        }
        ui->delete_btn->setText(QString::fromUtf8("删除选中"));
        ui->delete_btn->setProperty("type","deleteConfirm");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("取消操作"));
        ui->add_btn->setProperty("type","deleteCancel");
        style()->polish(ui->add_btn);
    }
    else if(ui->delete_btn->text()=="删除选中")
    {
        sort(vecItemIndex.rbegin(), vecItemIndex.rend());
        for (unsigned int i = 0 ; i < vecItemIndex.size(); i++)
        {
            RowDelete(vecItemIndex[i]);
        }
        vecItemIndex.clear();  //清空容器
        vecItemIndex.shrink_to_fit();   //将容器的容量缩减至和实际存储元素的个数相等，释放内存

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);

        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,0);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

    }
}

void Reporting::on_submit_btn_clicked()
{
//    IDGenerate();
    if(ui->confirm_checkBox->isChecked())
    {
        int ret = QMessageBox::question(this, "上传入库单","确认上传", QMessageBox::Yes, QMessageBox::No);
        if(ret == QMessageBox::Yes)
        {
            int rowIdx = ui->tableWidget->rowCount();
            qDebug() << rowIdx-l;
            QSqlQuery query;
            while(rowIdx-l){
                QString item_name = ui->tableWidget->item(l,1)==nullptr? "": ui->tableWidget->item(l,1)->text();
                QString item_code = ui->tableWidget->item(l,2)==nullptr? "": ui->tableWidget->item(l,2)->text();
                QString item_dispatchnum = ui->tableWidget->item(l,3)==nullptr? "": ui->tableWidget->item(l,3)->text();
                QString item_reportednum = ui->tableWidget->item(l,4)==nullptr? "": ui->tableWidget->item(l,4)->text();
                QString item_toreportnum = ui->tableWidget->item(l,5)==nullptr? "": ui->tableWidget->item(l,5)->text();
                QString item_checker = ui->tableWidget->item(l,6)==nullptr? "": ui->tableWidget->item(l,6)->text();
                QString insert_row = QString("insert into reporting_planning(产品名称,产品编码,生产派工数量,已报工数量,本次报工数量,质检员,生产工单,计划开始日期,生产报工单编号) values('%1','%2',%3,%4,%5,'%6','%7','%8','%9')").arg(item_name).arg(item_code).arg(item_dispatchnum)
                        .arg(item_reportednum).arg(item_toreportnum).arg(item_checker).arg(ui->type_combo_2->currentText()).arg(ui->datetime->dateToStr()).arg(ui->ID_4->text());
                qDebug() << insert_row;
                query.exec(insert_row);
                qDebug()<<query.lastError();
                l++;
            }

            QMessageBox::information(this,"上传入库单","上传成功");
            //不再显示tip
            if(tip_confirm_checkbox)
                 tip_confirm_checkbox->hide();
        }

    }
    else
    {
        tip_confirm_checkbox->setText(QString::fromUtf8("*必填"));
        tip_confirm_checkbox->setStyleSheet("color:red;");
        QPoint posA = ui->confirm_checkBox->mapToGlobal(QPoint(0,0));
        QPoint posB = this->mapFromGlobal(QPoint(posA.x(),posA.y()+ui->confirm_checkBox->height()));
        tip_confirm_checkbox->move(posB);
        tip_confirm_checkbox->show();
    }
}

void Reporting::on_datetime_currentTextChanged(const QString &arg1)
{

}

void Reporting::on_tableWidget_itemSelectionChanged()
{
    QSqlQuery sql_query;
    int rowIdx = ui->tableWidget->currentRow();
    int temp = 0;
    int i = 0;
    sql_query.exec(QString("select 产成品批次号 from reporting_planning where 生产工单 = '%1'").arg(ui->type_combo_2->currentText()));
    while(sql_query.next())
    {
        QString  batchno= sql_query.value(0).toString();
        if(batchno == nullptr)
        {
            ui->ID_3->setText("00"+QString::number(rowIdx+1));
        }
        if(i == rowIdx)
        {
            ui->ID_3->setText(batchno);
        }
        i++;
    }

    sql_query.exec(QString("select 生产报工单编号 from reporting_planning where 生产工单 = '%1'").arg(ui->type_combo_2->currentText()));
    while(sql_query.next())
    {
        QString flag = sql_query.value(0).toString();
        if(rowIdx > temp)
        {
            ui->ID_4->setText(flag);
            break;
        }
        else
        {
            QString order=ui->type_combo_2->currentText(),
                    time1=ui->datetime->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),   //去除字符‘-’
                    batchnum=ui->ID_3->text(),
                    workid=ui->tableWidget->item(rowIdx,7)->text(),
                    limit;
            limit=QString::fromUtf8("BGDH");
        //    limit += ui->tableWidget->item(currentRow,2)->text();
            limit += time1;
            limit += batchnum;
            limit += workid;
            ui->ID_4->setText(limit);
            qDebug()<<limit;
        }
        temp++;
    }


}

void Reporting::on_type_combo_2_currentTextChanged(const QString &arg1)
{
    ui->ID->setText(ui->type_combo_2->currentText());

    //查询
    for(int i=ui->tableWidget->rowCount()-1;i>=0;i--)
    {
        RowDelete(i);
    }
    QSqlQuery sql_query;
    sql_query.exec(QString("select * from reporting_planning where 生产工单 = '%1'").arg(ui->type_combo_2->currentText()));
    if(!sql_query.exec())
     {
       qDebug()<<sql_query.lastError();
     }
    else
     {   //行标志
        l = 0;
       while(sql_query.next())
         {
           QString  name= sql_query.value(0).toString();
           QString  code= sql_query.value(1).toString();
           QString  dispatchnum= sql_query.value(2).toString();
           QString reportednum = sql_query.value(3).toString();
           QString  toreportnum= sql_query.value(4).toString();
           QString checker=sql_query.value(5).toString();
           QString workid=sql_query.value(10).toString();
           qDebug()<<name;

           ui->tableWidget->setRowCount(l+1);

           ui->tableWidget->setItem(l,1,new QTableWidgetItem(name));
           ui->tableWidget->item(l, 1)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,2,new QTableWidgetItem(code));
           ui->tableWidget->item(l, 2)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,3,new QTableWidgetItem(dispatchnum));
           ui->tableWidget->item(l, 3)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,4,new QTableWidgetItem(reportednum));
           ui->tableWidget->item(l, 4)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,5,new QTableWidgetItem(toreportnum));
           ui->tableWidget->item(l, 5)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,6,new QTableWidgetItem(checker));
           ui->tableWidget->item(l, 6)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget->setItem(l,7,new QTableWidgetItem(workid));
           ui->tableWidget->item(l, 7)->setTextAlignment(Qt::AlignCenter);//居中显示
           l+=1;
           }
     }
}
