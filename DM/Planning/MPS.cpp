﻿#include "MPS.h"
#include "ui_mps.h"
#include "mainwindow.h"
#include "DataBase/database.h"
#include "qmessagebox.h"
#include <QtCore/QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <regex>

QString makeRandomString(int length)
{
    QString result;

    qsrand(QDateTime::currentMSecsSinceEpoch());//随机数种子
    const char array_str[] = "abcdefghijklmnopqrstuvwxyz";
    int array_size = sizeof(array_str);

    int idx = 0;
    for (int i = 0; i < length; ++i)
    {
        idx = qrand() % (array_size - 1);
        QChar ch = array_str[idx];
        result.append(ch);
    }

    return result;
}



MPS::MPS(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MPS)
{
    ui->setupUi(this);
    mydb = new Database();
    QSqlQuery query_duty;
    query_duty.exec("select 销售订单详细编号 FROM 订单统计表");
    while(query_duty.next()){
        QString duty = query_duty.value(0).toString();
        ui->comboBox_6->addItem(duty);


//     query_saleorders.exec("select saleorder FROM saleorders");
//     QString saleorders = query_saleorders.value(0).toString();
//        ui->comboBox_5->addItem(saleorders);

       // if(ui->comboBox->currentIndex()!=-1&&ui->comboBox_2->currentIndex()!=-1&&ui->comboBox_3->currentIndex()!=-1&&ui->comboBox_4->currentIndex()!=-1&&
         //       ui->comboBox_6->currentIndex()!=-1&&ui->comboBox_5->currentIndex()!=-1&&ui->comboBox_7->currentIndex()!=-1&&ui->lineEdit_4->text()!=NULL){
    }
    QSqlQuery query_scjhfzr;
    query_scjhfzr.exec("select name FROM 生产计划负责人员");
    while(query_scjhfzr.next()){
        QString user = query_scjhfzr.value(0).toString();
        ui->comboBox_5->addItem(user);
    }

}

MPS::~MPS()
{
    delete ui;
}



void MPS::on_pushButton_clicked()
{
    if(ui->comboBox->currentIndex()!=-1&&ui->comboBox_2->currentIndex()!=-1&&
            ui->comboBox_6->currentIndex()!=-1&&ui->comboBox_5->currentIndex()!=-1&&ui->lineEdit->text()!=NULL&&ui->lineEdit_4->text()!=NULL){

        ;
        QString MPSname = ui->lineEdit_4->text();
        QString MPStype= ui->comboBox->currentText();
        QString MPSstatus = ui->comboBox_2->currentText();
        QString MPSid= ui->lineEdit_5->text();
        QString saleorder_id=ui->lineEdit_2->text();
        QString MPSbegin_date=ui->datetime->datetime().toString();
        QString MPSend_date=ui->datetime_2->datetime().toString();
        QString MPSleader=ui->comboBox_5->currentText();
        QSqlQuery q_production_name;
        QString mm=QString("select 产品名称 from 订单统计表 where 销售订单详细编号 = '%1'").arg(saleorder_id);
        q_production_name.exec(mm);

        QSqlQuery q_production_count;
        QString count=QString("select 销售数量 from 订单统计表 where 销售订单详细编号 = '%1'").arg(saleorder_id);
        q_production_count.exec(count);

        q_production_name.next();
        q_production_count.next();

        if(ui->checkBox->isChecked()){
        QSqlQuery query;
        QString str=QString("insert into mps(MPSid,MPSname,MPStype,MPSstatus,saleorder_id,MPSbegin_date,MPSend_date,MPSleader) VALUES('%1', '%2','%3','%4','%5','%6','%7','%8')")
                .arg(MPSid).arg(MPSname).arg(MPStype).arg(MPSstatus).arg(saleorder_id).arg(MPSbegin_date).arg(MPSend_date).arg(MPSleader);
        if(query.exec(str)){
            query.next();
        QMessageBox::information(this,"计划设置提交","提交成功");
        QString m="生产工单";
        QString workorder_name=MPSname.replace("生产计划",m);
        QString workorder_id=MPSid.replace("SCJH","SCGD");
        QString workleader=ui->comboBox_5->currentText();
        QString workorder_status=MPSstatus;
        QString work_tester = "张楚乐";
        QString workorder_begindate = MPSbegin_date;
        QString workorder_enddate=MPSend_date;

        QSqlQuery query_workorder;
             QString str=QString("insert into workorders(生产工单名称,生产工单编号,生产班组长,工单状态,工单开始时间,工单结束时间,质检员,"
                                 "生产计划编号,生产产品数量,生产产品名称)"
                                 " VALUES('%1', '%2','%3','%4','%5','%6','%7','%8','%9','%10')")
                     .arg(workorder_name).arg(workorder_id).arg(workleader).arg(workorder_status).arg(workorder_begindate).arg(workorder_enddate)
                     .arg(work_tester).arg( ui->lineEdit_5->text()).arg(q_production_count.value(0).toString()).arg(q_production_name.value(0).toString());
             query_workorder.exec(str);
         qDebug()<< query_workorder.lastError();

         QSqlQuery qq;
         qq.exec(QString("INSERT INTO producing_orders(生产工单, 数量) VALUES('%1', '%2')").arg(workorder_name).arg(q_production_count.value(0).toString()));
    }
        else{
            qDebug()<<QString("insert into mps(MPSid,MPSname,MPStype,MPSstatus,saleorder_id) VALUES('%1', '%2','%3','%4','%5')").arg(MPSid).arg(MPSname).arg(MPStype).arg(MPSstatus).arg(saleorder_id);
            QMessageBox::information(this,"!","数据已经提交");
        }
        }
        else{
            QMessageBox::information(this,"!!","请勾选确认");
        }
    }
    else {
        QMessageBox::information(this,"提交失败","提交信息缺失");
    }
}

//void workorderinit(){
//    QSqlQuery query;
//     QString str=QString("select 产品名称 from 订单统计表 where 销售订单详细编号 ='%1'’").arg(ui->lineEdit_2->text());
//     query.exec(str);

//}

void MPS::on_comboBox_6_currentTextChanged(const QString &arg1)
{
    QSqlQuery query_saleorder_id;
    QSqlQuery query_saleorder_saleman;
    QString m=ui->comboBox_6->currentText();
    QString str=QString("select 销售订单详细编号 FROM 订单统计表 WHERE 销售订单详细编号 = '%1'").arg(m);
    //qDebug()<<QString("select 销售订单详细编号 FROM 订单统计表 WHERE 销售订单详细编号 = '%1'").arg(m);
    query_saleorder_id.exec(str);
    query_saleorder_id.next();
    ui->lineEdit_2->setText(query_saleorder_id.value(0).toString());
    QString string=QString("select 销售员 FROM 订单统计表 WHERE 销售订单详细编号 = '%1'").arg(m);
    query_saleorder_saleman.exec(string);
    query_saleorder_saleman.next();
    ui->lineEdit->setText(query_saleorder_saleman.value(0).toString());

}



void MPS::on_lineEdit_4_editingFinished()
{
    QString m=ui->lineEdit_2->text();
    QString n="SCJH";
    QString replaced = m.replace("XSDD", n);
    ui->lineEdit_5->setText(replaced);
}
