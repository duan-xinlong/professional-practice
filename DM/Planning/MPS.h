#ifndef MPS_H
#define MPS_H

#include <QMainWindow>


namespace Ui {
class MPS;
}

class MPS : public QMainWindow
{
    Q_OBJECT

public:
    explicit MPS(QWidget *parent = nullptr);
    ~MPS();
//    void workorderinit(const QString &arg1);

private slots:


    void on_pushButton_clicked();


  //  void on_lineEdit_4_textEdited(const QString &arg1);



    void on_comboBox_6_currentTextChanged(const QString &arg1);

    void on_lineEdit_4_editingFinished();

private:
    Ui::MPS *ui;
};

#endif // MPS_H
