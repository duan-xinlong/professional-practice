﻿#ifndef WAREHOUSING_H
#define WAREHOUSING_H

#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QString>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlTableModel>
#include "Tool/data_choose.h"
#include "DataBase/database.h"
#include "Tool/toolfunction.h"
#include <QTableWidgetItem>

namespace Ui {
class Warehousing;
}

class Warehousing : public QWidget
{
    Q_OBJECT

public:
    explicit Warehousing(QWidget *parent = nullptr);
    ~Warehousing();

private slots:
    void on_add_btn_clicked();

    void on_delete_btn_clicked();

    void on_select_member_clicked();

    void on_select_member_2_clicked();

    void on_submit_btn_2_clicked();

    void on_update_btn_clicked();

    void on_type_combo_currentTextChanged(const QString &arg1);

//    void on_tableWidget_itemSelectionChanged();

    void on_tableWidget_itemClicked(QTableWidgetItem *item);

private:
    Ui::Warehousing *ui;

    int k = 0;
    void choose_btn_clicked();      //点击按钮后选择产品基本信息

    void RightClickSlot(QPoint pos);        //菜单点击，获取当前位置

    void RightClickAddRow(QAction *act);        //得知菜单当前的位置并向上插入一行

    void RightClickDeleteRow(QAction *act);   //得知菜单当前的位置并删除

    int iPosRow;
    int row;//当前行

    DataChoose *dc;
    std::vector<int> vecItemIndex;//用于保存选中行的行号

    QAction *m_addAction;     //插入行
    QAction *m_deleteAction;  //删除行

    QLabel *tip_type_combo;
    QLabel *tip_storage_combo;
    QLabel *tip_tableWidget;
    QLabel *tip_confirm_checkbox;
    QLabel *tip_confirm_checkbox_2;
    QLabel *tip_datatimeEdit;
    QLabel *tip_select_member;

    QMenu *RightClickMenu;  //右键点击菜单

    QString IDGenerate();
    QString Status();

    void RowInit(int index);//行的每个单元格初始化
    void RowDelete(int index);//删除行
};

#endif // WAREHOUSING_H
