#ifndef REPORTING_H
#define REPORTING_H

#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include <QSqlRecord>
#include "Tool/data_choose.h"
#include "DataBase/database.h"
#include "Tool/toolfunction.h"

namespace Ui {
class Reporting;
}

class Reporting : public QWidget
{
    Q_OBJECT

public:
    explicit Reporting(QWidget *parent = nullptr);
    ~Reporting();

private slots:
    void on_add_btn_clicked();

    void on_delete_btn_clicked();

    void on_submit_btn_clicked();

    void on_datetime_currentTextChanged(const QString &arg1);

    void on_tableWidget_itemSelectionChanged();

    void on_type_combo_2_currentTextChanged(const QString &arg1);

private:
    Ui::Reporting *ui;

    int l = 0; //行数

    void choose_btn_clicked();      //点击按钮后选择产品基本信息

    void RightClickSlot(QPoint pos);        //菜单点击，获取当前位置

    void RightClickAddRow(QAction *act);        //得知菜单当前的位置并向上插入一行

    void RightClickDeleteRow(QAction *act);   //得知菜单当前的位置并删除

    int iPosRow;
    int row;//当前行

    DataChoose *dc;
    std::vector<int> vecItemIndex;//用于保存选中行的行号

    QAction *m_addAction;     //插入行
    QAction *m_deleteAction;  //删除行

    QLabel *tip_type_combo;
    QLabel *tip_storage_combo;
    QLabel *tip_tableWidget;
    QLabel *tip_confirm_checkbox;
    QLabel *tip_datatimeEdit;
    QLabel *tip_select_member;

    QMenu *RightClickMenu;  //右键点击菜单

    QString IDGenerate();

    void RowInit(int index);//行的每个单元格初始化
    void RowDelete(int index);//删除行
};

#endif // REPORTING_H
