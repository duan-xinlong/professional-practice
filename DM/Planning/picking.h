#ifndef PICKING_H
#define PICKING_H

#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include <QSqlRecord>
#include "Tool/data_choose.h"
#include "DataBase/database.h"
#include "Tool/toolfunction.h"

namespace Ui {
class Picking;
}

class Picking : public QWidget
{
    Q_OBJECT

public:
    explicit Picking(QWidget *parent = nullptr);
    ~Picking();

private slots:
    void on_warehouse_combo_activated(const QString &arg1);

    void on_add_btn_clicked();

    void choose_btn_clicked();      //点击按钮后选择产品基本信息

    void RightClickSlot(QPoint pos);        //菜单点击，获取当前位置

    void RightClickAddRow(QAction *act);        //得知菜单当前的位置并向上插入一行

    void RightClickDeleteRow(QAction *act);   //得知菜单当前的位置并删除

    void on_delete_btn_clicked();

    void on_select_member_clicked();

    void on_submit_btn_clicked();

    void on_add_btn_2_clicked();

    void choose_btn_clicked_2();      //点击按钮后选择产品基本信息

    void RightClickSlot_2(QPoint pos);        //菜单点击，获取当前位置

    void RightClickAddRow_2(QAction *act);        //得知菜单当前的位置并向上插入一行

    void RightClickDeleteRow_2(QAction *act);   //得知菜单当前的位置并删除

    void on_delete_btn_2_clicked();

    void on_select_member_2_clicked();

    void on_submit_btn_2_clicked();

    void on_order_combo_currentTextChanged(const QString &arg1);

    void on_tableWidget_itemSelectionChanged();

private:
    Ui::Picking *ui;

    int k = 0; //表一的行数
    int j = 0; //表二的行数

    int iPosRow;
    int row;//当前行

    DataChoose *dc;
    std::vector<int> vecItemIndex;//用于保存选中行的行号

    QAction *m_addAction;     //插入行
    QAction *m_deleteAction;  //删除行

    QLabel *tip_type_combo;
    QLabel *tip_storage_combo;
    QLabel *tip_tableWidget;
    QLabel *tip_confirm_checkbox;
    QLabel *tip_datatimeEdit;
    QLabel *tip_select_member;

    QMenu *RightClickMenu;  //右键点击菜单

    QString IDGenerate();

    void RowInit(int index);//行的每个单元格初始化
    void RowDelete(int index);//删除行
    void RowInit_2(int index);//行的每个单元格初始化
    void RowDelete_2(int index);//删除行

};

#endif // PICKING_H
