﻿#include "workorders.h"
#include "ui_workorders.h"
#include"mainwindow.h"
workOrders::workOrders(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::workOrders)
{
    ui->setupUi(this);
    ui->tableWidget_4->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    QSqlQuery query_duty;
    query_duty.exec("select username FROM dm_user");
    while(query_duty.next()){
        QString duty = query_duty.value(0).toString();
        ui->comboBox_3->addItem(duty);
    }
    QSqlQuery sql_query;

    if(!sql_query.exec(" SELECT * FROM workorders"))
         {
           QMessageBox::information(this,"wrong","wrong");
           qDebug()<<sql_query.lastError();
           }
   else
    {
        k=0;
        while(sql_query.next()){
            ui->tableWidget->insertRow(k);
            QString production_tasks_pool =sql_query.value(0).toString();
            ui->tableWidget->setItem(k,1,new QTableWidgetItem(production_tasks_pool));
            ui->tableWidget->item(k, 1)->setTextAlignment(Qt::AlignCenter);
            QString production_name = sql_query.value(1).toString();
            ui->tableWidget->setItem(k,2,new QTableWidgetItem(production_name));
            ui->tableWidget->item(k, 2)->setTextAlignment(Qt::AlignCenter);
            QString production_id = sql_query.value(2).toString();
            ui->tableWidget->setItem(k,3,new QTableWidgetItem(production_id));
            ui->tableWidget->item(k, 3)->setTextAlignment(Qt::AlignCenter);
            QString production_attribute = sql_query.value(3).toString();
            ui->tableWidget->setItem(k,4,new QTableWidgetItem(production_attribute));
            ui->tableWidget->item(k, 4)->setTextAlignment(Qt::AlignCenter);
            QString production_type = sql_query.value(4).toString();
            ui->tableWidget->setItem(k,5,new QTableWidgetItem(production_type));
            ui->tableWidget->item(k, 5)->setTextAlignment(Qt::AlignCenter);
            QString production_outputs = sql_query.value(5).toString();
            ui->tableWidget->setItem(k,6,new QTableWidgetItem(production_outputs));
            ui->tableWidget->item(k, 6)->setTextAlignment(Qt::AlignCenter);

            qDebug()<<"workorders: "<<k;
            k++;
        }
    }
    sql_query.clear();
    sql_query.exec("SELECT * FROM 订单统计表 ");
         k = 0;//行标志
       while(sql_query.next())
         {
           QString  name= sql_query.value(9).toString();
           QString  code= sql_query.value(11).toString();
           QString  dispatchnum= sql_query.value(10).toString();
           QString reportednum = sql_query.value(13).toString();
           qDebug()<<name;

           ui->tableWidget_4->setRowCount(k+1);

           ui->tableWidget_4->setItem(k,1,new QTableWidgetItem(name));
           ui->tableWidget_4->item(k, 1)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_4->setItem(k,2,new QTableWidgetItem(code));
           ui->tableWidget_4->item(k, 2)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_4->setItem(k,3,new QTableWidgetItem(dispatchnum));
           ui->tableWidget_4->item(k, 3)->setTextAlignment(Qt::AlignCenter);//居中显示
           ui->tableWidget_4->setItem(k,4,new QTableWidgetItem(reportednum));
           ui->tableWidget_4->item(k, 4)->setTextAlignment(Qt::AlignCenter);//居中显示
           k+=1;
        }
}


workOrders::~workOrders()
{
    delete ui;
}



void workOrders::RowInit(int index)
{
    ui->tableWidget->insertRow(index); //在最后一行的后面插入一行

    //在插入的行中输入产品选择按钮从数据库获得产品数据
    QPushButton *choose_btn = new QPushButton();
    choose_btn->setObjectName("choose_btn" + QString::number(index));
    choose_btn->setProperty("type", "databaseBtn"); //数据库按钮样式

    QWidget *widget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setMargin(0);//一定要有
    layout->addWidget(choose_btn);
    layout->setAlignment(choose_btn, Qt::AlignCenter);//控件在布局中居中显示
    widget->setLayout(layout);

    ui->tableWidget->setCellWidget(index, 0, widget);//放在表格第2列
    connect(choose_btn, &QPushButton::clicked, this, &workOrders::choose_btn_clicked);

    for(int i=0;i<ui->tableWidget->columnCount();i++)
    {
        if(i==0)    //为按钮列
            continue;
        QTableWidgetItem *item = new QTableWidgetItem();
        ui->tableWidget->setItem(index,i,item);
        ui->tableWidget->item(index,i)->setTextAlignment(Qt::AlignCenter);
    }
}
//void workOrders::RowInit_1(int index)
//{
//    ui->tableWidget_2->insertRow(index); //在最后一行的后面插入一行

//    //在插入的行中输入产品选择按钮从数据库获得产品数据
//    QPushButton *choose_btn_2 = new QPushButton();
//    choose_btn_2->setObjectName("choose_btn" + QString::number(index));
//    choose_btn_2->setProperty("type", "databaseBtn"); //数据库按钮样式

//    QWidget *widget = new QWidget();
//    QHBoxLayout *layout = new QHBoxLayout();
//    layout->setMargin(0);//一定要有
//    layout->addWidget(choose_btn_2);
//    layout->setAlignment(choose_btn_2, Qt::AlignCenter);//控件在布局中居中显示
//    widget->setLayout(layout);

//    ui->tableWidget_2->setCellWidget(index, 0, widget);//放在表格第1列
//    connect(choose_btn_2, &QPushButton::clicked, this, &workOrders::choose_btn_2_clicked);

//    for(int i=0;i<ui->tableWidget_2->columnCount();i++)
//    {
//        if(i==1)    //为按钮列
//            continue;
//        QTableWidgetItem *item = new QTableWidgetItem();
//        ui->tableWidget_2->setItem(index,i,item);
//        ui->tableWidget_2->item(index,i)->setTextAlignment(Qt::AlignCenter);
//    }
//}

void workOrders::RowInit_2(int index)
{
    ui->tableWidget_4->insertRow(index); //在最后一行的后面插入一行

    //在插入的行中输入产品选择按钮从数据库获得产品数据
    QPushButton *choose_btn_3 = new QPushButton();
    choose_btn_3->setObjectName("choose_btn_3" + QString::number(index));
    choose_btn_3->setProperty("type", "databaseBtn"); //数据库按钮样式

    QWidget *widget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setMargin(0);//一定要有
    layout->addWidget(choose_btn_3);
    layout->setAlignment(choose_btn_3, Qt::AlignCenter);//控件在布局中居中显示
    widget->setLayout(layout);

    ui->tableWidget_4->setCellWidget(index, 0, widget);//放在表格第1列
    connect(choose_btn_3, &QPushButton::clicked, this, &workOrders::choose_btn_3_clicked);

    for(int i=0;i<ui->tableWidget_4->columnCount();i++)
    {
        if(i==0)    //为按钮列
            continue;
        QTableWidgetItem *item = new QTableWidgetItem();
        ui->tableWidget_4->setItem(index,i,item);
        ui->tableWidget_4->item(index,i)->setTextAlignment(Qt::AlignCenter);
    }
}
void workOrders::RowDelete(int index)
{
    if(index!=-1)
    {
        for(int i=0;i<ui->tableWidget->columnCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(index,i);
            //确保不为空
            if(widget)
            {
                ui->tableWidget->removeCellWidget(index,i);
                delete widget;  //释放
            }
        }
        ui->tableWidget->removeRow(index);
    }
}
//void workOrders::RowDelete_1(int index)
//{
//    if(index!=-1)
//    {
//        for(int i=0;i<ui->tableWidget_2->columnCount();i++)
//        {
//            QWidget *widget=ui->tableWidget_2->cellWidget(index,i);
//            //确保不为空
//            if(widget)
//            {
//                ui->tableWidget_2->removeCellWidget(index,i);
//                delete widget;  //释放
//            }
//        }
//        ui->tableWidget->removeRow(index);
//    }
//}
void workOrders::RowDelete_2(int index)
{
    if(index!=-1)
    {
        for(int i=0;i<ui->tableWidget_4->columnCount();i++)
        {
            QWidget *widget=ui->tableWidget_4->cellWidget(index,i);
            //确保不为空
            if(widget)
            {
                ui->tableWidget_4->removeCellWidget(index,i);
                delete widget;  //释放
            }
        }
        ui->tableWidget_4->removeRow(index);
    }
}

void workOrders::choose_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    //按钮状态选择
    if(btn->property("type")=="check")
    {
        btn->setProperty("type","checked");
        style()->polish(btn);
        vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn->property("type")=="checked")
    {
        btn->setProperty("type","check");
        style()->polish(btn);
        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
    else if(btn->property("type")=="databaseBtn")
    {
        dc=new DataChoose();
        dc->myModel->setQuery("SELECT * FROM workorders");
        //mymodel       0   1   2 列
        //  对应
        //tablewidget   3   4   5
        for(int i=0;i<dc->myModel->columnCount();i++)
        {
            switch (i) {
            case 0:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"生产工单名称");
                break;
            case 1:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"生产工单编号");
                break;
            case 2:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"生产班组组长");
                break;
            case 3:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"生产工单编号");
                break;
            case 4:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"工单开始时间");
                break;
            case 5:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"工单结束时间");
                break;
            default:
                dc->hideColumn(i);
                break;
            }
        }
        dc->show();
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
                    ui->tableWidget->item(row,1)->setText(list[0].data().toString());

                    ui->tableWidget->item(row,2)->setText(list[1].data().toString());

                    ui->tableWidget->item(row,3)->setText(list[2].data().toString());
                    ui->tableWidget->item(row,4)->setText(list[3].data().toString());

                    ui->tableWidget->item(row,5)->setText(list[4].data().toString());

                    ui->tableWidget->item(row,6)->setText(list[5].data().toString());
        });
    }
}
//void workOrders::choose_btn_2_clicked()
//{
//    QPushButton* btn_2 = qobject_cast<QPushButton*>(this->sender());
//    if (btn_2 == nullptr)
//        return;

//    // 获取按钮所在的行
//    QModelIndex qIndex = ui->tableWidget_2->indexAt(QPoint(btn_2->parentWidget()->frameGeometry().x(), btn_2->parentWidget()->frameGeometry().y()));
//    row = qIndex.row();

//    //按钮状态选择
//    if(btn_2->property("type")=="check")
//    {
//        btn_2->setProperty("type","checked");
//        style()->polish(btn_2);
//        vecItemIndex.push_back(row);//存储选中的行号
//    }
//    else if(btn_2->property("type")=="checked")
//    {
//        btn_2->setProperty("type","check");
//        style()->polish(btn_2);
//        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
//    }
//    else if(btn_2->property("type")=="databaseBtn")
//    {
//        dc=new DataChoose();
//        dc->myModel->setTable("product_data_table");
//        dc->myModel->select();
//        //mymodel       0   1   2 列
//        //  对应
//        //tablewidget   3   4   5
//        for(int i=0;i<dc->myModel->columnCount();i++)
//        {
//            switch (i) {
//            case 0:
//                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品编码");
//                break;
//            case 1:
//                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品名称");
//                break;
//            case 2:
//                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品类型");
//                break;
//            default:
//                dc->hideColumn(i);
//                break;
//            }
//        }
//        dc->show();
//        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
//                this, [this](const QModelIndexList& list)
//        {
//            for(int i=0;i<list.size();i++)
//            {
//                QTableWidgetItem *item = new QTableWidgetItem(list[i].data().toString());
//                switch (i) {
//                case 0:
//                    ui->tableWidget_2->item(row,2)->setText(list[i].data().toString());
//                    break;
//                case 1:
//                    ui->tableWidget_2->item(row,3)->setText(list[i].data().toString());
//                    break;
//                case 2:
//                    ui->tableWidget_2->item(row,4)->setText(list[i].data().toString());
//                    break;
//                default:
//                    break;
//                }
//            }
//        });
//    }
//}
void workOrders::choose_btn_3_clicked()
{
    QPushButton* btn_3 = qobject_cast<QPushButton*>(this->sender());
    if (btn_3 == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget_4->indexAt(QPoint(btn_3->parentWidget()->frameGeometry().x(), btn_3->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    //按钮状态选择
    if(btn_3->property("type")=="check")
    {
        btn_3->setProperty("type","checked");
        style()->polish(btn_3);
        vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn_3->property("type")=="checked")
    {
        btn_3->setProperty("type","check");
        style()->polish(btn_3);
        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
    else if(btn_3->property("type")=="databaseBtn")
    {
        dc=new DataChoose();
        dc->myModel->setQuery("SELECT * FROM product_data_table");
        //mymodel       0   1   2 列
        //  对应
        //tablewidget   3   4   5
        for(int i=0;i<dc->myModel->columnCount();i++)
        {
            switch (i) {
            case 0:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品名称");
                break;
            case 1:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品编码");
                break;
            case 2:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品属性");
                break;
            case 3:
                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品计划生产数量");
            default:
                dc->hideColumn(i);
                break;
            }
        }
        dc->show();
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            for(int i=0;i<list.size();i++)
            {
                QTableWidgetItem *item = new QTableWidgetItem(list[i].data().toString());
                switch (i) {
                case 0:
                    ui->tableWidget_4->item(row,2)->setText(list[i].data().toString());
                    break;
                case 1:
                    ui->tableWidget_4->item(row,1)->setText(list[i].data().toString());
                    break;
                case 2:
                    ui->tableWidget_4->item(row,3)->setText(list[i].data().toString());
                    break;
                case 3:
                    ui->tableWidget_4->item(row,4)->setText(list[i].data().toString());
                    break;

                default:
                    break;
                }
            }
        });
    }
}
void workOrders::on_add_btn_clicked()
{
    if(ui->add_btn->text()=="添加")
    {
        RowInit(ui->tableWidget->rowCount());
    }
    else if(ui->add_btn->text()=="取消操作")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);
}

}



//void workOrders::on_add_btn_2_clicked()
//{
//    if(ui->add_btn_2->text()=="添加")
//    {
//        RowInit_1(ui->tableWidget_2->rowCount());
//    }
//    else if(ui->add_btn_2->text()=="取消操作")
//    {
//        for(int i=0;i<ui->tableWidget_2->rowCount();i++)
//        {
//            QWidget *widget=ui->tableWidget_2->cellWidget(i,1);
//            if(widget)
//            {
//                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
//                btn->setProperty("type","databaseBtn");
//                style()->polish(btn);
//            }
//        }

//        ui->delete_btn_2->setText(QString::fromUtf8("删除"));
//        ui->delete_btn_2->setProperty("type","deleteData");
//        style()->polish(ui->delete_btn_2);

//        ui->add_btn_2->setText(QString::fromUtf8("添加"));
//        ui->add_btn_2->setProperty("type","addData");
//        style()->polish(ui->add_btn_2);
//}
//}

//void workOrders::on_delete_btn_2_clicked()
//{
//    if(ui->delete_btn_2->text()=="删除")
//    {
//        for(int i=0;i<ui->tableWidget_2->rowCount();i++)
//        {
//            QWidget *widget=ui->tableWidget_2->cellWidget(i,0);
//            if(widget)
//            {
//                QPushButton* btn_2 = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
//                btn_2->setProperty("type","check");
//                style()->polish(btn_2);
//            }
//        }
//        ui->delete_btn_2->setText(QString::fromUtf8("删除选中"));
//        ui->delete_btn_2->setProperty("type","deleteConfirm");
//        style()->polish(ui->delete_btn_2);

//        ui->add_btn_2->setText(QString::fromUtf8("取消操作"));
//        ui->add_btn_2->setProperty("type","deleteCancel");
//        style()->polish(ui->add_btn_2);
//    }
//    else if(ui->delete_btn_2->text()=="删除选中")
//    {
//        sort(vecItemIndex.rbegin(), vecItemIndex.rend());
//        for (unsigned int i = 0 ; i < vecItemIndex.size(); i++)
//        {
//            RowDelete(vecItemIndex[i]);
//        }
//        vecItemIndex.clear();  //清空容器
//        vecItemIndex.shrink_to_fit();   //将容器的容量缩减至和实际存储元素的个数相等，释放内存

//        ui->delete_btn_2->setText(QString::fromUtf8("删除"));
//        ui->delete_btn_2->setProperty("type","deleteData");
//        style()->polish(ui->delete_btn_2);

//        ui->add_btn_2->setText(QString::fromUtf8("添加"));
//        ui->add_btn_2->setProperty("type","addData");
//        style()->polish(ui->add_btn_2);

//        for(int i=0;i<ui->tableWidget_2->rowCount();i++)
//        {
//            QWidget *widget=ui->tableWidget_2->cellWidget(i,0);
//            if(widget)
//            {
//                QPushButton* btn_2 = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
//                btn_2->setProperty("type","databasebtn");
//                style()->polish(btn_2);
//            }
//        }

//    }
//}

//void choose_btn_2_clicked(){
//    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
//    if (btn == nullptr)
//        return;

//    // 获取按钮所在的行
//    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
//    row = qIndex.row();

//    //按钮状态选择
//    if(btn->property("type")=="check")
//    {
//        btn->setProperty("type","checked");
//        style()->polish(btn);
//        vecItemIndex.push_back(row);//存储选中的行号
//    }
//    else if(btn->property("type")=="checked")
//    {
//        btn->setProperty("type","check");
//        style()->polish(btn);
//        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
//    }
//    else if(btn->property("type")=="databaseBtn")
//    {
//        dc=new DataChoose();
//        dc->myModel->setTable("product_data_table");
//        dc->myModel->select();
//        //mymodel       0   1   2 列
//        //  对应
//        //tablewidget   3   4   5
//        for(int i=0;i<dc->myModel->columnCount();i++)
//        {
//            switch (i) {
//            case 0:
//                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品编码");
//                break;
//            case 1:
//                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品名称");
//                break;
//            case 2:
//                dc->myModel->setHeaderData(i,Qt::Horizontal,"产品类型");
//                break;
//            default:
//                dc->hideColumn(i);
//                break;
//            }
//        }
//        dc->show();
//        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
//                this, [this](const QModelIndexList& list)
//        {
//            for(int i=0;i<list.size();i++)
//            {
//                QTableWidgetItem *item = new QTableWidgetItem(list[i].data().toString());
//                switch (i) {
//                case 0:
//                    ui->tableWidget->item(row,2)->setText(list[i].data().toString());
//                    break;
//                case 1:
//                    ui->tableWidget->item(row,3)->setText(list[i].data().toString());
//                    break;
//                case 2:
//                    ui->tableWidget->item(row,4)->setText(list[i].data().toString());
//                    break;
//                default:
//                    break;
//                }
//            }
//        });
//    }
//}

void workOrders::on_delete_btn_clicked()
{
    if(ui->delete_btn->text()=="删除")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,0);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","check");
                style()->polish(btn);
            }
        }
        ui->delete_btn->setText(QString::fromUtf8("删除选中"));
        ui->delete_btn->setProperty("type","deleteConfirm");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("取消操作"));
        ui->add_btn->setProperty("type","deleteCancel");
        style()->polish(ui->add_btn);
    }
    else if(ui->delete_btn->text()=="删除选中")
    {
        sort(vecItemIndex.rbegin(), vecItemIndex.rend());
        for (unsigned int i = 0 ; i < vecItemIndex.size(); i++)
        {
            RowDelete(vecItemIndex[i]);
        }
        vecItemIndex.clear();  //清空容器
        vecItemIndex.shrink_to_fit();   //将容器的容量缩减至和实际存储元素的个数相等，释放内存

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);

        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,0);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

    }
}

void workOrders::on_submitButton_clicked()
{
    if(ui->confirm_checkBox->isChecked())
      {
          int ret = QMessageBox::question(this, "上传生产工单明细","确认上传", QMessageBox::Yes, QMessageBox::No);
          if(ret == QMessageBox::Yes)
          {
              int k=0;
              int rowIdx = ui->tableWidget->rowCount();
              qDebug() << rowIdx-k;
              QSqlQuery query;
              while(rowIdx-k){
                  if(ui->tableWidget->cellWidget(k,0)!=NULL){
                  QString workorder_name = ui->tableWidget->item(k,1)==nullptr? "": ui->tableWidget->item(k,1)->text();
                  QString workorder_id = ui->tableWidget->item(k,2)==nullptr? "": ui->tableWidget->item(k,2)->text();
                  QString workorder_leader = ui->tableWidget->item(k,3)==nullptr? "": ui->tableWidget->item(k,3)->text();
                  QString workorder_status = ui->tableWidget->item(k,4)==nullptr? "": ui->tableWidget->item(k,4)->text();
                  QString workorder_begin= ui->tableWidget->item(k,5)==nullptr? "": ui->tableWidget->item(k,5)->text();
                  QString workorder_end = ui->tableWidget->item(k,6)==nullptr? "": ui->tableWidget->item(k,6)->text();
                  QString zhijianyuan = ui->comboBox_3->currentText()==nullptr?"":ui->comboBox_3->currentText();
                  QString insert_row = QString("insert into workorders(生产工单名称,生产工单编号,生产班组长,工单状态,工单开始时间,工单结束时间,质检员)"
                                               " values('%1','%2','%3','%4','%5','%6','%7')")
                          .arg(workorder_name).arg(workorder_id).arg(workorder_leader).arg(workorder_status).arg(workorder_begin).arg(workorder_end).arg(zhijianyuan)
                          ;
                  qDebug() << insert_row;
                  query.exec(insert_row);
                  }
                  k++;
                 }

              QMessageBox::information(this,"上传生产工单","上传成功");


      }
    }
}


