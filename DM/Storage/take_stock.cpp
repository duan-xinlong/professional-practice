﻿#include "take_stock.h"
#include "ui_take_stock.h"
#include <QDebug>

//tablewidget的列号
#define DetailID 0
#define ChooseProduct 1
#define ProductCode 2
#define ProductName 3
#define ProductType 4
#define ChooseBatch 5
#define ProductBatch 6
#define StorageNum 7
#define ProductLocation 8
#define RealNum 9
#define LossNum 10
#define SurplusNum 11
#define DIGIT "^[0-9]*$"   //仅输入数字的正则

TakeStock::TakeStock(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TakeStock)
{
    ui->setupUi(this);

    ui->storage_combo->addItems(WarehouseList());
    ui->storage_combo->setCurrentIndex(-1);

    ui->tableWidget->verticalHeader()->setVisible(false);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(ChooseProduct,QHeaderView::Fixed);//第二列“产品选择”宽度设置
    ui->tableWidget->setColumnWidth(ChooseProduct,75);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(ChooseBatch,QHeaderView::Fixed);//第六列“批次选择”宽度设置
    ui->tableWidget->setColumnWidth(ChooseBatch,75);

    //设置列不可编辑
    ReadOnlyDelegate* readOnlyDelegate = new ReadOnlyDelegate(this);
    ui->tableWidget->setItemDelegateForColumn(DetailID, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductCode, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductName, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductType, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductBatch, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(StorageNum, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(LossNum, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(SurplusNum, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductLocation, readOnlyDelegate);

    //设置列输入格式
    SetTextDelegate* setTextDelegate = new SetTextDelegate(this);
    setTextDelegate->reg = new QRegExpValidator(QRegExp(DIGIT));
    setTextDelegate->MaxLength = 5;
    ui->tableWidget->setItemDelegateForColumn(RealNum, setTextDelegate);

    RightClickMenu=new QMenu(ui->tableWidget);
    m_addAction=new QAction(QString::fromUtf8("插入"),this);
    m_deleteAction=new QAction(QString::fromUtf8("删除"),this);
    RightClickMenu->addAction(m_addAction);
    RightClickMenu->addAction(m_deleteAction);

    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(RightClickSlot(QPoint)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickAddRow(QAction*)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickDeleteRow(QAction*)));
}

TakeStock::~TakeStock()
{
    delete ui;
}

QString TakeStock::tableCellText(int row, int column)
{
    switch (column) {
    case DetailID :
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductCode:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductName:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductType:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductBatch:
        return ui->tableWidget->item(row,column)->text();
        break;
    case StorageNum:
        return ui->tableWidget->item(row,column)->text();
        break;
    case RealNum:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductLocation:
        return ui->tableWidget->item(row,column)->text();
        break;
    case LossNum:
        return ui->tableWidget->item(row,column)->text();
        break;
    case SurplusNum:
        return ui->tableWidget->item(row,column)->text();
        break;
    default:
        break;
    }
    return nullptr;
}

void TakeStock::RowInit(int index)
{
    ui->tableWidget->disconnect(SIGNAL(cellChanged(int,int))); //断开槽函数连接
    ui->tableWidget->insertRow(index); //在最后一行的后面插入一行

    //在插入的行中输入按钮——从数据库获得数据, 放在表格第2列
    QPushButton *choose_product_btn = new QPushButton();
    choose_product_btn->setProperty("type", "databaseBtn");
    choose_product_btn->setUpdatesEnabled(true);
    QWidget *widget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(choose_product_btn);
    layout->setAlignment(choose_product_btn, Qt::AlignCenter);//控件在布局中居中显示
    widget->setLayout(layout);
    ui->tableWidget->setCellWidget(index, ChooseProduct, widget);
    connect(choose_product_btn, &QPushButton::clicked, this, &TakeStock::choose_product_btn_clicked);

    //在插入的行中输入按钮——从数据库获得数据, 放在表格6列
    QPushButton *choose_batch_btn = new QPushButton();
    choose_batch_btn->setProperty("type", "databaseBtn");
    choose_batch_btn->setUpdatesEnabled(true);
    QWidget *widget1 = new QWidget();
    QHBoxLayout *layout1 = new QHBoxLayout();
    layout1->addWidget(choose_batch_btn);
    layout1->setAlignment(choose_batch_btn, Qt::AlignCenter);//控件在布局中居中显示
    widget1->setLayout(layout1);
    ui->tableWidget->setCellWidget(index, ChooseBatch, widget1);
    connect(choose_batch_btn, &QPushButton::clicked, this, &TakeStock::choose_batch_btn_clicked);

    for(int i=0;i<ui->tableWidget->columnCount();i++)
    {
        if(i==ChooseProduct || i==ChooseBatch )
            continue;
        else
        {
            QTableWidgetItem *item = new QTableWidgetItem();
            ui->tableWidget->setItem(index,i,item);
            ui->tableWidget->item(index,i)->setTextAlignment(Qt::AlignCenter | Qt::AlignCenter);
        }
    }
    //恢复连接
    connect(ui->tableWidget,SIGNAL(cellChanged(int,int)),this,SLOT(on_tableWidget_cellChanged(int,int)));

    //生成序号
    DetailIDGenerate(index);
}

void TakeStock::RowDelete(int index)
{
    if(index!=-1)
    {
        for(int i=0;i<ui->tableWidget->columnCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(index,i);
            //确保不为空
            if(widget)
            {
                ui->tableWidget->removeCellWidget(index,i);
                delete widget;  //释放
            }
        }
        ui->tableWidget->removeRow(index);
        DetailIDGenerate(index);
    }
}

//盘库单生成
//eg: Y1 20231023 01  , 12位 , 以开始时间为准
void TakeStock::IDGenerate()
{
    if(ui->type_combo->currentIndex()==-1)   //入库类型未选
        return;

    if(ui->storage_combo->currentIndex()==-1)   //入库仓库未选
        return;

    if(ui->start_date->currentText()=="")     //入库时间未选
        return;

    QString time=ui->start_date->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),   //去除字符‘-’
            limit = type;

    limit += time;
    QSqlQuery query= mydb->SortData("take_stock_record_table","TakeStockRecordID",false,"TakeStockRecordID",limit); //降序排列
    if(query.next())
    {
        QString str=query.value(0).toString();
        QString num=NumberIncrease(str.right(2)); //截取后2位,自增
        ui->ID->setText(limit+num);
    }
    else
        ui->ID->setText(limit+"01");
}

// 盘库详细单号生成
//eg: Y1 20231023 01 001 , 15位
void TakeStock::DetailIDGenerate(int index)
{
    if(ui->tableWidget->rowCount()==0 || ui->ID->text()==nullptr)
        return;

    QString limit=ui->ID->text();
    for(int i=index; i<ui->tableWidget->rowCount(); i++)
    {
        if(i==0)
            ui->tableWidget->item(i,DetailID)->setText(limit+"001");
        else
        {
            QString num=NumberIncrease(ui->tableWidget->item(i-1,DetailID)->text().right(3)); //截取上一单元格后3位,自增
            ui->tableWidget->item(i,DetailID)->setText(limit+num);
        }
    }
}

bool TakeStock::CheckTable()
{
    for(int i=0; i<ui->tableWidget->rowCount();i++)
    {
        for(int j=2;j<ui->tableWidget->columnCount();j++)   //前两列不用检查
        {
            if(j == ChooseBatch) //选择批次列是按钮，不检查
                continue;

            if(tableCellText(i,j)==nullptr)
                return false;
        }
    }
    return true;
}

bool TakeStock::OutStorage()
{
    QString ID = StorageID("C");
    QStringList IDlist = StorageDetailID("C", ui->tableWidget->rowCount());
    if(IDlist.empty())
        return true;

    QSqlQuery query;

    //更新 货位表
    query.prepare(QString("UPDATE storage_space_%1_table SET ProductNum=:ProductNum"
                          " WHERE LocationID = :LocationID").arg(type));
    for(int i=0;i<ui->tableWidget->rowCount();i++)
    {
        query.bindValue(":LocationID",tableCellText(i, ProductLocation));
        query.bindValue(":ProductNum", tableCellText(i,RealNum).toInt());
        if(!query.exec())
        {
            qDebug()<<"盘点OutStorage 1"<<query.lastError();
            return false;
        }
        UpdateMaterial(tableCellText(i,ProductCode),-tableCellText(i,LossNum).toInt());
    }
    query.clear();  //清空sql语句

    //插入 出库单表
    QString sql = QString("INSERT INTO out_storage_record_table(StorageRecordID,StorageType,StorageWarehouse,ResponsiblePersonID,dt)"
                          " VALUES ('%1','%2','%3','%4','%5');")
            .arg(ID)
            .arg("盘亏出库")
            .arg(ui->storage_combo->currentText())
            .arg(ui->select_member_btn->text().left(ui->select_member_btn->text().indexOf("(")))
            .arg(ui->start_date->currentText());
    if(!query.exec(sql))
    {
        qDebug()<<"盘点 OutStorage 2"<<query.lastError();
        return false;
    }

    query.clear();  //清空sql语句

    //插入 出库详细信息表
    query.prepare(QString("INSERT INTO out_storage_detail_table(StorageDetailID,ProductCode,ProductType,ProductBatch,OUTQTY,LocationID)"
                          " VALUES (:StorageDetailID,:ProductCode,:ProductType,:ProductBatch,:OUTQTY,:LocationID);"));
    int flag=0;
    for(int i=0;i<ui->tableWidget->rowCount();i++)
    {
        if(tableCellText(i,LossNum).toInt() > 0) //如果盘亏大于0,则该行应录入出库信息
        {
            query.bindValue(":StorageDetailID",IDlist.at(flag));
            query.bindValue(":ProductCode", tableCellText(i,ProductCode));
            query.bindValue(":ProductType", tableCellText(i,ProductType));
            query.bindValue(":ProductBatch", tableCellText(i,ProductBatch));
            query.bindValue(":OUTQTY", tableCellText(i,LossNum).toInt());
            query.bindValue(":LocationID",tableCellText(i,ProductLocation));
            flag++;
            if(!query.exec())
            {
                qDebug()<<"盘点 OutStorage 3"<<query.lastError();
                return false;
            }
        }
    }
    query.clear();  //清空sql语句

    //插入 出库关系表
    query.prepare(QString("INSERT INTO out_storage_relation_table(StorageRecordID,StorageDetailID)"
                          " VALUES (:StorageRecordID,:StorageDetailID);"));
    query.bindValue(":StorageRecordID",ID);
    flag=0;
    for(int i=0;i<ui->tableWidget->rowCount();i++)
    {
        if(tableCellText(i,LossNum).toInt() > 0) //如果盘亏大于0,则该行应录入出库信息
        {
            query.bindValue(":StorageDetailID", IDlist.at(flag));
            flag++;
            if(!query.exec())
            {
                qDebug()<<"盘点 OutStorage 4"<<query.lastError();
                return false;
            }
        }
    }
    query.clear();  //清空sql语句

    return true;
}

//盘盈入库
bool TakeStock::InStorage()
{
    QString ID = StorageID("R");
    QStringList IDlist = StorageDetailID("R", ui->tableWidget->rowCount());
    if(IDlist.empty())
        return true;

    QSqlQuery query;

    //更新 货位表  ,如果表中有，则修改
    query.prepare(QString("UPDATE storage_space_%1_table "
                          "SET ProductBatch=:ProductBatch, ProductCode=:ProductCode, ProductNum=:ProductNum "
                          "WHERE LocationID = :LocationID").arg(type));
    for(int i=0;i<ui->tableWidget->rowCount();i++)
    {
        query.bindValue(":LocationID",tableCellText(i,ProductLocation));
        query.bindValue(":ProductBatch",tableCellText(i,ProductBatch));
        query.bindValue(":ProductCode", tableCellText(i,ProductCode));
        query.bindValue(":ProductNum", tableCellText(i,RealNum).toInt());
        if(!query.exec())
        {
            qDebug()<<"盘点 InStorage1" << query.lastError();
            return false;
        }
        UpdateMaterial(tableCellText(i,ProductCode),tableCellText(i,SurplusNum).toInt());
    }
    query.clear();  //清空sql语句

    //插入 入库单表
    QString sql = QString("INSERT INTO storage_record_table(StorageRecordID,StorageType,StorageWarehouse,ResponsiblePersonID,dt)"
                          " VALUES ('%1','%2','%3','%4','%5');")
            .arg(ID)
            .arg("盘盈入库")
            .arg(ui->storage_combo->currentText())
            .arg(ui->select_member_btn->text().left(ui->select_member_btn->text().indexOf("(")))
            .arg(ui->start_date->currentText());
    if(!query.exec(sql))
    {
        qDebug()<<"盘点 InStorage2" << query.lastError();
        return false;
    }
    query.clear();  //清空sql语句

    //插入 入库详细信息表
    query.prepare(QString("INSERT INTO storage_detail_table(StorageDetailID,ProductCode,ProductType,ProductBatch,INQTY,LocationID)"
                          " VALUES (:StorageDetailID,:ProductCode,:ProductType,:ProductBatch,:INQTY,:LocationID);"));
    int flag=0;
    for(int i=0;i<ui->tableWidget->rowCount();i++)
    {
        if(tableCellText(i,SurplusNum).toInt()>0) //盘盈则录入 入库信息
        {
            query.bindValue(":StorageDetailID",IDlist.at(flag));
            query.bindValue(":ProductCode", tableCellText(i,ProductCode));
            query.bindValue(":ProductType", tableCellText(i,ProductType));
            query.bindValue(":ProductBatch", tableCellText(i,ProductBatch));
            query.bindValue(":INQTY", tableCellText(i,SurplusNum).toInt());
            query.bindValue(":LocationID",tableCellText(i,ProductLocation));
            if(!query.exec())
            {
                qDebug()<<"盘点 InStorage3" << query.lastError();
                return false;
            }
            flag++;
        }

    }
    query.clear();  //清空sql语句

    //插入 入库关系表
    query.prepare(QString("INSERT INTO storage_relation_table(StorageRecordID,StorageDetailID)"
                          " VALUES (:StorageRecordID,:StorageDetailID);"));
    query.bindValue(":StorageRecordID",ID);

    flag=0;
    for(int i=0;i<ui->tableWidget->rowCount();i++)
    {
        if(tableCellText(i,SurplusNum).toInt()>0) //盘盈则录入 入库信息
        {
            query.bindValue(":StorageDetailID", IDlist.at(flag));
            flag++;
            if(!query.exec())
            {
                qDebug()<<"盘点 InStorage4" << query.lastError();
                return false;
            }
        }
    }

    return true;
}

void TakeStock::UpdateMaterial(QString code, int ChangeNum)
{
    QSqlQuery query;
    int PurchaseRequest;
    int StockQty;  //变更后库存数量
    int OrderPoint;
    query.exec(QString("SELECT * FROM product_data_table WHERE ProductCode = '%1'").arg(code));
    /* 3 StockQty
     * 4 SafeStock
     * 5 LeadTime
     * 6 DailyDemand
     * 7 PurchaseRequest
     */
    while(query.next())
    {
        //订货点 为 安全库存量加上订货提前期乘以日需求量
        OrderPoint= query.value(4).toInt() +  query.value(5).toInt() *  query.value(6).toInt();
        StockQty = query.value(3).toInt() + ChangeNum;  //变更后库存数量
        PurchaseRequest = query.value(6).toInt();
        if(StockQty < OrderPoint)
            PurchaseRequest=1;
        else
            PurchaseRequest=0;
    }
    query.clear();

    //更新数据
    query.exec(QString("UPDATE product_data_table SET StockQty = %1, PurchaseRequest = %2 "
                       " WHERE ProductCode = '%3'").arg(StockQty).arg(PurchaseRequest).arg(code));
}

// 入库单号生成
//eg: R Y1 20231023 001 , 14位
QString TakeStock::StorageID(QString str)
{
    QString time=ui->start_date->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),   //去除字符‘-’
            limit = str+type;

    limit += time;
    QSqlQuery query;

    if(str == "R")
        query= mydb->SortData("storage_record_table","StorageRecordID",false,"StorageRecordID",limit); //降序排列

    else if(str == "C")
        query= mydb->SortData("out_storage_record_table","StorageRecordID",false,"StorageRecordID",limit); //降序排列

    if(query.next())
    {
        QString str=query.value(0).toString();
        QString num=NumberIncrease(str.right(3)); //截取后3位,自增
        return (limit+num);
    }
    else
        return (limit+"001");
}

// 入库详细单号生成
//eg: R Y1 20231023 001 01 , 16位
QStringList TakeStock::StorageDetailID(QString str, int rowCount)
{
    QString limit=StorageID(str);
    QStringList list;
    if(str=="C")  //出库
    {
        for(int i=0 ; i<rowCount; i++)
        {
            if(tableCellText(i,LossNum).toInt() > 0)
                list.append(limit + QString("%1").arg(list.count()+1,2,10,QLatin1Char('0')));
        }
    }
    else if(str=="R")  //入库
    {
        for(int i=0 ; i<rowCount; i++)
        {
            if(tableCellText(i,SurplusNum).toInt() > 0)
                list.append(limit + QString("%1").arg(list.count()+1,2,10,QLatin1Char('0')));
        }
    }

    return list;
}

void TakeStock::RightClickSlot(QPoint pos)
{

    QModelIndex index = ui->tableWidget->indexAt(pos);    //找到tableview当前位置信息
    iPosRow = index.row();    //获取到了当前右键所选的行数
    if(index.isValid())        //如果行数有效，则显示菜单
    {
        RightClickMenu->exec(QCursor::pos());
    }

}

void TakeStock::RightClickAddRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("插入"))   //选中了插入这个菜单
    {
        RowInit(iPosRow);
    }
}

void TakeStock::RightClickDeleteRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("删除"))   //选中了删除这个菜单
    {
        RowDelete(iPosRow);
    }
}

void TakeStock::choose_product_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行和列
    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    //按钮状态选择
    if(btn->property("type")=="check")
    {
        btn->setProperty("type","checked");
        style()->polish(btn);
        vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn->property("type")=="checked")
    {
        btn->setProperty("type","check");
        style()->polish(btn);
        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
    else if(btn->property("type")=="databaseBtn")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择产品");
        dc->myModel->setQuery(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table"));
        dc->myModel->setHeaderData(0,Qt::Horizontal,"产品编码");
        dc->myModel->setHeaderData(1,Qt::Horizontal,"产品名称");
        dc->myModel->setHeaderData(2,Qt::Horizontal,"产品类型");
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->tableWidget->item(row,ProductCode)->setText(list[0].data().toString());
            ui->tableWidget->item(row,ProductName)->setText(list[1].data().toString());
            ui->tableWidget->item(row,ProductType)->setText(list[2].data().toString());
        });
    }
}

void TakeStock::choose_batch_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    if(type == nullptr
            || tableCellText(row, ProductCode) == nullptr)
        return;

    //创建数据选择子界面
    dc=new DataChoose();
    dc->setTitle(QString("选择<font color=#00B050>%1").arg(tableCellText(row,ProductName)) + "<font color=black>批次");
    dc->myModel->setQuery(QString("SELECT ProductBatch, ProductNum, LocationID FROM storage_space_%1_table"
                                  " WHERE ProductCode = '%2'").arg(type).arg(tableCellText(row, ProductCode)));
    if(dc->myModel->rowCount()==0)
    {
        delete dc;
        warning(this , "该产品目前仓库无货");
        return;
    }
    dc->myModel->setHeaderData(0,Qt::Horizontal,"产品批次");
    dc->myModel->setHeaderData(1,Qt::Horizontal,"当前数量");
    dc->myModel->setHeaderData(2,Qt::Horizontal,"货位号");
    dc->show();

    //子界面向主界面传送选择的数据
    connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
            this, [this](const QModelIndexList& list)
    {
        ui->tableWidget->item(row,ProductBatch)->setText(list[0].data().toString());
        ui->tableWidget->item(row,StorageNum)->setText(list[1].data().toString());
        ui->tableWidget->item(row,ProductLocation)->setText(list[2].data().toString());
    });
}

//表格中添加新行
void TakeStock::on_add_btn_clicked()
{
    if(ui->add_btn->text()=="添加")
    {
        RowInit(ui->tableWidget->rowCount());
    }
    else if(ui->add_btn->text()=="取消操作")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);
    }
}

//表格中删除选中行
void TakeStock::on_delete_btn_clicked()
{
    if(ui->delete_btn->text()=="删除")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","check");
                style()->polish(btn);
            }
        }
        ui->delete_btn->setText(QString::fromUtf8("删除选中"));
        ui->delete_btn->setProperty("type","deleteConfirm");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("取消操作"));
        ui->add_btn->setProperty("type","deleteCancel");
        style()->polish(ui->add_btn);
    }
    else if(ui->delete_btn->text()=="删除选中")
    {
        sort(vecItemIndex.rbegin(), vecItemIndex.rend());
        for (unsigned int i = 0 ; i < vecItemIndex.size(); i++)
        {
            RowDelete(vecItemIndex[i]);
        }
        vecItemIndex.clear();  //清空容器
        vecItemIndex.shrink_to_fit();   //将容器的容量缩减至和实际存储元素的个数相等，释放内存

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);

        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

    }
}

void TakeStock::on_select_member_btn_clicked()
{
    //创建数据选择子界面
    dc=new DataChoose();
    dc->setTitle("选择盘点负责员");
    dc->myModel->setQuery("SELECT id, username, position FROM dm_user WHERE delete_flag=0");
    dc->myModel->setHeaderData(0,Qt::Horizontal,"编号");
    dc->myModel->setHeaderData(1,Qt::Horizontal,"姓名");
    dc->myModel->setHeaderData(2,Qt::Horizontal,"职位");
    dc->show();
    //子界面向主界面传送选择的数据
    connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
            this, [this](const QModelIndexList& list)
    {
        ui->select_member_btn->setText(list[0].data().toString() + "(" + list[1].data().toString() + ")");
        ui->select_member_btn->setProperty("type","SelectMember_ed");
        style()->polish(ui->select_member_btn);

    });
}

void TakeStock::on_submit_btn_clicked()
{
    if(ui->type_combo->currentIndex()==-1)
    {
        warning(this,"未选择盘点类型");
        return;
    }
    if(ui->start_date->currentText()==nullptr)
    {
        warning(this,"未选择盘点开始日期");
        return;
    }
    if(ui->end_date->currentText()==nullptr)
    {
        warning(this,"未选择盘点结束日期");
        return;
    }
    if(ui->select_member_btn->text()=="选择成员")
    {
        warning(this,"未选择盘点员");
        return;
    }
    if(ui->storage_combo->currentIndex()==-1)
    {
        warning(this,"未选择盘点仓库");
        return;
    }
    if(ui->tableWidget->rowCount()==0)
    {
        warning(this,"未录入盘点产品明细");
        return;
    }
    else if(CheckTable() == false)
    {
        warning(this,"产品明细录入不完整");
        return;
    }

    int ret = QMessageBox::question(this, "上传盘库单","确认上传？", QMessageBox::Yes, QMessageBox::No);
    if(ret == QMessageBox::Yes)
    {
        QSqlQuery query;

        //插入 盘库单表
        QString sql = QString("INSERT INTO take_stock_record_table(TakeStockRecordID,TakeStockType,"
                              " StorageWarehouse, ResponsiblePerson, StartDt, EndDt)"
                              " VALUES ('%1','%2','%3','%4','%5','%6');")
                .arg(ui->ID->text())
                .arg(ui->type_combo->currentText())
                .arg(ui->storage_combo->currentText())
                .arg(ui->select_member_btn->text().left(ui->select_member_btn->text().indexOf("(")))
                .arg(ui->start_date->currentText())
                .arg(ui->end_date->currentText());
        if(!query.exec(sql))
        {
            warning(this,"上传失败");
            return;
        }
        query.clear();  //清空sql语句

        //插入 盘点明细信息表
        query.prepare(QString("INSERT INTO take_stock_detail_table(TakeStockDetailID,ProductCode,ProductType,ProductBatch,LocationID,"
                              " StorageNum, RealNum, LossNum, SurplusNum)"
                              " VALUES (:StorageDetailID,:ProductCode,:ProductType,:ProductBatch,:LocationID,"
                              " :StorageNum, :RealNum, :LossNum, :SurplusNum);"));
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            query.bindValue(":StorageDetailID",tableCellText(i,DetailID));
            query.bindValue(":ProductCode", tableCellText(i,ProductCode));
            query.bindValue(":ProductType", tableCellText(i,ProductType));
            query.bindValue(":ProductBatch", tableCellText(i,ProductBatch));
            query.bindValue(":LocationID",tableCellText(i,ProductLocation));
            query.bindValue(":StorageNum", tableCellText(i,StorageNum));
            query.bindValue(":RealNum",tableCellText(i,RealNum));
            query.bindValue(":LossNum",tableCellText(i,LossNum));
            query.bindValue(":SurplusNum", tableCellText(i,SurplusNum));
            if(!query.exec())
            {
                qDebug()<<"盘点明细"<< query.lastError();
                warning(this,"上传失败");
                return;
            }
        }
        query.clear();  //清空sql语句

        //如果盘亏，则生成盘亏出库单
        if(!OutStorage())
        {
            warning(this,"上传失败");
            return;
        }
        //如果盘盈，则生成盘盈入库单
        if(!InStorage())
        {
            warning(this,"上传失败");
            return;
        }

        //插入 盘库关系表
        query.prepare(QString("INSERT INTO take_stock_relation_table(TakeStockRecordID,TakeStockDetailID)"
                              " VALUES (:TakeStockRecordID,:TakeStockDetailID);"));

        query.bindValue(":TakeStockRecordID",ui->ID->text());
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            query.bindValue(":TakeStockDetailID", tableCellText(i,DetailID));
            if(!query.exec())
            {
                warning(this,"上传失败");
                return;
            }
        }
        query.clear();  //清空sql语句

        QMessageBox::information(this, "上传盘库单","上传成功");
        clear();
    }
}


void TakeStock::on_type_combo_currentIndexChanged(const QString &arg1)
{
    IDGenerate();       //生成库编号
    DetailIDGenerate(0);  //刷新库详细信息
}

void TakeStock::on_storage_combo_currentIndexChanged(const QString &arg1)
{
    type = arg1.right(2);  //截取后两位

    IDGenerate();       //生成盘库编号
    DetailIDGenerate(0);  //刷新盘库详细信息
}

void TakeStock::on_start_date_editTextChanged(const QString &arg1)
{
    IDGenerate();       //生成盘库编号
    DetailIDGenerate(0);  //刷新详细信息
}

void TakeStock::on_tableWidget_cellChanged(int row, int column)
{
    if(column == ProductCode || column == DetailID)
    {
        for(int i=ProductBatch ; i<ui->tableWidget->columnCount();i++)   //清空表格部分列
        {
            ui->tableWidget->item(row, i)->setText("");
        }
    }
    if(column == RealNum && ui->tableWidget->item(row, StorageNum)!=nullptr) //更新盘亏、盘盈
    {
        int num = tableCellText(row, RealNum).toInt() - tableCellText(row, StorageNum).toInt();
        if(num >= 0) // 大于0盘盈， 小于0盘亏
        {
            ui->tableWidget->item(row, SurplusNum)->setText(QString("%1").arg(num));
            ui->tableWidget->item(row, LossNum)->setText(QString("%1").arg(0));
        }
        else if(num < 0) // 大于0盘盈， 小于0盘亏
        {
            ui->tableWidget->item(row, LossNum)->setText(QString("%1").arg(-num));
            ui->tableWidget->item(row, SurplusNum)->setText(QString("%1").arg(0));
        }
    }
}

void TakeStock::clear()
{
    ui->type_combo->setCurrentIndex(-1);
    ui->start_date->setEditText("");
    ui->end_date->setEditText("");
    ui->select_member_btn->setText("选择成员");
    ui->select_member_btn->setProperty("type","SelectMember");
    style()->polish(ui->select_member_btn);
    ui->ID->setText("");
    ui->storage_combo->setCurrentIndex(-1);
    type = "";
    for(int i = ui->tableWidget->rowCount()-1; i>=0 ; i--)
    {
        RowDelete(i);
    }
}
