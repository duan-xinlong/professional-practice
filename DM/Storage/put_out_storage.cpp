﻿#include "put_out_storage.h"
#include "ui_put_out_storage.h"

//tablewidget的列号
#define DetailID 0
#define ChooseProduct 1
#define ProductCode 2
#define ProductName 3
#define ProductType 4
#define ChooseBatch 5
#define ProductBatch 6
#define StorageNum 7
#define ProductNum 8
#define ProductLocation 9
#define DIGIT "^[0-9]*$"   //仅输入数字的正则


PutOutStorage::PutOutStorage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PutOutStorage)
{
    ui->setupUi(this);

    ui->storage_combo->addItems(WarehouseList());
    ui->storage_combo->setCurrentIndex(-1);

    ui->tableWidget->verticalHeader()->setVisible(false);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(ChooseProduct,QHeaderView::Fixed);//第二列“产品选择”宽度设置
    ui->tableWidget->setColumnWidth(ChooseProduct,75);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(ChooseBatch,QHeaderView::Fixed);//第六列“批次选择”宽度设置
    ui->tableWidget->setColumnWidth(ChooseBatch,75);

    //设置列不可编辑
    ReadOnlyDelegate* readOnlyDelegate = new ReadOnlyDelegate(this);
    ui->tableWidget->setItemDelegateForColumn(DetailID, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductCode, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductName, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductType, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductBatch, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(StorageNum, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductLocation, readOnlyDelegate);

    //设置列输入格式
    SetTextDelegate* setTextDelegate = new SetTextDelegate(this);
    setTextDelegate->reg = new QRegExpValidator(QRegExp(DIGIT));
    setTextDelegate->MaxLength = 5;
    ui->tableWidget->setItemDelegateForColumn(ProductNum, setTextDelegate);

    RightClickMenu=new QMenu(ui->tableWidget);
    m_addAction=new QAction(QString::fromUtf8("插入"),this);
    m_deleteAction=new QAction(QString::fromUtf8("删除"),this);
    RightClickMenu->addAction(m_addAction);
    RightClickMenu->addAction(m_deleteAction);

    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(RightClickSlot(QPoint)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickAddRow(QAction*)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickDeleteRow(QAction*)));
}

PutOutStorage::~PutOutStorage()
{
    delete ui;
}

QString PutOutStorage::tableCellText(int row, int column)
{
    switch (column) {
    case DetailID :
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductCode:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductName:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductType:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductBatch:
        return ui->tableWidget->item(row,column)->text();
        break;
    case StorageNum:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductNum:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductLocation:
        return ui->tableWidget->item(row,column)->text();
        break;
    default:
        break;
    }
    return nullptr;
}

void PutOutStorage::RowInit(int index)
{
    ui->tableWidget->disconnect(SIGNAL(cellChanged(int,int))); //断开槽函数连接
    ui->tableWidget->insertRow(index); //在最后一行的后面插入一行

    //在插入的行中输入按钮——从数据库获得数据, 放在表格第2列
    QPushButton *choose_product_btn = new QPushButton();
    choose_product_btn->setProperty("type", "databaseBtn");
    choose_product_btn->setUpdatesEnabled(true);
    QWidget *widget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(choose_product_btn);
    layout->setAlignment(choose_product_btn, Qt::AlignCenter);//控件在布局中居中显示
    widget->setLayout(layout);
    ui->tableWidget->setCellWidget(index, ChooseProduct, widget);
    connect(choose_product_btn, &QPushButton::clicked, this, &PutOutStorage::choose_product_btn_clicked);

    //在插入的行中输入按钮——从数据库获得数据, 放在表格6列
    QPushButton *choose_batch_btn = new QPushButton();
    choose_batch_btn->setProperty("type", "databaseBtn");
    choose_batch_btn->setUpdatesEnabled(true);
    QWidget *widget1 = new QWidget();
    QHBoxLayout *layout1 = new QHBoxLayout();
    layout1->addWidget(choose_batch_btn);
    layout1->setAlignment(choose_batch_btn, Qt::AlignCenter);//控件在布局中居中显示
    widget1->setLayout(layout1);
    ui->tableWidget->setCellWidget(index, ChooseBatch, widget1);
    connect(choose_batch_btn, &QPushButton::clicked, this, &PutOutStorage::choose_batch_btn_clicked);

    //插入货位选择框
//    DatabaseComboBox *location_combo=new DatabaseComboBox();
//    location_combo->setProperty("type", "dataBaseCombo"); //数据库选择框样式
//    ui->tableWidget->setCellWidget(index, ProductLocation, location_combo);

    for(int i=0;i<ui->tableWidget->columnCount();i++)
    {

        if(i==ChooseProduct || i==ChooseBatch ) //|| i==)
            continue;
        else
        {
            QTableWidgetItem *item = new QTableWidgetItem();
            ui->tableWidget->setItem(index,i,item);
            ui->tableWidget->item(index,i)->setTextAlignment(Qt::AlignCenter | Qt::AlignCenter);
        }
    }

    //恢复连接
    connect(ui->tableWidget,SIGNAL(cellChanged(int,int)),this,SLOT(on_tableWidget_cellChanged(int,int)));

    //生成序号
    DetailIDGenerate(index);
}

void PutOutStorage::RowDelete(int index)
{
    if(index!=-1)
    {
        for(int i=0;i<ui->tableWidget->columnCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(index,i);
            //确保不为空
            if(widget)
            {
                ui->tableWidget->removeCellWidget(index,i);
                delete widget;  //释放
            }
        }
        ui->tableWidget->removeRow(index);
        DetailIDGenerate(index);
    }
}

//出库单生成
//eg: C Y1 20231023 001  , 14位
void PutOutStorage::IDGenerate()
{
    if(ui->type_combo->currentIndex()==-1)   //入库类型未选
        return;

    if(ui->storage_combo->currentIndex()==-1)   //入库仓库未选
        return;

    if(ui->datetime->currentText()=="")     //入库时间未选
        return;

    QString time=ui->datetime->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),   //去除字符‘-’
            limit = "C"+type;

    limit += time;
    QSqlQuery query= mydb->SortData("out_storage_record_table","StorageRecordID",false,"StorageRecordID",limit); //降序排列

    if(query.next())
    {
        QString str=query.value(0).toString();
        QString num=NumberIncrease(str.right(3)); //截取后3位,自增
        ui->ID->setText(limit+num);
    }
    else
        ui->ID->setText(limit+"001");
}

// 出库详细单号生成
//eg: C Y1 20231023 001 01 , 16位
void PutOutStorage::DetailIDGenerate(int index)
{
    if(ui->tableWidget->rowCount()==0 || ui->ID->text()==nullptr)
        return;

    QString limit=ui->ID->text();
    for(int i=index; i<ui->tableWidget->rowCount(); i++)
    {
        if(i==0)
            ui->tableWidget->item(i,DetailID)->setText(limit+"01");
        else
        {
            QString num=NumberIncrease(ui->tableWidget->item(i-1,DetailID)->text().right(2)); //截取上一单元格后两位,自增
            ui->tableWidget->item(i,DetailID)->setText(limit+num);
        }
    }
}

bool PutOutStorage::CheckTable()
{
    for(int i=0; i<ui->tableWidget->rowCount();i++)
    {
        for(int j=2;j<ui->tableWidget->columnCount();j++)   //前两列不用检查
        {
            if(j == ChooseBatch) //选择批次列是按钮，不检查
                continue;

            if(tableCellText(i,j)==nullptr)
                return false;
        }
        if(tableCellText(i,ProductNum).toInt() > tableCellText(i, StorageNum).toInt())
            return false;
    }
    return true;
}

void PutOutStorage::UpdateMaterial(QString code, int ChangeNum)
{
    QSqlQuery query;
    int PurchaseRequest;
    int StockQty;  //变更后库存数量
    int OrderPoint;
    query.exec(QString("SELECT * FROM product_data_table WHERE ProductCode = '%1'").arg(code));
    /* 3 StockQty
     * 4 SafeStock
     * 5 LeadTime
     * 6 DailyDemand
     * 7 PurchaseRequest
     */
    while(query.next())
    {
        //订货点 为 安全库存量加上订货提前期乘以日需求量
        OrderPoint= query.value(4).toInt() +  query.value(5).toInt() *  query.value(4).toInt();
        StockQty = query.value(3).toInt() + ChangeNum;  //变更后库存数量
        if(StockQty < OrderPoint)
            PurchaseRequest=1;
        else
            PurchaseRequest=0;
    }
    query.clear();

    //更新数据
    query.exec(QString("UPDATE product_data_table SET StockQty = %1, PurchaseRequest = %2 "
                       " WHERE ProductCode = '%3'").arg(StockQty).arg(PurchaseRequest).arg(code));

}

void PutOutStorage::RightClickSlot(QPoint pos)
{
    QModelIndex index = ui->tableWidget->indexAt(pos);    //找到tableview当前位置信息
    iPosRow = index.row();    //获取到了当前右键所选的行数
    if(index.isValid())        //如果行数有效，则显示菜单
    {
        RightClickMenu->exec(QCursor::pos());
    }

}

void PutOutStorage::RightClickAddRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("插入"))   //选中了插入这个菜单
    {
        RowInit(iPosRow);
    }
}

void PutOutStorage::RightClickDeleteRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("删除"))   //选中了删除这个菜单
    {
        RowDelete(iPosRow);
    }
}

void PutOutStorage::choose_product_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    //按钮状态选择
    if(btn->property("type")=="check")
    {
        btn->setProperty("type","checked");
        style()->polish(btn);
        vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn->property("type")=="checked")
    {
        btn->setProperty("type","check");
        style()->polish(btn);
        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
    else if(btn->property("type")=="databaseBtn")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择产品");
        dc->myModel->setQuery(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table"));
        dc->myModel->setHeaderData(0,Qt::Horizontal,"产品编码");
        dc->myModel->setHeaderData(1,Qt::Horizontal,"产品名称");
        dc->myModel->setHeaderData(2,Qt::Horizontal,"产品类型");
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->tableWidget->item(row,ProductCode)->setText(list[0].data().toString());
            ui->tableWidget->item(row,ProductName)->setText(list[1].data().toString());
            ui->tableWidget->item(row,ProductType)->setText(list[2].data().toString());
        });
    }
}

void PutOutStorage::choose_batch_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    if(type == nullptr
            || tableCellText(row, ProductCode) == nullptr)
        return;

    //创建数据选择子界面
    dc=new DataChoose();
    dc->setTitle(QString("选择<font color=#00B050>%1").arg(tableCellText(row,ProductName)) + "<font color=black>批次");
    dc->myModel->setQuery(QString("SELECT ProductBatch, ProductNum, LocationID FROM storage_space_%1_table"
                                  " WHERE ProductCode = '%2'").arg(type).arg(tableCellText(row, ProductCode)));
    if(dc->myModel->rowCount()==0)
    {
        delete dc;
        warning(this , "该产品目前仓库无货");
        return;
    }
    dc->myModel->setHeaderData(0,Qt::Horizontal,"产品批次");
    dc->myModel->setHeaderData(1,Qt::Horizontal,"当前数量");
    dc->myModel->setHeaderData(2,Qt::Horizontal,"货位号");
    dc->show();

    //子界面向主界面传送选择的数据
    connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
            this, [this](const QModelIndexList& list)
    {
        ui->tableWidget->item(row,ProductBatch)->setText(list[0].data().toString());
        ui->tableWidget->item(row,StorageNum)->setText(list[1].data().toString());
        ui->tableWidget->item(row,ProductLocation)->setText(list[2].data().toString());

    });
}

//表格中添加新行
void PutOutStorage::on_add_btn_clicked()
{
    if(ui->add_btn->text()=="添加")
    {
        RowInit(ui->tableWidget->rowCount());
    }
    else if(ui->add_btn->text()=="取消操作")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);
    }
}

//表格中删除选中行
void PutOutStorage::on_delete_btn_clicked()
{
    if(ui->delete_btn->text()=="删除")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","check");
                style()->polish(btn);
            }
        }
        ui->delete_btn->setText(QString::fromUtf8("删除选中"));
        ui->delete_btn->setProperty("type","deleteConfirm");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("取消操作"));
        ui->add_btn->setProperty("type","deleteCancel");
        style()->polish(ui->add_btn);
    }
    else if(ui->delete_btn->text()=="删除选中")
    {
        sort(vecItemIndex.rbegin(), vecItemIndex.rend());
        for (unsigned int i = 0 ; i < vecItemIndex.size(); i++)
        {
            RowDelete(vecItemIndex[i]);
        }
        vecItemIndex.clear();  //清空容器
        vecItemIndex.shrink_to_fit();   //将容器的容量缩减至和实际存储元素的个数相等，释放内存

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);

        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

    }
}

void PutOutStorage::on_select_member_clicked()
{
    //创建数据选择子界面
    dc=new DataChoose();
    dc->setTitle("选择出库负责员");
    dc->myModel->setQuery("SELECT id, username, position FROM dm_user WHERE delete_flag=0");
    dc->myModel->setHeaderData(0,Qt::Horizontal,"编号");
    dc->myModel->setHeaderData(1,Qt::Horizontal,"姓名");
    dc->myModel->setHeaderData(2,Qt::Horizontal,"职位");
    dc->show();
    //子界面向主界面传送选择的数据
    connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
            this, [this](const QModelIndexList& list)
    {
        ui->select_member->setText(list[0].data().toString() + "(" + list[1].data().toString() + ")");
        ui->select_member->setProperty("type","SelectMember_ed");
        style()->polish(ui->select_member);

    });
}

//向数据库提交信息
void PutOutStorage::on_submit_btn_clicked()
{
    if(ui->type_combo->currentIndex()==-1)
    {
        warning(this,"未选择出库类型");
        return;
    }
    if(ui->storage_combo->currentIndex()==-1)
    {
        warning(this,"未选择出库仓库");
        return;
    }
    if(ui->datetime->currentText()==nullptr)
    {
        warning(this,"未选择出库日期");
        return;
    }

    if(ui->tableWidget->rowCount()==0)
    {
        warning(this,"未录入出库产品信息");
        return;
    }
    else if(CheckTable() == false)
    {
        warning(this,"产品信息录入不完整");
        return;
    }
    if(!ui->confirm_checkBox->isChecked())
    {
        warning(this,"未确认入库");
        return;
    }

    if(ui->select_member->text()=="选择成员")
    {
        warning(this,"未选择出库负责人");
        return;
    }

    int ret = QMessageBox::question(this, "上传出库单","确认上传？", QMessageBox::Yes, QMessageBox::No);
    if(ret == QMessageBox::Yes)
    {
        QSqlQuery query;
        //更新 货位表
        query.prepare(QString("UPDATE storage_space_%1_table SET ProductNum=:ProductNum"
                              " WHERE LocationID = :LocationID").arg(type));
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            query.bindValue(":LocationID",tableCellText(i, ProductLocation));
            query.bindValue(":ProductNum", tableCellText(i,StorageNum).toInt()-tableCellText(i,ProductNum).toInt());
            if(!query.exec())
            {
                warning(this,"上传失败");
                return;
            }
            UpdateMaterial(tableCellText(i,ProductCode), -tableCellText(i,ProductNum).toInt()); //更新物料表
        }
        query.clear();  //清空sql语句

        //插入 出库单表
        QString sql = QString("INSERT INTO out_storage_record_table(StorageRecordID,StorageType,StorageWarehouse,ResponsiblePersonID,dt)"
                              " VALUES ('%1','%2','%3','%4','%5');")
                .arg(ui->ID->text())
                .arg(ui->type_combo->currentText())
                .arg(ui->storage_combo->currentText())
                .arg(ui->select_member->text().left(ui->select_member->text().indexOf("(")))
                .arg(ui->datetime->currentText());
        if(!query.exec(sql))
        {
            warning(this,"上传失败");
            return;
        }
        query.clear();  //清空sql语句

        //插入 出库详细信息表
        query.prepare(QString("INSERT INTO out_storage_detail_table(StorageDetailID,ProductCode,ProductType,ProductBatch,OUTQTY,LocationID)"
                              " VALUES (:StorageDetailID,:ProductCode,:ProductType,:ProductBatch,:OUTQTY,:LocationID);"));
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            query.bindValue(":StorageDetailID",tableCellText(i,DetailID));
            query.bindValue(":ProductCode", tableCellText(i,ProductCode));
            query.bindValue(":ProductType", tableCellText(i,ProductType));
            query.bindValue(":ProductBatch", tableCellText(i,ProductBatch));
            query.bindValue(":OUTQTY", tableCellText(i,ProductNum).toInt());
            query.bindValue(":LocationID",tableCellText(i,ProductLocation));
            if(!query.exec())
            {
                warning(this,"上传失败");
                return;
            }
        }
        query.clear();  //清空sql语句

        //插入 出库关系表
        query.prepare(QString("INSERT INTO out_storage_relation_table(StorageRecordID,StorageDetailID)"
                              " VALUES (:StorageRecordID,:StorageDetailID);"));

        query.bindValue(":StorageRecordID",ui->ID->text());
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            query.bindValue(":StorageDetailID", tableCellText(i,DetailID));
            if(!query.exec())
            {
                warning(this,"上传失败");
                return;
            }
        }
        query.clear();  //清空sql语句

        //修改Salelist
        Salelist.removeDuplicates();
        for(int i=0;i<Salelist.count();i++)
        {
            query.exec(QString("UPDATE 订单统计表 SET 出库单号='%1' WHERE 销售订单详细编号 = '%2'")
                       .arg(ui->ID->text()).arg(Salelist.at(i)));
        }
        //修改Salelist2
        Salelist2.removeDuplicates();
        for(int i=0;i<Salelist2.count();i++)
        {
            query.exec(QString("UPDATE 换货统计表 SET 出库单号='%1' WHERE 换货单详细编号 = '%2'")
                       .arg(ui->ID->text()).arg(Salelist2.at(i)));
        }

        //修改Planlist
        Planlist.removeDuplicates();
        for(int i=0;i<Planlist.count();i++)
        {
            query.exec(QString("UPDATE picking SET 出库单号='%1' WHERE 生产领料单号 = '%2'")
                       .arg(ui->ID->text()).arg(Planlist.at(i)));
        }


        QMessageBox::information(this, "上传出库单","上传成功");
        clear();
    }

}

void PutOutStorage::on_tableWidget_cellChanged(int row, int column)
{
    if(column == ProductCode || column == DetailID)
    {
        ui->tableWidget->item(row, ProductBatch)->setText("");
        ui->tableWidget->item(row, StorageNum)->setText("");
        ui->tableWidget->item(row, ProductLocation)->setText("");
    }
}

void PutOutStorage::on_storage_combo_currentIndexChanged(const QString &arg1)
{
    type = arg1.right(2);  //截取后两位

    IDGenerate();       //生成出库编号
    DetailIDGenerate(0);  //刷新出库详细信息
}


void PutOutStorage::on_type_combo_currentIndexChanged(const QString &arg1)
{
    IDGenerate();       //生成出库编号
    DetailIDGenerate(0);  //刷新出库详细信息
}

void PutOutStorage::on_datetime_editTextChanged(const QString &arg1)
{
    IDGenerate();       //生成出库编号
    DetailIDGenerate(0);  //刷新出库详细信息
}

void PutOutStorage::clear()
{
    ui->comboBox->setCurrentIndex(-1);
    ui->type_combo->setCurrentIndex(-1);
    ui->storage_combo->setCurrentIndex(-1);
    ui->datetime->setEditText("");
    ui->ID->setText("");
    ui->select_member->setText("选择成员");
    ui->select_member->setProperty("type","SelectMember");
    style()->polish(ui->select_member);
    ui->confirm_checkBox->setCheckState(Qt::Unchecked);
    type = "";
    for(int i = ui->tableWidget->rowCount()-1; i>=0 ; i--)
    {
        RowDelete(i);
    }
    Salelist.clear();
    Salelist2.clear();
    Planlist.clear();
}

void PutOutStorage::on_comboBox_currentTextChanged(const QString &arg1)
{
    if(ui->comboBox->currentText() == "导入销售订单")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择销售出库单");
        dc->myModel->setQuery(QString("SELECT 销售订单详细编号, 产品编码, 产品名称, 销售数量, 出库单号 FROM 订单统计表"
                                      " WHERE 出库单号 IS Null"));
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->type_combo->setCurrentText("销售出库");
            RowInit(ui->tableWidget->rowCount());
            QSqlQuery query;
            query.exec(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table WHERE ProductCode = '%1'")
                       .arg(list[1].data().toString()));
            if(query.next())
            {
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductCode)->setText(query.value(0).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductName)->setText(query.value(1).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductType)->setText(query.value(2).toString());
            }
            ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductNum)->setText(list[3].data().toString());
            Salelist.append(list[0].data().toString());
        });
    }
    else if(ui->comboBox->currentText() == "导入领料单")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择领料单");
        dc->myModel->setQuery(QString("SELECT 生产领料单号, 产品编码, 产品名称, 需求数量, 出库单号 FROM picking"
                                      " WHERE 出库单号 IS Null"));
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->type_combo->setCurrentText("领料出库");
            RowInit(ui->tableWidget->rowCount());
            QSqlQuery query;
            query.exec(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table WHERE ProductCode = '%1'")
                       .arg(list[1].data().toString()));
            if(query.next())
            {
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductCode)->setText(query.value(0).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductName)->setText(query.value(1).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductType)->setText(query.value(2).toString());
            }
            ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductNum)->setText(list[3].data().toString());
            Planlist.append(list[0].data().toString());
        });

    }
    else if(ui->comboBox->currentText() == "导入销售换货单")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择销售出库单");
        dc->myModel->setQuery(QString("SELECT 换货单详细编号, 产品编码, 产品名称, 换货产品总数, 出库单号 FROM 换货统计表"
                                      " WHERE 出库单号 IS Null"));
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->type_combo->setCurrentText("销售换货出库");
            RowInit(ui->tableWidget->rowCount());
            QSqlQuery query;
            query.exec(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table WHERE ProductCode = '%1'")
                       .arg(list[1].data().toString()));
            if(query.next())
            {
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductCode)->setText(query.value(0).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductName)->setText(query.value(1).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductType)->setText(query.value(2).toString());
            }
            ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductNum)->setText(list[3].data().toString());
            Salelist2.append(list[0].data().toString());
        });
    }
}
