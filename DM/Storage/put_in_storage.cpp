﻿#include "put_in_storage.h"
#include "ui_put_in_storage.h"

#include <QTableWidget>

//tablewidget的列号
#define DetailID 0
#define ChooseProduct 1
#define ProductCode 2
#define ProductName 3
#define ProductType 4
#define ProductBatch 5
#define ProductNum 6
#define ProductLocation 7
#define DIGIT "^[0-9]*$"   //仅输入数字的正则


PutInStorage::PutInStorage(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PutInStorage)
{
    ui->setupUi(this);

    ui->storage_combo->addItems(WarehouseList());
    ui->storage_combo->setCurrentIndex(-1);

    ui->tableWidget->verticalHeader()->setVisible(false);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(ChooseProduct,QHeaderView::Fixed);//第二列“产品选择”宽度设置
    ui->tableWidget->setColumnWidth(ChooseProduct,75);

    //设置列不可编辑
    ReadOnlyDelegate* readOnlyDelegate = new ReadOnlyDelegate(this);
    ui->tableWidget->setItemDelegateForColumn(DetailID, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductCode, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductName, readOnlyDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductType, readOnlyDelegate);

    //设置几列输入格式
    SetTextDelegate* setTextDelegate = new SetTextDelegate(this);
    setTextDelegate->reg = new QRegExpValidator(QRegExp(DIGIT));
    setTextDelegate->MaxLength = 5;
    ui->tableWidget->setItemDelegateForColumn(ProductBatch, setTextDelegate);
    ui->tableWidget->setItemDelegateForColumn(ProductNum, setTextDelegate);

    RightClickMenu=new QMenu(ui->tableWidget);
    m_addAction=new QAction(QString::fromUtf8("插入"),this);
    m_deleteAction=new QAction(QString::fromUtf8("删除"),this);
    RightClickMenu->addAction(m_addAction);
    RightClickMenu->addAction(m_deleteAction);

    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(RightClickSlot(QPoint)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickAddRow(QAction*)));
    connect(RightClickMenu,SIGNAL(triggered(QAction*)),this,SLOT(RightClickDeleteRow(QAction*)));
}

PutInStorage::~PutInStorage()
{
    delete ui;
}

QString PutInStorage::tableCellText(int row, int column)
{
    switch (column) {
    case DetailID :
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductCode:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductName:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductType:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductBatch:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductNum:
        return ui->tableWidget->item(row,column)->text();
        break;
    case ProductLocation:
    {
        DatabaseComboBox *cbbox = (DatabaseComboBox*)( ui->tableWidget->cellWidget(row, column));
        return cbbox->currentText();
        break;
    }
    default:
        break;
    }
    return nullptr;
}

void PutInStorage::RowInit(int index)
{
    ui->tableWidget->insertRow(index); //在最后一行的后面插入一行

    //在插入的行中输入产品选择按钮从数据库获得产品数据
    QPushButton *choose_btn = new QPushButton();
    choose_btn->setObjectName("choose_btn" + QString::number(index));
    choose_btn->setProperty("type", "databaseBtn"); //数据库按钮样式

    QWidget *widget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout();
    layout->setMargin(0);//一定要有
    layout->addWidget(choose_btn);
    layout->setAlignment(choose_btn, Qt::AlignCenter);//控件在布局中居中显示
    widget->setLayout(layout);

    ui->tableWidget->setCellWidget(index, ChooseProduct, widget);//放在表格第2列
    connect(choose_btn, &QPushButton::clicked, this, &PutInStorage::choose_btn_clicked);

    DatabaseComboBox *location_combo=new DatabaseComboBox();
    location_combo->setProperty("type", "dataBaseCombo"); //数据库选择框样式
    ui->tableWidget->setCellWidget(index, ProductLocation, location_combo);
    for(int i=0;i<ui->tableWidget->columnCount();i++)
    {
        if(i==ChooseProduct || i==ProductLocation)
            continue;
        else
        {
            ui->tableWidget->setItem(index,i,new QTableWidgetItem);
            ui->tableWidget->item(index,i)->setTextAlignment(Qt::AlignCenter | Qt::AlignCenter);
        }
    }

    //生成序号
    DetailIDGenerate(index);
}

void PutInStorage::RowDelete(int index)
{
    if(index!=-1)
    {
        for(int i=0;i<ui->tableWidget->columnCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(index,i);
            //确保不为空
            if(widget)
            {
                ui->tableWidget->removeCellWidget(index,i);
                delete widget;  //释放
            }
        }
        ui->tableWidget->removeRow(index);
        DetailIDGenerate(index);
    }
}

// 入库详细单号生成
//eg: R Y1 20231023 001 01 , 16位
void PutInStorage::DetailIDGenerate(int index)
{
    if(ui->tableWidget->rowCount()==0 || ui->ID->text()==nullptr)
        return;

    QString limit=ui->ID->text();
    for(int i=index; i<ui->tableWidget->rowCount(); i++)
    {
        if(i==0)
            ui->tableWidget->item(i,DetailID)->setText(limit+"01");
        else
        {
            QString num=NumberIncrease(ui->tableWidget->item(i-1,DetailID)->text().right(2)); //截取上一单元格后两位,自增
            ui->tableWidget->item(i,DetailID)->setText(limit+num);
        }
    }
}

//检查表格是否完整
bool PutInStorage::CheckTable()
{
    for(int i=0; i<ui->tableWidget->rowCount();i++)
    {
        for(int j=2;j<ui->tableWidget->columnCount();j++)   //前两列不用检查
        {
            if(tableCellText(i,j)==nullptr)
            {
                return false;
            }
        }
    }
    return true;
}

int PutInStorage::getLocationMAXMUM(QString keytype , int row)
{
    if(row < 0)
        return 0;

    QSqlQuery query = mydb->ExactMatchQuery(QString("storage_space_%1_table").arg(keytype),"LocationID"
                                            ,tableCellText(row,ProductLocation).left(5));
    if(!query.next())
    {
        qDebug()<<"getLocationMAXMUM: "<<query.lastError();
        return 0;
    }
    return query.value(4).toInt();
}

void PutInStorage::UpdateMaterial(QString code, int ChangeNum)
{
    QSqlQuery query;
    int StockQty;  //变更后库存数量
    query.exec(QString("SELECT * FROM product_data_table WHERE ProductCode = '%1'").arg(code));
    /* 3 StockQty
     * 4 SafeStock
     * 5 LeadTime
     * 6 DailyDemand
     * 7 PurchaseRequest
     */
    while(query.next())
    {
        StockQty = query.value(3).toInt() + ChangeNum;  //变更后库存数量
    }
    query.clear();
    //更新数据
    query.exec(QString("UPDATE product_data_table SET StockQty = %1"
                       " WHERE ProductCode = '%2'").arg(StockQty).arg(code));
}

QStringList PutInStorage::getLocationlist(int row)
{
    QStringList list;
    list.clear();

    if(ui->tableWidget->item(row,ProductCode) ==nullptr
            || ui->tableWidget->item(row,ProductBatch) ==nullptr
            || tableCellText(row,ProductCode) ==nullptr
            || tableCellText(row,ProductBatch) ==nullptr
            || type==nullptr)

        return list;

    QSqlQuery query = mydb->ExactMatchQuery(QString("storage_space_%1_table").arg(type),"ProductCode"
                                            ,tableCellText(row,ProductCode));
    //优先展示已填充的货位
    while(query.next())
    {
        if(query.value(1).toString()==tableCellText(row,ProductBatch))
        {
            int num = query.value(4).toInt()-query.value(3).toInt();
            if(num>0)
                list.append(query.value(0).toString()+QString(" (余%1)").arg(num));
        }
    }

    //展示未填充的货位
    if((!query.exec(QString("SELECT * FROM storage_space_%1_table WHERE ProductNum=0; ").arg(type)))) //如果list为空，且填满
    {
        qDebug()<<query.lastError();
        qDebug()<<list;
        qDebug()<<type;
        if(list.empty())
            warning(this, "该仓库没有相匹配的货位");
        return list;
    }
    while(query.next())
    {
        int num = query.value(4).toInt()-query.value(3).toInt();
        list.append(query.value(0).toString()+QString(" (余%1)").arg(num));
    }

    list.removeDuplicates();  //移除重复元素

    return list;
}

void PutInStorage::RightClickSlot(QPoint pos)
{

    QModelIndex index = ui->tableWidget->indexAt(pos);    //找到tableview当前位置信息
    iPosRow = index.row();    //获取到了当前右键所选的行数
    if(index.isValid())        //如果行数有效，则显示菜单
    {
        RightClickMenu->exec(QCursor::pos());
    }

}

void PutInStorage::RightClickAddRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("插入"))   //选中了插入这个菜单
    {
        RowInit(iPosRow);
    }
}

void PutInStorage::RightClickDeleteRow(QAction *act)
{
    if(act->text() == QString::fromUtf8("删除"))   //选中了删除这个菜单
    {
        RowDelete(iPosRow);
    }
}

void PutInStorage::choose_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());
    if (btn == nullptr)
        return;

    // 获取按钮所在的行
    QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    row = qIndex.row();

    //按钮状态选择
    if(btn->property("type")=="check")
    {
        btn->setProperty("type","checked");
        style()->polish(btn);
        vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn->property("type")=="checked")
    {
        btn->setProperty("type","check");
        style()->polish(btn);
        vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
    else if(btn->property("type")=="databaseBtn")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择产品");
        dc->myModel->setQuery(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table"));
        dc->myModel->setHeaderData(0,Qt::Horizontal,"产品编码");
        dc->myModel->setHeaderData(1,Qt::Horizontal,"产品名称");
        dc->myModel->setHeaderData(2,Qt::Horizontal,"产品类型");
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->tableWidget->item(row,ProductCode)->setText(list[0].data().toString());
            ui->tableWidget->item(row,ProductName)->setText(list[1].data().toString());
            ui->tableWidget->item(row,ProductType)->setText(list[2].data().toString());
        });
    }
}

//eg: R Y1 20231023 001  , 14位
void PutInStorage::IDGenerate()
{
    if(ui->type_combo->currentIndex()==-1)   //入库类型未选
        return;

    if(ui->storage_combo->currentIndex()==-1)   //入库仓库未选
        return;

    if(ui->datetime->currentText()=="")     //入库时间未选
        return;

    QString time=ui->datetime->dateToStr().remove(QChar('-'), Qt::CaseInsensitive),   //去除字符‘-’
            limit = "R"+type;

    limit += time;
    QSqlQuery query= mydb->SortData("storage_record_table","StorageRecordID",false,"StorageRecordID",limit); //降序排列

    if(query.next())
    {
        QString str=query.value(0).toString();
        QString num=NumberIncrease(str.right(3)); //截取后3位,自增
        ui->ID->setText(limit+num);
    }
    else
        ui->ID->setText(limit+"001");
}

//表格中添加新行
void PutInStorage::on_add_btn_clicked()
{
    if(ui->add_btn->text()=="添加")
    {
        RowInit(ui->tableWidget->rowCount());
    }
    else if(ui->add_btn->text()=="取消操作")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);
    }
}

//表格中删除选中行
void PutInStorage::on_delete_btn_clicked()
{
    if(ui->delete_btn->text()=="删除")
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","check");
                style()->polish(btn);
            }
        }
        ui->delete_btn->setText(QString::fromUtf8("删除选中"));
        ui->delete_btn->setProperty("type","deleteConfirm");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("取消操作"));
        ui->add_btn->setProperty("type","deleteCancel");
        style()->polish(ui->add_btn);
    }
    else if(ui->delete_btn->text()=="删除选中")
    {
        sort(vecItemIndex.rbegin(), vecItemIndex.rend());
        for (unsigned int i = 0 ; i < vecItemIndex.size(); i++)
        {
            RowDelete(vecItemIndex[i]);
        }
        vecItemIndex.clear();  //清空容器
        vecItemIndex.shrink_to_fit();   //将容器的容量缩减至和实际存储元素的个数相等，释放内存
        ui->delete_btn->setText(QString::fromUtf8("删除"));
        ui->delete_btn->setProperty("type","deleteData");
        style()->polish(ui->delete_btn);

        ui->add_btn->setText(QString::fromUtf8("添加"));
        ui->add_btn->setProperty("type","addData");
        style()->polish(ui->add_btn);

        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QWidget *widget=ui->tableWidget->cellWidget(i,1);
            if(widget)
            {
                QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
                btn->setProperty("type","databaseBtn");
                style()->polish(btn);
            }
        }

    }
}

void PutInStorage::on_select_member_clicked()
{
    //创建数据选择子界面
    dc=new DataChoose();
    dc->setTitle("选择入库负责员");
    dc->myModel->setQuery("SELECT id, username, position FROM dm_user WHERE delete_flag=0");
    dc->myModel->setHeaderData(0,Qt::Horizontal,"编号");
    dc->myModel->setHeaderData(1,Qt::Horizontal,"姓名");
    dc->myModel->setHeaderData(2,Qt::Horizontal,"职位");
    dc->show();
    //子界面向主界面传送选择的数据
    connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
            this, [this](const QModelIndexList& list)
    {
        ui->select_member->setText(list[0].data().toString() + "(" + list[1].data().toString() + ")");
        ui->select_member->setProperty("type","SelectMember_ed");
        style()->polish(ui->select_member);

    });
}

//向数据库提交信息
void PutInStorage::on_submit_btn_clicked()
{
    if(ui->type_combo->currentIndex()==-1)
    {
        warning(this,"未选择入库类型");
        return;
    }
    if(ui->storage_combo->currentIndex()==-1)
    {
        warning(this,"未选择入库仓库");
        return;
    }
    if(ui->datetime->currentText()==nullptr)
    {
        warning(this,"未选择入库日期");
        return;
    }
    if(ui->tableWidget->rowCount()==0)
    {
        warning(this,"未录入产品信息");
        return;
    }
    else if(CheckTable() == false)
    {
        warning(this,"产品信息录入不完整");
        return;
    }
    else
    {
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QString str = tableCellText(i,ProductLocation).right(3).left(2);
            if(str == "00")
                str = "100";
            if(tableCellText(i, ProductNum).toInt() > str.toInt()
                    || tableCellText(i, ProductNum).toInt() ==0)
            {
                warning(this,QString("第%1行：入库数量出错").arg(i));
                return;
            }
        }

    }

    if(!ui->confirm_checkBox->isChecked())
    {
        warning(this,"未确认入库");
        return;
    }

    if(ui->select_member->text()=="选择成员")
    {
        warning(this,"未选择入库负责人");
        return;
    }

    int ret = QMessageBox::question(this, "上传入库单","确认上传？", QMessageBox::Yes, QMessageBox::No);
    if(ret == QMessageBox::Yes)
    {
        QSqlQuery query;
        //更新 货位表  ,如果表中有，则修改

        query.prepare(QString("UPDATE storage_space_%1_table "
                              "SET ProductBatch=:ProductBatch, ProductCode=:ProductCode, ProductNum=:ProductNum "
                              "WHERE LocationID = :LocationID").arg(type));
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QString str_Location = tableCellText(i,ProductLocation);
            QString num = str_Location.mid(str_Location.indexOf("余")+1);
            num = num.left(num.indexOf(")"));
            str_Location = str_Location.left(str_Location.indexOf(" "));

            query.bindValue(":LocationID",str_Location);
            query.bindValue(":ProductBatch",tableCellText(i,ProductBatch));
            query.bindValue(":ProductCode", tableCellText(i,ProductCode));
            query.bindValue(":ProductNum", getLocationMAXMUM(type,i)-num.toInt()+tableCellText(i,ProductNum).toInt());
            if(!query.exec())
            {
                warning(this,"上传失败");
                return;
            }
            UpdateMaterial(tableCellText(i,ProductCode), tableCellText(i,ProductNum).toInt());
        }
        query.clear();  //清空sql语句

        //插入 入库单表
        QString sql = QString("INSERT INTO storage_record_table(StorageRecordID,StorageType,StorageWarehouse,ResponsiblePersonID,dt)"
                              " VALUES ('%1','%2','%3','%4','%5');")
                .arg(ui->ID->text())
                .arg(ui->type_combo->currentText())
                .arg(ui->storage_combo->currentText())
                .arg(ui->select_member->text().left(ui->select_member->text().indexOf("(")))
                .arg(ui->datetime->currentText());
        if(!query.exec(sql))
        {
            warning(this,"上传失败");
            return;
        }
        query.clear();  //清空sql语句

        //插入 入库详细信息表
        query.prepare(QString("INSERT INTO storage_detail_table(StorageDetailID,ProductCode,ProductType,ProductBatch,INQTY,LocationID)"
                              " VALUES (:StorageDetailID,:ProductCode,:ProductType,:ProductBatch,:INQTY,:LocationID);"));
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            QString str_Location = tableCellText(i,ProductLocation);
            str_Location = str_Location.left(str_Location.indexOf(" "));

            query.bindValue(":StorageDetailID",tableCellText(i,DetailID));
            query.bindValue(":ProductCode", tableCellText(i,ProductCode));
            query.bindValue(":ProductType", tableCellText(i,ProductType));
            query.bindValue(":ProductBatch", tableCellText(i,ProductBatch));
            query.bindValue(":INQTY", tableCellText(i,ProductNum).toInt());
            query.bindValue(":LocationID",str_Location);
            if(!query.exec())
            {
                warning(this,"上传失败");
                return;
            }
        }
        query.clear();  //清空sql语句

        //插入 入库关系表
        query.prepare(QString("INSERT INTO storage_relation_table(StorageRecordID,StorageDetailID)"
                              " VALUES (:StorageRecordID,:StorageDetailID);"));
        query.bindValue(":StorageRecordID",ui->ID->text());
        for(int i=0;i<ui->tableWidget->rowCount();i++)
        {
            query.bindValue(":StorageDetailID", tableCellText(i,DetailID));
            if(!query.exec())
            {
                warning(this,"上传失败");
                return;
            }
        }
        query.clear();  //清空sql语句

        //修改Planlist
        Planlist.removeDuplicates();
        for(int i=0;i<Planlist.count();i++)
        {
            query.exec(QString("UPDATE testplanning SET 入库单号='%1' WHERE 序号 = '%2'")
                       .arg(ui->ID->text()).arg(Planlist.at(i)));
        }
        //修改Salelist
        Salelist.removeDuplicates();
        for(int i=0;i<Salelist.count();i++)
        {
            query.exec(QString("UPDATE 退货统计表 SET 入库单号='%1' WHERE 退货单详细编号 = '%2'")
                       .arg(ui->ID->text()).arg(Salelist.at(i)));
        }
        //修改Salelist2
        Salelist2.removeDuplicates();
        for(int i=0;i<Salelist2.count();i++)
        {
            query.exec(QString("UPDATE 换货统计表 SET 入库单号='%1' WHERE 换货单详细编号 = '%2'")
                       .arg(ui->ID->text()).arg(Salelist2.at(i)));
        }
        QMessageBox::information(this, "上传入库单","上传成功");
        this->clear();
    }
}

void PutInStorage::on_tableWidget_cellChanged(int row, int column)
{
    if(column==DetailID || column==ProductBatch || column== ProductCode)
    {
        DatabaseComboBox *cbbox = (DatabaseComboBox*)( ui->tableWidget->cellWidget(row, ProductLocation));
        if(cbbox)
        {
            cbbox->list = getLocationlist(row);
            cbbox->setCurrentIndex(-1);
        }
    }
}

void PutInStorage::on_storage_combo_currentIndexChanged(const QString &arg1)
{
    type = arg1.right(2);  //截取后两位

    IDGenerate();       //生成入库编号
    DetailIDGenerate(0);  //刷新入库详细信息
}

void PutInStorage::on_type_combo_currentIndexChanged(const QString &arg1)
{
    IDGenerate();
    DetailIDGenerate(0);
}

void PutInStorage::on_datetime_editTextChanged(const QString &arg1)
{
    IDGenerate();
    DetailIDGenerate(0);
}

void PutInStorage::clear()
{
    ui->comboBox->setCurrentIndex(-1);
    ui->type_combo->setCurrentIndex(-1);
    ui->storage_combo->setCurrentIndex(-1);
    ui->datetime->setEditText("");
    ui->ID->setText("");
    ui->select_member->setText("选择成员");
    ui->select_member->setProperty("type","SelectMember");
    style()->polish(ui->select_member);
    ui->confirm_checkBox->setCheckState(Qt::Unchecked);
    type = "";
    for(int i = ui->tableWidget->rowCount()-1; i>=0 ; i--)
    {
        RowDelete(i);
    }
    Planlist.clear();
    Salelist.clear();
    Salelist2.clear();
}

void PutInStorage::on_comboBox_currentTextChanged(const QString &arg1)
{
    if(ui->comboBox->currentText() == "导入生产成品单")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择成品入库单");
        dc->myModel->setQuery(QString("SELECT 序号, 产品编码, 产品名称, 本次入库数量, 产品批次号, 生产工单, 入库单号 FROM testplanning"
                                      " WHERE 入库单号 IS Null"));
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->type_combo->setCurrentText("生产成品入库");
            RowInit(ui->tableWidget->rowCount());
            QSqlQuery query;
            query.exec(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table WHERE ProductCode = '%1'")
                       .arg(list[1].data().toString()));
            if(query.next())
            {
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductCode)->setText(query.value(0).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductName)->setText(query.value(1).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductType)->setText(query.value(2).toString());
            }
            ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductBatch)->setText(list[4].data().toString());
            ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductNum)->setText(list[3].data().toString());
            Planlist.append(list[0].data().toString());
        });
    }
    else if(ui->comboBox->currentText() == "导入销售退货单")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择销售退货单");
        dc->myModel->setQuery(QString("SELECT 退货单详细编号, 产品编码, 产品名称, 退货产品总数, 入库单号 FROM 退货统计表"
                                      " WHERE 入库单号 IS Null"));
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->type_combo->setCurrentText("销售退货入库");
            RowInit(ui->tableWidget->rowCount());
            QSqlQuery query;
            query.exec(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table WHERE ProductCode = '%1'")
                       .arg(list[1].data().toString()));
            if(query.next())
            {
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductCode)->setText(query.value(0).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductName)->setText(query.value(1).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductType)->setText(query.value(2).toString());
            }
            ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductNum)->setText(list[3].data().toString());
            Salelist.append(list[0].data().toString());
        });

    }
    else if(ui->comboBox->currentText() == "导入销售换货单")
    {
        //创建数据选择子界面
        dc=new DataChoose();
        dc->setTitle("选择销售换货单");
        dc->myModel->setQuery(QString("SELECT 换货单详细编号, 产品编码, 产品名称, 换货产品总数, 入库单号 FROM 换货统计表"
                                      " WHERE 入库单号 IS Null"));
        dc->show();
        //子界面向主界面传送选择的数据
        connect(dc,static_cast<void(DataChoose::*)(const QModelIndexList&)>(&DataChoose::singal_selectedItems),
                this, [this](const QModelIndexList& list)
        {
            ui->type_combo->setCurrentText("销售换货入库");
            RowInit(ui->tableWidget->rowCount());
            QSqlQuery query;
            query.exec(QString("SELECT ProductCode, ProductName, ProductType FROM product_data_table WHERE ProductCode = '%1'")
                       .arg(list[1].data().toString()));
            if(query.next())
            {
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductCode)->setText(query.value(0).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductName)->setText(query.value(1).toString());
                ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductType)->setText(query.value(2).toString());
            }
            ui->tableWidget->item(ui->tableWidget->rowCount()-1,ProductNum)->setText(list[3].data().toString());
            Salelist2.append(list[0].data().toString());
        });
    }
}
