﻿#ifndef TRANSFER_STORAGE_H
#define TRANSFER_STORAGE_H

#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include <QTableWidget>
#include <QTableWidgetItem>

#include "Tool/data_choose.h"
#include "DataBase/database.h"
#include "Tool/toolfunction.h"
#include "Tool/database_comboBox.h"

namespace Ui {
class TransferStorage;
}

class TransferStorage : public QWidget
{
    Q_OBJECT

public:
    explicit TransferStorage(QWidget *parent = nullptr);
    ~TransferStorage();

    QString tableCellText(int row, int column); //获取单元格的内容

private:
    int iPosRow;

    std::vector<int> vecItemIndex;//用于保存选中行的行号

    DataChoose *dc;

    int row;    //  当前行

    QMenu *RightClickMenu;  //右键点击菜单
    QAction *m_addAction;     //插入行
    QAction *m_deleteAction;  //删除行

    QString Out_type;   //移出仓库类型
    QString Into_type;   //移出仓库类型

    void RowInit(int index); //行的每个单元格初始化

    void RowDelete(int index);  //删除行

    QStringList getLocationlist(int row, bool isOutStorage);   //得到货位信息 , bool 是否是调出库

    void IDGenerate();      //生成移库单号

    void DetailIDGenerate(int index);        //生成盘库明细单号

    bool CheckTable();  //检查表格数据是否填写完整

    bool OutStorage(); //调拨出库

    bool InStorage();  //调拨入库



    QString StorageID(QString str); //生成调拨入库单号，str= R ，则入 ； C 则出

    QStringList StorageDetailID(QString str,int rowCount);  //生成调拨出入库明细单号

    int getLocationMAXMUM(QString keytype, int row, QString Location);

private slots:

    void RightClickSlot(QPoint pos);        //菜单点击，获取当前位置

    void RightClickAddRow(QAction *act);        //得知菜单当前的位置并向上插入一行

    void RightClickDeleteRow(QAction *act);   //得知菜单当前的位置并删除

    void choose_product_btn_clicked();      //点击按钮后选择产品基本信息

    void choose_batch_btn_clicked();        //点击按钮后选择产品批次信息

    void Out_combo_currentTextChanged(QString arg);  //调出货位变化

    void Into_combo_currentTextChanged(QString arg); //调入货位变化

private slots:

    void on_add_btn_clicked();

    void on_delete_btn_clicked();

    void on_submit_btn_clicked();

    void on_select_member_btn_clicked();

    void on_type_combo_currentIndexChanged(const QString &arg1);

    void on_out_storage_combo_currentIndexChanged(const QString &arg1);

    void on_into_storage_combo_currentIndexChanged(const QString &arg1);

    void on_start_date_editTextChanged(const QString &arg1);

    void on_tableWidget_cellChanged(int row, int column);

private:
    Ui::TransferStorage *ui;

    void clear();    //清除所有内容
};

#endif // TRANSFER_STORAGE_H
