﻿#include "storage_stat.h"
#include "ui_storage_stat.h"

#include "mainwindow.h"


StorageStat::StorageStat(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StorageStat)
{
    ui->setupUi(this);

    ui->In_tableView->verticalHeader()->setVisible(false);
    ui->In_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    InModel = new QSqlQueryModel(this);
    ui->In_tableView->setModel(InModel);
    ui->In_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->In_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);     //整行选中

    ui->Out_tableView->verticalHeader()->setVisible(false);
    ui->Out_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    OutModel = new QSqlQueryModel(this);
    ui->Out_tableView->setModel(OutModel);
    ui->Out_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->Out_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);     //整行选中

    ui->Take_tableView->verticalHeader()->setVisible(false);
    ui->Take_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    TakeModel = new QSqlQueryModel(this);
    ui->Take_tableView->setModel(TakeModel);
    ui->Take_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->Take_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);     //整行选中

    ui->Transfer_tableView->verticalHeader()->setVisible(false);
    ui->Transfer_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    TransferModel = new QSqlQueryModel(this);
    ui->Transfer_tableView->setModel(TransferModel);
    ui->Transfer_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->Transfer_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);     //整行选中

    ui->Product_tableView->verticalHeader()->setVisible(false);
    ui->Product_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ProductDataModel = new QSqlQueryModel(this);
    ui->Product_tableView->setModel(ProductDataModel);
    ui->Product_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->Product_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);     //整行选中

    QAction* searchAction = new QAction();
    searchAction->setIcon(QIcon(":/Resources/Pictures/search.png"));
    ui->InEdit->addAction(searchAction,QLineEdit::LeadingPosition);
    ui->OutEdit->addAction(searchAction,QLineEdit::LeadingPosition);
    ui->TakeEdit->addAction(searchAction,QLineEdit::LeadingPosition);
    ui->TransferEdit->addAction(searchAction,QLineEdit::LeadingPosition);
    ui->ProductEdit->addAction(searchAction,QLineEdit::LeadingPosition);

    ui->tabWidget->setCurrentWidget(ui->InStorage);
}

StorageStat::~StorageStat()
{
    delete ui;
}

void StorageStat::on_tabWidget_currentChanged(int index)
{
    if(ui->tabWidget->currentWidget() == ui->InStorage)
    {
        InStorageInit();
    }
    else if(ui->tabWidget->currentWidget() == ui->OutStorage)
    {
        OutStorageInit();
    }
    else if(ui->tabWidget->currentWidget() == ui->TransferStorage)
    {
        TransferStorageInit();
    }
    else if(ui->tabWidget->currentWidget() == ui->TakeStorage)
    {
        TakeStorageInit();
    }
    else if(ui->tabWidget->currentWidget() == ui->ProductData)
    {
        ProductDataInit();
    }
}

void StorageStat::InStorageInit()
{
    InModel->setQuery(QString("SELECT * FROM storage_record_table "
                              " join storage_detail_table on storage_detail_table.StorageDetailID "
                              " like concat(storage_record_table.StorageRecordID, '%')"));
    InModel->setHeaderData(0,Qt::Horizontal,"入库单号");
    InModel->setHeaderData(1,Qt::Horizontal,"入库类型");
    InModel->setHeaderData(2,Qt::Horizontal,"入库仓库");
    InModel->setHeaderData(3,Qt::Horizontal,"负责人编号");
    InModel->setHeaderData(4,Qt::Horizontal,"日期");
    InModel->setHeaderData(5,Qt::Horizontal,"入库明细单号");
    InModel->setHeaderData(6,Qt::Horizontal,"产品编码");
    InModel->setHeaderData(7,Qt::Horizontal,"产品类型");
    InModel->setHeaderData(8,Qt::Horizontal,"产品批次");
    InModel->setHeaderData(7,Qt::Horizontal,"入库数量");
    InModel->setHeaderData(8,Qt::Horizontal,"货位号");
    MergeTable(ui->In_tableView, 5);
}

void StorageStat::OutStorageInit()
{
    OutModel->setQuery(QString("SELECT * FROM out_storage_record_table "
                              " join out_storage_detail_table on out_storage_detail_table.StorageDetailID "
                              " like concat(out_storage_record_table.StorageRecordID, '%')"));
    OutModel->setHeaderData(0,Qt::Horizontal,"出库单号");
    OutModel->setHeaderData(1,Qt::Horizontal,"出库类型");
    OutModel->setHeaderData(2,Qt::Horizontal,"出库仓库");
    OutModel->setHeaderData(3,Qt::Horizontal,"负责人编号");
    OutModel->setHeaderData(4,Qt::Horizontal,"日期");
    OutModel->setHeaderData(5,Qt::Horizontal,"出库明细单号");
    OutModel->setHeaderData(6,Qt::Horizontal,"产品编码");
    OutModel->setHeaderData(7,Qt::Horizontal,"产品类型");
    OutModel->setHeaderData(8,Qt::Horizontal,"产品批次");
    OutModel->setHeaderData(7,Qt::Horizontal,"出库数量");
    OutModel->setHeaderData(8,Qt::Horizontal,"货位号");
    MergeTable(ui->Out_tableView, 5);
}

void StorageStat::TakeStorageInit()
{
    TakeModel->setQuery(QString("SELECT * FROM take_stock_record_table "
                              " join take_stock_detail_table on take_stock_detail_table.TakeStockDetailID "
                              " like concat(take_stock_record_table.TakeStockRecordID, '%')"));
    TakeModel->setHeaderData(0,Qt::Horizontal,"盘库单号");
    TakeModel->setHeaderData(1,Qt::Horizontal,"盘点类型");
    TakeModel->setHeaderData(2,Qt::Horizontal,"盘点仓库");
    TakeModel->setHeaderData(3,Qt::Horizontal,"盘点人编号");
    TakeModel->setHeaderData(4,Qt::Horizontal,"盘点开始日期");
    TakeModel->setHeaderData(5,Qt::Horizontal,"盘点结束日期");
    TakeModel->setHeaderData(6,Qt::Horizontal,"盘库明细单号");
    TakeModel->setHeaderData(7,Qt::Horizontal,"产品编码");
    TakeModel->setHeaderData(8,Qt::Horizontal,"产品类型");
    TakeModel->setHeaderData(9,Qt::Horizontal,"产品批次");
    TakeModel->setHeaderData(10,Qt::Horizontal,"货位号");
    TakeModel->setHeaderData(11,Qt::Horizontal,"账面数量");
    TakeModel->setHeaderData(12,Qt::Horizontal,"实际数量");
    TakeModel->setHeaderData(13,Qt::Horizontal,"盘亏数量");
    TakeModel->setHeaderData(14,Qt::Horizontal,"盘盈数量");
    MergeTable(ui->Take_tableView, 6);
}

void StorageStat::TransferStorageInit()
{
    TransferModel->setQuery(QString("SELECT * FROM transfer_storage_record_table "
                              " join transfer_storage_detail_table on transfer_storage_detail_table.TransferStorageDetailID "
                              " like concat(transfer_storage_record_table.TransferStorageRecordID, '%')"));
    TransferModel->setHeaderData(0,Qt::Horizontal,"移库单号");
    TransferModel->setHeaderData(1,Qt::Horizontal,"移库类型");
    TransferModel->setHeaderData(2,Qt::Horizontal,"出库仓库");
    TransferModel->setHeaderData(3,Qt::Horizontal,"入库仓库");
    TransferModel->setHeaderData(4,Qt::Horizontal,"申请人编号");
    TransferModel->setHeaderData(5,Qt::Horizontal,"申请日期");
    TransferModel->setHeaderData(6,Qt::Horizontal,"申请到货日期");
    TransferModel->setHeaderData(7,Qt::Horizontal,"移库明细单号");
    TransferModel->setHeaderData(8,Qt::Horizontal,"产品编码");
    TransferModel->setHeaderData(9,Qt::Horizontal,"产品类型");
    TransferModel->setHeaderData(10,Qt::Horizontal,"产品批次");
    TransferModel->setHeaderData(11,Qt::Horizontal,"出库货位号");
    TransferModel->setHeaderData(12,Qt::Horizontal,"现有存量");
    TransferModel->setHeaderData(13,Qt::Horizontal,"入库货位号");
    TransferModel->setHeaderData(14,Qt::Horizontal,"现有存量");
    TransferModel->setHeaderData(15,Qt::Horizontal,"调拨数量");
    MergeTable(ui->Transfer_tableView, 7);
}

void StorageStat::ProductDataInit()
{
    ProductDataModel->setQuery(QString("SELECT * FROM product_data_table ;"));
    ProductDataModel->setHeaderData(0,Qt::Horizontal,"产品编码");
    ProductDataModel->setHeaderData(1,Qt::Horizontal,"产品名称");
    ProductDataModel->setHeaderData(2,Qt::Horizontal,"产品类型");
    ProductDataModel->setHeaderData(3,Qt::Horizontal,"库存数量");
    ProductDataModel->setHeaderData(4,Qt::Horizontal,"安全库存");
    ProductDataModel->setHeaderData(5,Qt::Horizontal,"提前期");
    ProductDataModel->setHeaderData(6,Qt::Horizontal,"日需求量");
    ProductDataModel->setHeaderData(7,Qt::Horizontal,"采购请求");
    ProductDataModel->setHeaderData(8,Qt::Horizontal,"缺料警告");
}

void StorageStat::MergeTable(QTableView *v, int columnCount)
{
    QString ID="";
    int MergeRowStart = 0;
    int MergeRowCount = 1;
    for(int i=0; i< v->model()->rowCount();i++)
    {
        if(ID == "")
        {
            ID = v->model()->index(i,0).data().toString();
            MergeRowStart = i;
            MergeRowCount = 1;
            continue;
        }
        if(v->model()->index(i,0).data().toString() == ID)
        {
            MergeRowCount ++;
            continue;
        }
        else
        {
            if(MergeRowCount == 1)  //仅有一行就不合并
            {
                ID = "";
                i--;
                continue;
            }
            for(int j = 0; j<columnCount; j++)
            {
                v->setSpan(MergeRowStart, j, MergeRowCount, 1);
            }
            ID = "";
            i--;
        }
    }
}

void StorageStat::SearchRow(QTableView *v, QString str)
{
    if(str=="")
    {
        for(int i=0;i < v->model()->rowCount();i++)
            v->setRowHidden(i,false);
    }
    else
    {
        str.remove(QRegExp("\\s"));     //查询字去除空格
        for(int i=0;i < v->model()->rowCount();i++)
        {
            v->setRowHidden(i,true);

            //提取表格行信息到一个字符串中，然后字符串匹配
            QString r="";
            QString celltext;
            QModelIndex index;
            for(int j=0;j<v->model()->columnCount();j++)
            {
                index=v->model()->index(i,j);
                celltext = v->model()->data(index).toString();
                celltext.remove(QRegExp("\\s"));     //被查询单元格内去除空格
                r += celltext;
                r += " ";       //被查询单元格间添加空格以分割。
            }
            if(r.contains(str,Qt::CaseInsensitive)) //不区分大小写查询
                v->setRowHidden(i,false);
        }
    }
}

void StorageStat::on_InEdit_textChanged(const QString &arg1)
{
    SearchRow(ui->In_tableView, ui->InEdit->text());
}

void StorageStat::on_OutEdit_textChanged(const QString &arg1)
{
    SearchRow(ui->Out_tableView, ui->OutEdit->text());
}

void StorageStat::on_TransferEdit_textChanged(const QString &arg1)
{
    SearchRow(ui->Transfer_tableView, ui->TransferEdit->text());
}

void StorageStat::on_TakeEdit_textChanged(const QString &arg1)
{
    SearchRow(ui->Take_tableView, ui->TakeEdit->text());
}

void StorageStat::on_ProductEdit_textChanged(const QString &arg1)
{
    SearchRow(ui->Product_tableView, ui->ProductEdit->text());
}

void StorageStat::on_In_refresh_btn_clicked()
{
    InStorageInit(); //入库表格初始化
}

void StorageStat::on_Out_refresh_btn_clicked()
{
    OutStorageInit();
}

void StorageStat::on_Transfer_refresh_btn_clicked()
{
    TransferStorageInit();
}

void StorageStat::on_Take_refresh_btn_clicked()
{
    TakeStorageInit();
}


void StorageStat::on_Product_refresh_btn_clicked()
{
    ProductDataInit();
}
