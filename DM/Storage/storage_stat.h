﻿#ifndef STORAGESTAT_H
#define STORAGESTAT_H

#include <QWidget>
#include <QAction>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QTableView>

namespace Ui {
class StorageStat;
}

class StorageStat : public QWidget
{
    Q_OBJECT

public:
    explicit StorageStat(QWidget *parent = nullptr);
    ~StorageStat();

private slots:
    void on_tabWidget_currentChanged(int index);

    void on_InEdit_textChanged(const QString &arg1);

    void on_OutEdit_textChanged(const QString &arg1);

    void on_TransferEdit_textChanged(const QString &arg1);

    void on_TakeEdit_textChanged(const QString &arg1);

    void on_ProductEdit_textChanged(const QString &arg1);

    void on_In_refresh_btn_clicked();

    void on_Out_refresh_btn_clicked();

    void on_Transfer_refresh_btn_clicked();

    void on_Take_refresh_btn_clicked();

    void on_Product_refresh_btn_clicked();

private:

    QSqlQueryModel *InModel;
    QSqlQueryModel *OutModel;
    QSqlQueryModel *TakeModel;
    QSqlQueryModel *TransferModel;
    QSqlQueryModel *ProductDataModel;

    void InStorageInit(); //入库表格初始化

    void OutStorageInit();

    void TakeStorageInit();

    void TransferStorageInit();

    void ProductDataInit();

private:

    Ui::StorageStat *ui;

    void MergeTable(QTableView *v, int columnCount); //columnCount为0到？-1

    void SearchRow(QTableView *v,QString str);
};

#endif // STORAGESTAT_H
