﻿#ifndef PUT_IN_STORAGE_H
#define PUT_IN_STORAGE_H

#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include <QTableWidgetItem>

#include "Tool/data_choose.h"
#include "DataBase/database.h"
#include "Tool/toolfunction.h"
#include "Tool/database_comboBox.h"

namespace Ui {
class PutInStorage;
}

class PutInStorage : public QWidget
{
    Q_OBJECT

public:
    explicit PutInStorage(QWidget *parent = nullptr);
    ~PutInStorage();

    QString tableCellText(int row, int column); //获取单元格的内容

private:
    int iPosRow;
    std::vector<int> vecItemIndex;//用于保存选中行的行号

    DataChoose *dc;

    QStringList Planlist;
    QStringList Salelist;
    QStringList Salelist2;

    int row;    //  当前行

    QString type;   //入库类型

    QMenu *RightClickMenu;  //右键点击菜单
    QAction *m_addAction;     //插入行
    QAction *m_deleteAction;  //删除行

    void RowInit(int index); //行的每个单元格初始化

    void RowDelete(int index);  //删除行

    QStringList getLocationlist(int row);   //得到货位信息

    void IDGenerate();      //生成入库单号

    void DetailIDGenerate(int index);        //生成入库明细单号

    bool CheckTable();  //检查表格数据是否填写完整

    int getLocationMAXMUM(QString keytype , int row);

    /*
      * @brief              更新物料表，判断是否需要发出采购需求
      * @param code         变更的物料编码
      * @param ChangeNum    变更数量，出库负，入库正
      */
    void UpdateMaterial(QString code, int ChangeNum);

private slots:

    void RightClickSlot(QPoint pos);        //菜单点击，获取当前位置

    void RightClickAddRow(QAction *act);        //得知菜单当前的位置并向上插入一行

    void RightClickDeleteRow(QAction *act);   //得知菜单当前的位置并删除

    void choose_btn_clicked();      //点击按钮后选择产品基本信息  

private slots:

    void on_add_btn_clicked();   //表格中添加新行

    void on_delete_btn_clicked();

    void on_select_member_clicked();

    void on_submit_btn_clicked();

    void on_tableWidget_cellChanged(int row, int column);

    void on_storage_combo_currentIndexChanged(const QString &arg1);

    void on_type_combo_currentIndexChanged(const QString &arg1);

    void on_datetime_editTextChanged(const QString &arg1);

    void on_comboBox_currentTextChanged(const QString &arg1);

private:
    Ui::PutInStorage *ui;

    void clear();  //清除所有内容

};

#endif // PUT_IN_STORAGE_H
