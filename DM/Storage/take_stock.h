﻿#ifndef TAKE_STOCK_H
#define TAKE_STOCK_H

#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include <QTableWidget>
#include <QTableWidgetItem>

#include "Tool/data_choose.h"
#include "DataBase/database.h"
#include "Tool/toolfunction.h"
#include "Tool/database_comboBox.h"

namespace Ui {
class TakeStock;
}

class TakeStock : public QWidget
{
    Q_OBJECT

public:
    explicit TakeStock(QWidget *parent = nullptr);
    ~TakeStock();

    QString tableCellText(int row, int column); //获取单元格的内容

private:
    int iPosRow;

    std::vector<int> vecItemIndex;//用于保存选中行的行号

    DataChoose *dc;

    int row;    //  当前行

    QMenu *RightClickMenu;  //右键点击菜单
    QAction *m_addAction;     //插入行
    QAction *m_deleteAction;  //删除行

    QString type;   //仓库类型

    void RowInit(int index); //行的每个单元格初始化

    void RowDelete(int index);  //删除行

    void IDGenerate();      //生成盘库单号

    void DetailIDGenerate(int index);        //生成盘库明细单号

    bool CheckTable();  //检查表格数据是否填写完整

    bool OutStorage(); //盘亏出库

    bool InStorage();  //盘盈入库

    /*
      * @brief              更新物料表，判断是否需要发出采购需求
      * @param code         变更的物料编码
      * @param ChangeNum    变更数量，出库负，入库正
      */
    void UpdateMaterial(QString code, int ChangeNum);

    QString StorageID(QString str); //生成盘盈入库单号，str= R ，则入 ； C 则出

    QStringList StorageDetailID(QString str,int rowCount);  //生成盘盈入库明细单号

private slots:
    void RightClickSlot(QPoint pos);        //菜单点击，获取当前位置

    void RightClickAddRow(QAction *act);        //得知菜单当前的位置并向上插入一行

    void RightClickDeleteRow(QAction *act);   //得知菜单当前的位置并删除

    void choose_product_btn_clicked();      //点击按钮后选择产品基本信息

    void choose_batch_btn_clicked();        //点击按钮后选择产品批次信息

private slots:
    void on_add_btn_clicked();

    void on_delete_btn_clicked();

    void on_submit_btn_clicked();

    void on_select_member_btn_clicked();

    void on_type_combo_currentIndexChanged(const QString &arg1);

    void on_storage_combo_currentIndexChanged(const QString &arg1);

    void on_start_date_editTextChanged(const QString &arg1);

    void on_tableWidget_cellChanged(int row, int column);

private:
    Ui::TakeStock *ui;

    void clear();  //清除所有内容
};

#endif // TAKE_STOCK_H
