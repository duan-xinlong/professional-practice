#ifndef SALESSTATISTICS_H
#define SALESSTATISTICS_H

#include <QWidget>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlError>
#include <QString>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QDebug>
#include <QPainter>
#include <QKeyEvent>
#include <QAction>
#include "DataBase/database.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

using namespace std;

namespace Ui {
class salesstatistics;
}

class salesstatistics : public QWidget
{
    Q_OBJECT

public:
    explicit salesstatistics(QWidget *parent = nullptr);
    ~salesstatistics();
    QSqlQueryModel *myModel;
private slots:
//    void on_pushButton_clicked();

private:
    Ui::salesstatistics *ui;
    bool refresh(QSqlQuery query);
    void priceSeasonNum();
    void Arima();
    double mean(vector<double> data);
    double stdDev(vector<double> data);
    vector<double> diff(vector<double> data, int order);
    double predict(vector<double> data);


    //定义ARIMA模型参数
    int p=11;//AR阶数
    int d=1;//差分阶数
    int q=1;//MA阶数

    //定义时间序列数据
    vector<double> data;

};

#endif // SALESSTATISTICS_H
