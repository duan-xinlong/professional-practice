#include "clientchoose.h"
#include "ui_clientchoose.h"
#include <QDebug>

clientchoose::clientchoose(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::clientchoose)
{
    ui->setupUi(this);

    refresh(query);
}

clientchoose::~clientchoose()
{
    delete ui;
}

bool clientchoose::refresh(QSqlQuery query)
{
    QSqlQuery query_client;
    ui->tableWidget->setColumnWidth(1, 300);
    ui->tableWidget->setColumnWidth(3, 500);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(6, QHeaderView::Stretch);
    if(query.size() <= 0){
        ui->tableWidget->setRowCount(0);
        return false;
    }
    qDebug() << query.size();
    int i=0;
    while(query.next()){
        //第一列设置checkbox进行勾选
        //在插入的行中输入产品选择按钮从数据库获得产品数据
        QPushButton *choose_btn = new QPushButton();
        choose_btn->setObjectName("choose_btn" + QString::number(i));
        choose_btn->setProperty("type", "check"); //勾选按钮样式

        QWidget *widget = new QWidget();
        QHBoxLayout *layout = new QHBoxLayout();
        layout->setMargin(0);//一定要有
        layout->addWidget(choose_btn);
        layout->setAlignment(choose_btn, Qt::AlignCenter);//控件在布局中居中显示
        widget->setLayout(layout);

        ui->tableWidget->setCellWidget(i, 0, widget);//放在表格第1列
        connect(choose_btn, &QPushButton::clicked, this, &clientchoose::choose_btn_clicked);
//        QWidget *widget=ui->tableWidget->cellWidget(i,0);
//        if(widget)
//        {
//            QPushButton* btn = dynamic_cast<QPushButton*>(widget->layout()->itemAt(0)->widget());
//            btn->setProperty("type","check");
//            style()->polish(btn);
//        }
        for(int j=0; j<6; j++){
             ui->tableWidget->setItem(i,j+1,new QTableWidgetItem(query.value(j).toString()));
        }
        //全部居中
        for(int j=0; j<=6; j++){
            if(ui->tableWidget->item(i,j) == nullptr) continue;
            ui->tableWidget->item(i,j)->setTextAlignment(Qt::AlignCenter);
        }
    i++;

    }
    return true;
}

void clientchoose::choose_btn_clicked()
{
    QPushButton* btn = qobject_cast<QPushButton*>(this->sender());//判断触发信号的对象
    if (btn == nullptr)
        return;
    //QModelIndex qIndex = ui->tableWidget->indexAt(QPoint(btn->parentWidget()->frameGeometry().x(), btn->parentWidget()->frameGeometry().y()));
    // row = qIndex.row();

    //按钮状态选择
    if(btn->property("type")=="check")
    {
        btn->setProperty("type","checked");
        style()->polish(btn);
       // vecItemIndex.push_back(row);//存储选中的行号
    }
    else if(btn->property("type")=="checked")
    {
        btn->setProperty("type","check");
        style()->polish(btn);
       // vecItemIndex.erase(std::remove(vecItemIndex.begin(), vecItemIndex.end(), row), vecItemIndex.end()); //删除选中的行号
    }
}

int clientchoose::search(QSqlQuery query)
{

   //  QSqlQuery query_client;
    for(int i=0;i<2;i++){
        qDebug()<<i;
        QWidget *parentWidget = (QWidget*)(ui->tableWidget->cellWidget(i,0));
        QPushButton *choose_btn = parentWidget->findChild<QPushButton *>("choose_btn"+ QString::number(i));


        //qDebug()<<choose_btn;
        if(choose_btn->property("type")=="checked")
        {
            checked_index=i;
            qDebug()<<checked_index;
            break;
        }
    }
    //qDebug()<<checked_index;
    return checked_index;
}

void clientchoose::on_btnConfirm_clicked()
{
   //QPushButton* btn = qobject_cast<QPushButton*>(this->sender());//判断触发信号的对象
   if(search(query)==-1)
   {
       QMessageBox::information(this,"选择客户","请重新确认勾选");
   }
   else
   {
    //checked_index是确认勾选的行数
        QString str=ui->tableWidget->item(checked_index,2)->text();
        emit getStr(str);
   }

}
