#ifndef CLIENTCHOOSE_H
#define CLIENTCHOOSE_H

#include <QWidget>
#include "DataBase/database.h"
#include <QMessageBox>
namespace Ui {
class clientchoose;
}

class clientchoose : public QWidget
{
    Q_OBJECT

public:
    explicit clientchoose(QWidget *parent = nullptr);
    ~clientchoose();
    int checked_index=-1;
    QSqlQuery query = mydb->query("客户信息表");

private slots:
    void on_btnConfirm_clicked();


private:
    Ui::clientchoose *ui;
    bool refresh(QSqlQuery query);
    int search(QSqlQuery query);
    void choose_btn_clicked();

signals:
    void getStr(QString &str);
};

#endif // CLIENTCHOOSE_H
