#ifndef SALESRETURN_H
#define SALESRETURN_H

#include<iostream>
#include<sstream>
#include <QWidget>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>
#include <QTableWidget>
#include "Tool/data_choose.h"
#include "DataBase/database.h"
#include "Tool/toolfunction.h"

namespace Ui {
class salesreturn;
}

class salesreturn : public QWidget
{
    Q_OBJECT

public:
    explicit salesreturn(QWidget *parent = nullptr);
    ~salesreturn();

private:
    Ui::salesreturn *ui;
    int iPosRow;
    QTableWidget *tbwgt;
    std::vector<int> vecItemIndex;//用于保存选中行的行号

    DataChoose *dc;

    int row;    //  当前行
    QMenu *RightClickMenu;  //右键点击菜单
    QAction *m_addAction;     //插入行
    QAction *m_deleteAction;  //删除行
    void SalesChoose();
    void RowInit(QTableWidget *T,int index); //行的每个单元格初始化
    void RowDelete(QTableWidget *T,int index);  //删除行
    void IDGenerate();
    void DetailIDGenerate(int index);
    void returnedProductTotal();
    QStringList  returnReason();
    void clear();

private slots:
    void RightClickSlot(QPoint pos);        //菜单点击，获取当前位置
    void RightClickAddRow(QAction *act);        //得知菜单当前的位置并向上插入一行
    void RightClickDeleteRow(QAction *act);     //得知菜单当前的位置并删除

    void choose_btn_clicked();      //点击按钮后选择产品基本信息
    void on_add_btn_clicked();      //销售明细表格中添加新行
    void on_delete_btn_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
    void on_comboBox_2_editTextChanged(const QString &arg1);
};

#endif // SALESRETURN_H
