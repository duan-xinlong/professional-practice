#ifndef PICTURE_H
#define PICTURE_H

#include <QWidget>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>

QT_CHARTS_USE_NAMESPACE
class picture:public QChartView
{
    Q_OBJECT
public:
    explicit picture(QWidget *parent = nullptr);

signals:

public slots:
private:
    QLineSeries *series;
    QChart *chart;
    QChartView *chartView;
};


#endif // PICTURE_H
