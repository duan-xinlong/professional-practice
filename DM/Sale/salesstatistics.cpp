#include "salesstatistics.h"
#include "ui_salesstatistics.h"
#include"linechart.h"
#include"barchart.h"

#include "DataBase/database.h"

#include <QVariantMap>
#include <QDebug>

salesstatistics::salesstatistics(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::salesstatistics)
{
    ui->setupUi(this);
    //销售订单明细表
//    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);//适应
//    ui->tableWidget_3->verticalHeader()->setVisible(false);
//    ui->tableWidget->setFocusPolicy(Qt::NoFocus);
//    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
//    connect(ui->tableWidget,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(RightClickSlot(QPoint)));
    QSqlQuery query = mydb->query("订单统计表");
    refresh(query);

//    //销售金额统计图
//    QStringList categories;
//     categories << "Jan" << "Feb" << "Mar" << "Apr" << "May" << "Jun" << "Jul" << "Aug" << "Sept" << "Oct" << "Nov" << "Dec";
//    QVariantList val;
//    val << 500 << 600 << 243 << 158 << 521 << 465 << 888 << 1310 << 933 << 125 << 445 << 758;
//    QVariantMap data;
//    data.insert("Jane", val);
//    QVariantList line;
//    line << QPointF(0,500)<< QPointF(1, 600) << QPointF(2, 243) << QPointF(3, 158) << QPointF(4, 521) << QPointF(5, 465)
//         << QPointF(6, 888) << QPointF(7, 1310) << QPointF(8, 933) << QPointF(9, 125) << QPointF(10, 445) << QPointF(11, 758);
//    line << QPointF(0, 200) << QPointF(1, 403) << QPointF(2, 507) << QPointF(3, 450) << QPointF(4, 485) << QPointF(5, 655)
//         << QPointF(6, 480) << QPointF(7, 765) << QPointF(8, 587) << QPointF(9, 764) << QPointF(10, 682) << QPointF(11, 998);
//    ui->wdgVer->setLineChart(line);
    priceSeasonNum();
    Arima();

 }

salesstatistics::~salesstatistics()
{
    delete ui;
}

bool salesstatistics::refresh(QSqlQuery query)
{
    myModel = new QSqlQueryModel(this);
    ui->tableView->setModel(myModel);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    myModel->setQuery(QString("SELECT * From 订单统计表"));

    ui->tableView->show();

    int total_priceNum=0;

    for(int i=0;i<ui->tableView->model()->columnCount();i++)
    {
     total_priceNum+=ui->tableView->model()->index(i,15).data().toInt();
    }
    QString total_clientNum;
    QString total_orderNum;
//    total_clientNum =(QString("SELECT COUNT(DISTINCT 客户名称) FROM 订单统计表"));
//    total_orderNum = QString("SELECT SELECT COUNT(DISTINCT 销售订单编号) FROM 订单统计表");
    QSqlQuery q;
    q.exec("SELECT COUNT(DISTINCT 客户名称) FROM 订单统计表");
    q.next();
    total_clientNum = q.value(0).toString();
    q.exec("SELECT COUNT(DISTINCT 销售订单编号) FROM 订单统计表");
    q.next();
    total_orderNum = q.value(0).toString();

    ui->lineEdit_3->setText(QString(total_clientNum));
    ui->lineEdit_4->setText(QString(total_orderNum));
    ui->lineEdit_2->setText(QString("%1").arg(total_priceNum));
    return true;
}

void salesstatistics::priceSeasonNum()
{
    int Jan=0,Feb=0,Mar=0,Apr=0,May=0,Jun=0,Jul=0,Aug=0,Sept=0,Oct=0,Nov=0,Dec=0;
    QSqlQuery q;
    q.exec("SELECT 订单签订日期,售价合计 FROM 订单统计表");
    while(q.next())
    {
        int date;
        date = q.value(0).toString().remove(QChar('-'), Qt::CaseInsensitive).mid(4,2).toInt();
        //qDebug()<<date;
        if(date==1){
        Jan+=q.value(1).toInt();
        }
        if(date==2){
        Feb+=q.value(1).toInt();
        }
        if(date==3){
        Mar+=q.value(1).toInt();
        }
        if(date==4){
        Apr+=q.value(1).toInt();
        }
        if(date==5){
        May+=q.value(1).toInt();
        }
        if(date==6){
        Jun+=q.value(1).toInt();
        }
        if(date==7){
        Jul+=q.value(1).toInt();
        }
        if(date==8){
        Aug+=q.value(1).toInt();
        }
        if(date==9){
        Sept+=q.value(1).toInt();
        }
        if(date==10){
        Oct+=q.value(1).toInt();
        }
        if(date==11){
        Nov+=q.value(1).toInt();
        }
        if(date==12){
        Dec+=q.value(1).toInt();
        }

    }
    //销售金额统计图
    QStringList categories;
     categories << "Jan" << "Feb" << "Mar" << "Apr" << "May" << "Jun" << "Jul" << "Aug" << "Sept" << "Oct" << "Nov" << "Dec";
    QVariantList val;
    val << Jan << Feb << Mar << Apr << May << Jun << Jul << Aug << Sept << Oct << Nov << Dec;
    QVariantMap data;
    data.insert("Jane", val);
    ui->wdgVer->createVerticalChart(categories, data, 0, 1400, QColor("#D97559"), "销售金额统计");
//    QVariantList line;
//    line << QPointF(0,Jan)<< QPointF(1, Feb) << QPointF(2, Mar) << QPointF(3, Apr) << QPointF(4, May) << QPointF(5, Jun)
//         << QPointF(6, Jul) << QPointF(7, Aug) << QPointF(8, Sept) << QPointF(9, Oct) << QPointF(10, Nov) << QPointF(11, Dec);
//    ui->wdgVer->setLineChart(line);
}

//void salesstatistics::on_pushButton_clicked()
//{
//   QSqlQuery query = mydb->query("订单统计表");
//    refresh(query);
//    priceSeasonNum();
//}

void salesstatistics::Arima()
{
    double Jan=0,Feb=0,Mar=0,Apr=0,May=0,Jun=0,Jul=0,Aug=0,Sept=0,Oct=0,Nov=0,Dec=0;
    QSqlQuery q;
    q.exec("SELECT 订单签订日期,售价合计 FROM 订单统计表");
    while(q.next())
    {
        int date;
        date = q.value(0).toString().remove(QChar('-'), Qt::CaseInsensitive).mid(4,2).toInt();
        //qDebug()<<date;
        if(date==1){
        Jan+=q.value(1).toInt();
        }
        if(date==2){
        Feb+=q.value(1).toInt();
        }
        if(date==3){
        Mar+=q.value(1).toInt();
        }
        if(date==4){
        Apr+=q.value(1).toInt();
        }
        if(date==5){
        May+=q.value(1).toInt();
        }
        if(date==6){
        Jun+=q.value(1).toInt();
        }
        if(date==7){
        Jul+=q.value(1).toInt();
        }
        if(date==8){
        Aug+=q.value(1).toInt();
        }
        if(date==9){
        Sept+=q.value(1).toInt();
        }
        if(date==10){
        Oct+=q.value(1).toInt();
        }
        if(date==11){
        Nov+=q.value(1).toInt();
        }
        if(date==12){
        Dec+=q.value(1).toInt();
        }

    }
    data.push_back(Jan);
    data.push_back(Feb);
    data.push_back(Mar);
    data.push_back(Apr);
    data.push_back(Mar);
    data.push_back(May);
    data.push_back(Jun);
    data.push_back(Jul);
    data.push_back(Aug);
    data.push_back(Sept);
    data.push_back(Oct);
    data.push_back(Nov);
   // data.push_back(Dec);
   // qDebug()<<data;

    double y = predict(data);
 //   qDebug()<<y<<endl;
    int x=y;
    QString str = QString("%1").arg(x);

    ui->lineEdit_9->setText(str);
}

//计算时间序列的均值
double salesstatistics::mean(vector<double> data)
{
    double sum=0;
    for(int i=0;i<data.size();i++)
    {
        sum+=data[i];
    }
    qDebug()<<"均值"<<sum/data.size()<<endl;
    return sum/data.size();

}

//计算时间序列的标准差
double salesstatistics::stdDev(vector<double> data)
{
    double avg =mean(data);
    double sum=0;
    for(int i=0;i<data.size();i++)
    {
        sum+=pow(data[i]-avg,2);
    }
    qDebug()<<"标准差"<<sqrt(sum/(data.size()-1))<<endl;
    return sqrt(sum/(data.size()-1));

}

//差分运算
vector<double> salesstatistics::diff(vector<double> data,int order)
{
    vector<double> result(data.size()-order);
    for(int i=0;i<result.size();i++)
    {
        result[i]=data[i+order]-data[i];
    }
    return result;
}

//ARIMA模型的预测函数
double salesstatistics::predict(vector<double> data)
{
    //进行差分运算
    for(int i=0;i<d;i++)
    {
        data=diff(data,1);
    }
    qDebug()<<"result:"<<data;

    //计算均值和标准差
    double mu = mean(data);
    double sigma = stdDev(data);

    //初始化ARIMA模型参数
    vector<double> ar(p,0);
    vector<double> ma(q,0);
    double c=mu;

    //迭代求解ARIMA模型参数
    for(int t=p+d;t<data.size();t++)
    {
        //计算残差
        double e=data[t]-c;
        for(int i=0;i<p;i++)
        {
            e-=ar[i]*data[t-i-1];
        }
        for(int i=0;i<q;i++)
        {
            e-=ma[i]*(data[t-i-1]-c);
        }

        //更新ARIMA模型参数
        c+=e;
        for(int i=0;i<p;i++)
        {
            ar[i]+=e*data[t-i-1];
        }
        for(int i=0;i<q;i++)
        {
            ma[i]+=e*(data[t-i-1]-c);
        }
    }

    //计算ARIMA模型的预测值
    double y=c;
    for(int i=0;i<p;i++)
    {
        y+=ar[i]*data[data.size()-i-1];
    }
    for(int i=0;i<q;i++)
    {
        y+=ma[i]*(data[data.size()-i-1]-c);
    }

    return y;
}
