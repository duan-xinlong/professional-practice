﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QMainWindow>
#include <QTreeWidgetItem>

#include "Storage/put_in_storage.h"
#include "Storage/put_out_storage.h"
#include "Storage/transfer_storage.h"
#include "Storage/take_stock.h"
#include "Storage/storage_stat.h"

#include "Sale/salesorder.h"
#include "Sale/salesreturn.h"
#include "Sale/saleschange.h"
#include "Sale/salesstatistics.h"

#include "Planning/MPS.h"
#include "Planning/workorders.h"

#include "Purchase/purchaseorder.h"
#include "Purchase/materialdata.h"
#include "Purchase/buyerdata.h"
#include "Purchase/supplierdata.h"

#include "Planning/picking.h"
#include "Planning/reporting.h"
#include "Planning/warehousing.h"

#include "System/syslogmgmt.h"
#include "System/rolemgmt.h"
#include "System/usermgmt.h"
#include "System/orgmgmt.h"
#include "System/sysconfig.h"

#include "Login/personcenter.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr, int id = 0);
    ~MainWindow();

    PutInStorage *toStorage;
    PutOutStorage *outStorage;
    TransferStorage *transferStorage;
    TakeStock *takeStock;
    StorageStat *storageStat;

    salesorder *saleOrder;
    materialdata *materialData;
    buyerdata *buyerData;
    supplierdata *supplierData;
    salesreturn *saleReturn;
    saleschange *saleChange;
    salesstatistics *saleStatistics;

    MPS *mps;
    workOrders *workorders;
    Picking *picking;
    Reporting *reporting;
    Warehousing *warehousing;

    PurchaseOrder *purchaseOrder;

    SysLogMgmt *syslogmgmt;
    RoleMgmt *rolemgmt;
    UserMgmt *usermgmt;
    OrgMgmt *orgmgmt;
    SysConfig *sysconfig;
    PersonCenter* personcenter;

public:
    int current_user_id = 0;
    QString current_user_name = "测试用户";
    QMap<QString,QString> current_access_module;
    QMap<QString,QString> current_access_value;
    QString current_access_type = "个人";

private slots:
    void on_btn_exit_clicked();
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

private:
    Ui::MainWindow *ui;
    QStringList tabsList;
};

extern MainWindow *DM_Window;

#endif // MAINWINDOW_H
