#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class LoginDialog; }
QT_END_NAMESPACE

class LoginDialog : public QWidget
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = nullptr);
    ~LoginDialog();

private slots:
    void on_btn_signin_clicked();
    void on_btn_signup_clicked();
    void on_btn_to_signup_clicked();
    void on_btn_to_signin_clicked();

private:
    Ui::LoginDialog *ui;
    QString tablename = "dm_user";

    void keyPressEvent(QKeyEvent * event);
};
#endif // LOGINDIALOG_H
