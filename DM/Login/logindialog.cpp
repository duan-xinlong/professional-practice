#include "logindialog.h"
#include "ui_logindialog.h"
#include "../Database/database.h"
#include "../mainwindow.h"

#include <QDebug>
#include <QMessageBox>
#include <QGraphicsDropShadowEffect>

LoginDialog::LoginDialog(QWidget *parent)
    : QWidget(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    setFixedSize(this->width(),this->height());
    ui->stackedWidget->setCurrentIndex(0);
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_btn_signin_clicked()
{
    QString username = ui->lineEdit_username->text();
    QString password = ui->lineEdit_password->text();
    if(username == "" || password == ""){
        QMessageBox::information(this,"登录失败","请完整填写登录信息");
        return;
    }

    QSqlQuery query = mydb->query("dm_user", "status,id", QString(" WHERE login_name='%1' and password='%2' and delete_flag='0'").arg(username).arg(password));
    if(query.next()){
        if(query.value(0).toInt() == 1){
            qDebug()<<"Login success!";
            //主界面
            DM_Window = new MainWindow(nullptr, query.value(1).toInt());
            DM_Window->showMaximized();
            //登录日志
            mydb->addLog(DM_Window->current_user_id, "用户中心", "登录系统", "1");
            this->close();
        }else{
            QMessageBox::information(this,"登录失败","账户状态异常");
        }
    }else{
        QMessageBox::information(this,"登录失败","账户或者密码错误");
    }
}

void LoginDialog::on_btn_signup_clicked()
{
    QString username = ui->lineEdit_username_2->text();
    QString password = ui->lineEdit_password_2->text();
    QString surepass = ui->lineEdit_password_3->text();
    QSqlQuery query;
    QString sql;
//    int newId = 0;

    if(username == "" || password == ""){
        QMessageBox::information(this,"注册失败","用户名和密码不能为空");
    }else if(password != surepass){
        QMessageBox::information(this,"注册失败","两次密码输入不一致");
    }else{
        sql = QString("select * from dm_user where login_name='%1'").arg(username);

        if(!query.exec(sql)){
            QMessageBox::information(this,"注册失败","数据库异常<1>!");
        }else if(query.next()){
            QMessageBox::information(this,"注册失败","该用户已注册");
        }else{
//            sql = QString("select max(id) from dm_user");
//            query.exec(sql);
//            query.next();
//            newId = query.value(0).toInt() + 1;

            sql = QString("insert into dm_user(username, login_name, password, status, create_time) values('%1','%2','%3','%4', NOW());")
                  .arg("新用户<"+username+">").arg(username).arg(password).arg(0);
            if(!query.exec(sql)){
                QMessageBox::information(this,"注册失败","数据库异常<2>！");
            }else{
                QMessageBox::information(this,"注册成功","注册成功，登录后请完善个人信息！");
                query = mydb->query("dm_user","id"," WHERE id=last_insert_id()");
                if(query.next()){
                    mydb->addLog(query.value(0).toInt(), "用户中心", "用户注册", "1");
                }
                on_btn_to_signin_clicked();    //返回登录
            }
        }
    }
}

void LoginDialog::on_btn_to_signup_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}
void LoginDialog::on_btn_to_signin_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void LoginDialog::keyPressEvent(QKeyEvent * event)
{
    if (event->key() == Qt::Key_Return){
        ui->stackedWidget->currentIndex() == 0 ? ui->btn_signin->click() : ui->btn_signup->click();
    }
}
