﻿#include "personcenter.h"
#include "ui_personcenter.h"

PersonCenter::PersonCenter(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PersonCenter)
{
    ui->setupUi(this);
    ui->label->setAlignment(Qt::AlignCenter);
}

PersonCenter::~PersonCenter()
{
    delete ui;
}
