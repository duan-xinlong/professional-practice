﻿#include "mainwindow.h"
#include "Login/logindialog.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMessageBox>
#include <QWidget>
#include <QStackedWidget>
#include <QScrollArea>
#include <QGraphicsDropShadowEffect>
#include <QMessageBox>
#include <QMap>

MainWindow *DM_Window;

MainWindow::MainWindow(QWidget *parent, int id)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //选中首页
    ui->treeWidget->setCurrentItem(ui->treeWidget->findItems("首页", Qt::MatchContains | Qt::MatchRecursive)[0]);
    on_treeWidget_itemClicked(ui->treeWidget->currentItem(), 0);
    this->setWindowTitle("DManfacturing");
    //this->setWindowFlags(Qt::FramelessWindowHint); //隐藏头

    //标签页可关闭
    ui->tabWidget->setTabsClosable(true);
    connect(ui->tabWidget, &QTabWidget::tabCloseRequested, this, [=](int index){
        // 移除标题列表
        QString title = ui->tabWidget->tabText(index);
        if(tabsList.indexOf(title) != -1){
            tabsList.removeOne(title);
        }
        // 移除tab页
        ui->tabWidget->removeTab(index);
        //关闭最后一个tab打开首页
        if(ui->tabWidget->currentWidget() == nullptr){
            ui->treeWidget->setCurrentItem(ui->treeWidget->findItems("首页", Qt::MatchContains | Qt::MatchRecursive)[0]);
            on_treeWidget_itemClicked(ui->treeWidget->currentItem(), 0);
        }
    });

    //获取姓名和id;
    current_user_id = id;
    QSqlQuery query = mydb->query("dm_user","username", QString(" WHERE id='%1'").arg(id));
    if(query.next()){
        current_user_name = query.value(0).toString();
    }else{
        current_user_name = "未知用户";
    }
    ui->lb_user->setText("欢迎你，"+current_user_name);

    //用户权限 目前user和role一一对应
    query = mydb->query("dm_user JOIN dm_role ON dm_user.role_id=dm_role.id","dm_role.module,dm_role.value,dm_role.type", QString(" WHERE dm_user.id='%1'").arg(id));
    if(query.next()){
        current_access_module = mydb->stringToMap(query.value(0).toString());
        current_access_value = mydb->stringToMap(query.value(1).toString());
        current_access_type = query.value(2).toString();
    }

    //无权限则隐藏项目
    QTreeWidgetItemIterator it(ui->treeWidget);
    while (*it) {
        if((*it)->text(0) == "首页"){

        }else{
            if(current_access_module.contains((*it)->text(0))){

            }else if(current_access_value.contains((*it)->text(0))){

            }else{
                (*it)->setHidden(true);
            }
        }
        it++;
    }
}


MainWindow::~MainWindow()
{
    delete ui;
}

//退出登录
void MainWindow::on_btn_exit_clicked()
{
    QMessageBox:: StandardButton result  = QMessageBox::information(this,"退出","确认退出登录？", QMessageBox::Yes | QMessageBox::No);
    if(result == QMessageBox::Yes){
        LoginDialog *w = new LoginDialog();
        w->show();
        this->close();
    }
}

void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column)

    if(tabsList.indexOf(item->text(0)) == -1){
        //添加标题到列表
        tabsList.append(item->text(0));
        //新生成标签页
        if(item->text(0) == "首页"){
            personcenter = new PersonCenter();
            ui->tabWidget->addTab(personcenter, item->text(0));
        }
        //销售管理模块
        else if(item->text(0)=="销售订单"){
            saleOrder = new salesorder(this);
            ui->tabWidget->addTab(saleOrder, item->text(0));
        }
        else if(item->text(0)=="退货订单"){
            saleReturn = new salesreturn(this);
            ui->tabWidget->addTab(saleReturn, item->text(0));
        }
        else if(item->text(0)=="换货订单"){
            saleChange = new saleschange(this);
            ui->tabWidget->addTab(saleChange, item->text(0));
        }
        else if(item->text(0)=="订单统计"){
            saleStatistics = new salesstatistics(this);
            ui->tabWidget->addTab(saleStatistics, item->text(0));
        }
        //库存管理
        else if(item->text(0)=="入库单"){
            toStorage = new PutInStorage(this);
            ui->tabWidget->addTab(toStorage, item->text(0));
        }
        else if(item->text(0)=="出库单"){
            outStorage = new PutOutStorage(this);
            ui->tabWidget->addTab(outStorage, item->text(0));
        }
        else if(item->text(0)=="移库记录"){
            transferStorage = new TransferStorage(this);
            ui->tabWidget->addTab(transferStorage, item->text(0));
        }
        else if(item->text(0)=="库存盘点"){
            takeStock = new TakeStock(this);
            ui->tabWidget->addTab(takeStock, item->text(0));
        }
        else if(item->text(0)=="产品统计"){
            storageStat = new StorageStat(this);
            ui->tabWidget->addTab(storageStat, item->text(0));
        }
        //基础
        else if(item->text(0)=="系统日志"){
            syslogmgmt = new SysLogMgmt(this);
            ui->tabWidget->addTab(syslogmgmt, item->text(0));
        }
        else if(item->text(0)=="角色管理"){
            rolemgmt = new RoleMgmt(this);
            ui->tabWidget->addTab(rolemgmt, item->text(0));
        }
        else if(item->text(0)=="用户管理"){
            usermgmt = new UserMgmt(this);
            ui->tabWidget->addTab(usermgmt, item->text(0));
        }
        else if(item->text(0)=="机构管理"){
            orgmgmt = new OrgMgmt(this);
            ui->tabWidget->addTab(orgmgmt, item->text(0));
        }
        else if(item->text(0)=="采购订单"){
            purchaseOrder = new PurchaseOrder(this);
            ui->tabWidget->addTab(purchaseOrder, item->text(0));
        }
        else if(item->text(0)=="采购员"){
            buyerData = new buyerdata(this);
            ui->tabWidget->addTab(buyerData, item->text(0));
        }
        else if(item->text(0)=="供应商"){
            supplierData = new supplierdata(this);
            ui->tabWidget->addTab(supplierData, item->text(0));
        }
        else if(item->text(0)=="采购材料"){
            materialData = new materialdata(this);
            ui->tabWidget->addTab(materialData, item->text(0));
        }
        else if(item->text(0)=="系统配置"){
            sysconfig = new SysConfig(this);
            ui->tabWidget->addTab(sysconfig, item->text(0));
        }
        //计划管理模块
        else if(item->text(0)=="生产工单"){
            workorders = new workOrders();
            ui->tabWidget->addTab(workorders, item->text(0));
        }
        else if(item->text(0)=="领料退料"){
            picking = new Picking();
            ui->tabWidget->addTab(picking, item->text(0));
        }
        else if(item->text(0)=="生产入库"){
            warehousing = new Warehousing();
            ui->tabWidget->addTab(warehousing, item->text(0));
        }
        else if(item->text(0)=="生产派工"){
            reporting = new Reporting();
            ui->tabWidget->addTab(reporting, item->text(0));
        }
        else if(item->text(0) == "生产计划"){
            mps = new MPS();
            ui->tabWidget->addTab(mps, item->text(0));
        }
        else{
            tabsList.removeLast();
        }
    }

    //切换标签页
    ui->tabWidget->setCurrentIndex(tabsList.indexOf(item->text(0)));
}
