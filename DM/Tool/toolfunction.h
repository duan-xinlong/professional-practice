﻿#ifndef TOOLFUNCTION_H
#define TOOLFUNCTION_H

#include <QSqlTableModel>
#include <QMessageBox>

#include <QWidget>
#include <QItemDelegate>
#include <QStyledItemDelegate>
#include <QLineEdit>

// 设置tablewidget某行/列不可编辑,
class ReadOnlyDelegate: public QItemDelegate
{

public:
    ReadOnlyDelegate(QWidget *parent = NULL):QItemDelegate(parent){}

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const override;

};

// 设置tablewidget某行/列为特定输入
class SetTextDelegate : public QStyledItemDelegate
{

public:
    SetTextDelegate(QObject *parent = nullptr) : QStyledItemDelegate(parent) {}

    QRegExpValidator *reg;  //限制输入的条件
    int MaxLength = 20;  //最大长度, 默认为20

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    void setEditorData(QWidget *editor, const QModelIndex &index) const override;

    void commitAndCloseEditor();
};

//仓库序列
QStringList WarehouseList();


//字符串自增 0000->0001  ,9999->0000
QString NumberIncrease(QString num);

//弹出警告框
void warning(QWidget *parent, QString str);

#endif // TOOLFUNCTION_H
