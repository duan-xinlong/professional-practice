﻿#ifndef DATE_EDIT_H
#define DATE_EDIT_H

#include "date_widget.h"
#include <QComboBox>

class DateEdit : public QComboBox
{
	Q_OBJECT
public:
    explicit DateEdit(QWidget *parent = 0);

    QDate date() const;
    void setDate(const QDate& dt);
    QString dateToStr();

private:

    DateWidget *cell;

	void initPage();

    void mousePressEvent(QMouseEvent *e) override;
};
#endif // DATE_EDIT_H
