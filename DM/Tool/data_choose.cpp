﻿#include "data_choose.h"
#include "ui_data_choose.h"

#include "mainwindow.h"

DataChoose::DataChoose(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataChoose)
{
    ui->setupUi(this);
    ui->tableView->verticalHeader()->setVisible(false);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QAction* searchAction = new QAction(ui->lineEdit);
    searchAction->setIcon(QIcon(":/Resources/Pictures/search.png"));
    ui->lineEdit->addAction(searchAction,QLineEdit::LeadingPosition);


    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setWindowModality(Qt::ApplicationModal);
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Tool); //隐藏任务栏

    m_widget = new QWidget(DM_Window);
    m_widget->resize (DM_Window->width(), DM_Window->height());
    m_widget->move (0,0);
    m_widget->setStyleSheet("background-color:rgba(0, 0, 0,30%);");
    m_widget->show();

    myModel = new QSqlQueryModel(this);
    ui->tableView->setModel(myModel);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);     //整行选中
}

DataChoose::~DataChoose()
{
    delete ui;
    delete m_widget;
}

void DataChoose::hideColumn(int index)
{
    ui->tableView->setColumnHidden(index,true);
}

void DataChoose::setTitle(QString str)
{
    ui->label->setText(str);
}

void DataChoose::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);  // 反锯齿;
    painter.setBrush(QBrush(QColor(255, 255, 255)));
    painter.setPen(Qt::transparent);
    QRect rect = this->rect();
    painter.drawRoundedRect(rect, 15, 15);  //设置窗口圆角 15px
}

void DataChoose::showEvent(QShowEvent *event)
{
    int count = myModel->columnCount();
    int width = 0;
    for(int i=0; i< count;i++)
        width += ui->tableView->columnWidth(i);
    qDebug()<<ui->tableView->width();
    if(width <= DM_Window->width()/2.1)
        ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    this->resize(DM_Window->width()/2, DM_Window->height()/1.5);
    QPoint globalPos = DM_Window->mapToGlobal(QPoint(0,0));
    int x = globalPos.x() + (DM_Window->width() - this->width()) / 2;
    int y = globalPos.y() + (DM_Window->height() - this->height()) / 2;
    this->move(x,y);
}

void DataChoose::on_quit_btn_clicked()
{
    this->close();
    delete ui;
    delete m_widget;
}

void DataChoose::on_btnCancel_clicked()
{
    this->close();
    delete ui;
    delete m_widget;
}

void DataChoose::on_btnConfirm_clicked()
{
    QModelIndexList list = ui->tableView->selectionModel()->selectedIndexes();

    if(!list.empty())
        emit singal_selectedItems(list);
    this->close();
    delete ui;
    delete m_widget;
}

void DataChoose::on_lineEdit_textChanged(const QString &arg1)
{
    if(ui->lineEdit->text()=="")
    {
        for(int i=0;i < ui->tableView->model()->rowCount();i++)
            ui->tableView->setRowHidden(i,false);
    }
    else
    {
        QString str=ui->lineEdit->text();
        str.remove(QRegExp("\\s"));     //查询字去除空格
        for(int i=0;i < ui->tableView->model()->rowCount();i++)
        {
            ui->tableView->setRowHidden(i,true);

            //提取表格行信息到一个字符串中，然后字符串匹配
            QString r="";
            QString celltext;
            QModelIndex index;
            for(int j=0;j<ui->tableView->model()->columnCount();j++)
            {
                index=myModel->index(i,j);
                celltext = myModel->data(index).toString();
                celltext.remove(QRegExp("\\s"));     //被查询单元格内去除空格
                r += celltext;
                r += " ";       //被查询单元格间添加空格以分割。
            }
            if(r.contains(str,Qt::CaseInsensitive)) //不区分大小写查询
                ui->tableView->setRowHidden(i,false);
        }
    }
}
