﻿#ifndef MYCOMBOBOX_H
#define MYCOMBOBOX_H

#include <QComboBox>
#include <QMouseEvent>

class DatabaseComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit DatabaseComboBox(QWidget *parent = 0);

    DatabaseComboBox(QStringList list);

    QStringList list;

private:
    void mousePressEvent(QMouseEvent *e) override;
};

#endif // MYCOMBOBOX_H
