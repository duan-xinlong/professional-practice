#ifndef DATE_WIDGET_H
#define DATE_WIDGET_H

#include <QWidget>

namespace Ui
{
    class DateWidget;
}

class DateWidget : public QWidget
{
Q_OBJECT

public:
    explicit DateWidget(QWidget* parent = nullptr);
    ~DateWidget();

    void setDate(const QDate& dt);

protected:
	void showEvent(QShowEvent *event) override;

private:
	void initPage();
	void updateDays(int current);

signals:
	void signal_cancel();
    void signal_date(const QDate&);

private:
    Ui::DateWidget* ui;
    QVariantList m_dayList;
    QVariantList m_yearList;
};

#endif // DATE_WIDGET_H
