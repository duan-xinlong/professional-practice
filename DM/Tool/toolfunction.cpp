﻿#include "toolfunction.h"


QWidget *ReadOnlyDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const //final
{
    Q_UNUSED(parent)
    Q_UNUSED(option)
    Q_UNUSED(index)

    return NULL;
}

QWidget *SetTextDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    // 创建一个QLineEdit作为编辑器
    QLineEdit *editor = new QLineEdit(parent);

    // 设置仅输入数字, 最大长度为3
    editor->setValidator(reg);
    editor->setMaxLength(MaxLength);
    return editor;
}

void SetTextDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    // 获取模型中的数据
    QString value = index.model()->data(index, Qt::EditRole).toString();
    // 将数据显示在编辑器中
    QLineEdit *lineEdit = static_cast<QLineEdit*>(editor);
    lineEdit->setText(value);
    // 连接textEdited信号和commitData信号
    connect(lineEdit, &QLineEdit::textEdited, this, &SetTextDelegate::commitAndCloseEditor);
}

void SetTextDelegate::commitAndCloseEditor()
{
    // 提交数据给模型
    QLineEdit *editor = qobject_cast<QLineEdit*>(sender());
    emit commitData(editor);
}

QString NumberIncrease(QString num)
{
    unsigned char length = num.size();
    QByteArray byteArray(num.toLatin1());

    for (signed char i = length-1; i >= 0; --i)
    {
        byteArray[i] = byteArray[i] + 1;
        if (byteArray[i] > '9')
        {
            byteArray[i] = '0';
        }
        else {
            return QString(byteArray);
        }
    }

    byteArray[length-1] = '1';
    return QString(byteArray);
}

void warning(QWidget *parent, QString str)
{
    QMessageBox::warning(parent,"警告",str);
}

QStringList WarehouseList()
{
    QStringList list;
    list <<"原料仓 Y1" <<"原料仓 Y2" <<"原料仓 Y3"
         <<"成品仓 C1" <<"成品仓 C2" <<"成品仓 C3";
    return list;
}
