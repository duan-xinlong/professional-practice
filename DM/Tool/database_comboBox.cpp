﻿#include "database_comboBox.h"

DatabaseComboBox::DatabaseComboBox(QWidget *parent) : QComboBox(parent)
{

}

DatabaseComboBox::DatabaseComboBox(QStringList list)
{
    this->list = list;
}

void DatabaseComboBox::mousePressEvent(QMouseEvent *e)
{
    QString str = this->currentText();
    this->clear();
    this->addItems(list);
    if(list.contains(str))
        this->setCurrentText(str);
    showPopup();
}


