﻿#ifndef BUTTON_DAY_H
#define BUTTON_DAY_H

#include "date_time_defines.h"
#include <QPushButton>
#include <QMouseEvent>

class ButtonDay : public QPushButton
{
	Q_OBJECT
	Q_PROPERTY(int id READ id WRITE setId DESIGNABLE true)
	Q_PROPERTY(QVariant data READ data WRITE setData DESIGNABLE true)


public:
	//using 关键字是在C++11之后出现的，有些版本是不支持的，所以这边我们不使用
	//using QPushButton::QPushButton;

	explicit ButtonDay(QWidget *parent = 0);
	explicit ButtonDay(int id, QWidget *parent = 0);
	~ButtonDay();

	int id() const
	{
		return m_id;
	}

	void setId(int id)
	{
		m_id = id;
	}

    //DateTime---------------------------------------------------
    DateTime::DayDisplay DateTime_role() const
	{
        return datetime_role;
	}

	void setRole(const DateTime::DayDisplay& role);

    DateTime::DayType DateTime_type() const
	{
        return datetime_type;
	}

	void setType(const DateTime::DayType& type)
	{
        datetime_type = type;
	}

    DateTime::DayMonth DateTime_month() const
	{
        return datetime_month;
	}

	void setMonth(const DateTime::DayMonth& month)
	{
        datetime_month = month;
	}

    //Date-------------------------------------------------
    Date::DayDisplay Date_role() const
    {
        return date_role;
    }

    void setRole(const Date::DayDisplay& role);

    Date::DayType Date_type() const
    {
        return date_type;
    }

    void setType(const Date::DayType& type)
    {
        date_type = type;
    }

    Date::DayMonth Date_month() const
    {
        return date_month;
    }

    void setMonth(const Date::DayMonth& month)
    {
        date_month = month;
    }



	QVariant data() const
	{
		return m_data;
	}

	void setData(const QVariant& data)
	{
		m_data = data;
	}

private:
	int m_id{ -1 };
	QVariant m_data{ QVariant() };
    DateTime::DayDisplay datetime_role{ DateTime::CURRENT_DAY };
    DateTime::DayType datetime_type{ DateTime::WORKDAY };
    DateTime::DayMonth datetime_month{ DateTime::CURRENT_MONTH_DAY };

    Date::DayDisplay date_role{ DateTime::CURRENT_DAY };
    Date::DayType date_type{ DateTime::WORKDAY };
    Date::DayMonth date_month{ DateTime::CURRENT_MONTH_DAY };
};

#endif // BUTTON_DAY_H
