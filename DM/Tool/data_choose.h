﻿#ifndef DATA_CHOOSE_H
#define DATA_CHOOSE_H

#include <QWidget>
#include <QWidget>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlError>
#include <QString>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QDebug>
#include <QPainter>
#include <QKeyEvent>
#include <QAction>


namespace Ui {
class DataChoose;
}

class DataChoose : public QWidget
{
    Q_OBJECT

public:
    explicit DataChoose(QWidget *parent = nullptr);
    ~DataChoose();

    QString table_name;

    QSqlQueryModel *myModel;

    void hideColumn(int index);     //隐藏某列

    void setTitle(QString str);

private:

    QWidget *m_widget; 

    void paintEvent(QPaintEvent *event) override;

    void showEvent(QShowEvent *event) override;

private slots:
    void on_quit_btn_clicked();

    void on_btnCancel_clicked();

    void on_btnConfirm_clicked();

    void on_lineEdit_textChanged(const QString &arg1);

signals:
    void singal_selectedItems(QModelIndexList const &list);

private:
    Ui::DataChoose *ui;
};

#endif // DATA_CHOOSE_H
