﻿#ifndef DATE_TIME_EDIT_H
#define DATE_TIME_EDIT_H

#include <QComboBox>
#include <QDebug>

#include "date_time_widget.h"

class DateTimeEdit : public QComboBox
{
    Q_OBJECT
public:
    explicit DateTimeEdit(QWidget *parent = 0);

    QDateTime datetime() const;
    void setDateTime(const QDateTime& dt);
    QString dateToStr();

private:

    DateTimeWidget *cell;

    void initPage();

    void mousePressEvent(QMouseEvent *e) override;
};
#endif // DATE_TIME_EDIT_H
