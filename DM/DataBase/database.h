﻿#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QMutex>//互斥锁，保持数据库的原子性

#include "math.h"

class Database
{
public:
    Database();

    void addLog(int user_id, QString part, QString op, QString status);
    QSqlQuery query(QString table, QString cols="*", QString key="", QString order="", QString limit="");
    int query_max_id(QString table);
    QString createGlobalKey(QString table, QString key, QStringList cols);

    /*
      * @brief              模糊查询并排序数据 select * from ···
      * @param table        表名
      * @param name         排序依据的属性名
      * @param sort         true:升序，fasle:降序
      * @param limit_name   限制的属性名
      * @param limit        限制的内容
      * @return             QSqlquery
      */
    QSqlQuery SortData(QString table, QString name, bool sort, QString limit_name=nullptr, QString limit=nullptr);

    /*
      * @brief              精确查询数据
      * @param table        表名
      * @param limit_name   查询的属性名
      * @param limit        查询的内容(限定字符串)
      * @return             QSqlquery
      */
    QSqlQuery ExactMatchQuery(QString table, QString limit_name, QString limit);


    //Map和QString互转
    QString mapToString(QMap<QString, QString> map);
    QMap<QString, QString> stringToMap(QString string);

private:
    bool init();
    QSqlDatabase db;
    QString client_ip = "0.0.0.0";


};

extern Database *mydb;

#endif // DATABASE_H
