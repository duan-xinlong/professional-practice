#ifndef USERCONFIGDIALOG_H
#define USERCONFIGDIALOG_H

#include <QDialog>
#include <QTreeWidgetItem>
#include "usermgmt.h"

namespace Ui {
class UserConfigDialog;
}

class UserConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UserConfigDialog(QWidget *parent = nullptr, int id = 0);
    ~UserConfigDialog();

private slots:
    void on_btn_cancel_clicked();

    void on_btn_save_clicked();

private:
    Ui::UserConfigDialog *ui;
    int user_id = -1;
    UserMgmt * parent_pointer = nullptr;

    QMap<QString,int> map_org;
    QMap<QString,int> map_role;
};

#endif // USERCONFIGDIALOG_H
