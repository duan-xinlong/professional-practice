#ifndef ORGMGMT_H
#define ORGMGMT_H

#include <QWidget>
#include <QTreeWidgetItem>
#include <QStandardItem>

namespace Ui {
class OrgMgmt;
}

class OrgMgmt : public QWidget
{
    Q_OBJECT

public:
    explicit OrgMgmt(QWidget *parent = nullptr);
    ~OrgMgmt();

private slots:
    void on_btn_add_clicked();
    void on_btn_cancel_clicked();
    void on_btn_save_clicked();
    void on_btn_refresh_clicked();
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

    void on_btn_del_clicked();

private:
    Ui::OrgMgmt *ui;
    QTreeWidgetItem* AddTreeNode(QTreeWidgetItem *parent,QString name, QTreeWidget *root);
    void InitTree(QTreeWidgetItem *parent, int id, QTreeWidget *root);

    int item_id = -1;
    QMap<QString, int> map_name_id, map_name_parent; //记录名字和ID键值对

    QModelIndex Root_Index;
    QTreeWidget *comboboxTree;
};

#endif // ORGMGMT_H
