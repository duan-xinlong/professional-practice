#ifndef USERTOOLBOX_H
#define USERTOOLBOX_H

#include <QWidget>
#include "usermgmt.h"

namespace Ui {
class UserToolBox;
}

class UserToolBox : public QWidget
{
    Q_OBJECT

public:
    explicit UserToolBox(UserMgmt *parent, QWidget *parent2 = nullptr);
    ~UserToolBox();

private:
    Ui::UserToolBox *ui;
};

#endif // USERTOOLBOX_H
