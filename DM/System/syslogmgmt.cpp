#include "syslogmgmt.h"
#include "ui_syslogmgmt.h"
#include "DataBase/database.h"
#include <QDebug>

SysLogMgmt::SysLogMgmt(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SysLogMgmt)
{
    ui->setupUi(this);
    switchToPage(1);

    //调整表格
    int widths[] = {60, 150, 500, 150, 150, 100, 200, 260};
    for(int i=0; i<=7; i++)
        ui->tableWidget->setColumnWidth(i, widths[i]);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    //起止时间
    ui->dte_start->setDateTime(QDateTime::currentDateTime().addDays(-1));
    ui->dte_end->setDateTime(QDateTime::currentDateTime());
}

SysLogMgmt::~SysLogMgmt()
{
    delete ui;
}

bool SysLogMgmt::refresh(QSqlQuery query)
{
    if(query.size() <= 0){
        ui->tableWidget->setRowCount(0);
        return false;
    }

    int list[] = {0, 2, 3, 9, 8, -4, 5, 6};
    ui->tableWidget->setRowCount(query.size());
    for(int i=0; i<query.size(); i++){
        if(query.next()){
            for(int j=0; j<=4; j++){
                ui->tableWidget->setItem(i,j,new QTableWidgetItem(query.value(list[j]).toString()));
            }
//            ui->tableWidget->setItem(i,3,new QTableWidgetItem(query.value(9).toString()));
//            ui->tableWidget->setItem(i,4,new QTableWidgetItem(query.value(8).toString()));

            //状态列
            ui->tableWidget->setItem(i,5,new QTableWidgetItem(query.value(4).toInt() == 1 ? "成功" : "失败"));
            //ip地址列
            ui->tableWidget->setItem(i,6,new QTableWidgetItem(query.value(list[6]).toString()));
            //操作时间列
            ui->tableWidget->setItem(i,7,new QTableWidgetItem(query.value(6).toDateTime().toString("yyyy-MM-dd hh:mm:ss")));

            //全部居中
            for(int j=0; j<=7; j++){
                if(ui->tableWidget->item(i,j) == nullptr) continue;
                ui->tableWidget->item(i,j)->setTextAlignment(Qt::AlignCenter);
            }
        }else{
            break;
        }
    }
    return true;
}

//更新当前页和翻页部分
void SysLogMgmt::updatePage(){switchToPage(nowPage);}
void SysLogMgmt::on_btn_page_minus_clicked(){switchToPage(nowPage-1);}
void SysLogMgmt::on_btn_page_plus_clicked(){switchToPage(nowPage+1);}
void SysLogMgmt::on_btn_page_head_clicked(){switchToPage(1);}
void SysLogMgmt::on_btn_page_end_clicked(){switchToPage(999999999);}

void SysLogMgmt::switchToPage(int page)
{
    //联合查询
    QString table = "dm_log LEFT JOIN dm_user ON COALESCE(dm_log.user_id,'') = COALESCE(dm_user.id,'')";
    QSqlQuery query = mydb->query(table, "COUNT(*)", keyword);
    if(!query.next()) return;

    totalLineNum = query.value(0).toInt();
    totalPage = (int)ceil((float) totalLineNum/pageLineNum);
    ui->lb_totolpage->setText(QString("/%1").arg(totalPage));

    if(page > totalPage) page = totalPage;
    if(page < 1) page = 1;
    nowPage = page;
    ui->le_nowpage->setText(QString("%1").arg(page));

    QString limit = QString(" LIMIT %1,%2").arg((page-1)*pageLineNum).arg(pageLineNum);
    query = mydb->query(table, "*", keyword, order, limit);
    refresh(query);
}

//查询和重置
void SysLogMgmt::on_btn_reset_clicked()
{
    keyword = defalut_keyword;
    switchToPage(1);
}
void SysLogMgmt::on_btn_search_clicked()
{
    keyword = QString(" WHERE dm_log.create_time BETWEEN '%1' AND '%2'")
                  .arg(ui->dte_start->dateTime().toString("yyyy-MM-dd hh:mm:ss"))
                  .arg(ui->dte_end->dateTime().toString("yyyy-MM-dd hh:mm:ss"));
    if(!ui->le_module->text().isEmpty()){
        keyword += QString(" AND dm_log.operation LIKE '%%1%'").arg(ui->le_module->text());
        qDebug() << ui->le_module->text();
    }
    if(!ui->le_content->text().isEmpty()){
        keyword += QString(" AND dm_log.content LIKE '%%1%'").arg(ui->le_content->text());
    }
    if(!ui->le_person->text().isEmpty()){
        keyword += QString(" AND dm_user.username LIKE '%%1%'").arg(ui->le_person->text());
    }
    switchToPage(1);
}
