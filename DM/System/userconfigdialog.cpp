#include "userconfigdialog.h"
#include "ui_userconfigdialog.h"

#include "DataBase/database.h"
#include "mainwindow.h"
#include <QDebug>

//三表联合查询
static QString cols = "u.username,u.login_name,u.position,u.phone,u.email,u.sort,u.remark,r.name,o.org_full_name,u.status";
static QString table = "dm_user u \
LEFT JOIN dm_role r ON COALESCE(u.role_id,'')=COALESCE(r.id,'') \
LEFT JOIN dm_organization o ON COALESCE(o.org_no,'')=COALESCE(u.org_no,'')";

UserConfigDialog::UserConfigDialog(QWidget *parent, int id) :
    QDialog(parent),
    ui(new Ui::UserConfigDialog)
{
    ui->setupUi(this);
    setWindowModality(Qt::ApplicationModal);

    ui->cmb_org->addItem("---");
    ui->cmb_role->addItem("---");

    parent_pointer = (UserMgmt *) parent;
    //机构
    QSqlQuery query = mydb->query("dm_organization","org_no,org_full_name", " WHERE delete_flag=0");
    while(query.next()){
        ui->cmb_org->addItem(query.value(1).toString());
        map_org.insert(query.value(1).toString(), query.value(0).toInt());
    }
    //角色
    query = mydb->query("dm_role","id,name", " WHERE delete_flag=0");
    while(query.next()){
        ui->cmb_role->addItem(query.value(1).toString());
        map_role.insert(query.value(1).toString(), query.value(0).toInt());
    }

    if(id <= 0){
        setWindowTitle("新增用户");
    }else{
        setWindowTitle("编辑用户");
        query = mydb->query(table,cols,QString(" WHERE u.id='%1'").arg(id));
        if(query.next()){
            user_id = id;
            ui->le_username->setText(query.value(0).toString());
            ui->le_loginname->setText(query.value(1).toString());
            ui->le_pos->setText(query.value(2).toString());
            ui->le_phone->setText(query.value(3).toString());
            ui->le_email->setText(query.value(4).toString());
            ui->le_sort->setText(query.value(5).toString());
            ui->pte_remark->setPlainText(query.value(6).toString());

            ui->cmb_role->setCurrentIndex(ui->cmb_role->findText(query.value(7).toString()));
            ui->cmb_org->setCurrentIndex(ui->cmb_org->findText(query.value(8).toString()));
            ui->cmb_status->setCurrentIndex(query.value(9).toInt());

            setWindowTitle("编辑用户 - " + query.value(1).toString());
        }
    }
}

UserConfigDialog::~UserConfigDialog()
{
    delete ui;
}

void UserConfigDialog::on_btn_cancel_clicked()
{
    this->close();
}


void UserConfigDialog::on_btn_save_clicked()
{
    QString username = ui->le_username->text();
    QString loginname = ui->le_loginname->text();
    QString position = ui->le_pos->text();
    QString phone = ui->le_phone->text();
    QString email = ui->le_email->text();
    int sort = ui->le_sort->text().toInt();
    QString remark = ui->pte_remark->toPlainText();

    QSqlQuery query;
    QString sql,sql_role;

    int role_id = map_role.contains(ui->cmb_role->currentText()) ? map_role.value(ui->cmb_role->currentText()) : 0;
    int org_no = map_org.contains(ui->cmb_org->currentText()) ? map_org.value(ui->cmb_org->currentText()) : 0;
    int status = ui->cmb_status->currentIndex();

    if(user_id <= 0){
        sql=QString("INSERT dm_user(username,login_name,position,org_no,phone,email,sort,remark,status,role_id,\
create_time,password)VALUES('%1','%2','%3','%4','%5','%6','%7','%8','%9','%10',NOW(),'123456')" ).arg(username).arg(loginname)
.arg(position).arg(org_no).arg(phone).arg(email).arg(sort).arg(remark).arg(status).arg(role_id);
        if(!query.exec(sql)) return;
        mydb->addLog(DM_Window->current_user_id,"用户中心", QString("新增用户<%1>").arg(username), "1");
    }else{
        sql=QString("UPDATE dm_user SET username='%1',login_name='%2',position='%3',org_no='%4',\
phone='%5',email='%6',sort='%7',remark='%8',status='%9',role_id='%10' WHERE id='%11';").arg(username).arg(loginname)
.arg(position).arg(org_no).arg(phone).arg(email).arg(sort).arg(remark).arg(status).arg(role_id).arg(user_id);
        if(!query.exec(sql)) return;
        mydb->addLog(DM_Window->current_user_id,"用户中心", QString("更改用户<%1>").arg(username), "1");
    }
    qDebug() << sql;

    parent_pointer->updatePage();
    this->close();
}
