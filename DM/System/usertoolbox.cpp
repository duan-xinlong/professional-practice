#include "usertoolbox.h"
#include "ui_usertoolbox.h"

UserToolBox::UserToolBox(UserMgmt *parent, QWidget *parent2) :
    QWidget(parent2),
    ui(new Ui::UserToolBox)
{
    ui->setupUi(this);
    connect(ui->btn_func, SIGNAL(clicked()), parent, SLOT(btn_func_clicked()));
    connect(ui->btn_edit, SIGNAL(clicked()), parent, SLOT(btn_edit_clicked()));
    connect(ui->btn_del, SIGNAL(clicked()), parent, SLOT(btn_del_clicked()));
}

UserToolBox::~UserToolBox()
{
    delete ui;
}


