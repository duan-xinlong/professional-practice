#include "roletoolbox.h"
#include "ui_roletoolbox.h"

RoleToolBox::RoleToolBox(RoleMgmt *parent, QWidget *parent2) :
    QWidget(parent2),
    ui(new Ui::RoleToolBox)
{
    ui->setupUi(this);
//    connect(ui->btn_accs, SIGNAL(clicked()), parent, SLOT(on_btn_accs_clicked()));
    connect(ui->btn_func, SIGNAL(clicked()), parent, SLOT(btn_func_clicked()));
    connect(ui->btn_edit, SIGNAL(clicked()), parent, SLOT(btn_edit_clicked()));
    connect(ui->btn_del, SIGNAL(clicked()), parent, SLOT(btn_del_clicked()));
}

RoleToolBox::~RoleToolBox()
{
    delete ui;
}
