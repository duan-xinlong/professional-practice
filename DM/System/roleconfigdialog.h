#ifndef ROLECONFIGDIALOG_H
#define ROLECONFIGDIALOG_H

#include <QDialog>
#include "DataBase/database.h"
#include "rolemgmt.h"

namespace Ui {
class RoleConfigDialog;
}

class RoleConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RoleConfigDialog(QWidget *parent = nullptr, int id = 0);
    ~RoleConfigDialog();

private slots:
    void on_btn_cancel_clicked();
    void on_btn_save_clicked();

private:
    Ui::RoleConfigDialog *ui;

    int item_id = -1;
    RoleMgmt *item_parent = nullptr;
};

#endif // ROLECONFIGDIALOG_H
