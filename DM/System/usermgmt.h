#ifndef USERMGMT_H
#define USERMGMT_H

#include <QWidget>
#include "DataBase/database.h"
#include <QPushButton>

namespace Ui {
class UserMgmt;
}

class UserMgmt : public QWidget
{
    Q_OBJECT

public:
    explicit UserMgmt(QWidget *parent = nullptr);
    ~UserMgmt();
    void updatePage();

public slots:
    void btn_func_clicked();
    void btn_edit_clicked();
    void btn_del_clicked();

private slots:
    void on_btn_page_minus_clicked();
    void on_btn_page_plus_clicked();
    void on_btn_page_head_clicked();
    void on_btn_page_end_clicked();

    void on_btn_search_clicked();
    void on_btn_reset_clicked();
    void on_btn_add_clicked();

    void on_btn_batch_del_clicked();

private:
    Ui::UserMgmt *ui;
    bool refresh(QSqlQuery query);
    int getBtnRow(QPushButton* btn);

    int totalLineNum = 0;
    int pageLineNum = 20;
    int totalPage = 1;
    int nowPage = 1;
    void switchToPage(int page);

    QString order = " ORDER BY u.sort";
    QString defalut_keyword = " WHERE u.delete_flag=0";
    QString keyword = defalut_keyword; //查询关键词
};

#endif // USERMGMT_H
