#ifndef MYSQLTABLEMODEL_H
#define MYSQLTABLEMODEL_H
#include "../DataBase/database.h"
#include <QSqlTableModel>
#include <QObject>
#include <QVariant>

class MySqlTableModel : public QSqlTableModel
{
    Q_OBJECT
public:
    MySqlTableModel(QObject *parent);

    bool setData( const QModelIndex &index, const QVariant &value, int role);
    QVariant data(const QModelIndex &index, int role=Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    //为了在新的一列加入按钮重写列
//    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    //重写表头
//    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QMap<int, Qt::CheckState> check_state_map;//存放复选框的选择还是未选中状态
    int checkColumn = 0;//将复选框放到第几列，从0开始计数
//    void onStateChanged();//更新表头的复选框状态A

//public slots:
//    void onStateChanged(int state);//更新复选框状态

//signals:
//    void stateChanged(int state);//复选框被点击发送信号
};

#endif // MYSQLTABLEMODEL_H
