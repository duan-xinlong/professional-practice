#ifndef SYSLOGMGMT_H
#define SYSLOGMGMT_H

#include <QWidget>
#include <QSqlQuery>
#include <QPushButton>

namespace Ui {
class SysLogMgmt;
}

class SysLogMgmt : public QWidget
{
    Q_OBJECT

public:
    explicit SysLogMgmt(QWidget *parent = nullptr);
    ~SysLogMgmt();

    void updatePage();

private slots:
    void on_btn_page_minus_clicked();
    void on_btn_page_plus_clicked();
    void on_btn_page_head_clicked();
    void on_btn_page_end_clicked();

    void on_btn_reset_clicked();
    void on_btn_search_clicked();

private:
    Ui::SysLogMgmt *ui;

    bool refresh(QSqlQuery query);
    void switchToPage(int page);

    int totalLineNum = 0;
    int totalPage = 1;
    int pageLineNum = 20;
    int nowPage = 1;
    QString defalut_keyword = "";
    QString keyword = defalut_keyword; //查询关键词
    QString order = " ORDER BY dm_log.create_time";
};

#endif // SYSLOGMGMT_H
