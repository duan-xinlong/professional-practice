#ifndef ROLETOOLBOX_H
#define ROLETOOLBOX_H

#include <QWidget>
#include "rolemgmt.h"

namespace Ui {
class RoleToolBox;
}

class RoleToolBox : public QWidget
{
    Q_OBJECT

public:
    explicit RoleToolBox(RoleMgmt *parent, QWidget *parent2 = nullptr);
    ~RoleToolBox();

private:
    Ui::RoleToolBox *ui;
};

#endif // ROLETOOLBOX_H
