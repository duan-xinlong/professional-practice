#ifndef MYTOOLBOX2_H
#define MYTOOLBOX2_H

#include <QWidget>

namespace Ui {
class MyToolBox2;
}

class MyToolBox2 : public QWidget
{
    Q_OBJECT

public:
    //explicit MyToolBox2(QVariant p, QString table = "", int id = 0, QWidget *parent = nullptr);
    explicit MyToolBox2(QString type, void *pointer, QString table, int id, QWidget *parent = nullptr);
    ~MyToolBox2();

private slots:
    void on_btn_del_clicked();
    void on_btn_edit_clicked();

private:
    Ui::MyToolBox2 *ui;
    QString type;
    QString table;
    void *pointer;
    int id;
};

#endif // MYTOOLBOX2_H
