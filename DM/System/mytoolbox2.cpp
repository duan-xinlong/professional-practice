#include "mytoolbox2.h"
#include "ui_mytoolbox2.h"
#include "../DataBase/database.h"
#include "../mainwindow.h"
#include <QDebug>

QVariant par;

MyToolBox2::MyToolBox2(QString _type, void *_pointer, QString _table, int _id, QWidget *parent) :
//MyToolBox2::MyToolBox2(void *f(), QString _table, int _id, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MyToolBox2)
{
    ui->setupUi(this);
    type=_type; pointer=_pointer; id=_id; table = _table;
}

MyToolBox2::~MyToolBox2()
{
    delete ui;
}

void MyToolBox2::on_btn_del_clicked()
{
    if(QMessageBox::Yes == QMessageBox::question(this,"删除","确定删除这行数据吗？", QMessageBox::Yes | QMessageBox::No , QMessageBox::No)){
        QString sql = QString("DELETE FROM %1 WHERE id=%2").arg(table).arg(id);
        QSqlQuery query(sql);

        if(type == "UserMgmt*"){
            ((UserMgmt*) pointer)->updatePage();
        }
    }
}


void MyToolBox2::on_btn_edit_clicked()
{

}

