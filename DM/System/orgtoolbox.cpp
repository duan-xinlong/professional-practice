#include "orgtoolbox.h"
#include "ui_orgtoolbox.h"

OrgToolBox::OrgToolBox(int id, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OrgToolBox)
{
    ui->setupUi(this);
}

OrgToolBox::~OrgToolBox()
{
    delete ui;
}
