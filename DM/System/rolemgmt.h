#ifndef ROLEMGMT_H
#define ROLEMGMT_H

#include <QWidget>

#include "DataBase/database.h"
#include <QPushButton>
#include <QComboBox>
#include <QMessageBox>

namespace Ui {
class RoleMgmt;
}

class RoleMgmt : public QWidget
{
    Q_OBJECT

public:
    explicit RoleMgmt(QWidget *parent = nullptr);
    ~RoleMgmt();
    void updatePage();

public slots:
    void btn_func_clicked();
    void btn_edit_clicked();
    void btn_del_clicked();

private slots:
    void on_btn_page_minus_clicked();
    void on_btn_page_plus_clicked();
    void on_btn_page_head_clicked();
    void on_btn_page_end_clicked();

    void on_btn_reset_clicked();
    void on_btn_search_clicked();
    void on_btn_add_clicked();

    void on_btn_batch_del_clicked();

private:
    Ui::RoleMgmt *ui;

    bool refresh(QSqlQuery query);
    int getBtnRow(QPushButton* btn);
    void switchToPage(int page);

    int totalLineNum = 0;
    int totalPage = 1;
    int pageLineNum = 20;
    int nowPage = 1;
    QString defalut_keyword = " WHERE delete_flag='0'";
    QString keyword = defalut_keyword; //查询关键词
    QString order = " ORDER BY sort";
};

#endif // ROLEMGMT_H
