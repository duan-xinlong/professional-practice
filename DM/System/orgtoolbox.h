#ifndef ORGTOOLBOX_H
#define ORGTOOLBOX_H

#include <QWidget>

namespace Ui {
class OrgToolBox;
}

class OrgToolBox : public QWidget
{
    Q_OBJECT

public:
    explicit OrgToolBox(int id, QWidget *parent = nullptr);
    ~OrgToolBox();

private:
    Ui::OrgToolBox *ui;

signals:
    void update();
};

#endif // ORGTOOLBOX_H
