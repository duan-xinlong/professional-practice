#include "roleaccessdialog.h"
#include "ui_roleaccessdialog.h"

#include <QDebug>
#include "DataBase/database.h"
#include "mainwindow.h"

static QTreeWidgetItem * item;

RoleAccessDialog::RoleAccessDialog(QWidget *parent, int id, QString name) :
    QDialog(parent),
    ui(new Ui::RoleAccessDialog)
{
    ui->setupUi(this);
    setWindowModality(Qt::ApplicationModal);
    ui->stackedWidget->setCurrentIndex(0);
    item_id = id;
    item_name = name;

    setWindowTitle("权限管理 - "+name);

    QSqlQuery query = mydb->query("dm_function", "*");
    while(query.next()){
        map_id.insert(query.value(2).toString(), query.value(0).toString());
        map_number.insert(query.value(2).toString(), query.value(1).toString());
        map_parent.insert(query.value(2).toString(), query.value(3).toString());
    }
    ui->treeWidget->clear();
    InitTree(nullptr, "0");

    query = mydb->query("dm_role", "module,value", QString(" WHERE id=%1").arg(item_id));
    if(query.next()){
//        qDebug() << query.value(0).toString() << query.value(1).toString();
        map_module = mydb->stringToMap(query.value(0).toString());
        map_func = mydb->stringToMap(query.value(1).toString());
        qDebug() << map_module << map_func;
    }

    //选择联动
    QTreeWidgetItemIterator it(ui->treeWidget);
    while (*it) {
        if(map_func.contains( (*it)->text(0) )) (*it)->setCheckState(0, Qt::Checked);
        (*it)->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsAutoTristate);
        ++it;
    }
    ui->treeWidget->expandAll();

    ui->tableWidget->setColumnWidth(0, 60);
    ui->tableWidget->setColumnWidth(2, 80);
    ui->tableWidget->setColumnWidth(3, 80);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
}

RoleAccessDialog::~RoleAccessDialog()
{
    delete ui;
}

QTreeWidgetItem* RoleAccessDialog::AddTreeNode(QTreeWidgetItem *parent, QString name, QString id)
{
    QTreeWidgetItem * item = new QTreeWidgetItem(QStringList()<<name/*<<id*/);
    if(parent == nullptr) ui->treeWidget->addTopLevelItem(item);
    else parent->addChild(item);
    item->setCheckState(0, Qt::Unchecked);
    return item;
}

//树搜索
void RoleAccessDialog::InitTree(QTreeWidgetItem *parent, QString number)
{
//    QString sql = QString("select * from dm_function where parent_number='%1';").arg(number);
//    QSqlQuery query(sql);
//    while(query.next()){
//        item = AddTreeNode(parent, query.value(2).toString(), query.value(0).toString());
//        //遍历子节点 number
//        InitTree(item, query.value(1).toString());
//    }
    QList<QString> list = map_parent.keys(number);
    for(int i=0; i<list.size(); i++){
        item = AddTreeNode(parent, list[i], map_id.value(list[i]));
        //遍历子节点
        InitTree(item, map_number.value(list[i]));
    }
}

void RoleAccessDialog::on_btn_go_clicked()
{
    int row = 0;
    QCheckBox *edit, *watch;

    map_module.clear();

    ui->tableWidget->setRowCount(0);
    QTreeWidgetItemIterator it(ui->treeWidget);
    while (*it) {
        if((*it)->checkState(0) == Qt::Checked || (*it)->checkState(0) == Qt::PartiallyChecked){
            if((*it)->childCount() > 0){ //有儿子
                map_module.insert((*it)->text(0), "1");
            }else{
                ui->tableWidget->setRowCount(row + 1);
                ui->tableWidget->setItem(row, 0, new QTableWidgetItem( QString("%1").arg(row+1) ));
                ui->tableWidget->item(row, 0)->setTextAlignment(Qt::AlignCenter);
                ui->tableWidget->setItem(row, 1, new QTableWidgetItem( (*it)->text(0) ));
                ui->tableWidget->item(row, 1)->setTextAlignment(Qt::AlignCenter);

                edit = new QCheckBox();
                edit->setText("编辑");
                ui->tableWidget->setCellWidget(row, 2, edit);

                watch = new QCheckBox();
                watch->setText("审核");
                ui->tableWidget->setCellWidget(row, 3, watch);

                if(map_func.contains((*it)->text(0))){
                    if(map_func.value((*it)->text(0)) == "2") edit->setCheckState(Qt::Checked);
                    else if(map_func.value((*it)->text(0)) == "1") watch->setCheckState(Qt::Checked);
                }
                row++;
            }
        }
        ++it;
    }
    ui->stackedWidget->setCurrentIndex(1);
}

void RoleAccessDialog::on_btn_ok_clicked()
{
    map_func.clear();

    for(int i=0; i<ui->tableWidget->rowCount(); i++){
        if( ( (QCheckBox *) ui->tableWidget->cellWidget(i, 2) )->isChecked() ){
            map_func.insert(ui->tableWidget->item(i,1)->text(), "2");
        }else if( ( (QCheckBox *) ui->tableWidget->cellWidget(i, 3) )->isChecked() ){
            map_func.insert(ui->tableWidget->item(i,1)->text(), "1");
        }
    }

    QString sql = QString("UPDATE dm_role SET module='%1',value='%2' WHERE id='%3'")
                      .arg(mydb->mapToString(map_module)).arg(mydb->mapToString(map_func)).arg(item_id);
    QSqlQuery query;
    if(query.exec(sql)){
        mydb->addLog(DM_Window->current_user_id, "角色管理", QString("更改角色<%1>权限").arg(item_name), "1");
    }
    this->close();
}

void RoleAccessDialog::on_btn_return_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}
void RoleAccessDialog::on_btn_cancel_clicked()
{
    this->close();
}
void RoleAccessDialog::on_btn_close_clicked()
{
    this->close();
}
