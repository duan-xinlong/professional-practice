#ifndef ROLEACCESSDIALOG_H
#define ROLEACCESSDIALOG_H

#include <QDialog>
#include <QCheckBox>
#include <QTreeWidgetItem>


namespace Ui {
class RoleAccessDialog;
}

class RoleAccessDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RoleAccessDialog(QWidget *parent = nullptr, int id = 0, QString name = "");
    ~RoleAccessDialog();

private slots:
    void on_btn_go_clicked();
    void on_btn_return_clicked();
    void on_btn_cancel_clicked();
    void on_btn_close_clicked();
    void on_btn_ok_clicked();

private:
    Ui::RoleAccessDialog *ui;

    QTreeWidgetItem* AddTreeNode(QTreeWidgetItem *parent,QString name,QString desc);
    void InitTree(QTreeWidgetItem *parent, QString number);

    QMap<QString, QString> map_id;
    QMap<QString, QString> map_number;
    QMap<QString, QString> map_parent;

    QMap<QString, QString> map_module;
    QMap<QString, QString> map_func;

    int item_id = 0;
    QString item_name = "";
};

#endif // ROLEACCESSDIALOG_H
