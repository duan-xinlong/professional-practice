#ifndef PERSONCENTER_H
#define PERSONCENTER_H

#include <QWidget>

namespace Ui {
class PersonCenter;
}

class PersonCenter : public QWidget
{
    Q_OBJECT

public:
    explicit PersonCenter(QWidget *parent = nullptr);
    ~PersonCenter();

private:
    Ui::PersonCenter *ui;
};

#endif // PERSONCENTER_H
