/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80034
 Source Host           : localhost:3306
 Source Schema         : dm_database

 Target Server Type    : MySQL
 Target Server Version : 80034
 File Encoding         : 65001

 Date: 22/10/2023 14:54:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dm_function
-- ----------------------------
DROP TABLE IF EXISTS `dm_function`;
CREATE TABLE `dm_function`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `number` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '名称',
  `parent_number` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '上级编号',
  `sort` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '排序',
  `push_btn` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '功能按钮',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 283 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '功能模块表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dm_function
-- ----------------------------
INSERT INTO `dm_function` VALUES (10, '0001', '系统管理', '0', '', '');
INSERT INTO `dm_function` VALUES (11, '000101', '系统日志', '0001', '', '1');
INSERT INTO `dm_function` VALUES (12, '000102', '角色管理', '0001', '', '1,2');
INSERT INTO `dm_function` VALUES (13, '000103', '用户管理', '0001', '', '1,2');
INSERT INTO `dm_function` VALUES (14, '000104', '机构管理', '0001', NULL, '1,2');
INSERT INTO `dm_function` VALUES (15, '000105', '系统配置', '0001', NULL, '1,2');
INSERT INTO `dm_function` VALUES (20, '0101', '生产计划', '0', NULL, NULL);
INSERT INTO `dm_function` VALUES (21, '010101', '生产工单', '0101', NULL, NULL);
INSERT INTO `dm_function` VALUES (22, '010102', '领料退单', '0101', NULL, NULL);
INSERT INTO `dm_function` VALUES (23, '010103', '生产报工', '0101', NULL, NULL);
INSERT INTO `dm_function` VALUES (24, '010104', '生产退料', '0101', NULL, NULL);
INSERT INTO `dm_function` VALUES (30, '0201', '销售管理', '0', NULL, NULL);
INSERT INTO `dm_function` VALUES (31, '020101', '销售订单', '0201', NULL, NULL);
INSERT INTO `dm_function` VALUES (32, '020102', '退货订单', '0201', NULL, NULL);
INSERT INTO `dm_function` VALUES (33, '020103', '换货订单', '0201', NULL, NULL);
INSERT INTO `dm_function` VALUES (40, '0301', '采购管理', '0', NULL, NULL);
INSERT INTO `dm_function` VALUES (41, '030101', '采购订单', '0301', NULL, NULL);
INSERT INTO `dm_function` VALUES (42, '030102', '采购材料', '0301', NULL, NULL);
INSERT INTO `dm_function` VALUES (43, '030103', '采购员表', '0301', NULL, NULL);
INSERT INTO `dm_function` VALUES (44, '030104', '供应商表', '0301', NULL, NULL);
INSERT INTO `dm_function` VALUES (45, '030105', '订单评价', '0301', NULL, NULL);
INSERT INTO `dm_function` VALUES (50, '0401', '库存管理', '0', NULL, NULL);
INSERT INTO `dm_function` VALUES (51, '040101', '入库单', '0401', NULL, NULL);
INSERT INTO `dm_function` VALUES (52, '040102', '出库单', '0401', NULL, NULL);
INSERT INTO `dm_function` VALUES (53, '040103', '移库记录', '0401', NULL, NULL);
INSERT INTO `dm_function` VALUES (54, '040104', '库存盘点', '0401', NULL, NULL);
INSERT INTO `dm_function` VALUES (55, '040105', '物料信息', '0401', NULL, NULL);

-- ----------------------------
-- Table structure for dm_log
-- ----------------------------
DROP TABLE IF EXISTS `dm_log`;
CREATE TABLE `dm_log`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(0) NOT NULL,
  `operation` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `content` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `status` tinyint(0) NULL DEFAULT NULL,
  `client_ip` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `用户id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 180 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dm_log
-- ----------------------------
INSERT INTO `dm_log` VALUES (225, 1, '用户中心', '登录系统', 1, 'localhost', '2023-10-22 14:25:31');
INSERT INTO `dm_log` VALUES (226, 1, '用户中心', '登录系统', 1, 'localhost', '2023-10-22 14:27:38');
INSERT INTO `dm_log` VALUES (227, 1, '用户中心', '登录系统', 1, 'localhost', '2023-10-22 14:28:20');
INSERT INTO `dm_log` VALUES (228, 1, '机构管理', '更改机构\'可以\'为\'可以的\'', 1, 'localhost', '2023-10-22 14:28:28');
INSERT INTO `dm_log` VALUES (229, 1, '用户中心', '登录系统', 1, 'localhost', '2023-10-22 14:41:53');
INSERT INTO `dm_log` VALUES (230, 1, '用户中心', '登录系统', 1, 'localhost', '2023-10-22 14:42:40');
INSERT INTO `dm_log` VALUES (231, 1, '角色管理', '变更角色\'超级管理员\'权限', 1, 'localhost', '2023-10-22 14:42:45');
INSERT INTO `dm_log` VALUES (232, 1, '角色管理', '更改角色\'超级管理员\'', 1, 'localhost', '2023-10-22 14:42:50');
INSERT INTO `dm_log` VALUES (233, 1, '角色管理', '删除角色\'324\'', 1, 'localhost', '2023-10-22 14:42:54');
INSERT INTO `dm_log` VALUES (234, 1, '机构管理', '新增机构\'123\'', 1, 'localhost', '2023-10-22 14:43:07');

-- ----------------------------
-- Table structure for dm_orga_user_rel
-- ----------------------------
DROP TABLE IF EXISTS `dm_orga_user_rel`;
CREATE TABLE `dm_orga_user_rel`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(0) NULL DEFAULT NULL,
  `user_id` bigint(0) NULL DEFAULT NULL,
  `user_blng_orga_dspl_seq` bigint(0) NULL DEFAULT NULL,
  `delete_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '删除标记',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `creator` bigint(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `updater` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dm_orga_user_rel
-- ----------------------------

-- ----------------------------
-- Table structure for dm_organization
-- ----------------------------
DROP TABLE IF EXISTS `dm_organization`;
CREATE TABLE `dm_organization`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `org_no` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `org_full_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `org_abr` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `delete_flag` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `parent_id` bigint(0) NULL DEFAULT NULL,
  `sort` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `route` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '索引路径',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dm_organization
-- ----------------------------
INSERT INTO `dm_organization` VALUES (1, '100', '计划部', NULL, '0', 0, '1', NULL, NULL, NULL, NULL);
INSERT INTO `dm_organization` VALUES (2, '200', '销售部', NULL, '0', 0, '2', NULL, NULL, NULL, NULL);
INSERT INTO `dm_organization` VALUES (3, '300', '采购部', NULL, '0', 0, '3', NULL, NULL, NULL, NULL);
INSERT INTO `dm_organization` VALUES (4, '400', '物流部', NULL, '0', 0, '4', NULL, NULL, NULL, NULL);
INSERT INTO `dm_organization` VALUES (5, '500', '信息部', NULL, '0', 0, '5', NULL, NULL, NULL, NULL);
INSERT INTO `dm_organization` VALUES (6, '600', '生产部', NULL, '0', 0, '6', NULL, NULL, NULL, NULL);
INSERT INTO `dm_organization` VALUES (7, '700', '财务部', NULL, '0', 0, '7', NULL, NULL, NULL, NULL);
INSERT INTO `dm_organization` VALUES (8, '800', '行政部', NULL, '0', 0, '8', NULL, NULL, '2023-10-21 02:27:10', NULL);
INSERT INTO `dm_organization` VALUES (9, '401', '仓库管理处', NULL, '0', 4, '1', NULL, NULL, NULL, '4');
INSERT INTO `dm_organization` VALUES (10, '503', '五十', NULL, '0', 9, '10', '棒', '2023-10-21 02:38:31', '2023-10-21 12:07:25', '4,9');
INSERT INTO `dm_organization` VALUES (11, '001', '好吧', NULL, '0', 10, '2', '', '2023-10-21 02:40:11', '2023-10-22 14:25:49', '4,9,10');
INSERT INTO `dm_organization` VALUES (12, '009', '可以的', NULL, '0', 10, '16', '123', '2023-10-21 02:46:46', '2023-10-22 14:28:28', '4,9,10');
INSERT INTO `dm_organization` VALUES (13, '907', '教五', NULL, NULL, 1, '123', '', '2023-10-21 13:14:20', '2023-10-21 13:14:39', '1');
INSERT INTO `dm_organization` VALUES (15, '1231', '123', NULL, NULL, 13, '1', '', '2023-10-22 14:43:07', NULL, '1,13');

-- ----------------------------
-- Table structure for dm_role
-- ----------------------------
DROP TABLE IF EXISTS `dm_role`;
CREATE TABLE `dm_role`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `type` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `module` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `value` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `description` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `enabled` tinyint(1) NULL DEFAULT NULL,
  `sort` bigint(0) NULL DEFAULT NULL,
  `delete_flag` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 95006 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dm_role
-- ----------------------------
INSERT INTO `dm_role` VALUES (1, '采购经理', '个人', '系统管理:1', '机构管理:1,系统日志:1', '管理员2', 1, 2, 0);
INSERT INTO `dm_role` VALUES (2, '采购专员2', '部门', NULL, NULL, 'good1', 1, 30, 0);
INSERT INTO `dm_role` VALUES (3, '信息部管理员', '个人', NULL, NULL, '', 0, 1, 0);
INSERT INTO `dm_role` VALUES (4, 'hao', '个人', NULL, NULL, '3', 1, 4, 0);
INSERT INTO `dm_role` VALUES (5, '123123', '个人', '库存管理:1,系统管理:1', '入库单:2,出库单:2,库存盘点:2,机构管理:2,用户管理:2,移库记录:2,系统日志:2,系统配置:2,角色管理:2', '123', 1, 3, 0);
INSERT INTO `dm_role` VALUES (6, '324', '部门', NULL, NULL, '213', 0, 234, 0);
INSERT INTO `dm_role` VALUES (95005, '好', '全部', NULL, NULL, 'good', 1, 10, 0);
INSERT INTO `dm_role` VALUES (95006, '超级管理员', '全部', '库存管理:1,生产计划:1,系统管理:1,采购管理:1,销售管理:1', '供应商表:2,入库单:2,出库单:2,库存盘点:2,换货订单:2,机构管理:2,物料信息:2,生产工单:2,生产报工:2,生产退料:2,用户管理:2,移库记录:2,系统日志:2,系统配置:2,角色管理:2,订单评价:2,退货订单:2,采购员表:2,采购材料:2,采购订单:2,销售订单:2,领料退单:2', '超级管理员', 1, 1, 0);

-- ----------------------------
-- Table structure for dm_user
-- ----------------------------
DROP TABLE IF EXISTS `dm_user`;
CREATE TABLE `dm_user`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '用户姓名',
  `login_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '登录用户名',
  `password` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `position` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `department` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `phone` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `status` tinyint(0) NOT NULL COMMENT '状态，0：正常，1：删除，2封禁',
  `isystem` tinyint(0) NULL DEFAULT NULL COMMENT '是否系统测试员',
  `description` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `remark` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `delete_flag` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `sort` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE,
  INDEX `#`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 134 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dm_user
-- ----------------------------
INSERT INTO `dm_user` VALUES (1, '123', '123', '123', NULL, '500', '123456789', NULL, NULL, 1, NULL, NULL, NULL, '0', 1);
INSERT INTO `dm_user` VALUES (4, '123456', '123456', '123', NULL, NULL, NULL, NULL, '2023-10-15 23:33:53', 1, NULL, NULL, NULL, '0', NULL);
INSERT INTO `dm_user` VALUES (5, 'hello', 'hello', '123', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, '0', NULL);
INSERT INTO `dm_user` VALUES (63, '测试用户', 'jsh', 'e10', '主管', NULL, NULL, '666666@qq.com', NULL, 1, 1, NULL, NULL, '0', NULL);
INSERT INTO `dm_user` VALUES (120, '管理员', 'admin', '123456', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, '0', 12);
INSERT INTO `dm_user` VALUES (131, 'test123', 'test123', 'e10', '总监', NULL, NULL, '7777777@qq.com', NULL, 1, 0, NULL, NULL, '0', NULL);
INSERT INTO `dm_user` VALUES (132, '新用户<admin2>', 'admin2', '123456', NULL, NULL, NULL, NULL, '2023-10-16 18:50:43', 1, NULL, NULL, NULL, '0', NULL);
INSERT INTO `dm_user` VALUES (133, '新用户<a>', 'a', 'a', NULL, NULL, NULL, NULL, '2023-10-19 22:59:25', 1, NULL, NULL, NULL, '0', NULL);

-- ----------------------------
-- Table structure for dm_user_business
-- ----------------------------
DROP TABLE IF EXISTS `dm_user_business`;
CREATE TABLE `dm_user_business`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `key_id` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `value` varchar(10000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `btn_str` varchar(2000) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `delete_flag` varchar(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dm_user_business
-- ----------------------------

-- ----------------------------
-- Table structure for dm_user_role_rel
-- ----------------------------
DROP TABLE IF EXISTS `dm_user_role_rel`;
CREATE TABLE `dm_user_role_rel`  (
  `id` bigint(0) NOT NULL,
  `user_id` bigint(0) NULL DEFAULT NULL,
  `role_id` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dm_user_role_rel
-- ----------------------------
INSERT INTO `dm_user_role_rel` VALUES (1, 120, 95006);
INSERT INTO `dm_user_role_rel` VALUES (2, 1, 5);

SET FOREIGN_KEY_CHECKS = 1;
